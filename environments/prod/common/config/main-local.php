<?php
return [
    'components' => [
        'cacheCommon' => [
            'class' => '',
        ],
        'cacheCommonPost' => [
            'class' => '',
        ],
        'cache' => [
            'class' => '',
            // if you do not use files, specify 'cache' in each main-local
        ],
        'db' => [
            'dsn' => 'pgsql:dbname=lenovo-service',
            'username' => 'lenovo-service',
            'password' => 'lenovo-service',
        ],
    ],
];
