<?php
return [
    'components' => [
        'log' => [
            'flushInterval' => 1,
            'targets' => [
                'debugCommon' => [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/debug.log',
                    'levels' => ['trace'],
                    'categories' => ['debug\*'],
                    'logVars' => [],
                    'exportInterval' => 1,
                ],
                'sql' => [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/sql.log',
                    'levels' => ['info'],
                    'categories' => ['yii\db\Command*'],
                    'logVars' => [],
                    'exportInterval' => 1,
                ],
            ],
        ],
        'cacheCommon' => [
            'class' => 'yii\caching\FileCache',
            'gcProbability' => 0,
            'cachePath' => '@common/runtime/cache',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'gcProbability' => 0,
        ],
        'cacheCommonPost' => [
            'class' => 'yii\caching\FileCache',
            'gcProbability' => 0,
            'cachePath' => '@common/runtime/cache_post',
        ],
        'db' => [
            'dsn' => 'pgsql:host=192.168.1.3;dbname=lenovo-service',
            'username' => 'lenovo-service',
            'password' => 'lenovo-service',
        ],
        'mailer' => [
            'useFileTransport' => true,
        ],
    ],
];
