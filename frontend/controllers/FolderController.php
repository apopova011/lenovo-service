<?php

namespace frontend\controllers;

use Yii;
use \yii\web\Controller;
use \yii\web\NotFoundHttpException;
use \common\models\Folder;
use \common\models\PostForFeed;

class FolderController extends Controller
{
    /**
     * @param string $path
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionView($path)
    {
        $folders = Folder::getById();
        $notFound = true;
        foreach ($folders as $folderId => $folder) {
            if ($folder['path'] === $path) {
                $notFound = false;
                break;
            }
        }

        if ($notFound) {
            throw new NotFoundHttpException('Запрошенная категория не найдена.');
        }

        return $this->render('view', [
            'item'  => Folder::find()->forView($folderId)->one(),
            'posts' => PostForFeed::find()->forFeed(Yii::$app->getRequest()->get())->byFolder($folderId)->asArray()->all(),
        ]);
    }
}
