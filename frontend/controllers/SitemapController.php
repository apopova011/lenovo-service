<?php

namespace frontend\controllers;

use Yii;
use \yii\helpers\ArrayHelper;
use \yii\web\Controller;
use \yii\web\NotFoundHttpException;
use \yii\web\Response;
use \common\helpers\Inflector;
use \common\helpers\Url;
use \common\models\Folder;
use \common\models\Post;
use \common\models\PostFolder;
use \common\models\PostFolderQuery;
use \common\models\PostForFeed;
use \common\models\PostQuery;
use \common\models\PostTag;
use \common\models\PostTagQuery;
use \common\models\Tag;
use \frontend\components\SitemapResponseFormatter;
use \frontend\models\MainPage;

class SitemapController extends Controller
{
    /**
     * Count items per sitemap page.
     */
    const PAGE_LIMIT = 1000;

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidParamException
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        $response = Yii::$app->getResponse();
        $response->format = $response::FORMAT_XML;
        $formatter = new SitemapResponseFormatter();

        if ($action->id === 'index') {
            $formatter->rootTag = 'sitemapindex';
            $formatter->itemTag = 'sitemap';
        } else {
            $response->on(Response::EVENT_AFTER_PREPARE, function ($event) {
                $file = static::getSitemapDir() . $_SERVER['REQUEST_URI']; // TODO: урл должен быть генерённый
                $content = $event->sender->content;

                file_put_contents($file, $content);
                $file = gzopen($file . '.gz', 'w9');
                gzwrite($file, $content);
                gzclose($file);
            });
        }

        $response->formatters[$response::FORMAT_XML] = $formatter;

        return $result;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $structure = [
            $this->getSitemap(['sitemap/page']),
            $this->getSitemap(['sitemap/folder']),
        ];

        $items = [
            'post'            => static::getPagesCount(Post::find()->isPublished()),
            'post-timeline'   => static::getPagesCount(Post::find()->isPublished()->inCommonFeed()),
            'tag'             => static::getPagesCount(Tag::find()),
            'tag-timeline'    => static::getPagesCount(PostTag::find()->innerJoinPostPublished()),
            'folder-timeline' => static::getPagesCount(PostFolder::find()->innerJoinPostPublished()),
        ];

        foreach ($items as $model => $page) {
            for (; $page >= 1; --$page) {
                $structure[] = $this->getSitemap(['sitemap/' . $model, 'page' => $page]);
            }
        }

        return $structure;
    }

    /**
     * @param string $page
     * @throws NotFoundHttpException
     * @return array
     */
    public function actionPost($page)
    {
        $pages = static::getPagesCount(Post::find()->isPublished());
        if ($page <= $pages) {
            return $this->makeStructure(Post::find()->forSitemap()->limit(static::PAGE_LIMIT)->offset(($page - 1) * static::PAGE_LIMIT)->all(), '0.9');
        } else {
            throw new NotFoundHttpException('Запрошенная схема не найдена.');
        }
    }

    /**
     * @return array
     */
    public function actionFolder()
    {
        return $this->makeStructure(Folder::find()->forSitemap()->all(), '0.7');
    }

    /**
     * @param string|int $page
     * @throws NotFoundHttpException
     * @return array
     */
    public function actionTag($page)
    {
        $pages = static::getPagesCount(Tag::find());
        if ($page <= $pages) {
            return $this->makeStructure(
                Tag::find()->forSitemap()->limit(static::PAGE_LIMIT)->offset(($page - 1) * static::PAGE_LIMIT)->all(),
                function ($item) {
                    return ($item->title_seo ? '0.7' : '0.4');
                }
            );
        } else {
            throw new NotFoundHttpException('Запрошенная схема не найдена.');
        }
    }

    /**
     * @return array
     */
    public function actionPage()
    {
        return $this->makeStructure([new MainPage()], '1.0');
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function actionNews()
    {
        $posts = PostForFeed::find()->forNewsSitemap()->limit(static::PAGE_LIMIT)->all();
        $brandName = Yii::$app->params['brandName'];
        $structure = [];
        $keywords = [];

        foreach ($posts as $post) {
            /** @var Post $post */
            if (!empty($post['folders'])) {
                $keywords = implode(', ', ArrayHelper::getColumn($post['folders'], 'name'));
            }
            if (!empty($post->tags_name)) {
                $keywords = isset($keywords) ? $keywords . ', ' . $post->tags_name : $post->tags_name;
            }

            $structure[] = [
                'loc' => $post->getUrl(true),
                'news:news' => [
                    'news:publication' => [
                        'news:name' => $brandName,
                        'news:language' => 'ru'
                    ],
                    'news:publication_date' => date('c', $post->updated),
                    'news:title' => $post->name,
                    'news:keywords' => $keywords,
                ]
            ];
        }

        return $structure;
    }

    /**
     * @param array $items
     * @param string|\Closure $priority
     * @param string $changeFreq
     * @return array
     */
    protected function makeStructure($items, $priority = '0.5', $changeFreq = '')
    {
        $time = time();
        $structure = [];

        foreach ($items as $item) {
            $updated = $item->updated;

            if (!$changeFreq) {
                $daysWithoutUpdate = ($time - $updated) / 86400; // 60 * 60 * 24
                $itemChangeFreq = $daysWithoutUpdate < 1 ? 'hourly' : ($daysWithoutUpdate < 7 ? 'daily' : ($daysWithoutUpdate < 31 ? 'weekly' : 'monthly'));
            } else {
                $itemChangeFreq = $changeFreq;
            }

            $structure[] = [
                'loc' => $item->getUrl(true),
                'lastmod' => date('c', $updated),
                'changefreq' => $itemChangeFreq,
                'priority' => $priority instanceof \Closure ? call_user_func($priority, $item) : $priority,
            ];
        }

        return $structure;
    }

    /**
     * @param array|string $route
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    protected function getSitemap($route)
    {
        $file = Url::toRoute($route);
        $route = ['loc' => Url::to($file, true)];
        $file = static::getSitemapDir() . $file;

        if (file_exists($file)) {
            $route['lastmod'] = date('c', filemtime($file));
        }

        return $route;
    }

    /**
     * @param $page
     * @throws NotFoundHttpException
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function actionPostTimeline($page)
    {
        $pages = static::getPagesCount(Post::find()->isPublished()->inCommonFeed());
        if ($page <= $pages) {
            return $this->makeTimelineStructure($this->getPostsForTimeline(PostForFeed::find(), $page), '0.1');
        } else {
            throw new NotFoundHttpException('Запрошенная схема не найдена.');
        }
    }

    /**
     * @param $page
     * @throws NotFoundHttpException
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function actionFolderTimeline($page)
    {
        $pages = static::getPagesCount(PostFolder::find()->innerJoinPostPublished());

        if ($page <= $pages) {
            return $this->makeTimelineStructure($this->getPostsForTimeline(PostFolder::find(), $page), '0.1', 'folder');
        } else {
            throw new NotFoundHttpException('Запрошенная схема не найдена.');
        }
    }

    /**
     * @param $page
     * @throws NotFoundHttpException
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function actionTagTimeline($page)
    {
        $pages = static::getPagesCount(PostTag::find()->innerJoinPostPublished());

        if ($page <= $pages) {
            return $this->makeTimelineStructure($this->getPostsForTimeline(PostTag::find(), $page), '0.1', 'tag');
        } else {
            throw new NotFoundHttpException('Запрошенная схема не найдена.');
        }
    }

    /**
     * @param PostQuery|PostTagQuery|PostFolderQuery $data
     * @param integer $page
     * @return array
     */
    protected function getPostsForTimeline($data, $page)
    {
        return $data
            ->forSitemapTimeline()
            ->limit(static::PAGE_LIMIT)
            ->offset(($page - 1) * static::PAGE_LIMIT)
            ->all();
    }

    /**
     * @param \yii\db\ActiveQuery[] $items
     * @param string|\Closure $priority
     * @param string $type
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    protected function makeTimelineStructure(array $items = [], $priority = '0.1', $type = 'post')
    {
        $isPost = $type === 'post';
        $structure = [];

        if (!$isPost) {
            $column = $type . '_id';

            $maxPublished = '\common\models\Post' . Inflector::camelize($type);
            /** @var PostTag|PostFolder $maxPublished */
            $maxPublished = $maxPublished::find()
                ->innerJoinWith([
                    'post' => function ($query) {
                        /** @var $query PostQuery */
                        $query->isPublished();
                    }
                ])
                ->select([
                    $column,
                    'max_published' => 'EXTRACT(EPOCH FROM MAX(' . Post::tableName() . '.published) AT TIME ZONE current_setting(\'TIMEZONE\'))',
                ])
                ->andWhere([$column => ArrayHelper::getColumn($items, $type . '.id')])
                ->groupBy($column)
                ->createCommand()
                ->queryAll();

            /** @var array $maxPublished */
            $maxPublished = ArrayHelper::map($maxPublished, $column, 'max_published');
        } else {
            $model = new MainPage();
        }

        foreach ($items as $item) {
            $published = ($item->hasAttribute('published', false) ? $item->published : $item->post->published);
            if (!$isPost && $published === $maxPublished[$item[$type]['id']]) {
                continue;
            }

            if (!$isPost) {
                $model = $item->{$type};
            }

            /** @var Tag|Folder|MainPage $model */
            $structure[] = [
                'loc' => $model->getUrl(true, ['published' => $published]),
                'lastmod' => date('c', $published),
                'changefreq' => 'monthly',
                'priority' => $priority instanceof \Closure ? call_user_func($priority, $item) : $priority,
            ];
        }

        return $structure;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    protected static function getSitemapDir()
    {
        return Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'sitemap';
    }

    /**
     * @param \yii\db\ActiveQuery $activeQuery
     * @return integer
     */
    protected static function getPagesCount($activeQuery)
    {
        return ceil($activeQuery->count() / static::PAGE_LIMIT);
    }
}