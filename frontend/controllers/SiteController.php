<?php
namespace frontend\controllers;

use common\components\Dadata;
use Exception;
use Yii;
use \yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param $method
     * @return array|mixed
     */
    public function actionDadata($method)
    {
        $variants = [];
        $response = Yii::$app->getResponse();
        $response->format = $response::FORMAT_JSON;
        $string = Yii::$app->getRequest()->get('q');

        if (!$string) {
            return $variants;
        }

        try {
            $suggestions = Dadata::suggestions($method, $string);

            if (!is_array($suggestions)) {
                return $variants;
            }

            if (array_key_exists('suggestions', $suggestions)) {
                $variants = $suggestions['suggestions'];
            }

            return array_map(static function ($variant) {
                return [
                    'label' => $variant['value'],
                    'value' => $variant['value'],
                ];
            }, $variants);
        } catch (Exception $e) {
            Yii::error($e->getMessage());
        }

        return $variants;
    }
}
