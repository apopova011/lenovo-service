<?php

namespace frontend\controllers;

use Yii;
use \yii\helpers\ArrayHelper;
use \yii\web\Controller;
use \yii\web\NotFoundHttpException;
use \yii\widgets\ActiveForm;
use \common\helpers\File;
use \common\helpers\Inflector;
use \common\models\Comment;
use \common\models\CommentVote;
use \common\models\Post;
use \common\models\PostForFeed;
use \common\models\User;
use \frontend\models\CommentForm;
use \frontend\models\MainPage;

class PostController extends Controller
{
    /**
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $posts = PostForFeed::find()->forFeed(Yii::$app->getRequest()->get())->inCommonFeed()->asArray()->all();

        // занимаемся этим здесь, ибо нужная выборка и запрос к редиске уже сделаны
        if (Yii::$app->getRequest()->get('year') === null) {
            $cacheKey = 'post:feed:1';

            $postsIdFeedFirstPage = ArrayHelper::getColumn($posts, 'id');
            $postsIdFeedFirstPageLast = Yii::$app->cacheCommon->get($cacheKey);
            if (empty($postsIdFeedFirstPageLast)) {
                Yii::$app->cacheCommon->set($cacheKey, $postsIdFeedFirstPage);
            } else {
                $postsIdFeedFirstPageLast = array_diff(array_slice($postsIdFeedFirstPage, 0, 9), $postsIdFeedFirstPageLast);
                // новый материал на непоследних позициях
                if (!empty($postsIdFeedFirstPageLast)) {
                    Yii::$app->cacheCommon->set($cacheKey, $postsIdFeedFirstPage);

                    Post::updateUpdatedOnRelationsByPublished($postsIdFeedFirstPageLast);

                    File::deleteRss();
                    File::deleteSitemapPost(min($postsIdFeedFirstPageLast));
                    File::deleteSitemapFolder();
                    File::deleteSitemapTag();
                }
            }
        }

        return $this->render('index', [
            'item'  => new MainPage,
            'posts' => $posts,
        ]);
    }

    /**
     * @param string $folder_slug
     * @param string $post_slug
     * @param string $post_id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionView($folder_slug, $post_slug, $post_id)
    {
        $type = Post::find()->andWhere(['id' => $post_id])->select(['type'])->scalar();
        if (!$type) {
            throw new NotFoundHttpException('Запрошенный материал не найден.');
        }

        $class = Post::className() . Inflector::camelize($type);
        /** @var Post $class */
        $post = $class::find()->forView()->andWhere(['id' => $post_id])->one();

        if (!$post) {
            throw new NotFoundHttpException('Запрошенный материал не найден.');
        }

        $postUrl = $post->getUrl();
        if ($postUrl !== parse_url($_SERVER['REQUEST_URI'])['path']) {
            $this->redirect($postUrl, 301);
        }

        return $this->renderPost($post, $postUrl);
    }

    /**
     * @param string $slug
     * @throws NotFoundHttpException
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionViewBySlug($slug)
    {
        $type = Post::find()->andWhere(['slug' => $slug])->select(['type'])->scalar();
        if (!$type) {
            throw new NotFoundHttpException('Запрошенный материал не найден.');
        }

        $class = Post::className() . Inflector::camelize($type);
        /** @var Post $class */
        $post = $class::find()->forView()->andWhere(['slug' => $slug])->one();

        if (!$post) {
            throw new NotFoundHttpException('Запрошенный материал не найден.');
        }

        return $this->renderPost($post, $post->getUrl());
    }

    /**
     * @param Post $post
     * @param string $postUrl
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    protected function renderPost($post, $postUrl)
    {
        $postId = $post['id'];
        $commentModel = new CommentForm();
        $user = Yii::$app->getUser();
        $comments = Comment::loadByPost($postId);
        $votedComments = [];

        if ($user->getIsGuest()) {
            $user = null;
        } else {
            $user = $user->getIdentity();
            /** @var User $user */
            $commentModel->title = $user->title;
            $commentModel->email = $user->email;

            $userId = $user->id;
        }

        $session = Yii::$app->getSession();
        if ($session->getHasSessionId()) {
            $session->open();
            $sessionId = $session->getId();

            if (empty($userId)) {
                $userAnonym = $session->get('userAnonym');
                if (!empty($userAnonym)) {
                    // обязательна перепроверка - за это время человек может стать верифицированным и неавторизованных к нему вязать нельзя
                    $userId = User::find()
                        ->select('id')
                        ->andWhere(['id' => $userAnonym['id']])
                        ->emailNotVerified()
                        ->scalar();

                    if (empty($userId)) {
                        $session->remove('userAnonym');
                        $userAnonym = null;
                    }
                }
            }
        }

        $request = Yii::$app->getRequest();
        $isAjax = $request->getIsAjax();
        if ($commentModel->load($request->post()) || $isAjax) {
            if ($isAjax) {
                $isAjax = Yii::$app->getResponse();
                $isAjax->format = $isAjax::FORMAT_JSON;

                return ActiveForm::validate($commentModel);
            } else {
                $commentId = $commentModel->save($postId);
                if ($commentId) {
                    $this->redirect($postUrl . '#comment-' . $commentId);
                }
            }
        } else if (!empty($userAnonym) && $request->getIsGet()) {
            $commentModel->title = $userAnonym['title'];
            $commentModel->email = $userAnonym['email'];
        }

        $hasSession = !empty($sessionId);
        $hasUserId  = !empty($userId);

        if ($hasSession || $hasUserId) {
            $bySession = $hasSession ? ['user_session' => $sessionId] : [];
            $byUserId  = $hasUserId  ? ['user_id'      => $userId]    : [];

            if ($hasSession && $hasUserId) {
                $condition = ['OR', $bySession, $byUserId];
            } else if ($hasSession) {
                $condition = $bySession;
            } else {
                $condition = $byUserId;
            }

            $votedComments = CommentVote::find()
                ->select('comment_id')
                ->andWhere(['IN', 'comment_id', ArrayHelper::getColumn($comments, 'id')])
                ->andWhere($condition)
                ->column();
        }

        $commentsFiltered = [];
        foreach ($comments as $comment) {
            $ownComment =
                ($hasUserId  &&    $userId === $comment['author_id']) ||
                ($hasSession && $sessionId === $comment['user_session']);

            if (!($ownComment || in_array($comment['status'], Comment::$showedStatuses, true))) {
                continue;
            }

            $comment['showVoting'] = !(in_array($comment['id'], $votedComments, true) || $ownComment);
            if ($comment['votes_value'] > 0) {
                $comment['votes_value'] = '+' . $comment['votes_value'];
            }

            $commentsFiltered[] = $comment;
        }

        $class = $post::className();

        return $this->render('view', [
            'post' => $post,
            'postUrl' => $postUrl,
            'postsSimilar' => $class::loadSimilarity($post),
            'comments' => $commentsFiltered,
            'commentModel' => $commentModel,
            'user' => $user,
        ]);
    }
}
