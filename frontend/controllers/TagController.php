<?php

namespace frontend\controllers;

use Yii;
use \yii\web\Controller;
use \yii\web\NotFoundHttpException;
use \common\models\PostForFeed;
use \common\models\Tag;

class TagController extends Controller
{
    /**
     * @param string $slug
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionView($slug)
    {
        $item = Tag::find()->forView($slug)->one();
        if ($item === null) {
            throw new NotFoundHttpException('Запрошенная тема не найдена.');
        }

        return $this->render('view', [
            'item'  => $item,
            'posts' => PostForFeed::find()->forFeed(Yii::$app->getRequest()->get())->byTag($item['id'])->asArray()->all(),
        ]);
    }
}
