<?php

namespace frontend\controllers;

use Yii;
use \yii\web\Controller;

class SearchController extends Controller
{
    /**
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $requested = Yii::$app->getRequest()->get('q');
        $queryExists = $requested !== null;

        if ($queryExists) {
            $query = trim($requested);
        }

        $emptyQuery = !$queryExists || $query === '';

        if ($emptyQuery || $query !== $requested) {
            if ($emptyQuery) {
                // если параметр не передан или пуст после трима
                $query = 'Я не знаю, что хочу найти';
            }

            // если не равен исходному после трима
            return $this->redirect(['search/index', 'q' => $query], 301);
        }

        return $this->render('index', [
            'q' => $query,
        ]);
    }
}
