<?php

namespace frontend\controllers;

use Yii;
use \yii\web\Controller;
use \common\helpers\Url;
use \common\models\Feedback;

class FeedbackController extends Controller
{
    /**
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $app = Yii::$app;
        $model = new Feedback();
        $model->setScenario('send');

        if ($model->load($app->getRequest()->post())) {
            $response = $app->getResponse();
            if ($model->validate()) {
                if ($model->save(false)) {
                    return $this->redirect(Url::current(['#' => 'sended']));
                } else {
                    $response->setStatusCode(400);
                    $app->getSession()->setFlash('error', 'Ваше сообщение не отправлено.');
                }
            } else {
                $response->setStatusCode(422, 'Validation Failed');
            }
        } else if (!$app->getUser()->getIsGuest()) {
            $identity = $app->getUser()->getIdentity();
            $model->user_title = $identity->getNameString();
            $model->user_email = $identity->email;
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
