<?php

namespace frontend\controllers;

use Yii;
use \yii\helpers\ArrayHelper;
use \yii\web\Controller;
use \yii\web\Response;
use \common\helpers\File;
use \common\helpers\Html;
use \common\helpers\Url;
use \common\models\Post;
use \common\models\PostForFeed;
use \frontend\components\RssResponseFormatter;

class RssController extends Controller
{
    /**
     * Count rss item for user rss.
     */
    const LIMIT = 20;

    /**
     * Count rss item for bots.
     */
    const LIMIT_FOR_BOT = 5;

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function beforeAction($action)
    {
        $response = Yii::$app->getResponse();
        $response->format = $response::FORMAT_XML;
        $response->formatters[$response::FORMAT_XML] = Yii::createObject(RssResponseFormatter::className());
        $response->on(Response::EVENT_AFTER_PREPARE, function ($event) {
            $rssFile = Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'rss' . DIRECTORY_SEPARATOR . Yii::$app->requestedAction->id . '.xml';
            $content = $event->sender->content;
            file_put_contents($rssFile, $content);

            $gz = gzopen($rssFile . '.gz', 'w9');
            gzwrite($gz, $content);
            gzclose($gz);
        });

        return parent::beforeAction($action);
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function actionAll()
    {
        $posts = PostForFeed::find()
            ->forRss()
            ->with([
                'folders' => function ($query) {
                    /* @var \common\models\FolderQuery $query */
                    $query->select(['id', 'title']);
                }
            ])
            ->limit(static::LIMIT)
            ->asArray()
            ->all();

        return $this->makeStructure($posts, 'all');
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function actionYandex()
    {
        $posts = PostForFeed::find()->forRss()->limit(static::LIMIT_FOR_BOT)->asArray()->all();
        return $this->makeStructure($posts, 'yandex');
    }

    /**
     * @param array $posts
     * @param string $type
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    protected function makeStructure($posts, $type = 'all')
    {
        $url = Url::toRoute('/', true);
        $infoEmail = Yii::$app->params['emailInfo'];
        $lang = Yii::$app->language;

        $structure = [
            'title' => Yii::$app->params['mainOgTitle_' . $lang],
            'link' => $url,
            'description' => Yii::$app->params['mainOgDescription_' . $lang],
            'managingEditor' => $infoEmail . ' (' . Yii::$app->params['defaultAuthor']['title'] . ')',
        ];

        $structure['atom:link'] = [
            'href' => Yii::$app->getRequest()->absoluteUrl,
            'rel' => 'self',
            'type' => 'application/rss+xml',
        ];

        if ($type === 'yandex') {
            $structure['yandex:logo'] = [
                [$url . 'yandex-icon-100x100.png'],
                [$url . 'yandex-icon-180x180.png', 'type' => 'square'],
            ];
        } else {
            $structure['image'] = [
                'url' => $url . 'apple-touch-icon-120x120.png',
                'title' => $structure['title'],
                'link' => $url,
                'width' => '120',
                'height' => '120',
            ];
        }

        $url = Url::imageDomain();
        foreach ($posts as $post) {
            $item = [
                'title' => Html::encodeYandex($post['name']),
                'link' => Url::toRoute(Post::getUrlParams($post), true),
                'pubDate' => date('r', $post['published']),
                'category' => empty($post['folders']) ? $post['folderMain']['name'] : ArrayHelper::getColumn($post['folders'], 'title'),
                'guid' => ['post:' . $post['id'], 'isPermaLink' => 'false'],
            ];

            $imgUrl = empty($post['icon']) ? false : ($url . Url::thumbnail($post['icon'], 1920, 1080));
            if ($imgUrl) {
                $item['enclosure'] = [
                    ['url' => $imgUrl, 'type' => File::getMimeTypeByExtension($post['icon'])],
                ];
            }

            $item['description'] = Html::encodeYandex($post['description']);

            if ($type === 'yandex') {
                if (!(empty($post['author']['first_name']) || empty($post['author']['last_name']))) {
                    $item['author'] = $post['author']['title'];
                }

                $item['yandex:full-text'] = Html::encodeYandex(Html::toText($post['content']));
                $item['yandex:genre'] = 'article';

                foreach ($this->getImagesFromText($post['content']) as $enclosureSrc) {
                    $item['enclosure'][] = [
                        'url' => $enclosureSrc,
                        'type' => File::getMimeTypeByExtension($enclosureSrc),
                    ];
                }
            } else {
                $item['author'] = $infoEmail . ' (' . $post['author']['title'] . ')';

                $content = nl2br(Html::encode($post['description'])) . ' ' . Html::a('читать далее', $item['link']);
                if ($imgUrl) {
                    $image = Html::img($url . Url::thumbnail($post['icon'], 240, 135), [
                        'style' => 'float:left;margin:0 1em 1em 0;',
                    ]);
                    $content = $image . $content;
                }

                $item['content:encoded'] = Html::tag('p', $content);
            }

            $structure[] = $item;
        }

        return $structure;
    }

    /**
     * @param string $text
     * @return array
     */
    protected function getImagesFromText($text)
    {
        if (preg_match_all('/<a[^>]+class="image-big"[^>]+href="([^"]++)"[^>]*>/', $text, $images)) {
            list($bigImages, $imagesSrc) = $images;

            array_walk($bigImages, function (&$value, $key) {
                $value = preg_quote($value, '/');
            });

            $exclude = '(?<!' . implode('|', $bigImages) . ')';
        } else {
            $imagesSrc = [];
            $exclude = '';
        }

        if (preg_match_all('/' . $exclude . '<img[^>]+src="([^"]++)/', $text, $images)) {
            $images = $images[1];

            array_walk($images, function (&$value, $key) {
                $value = preg_replace('/\-\d+x\d+\.(jpe?g|gif|png)$/', '.$1', $value);
            });

            $imagesSrc = array_merge($imagesSrc, $images);
        }

        return $imagesSrc;
    }
}
