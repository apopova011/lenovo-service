<?php

namespace frontend\controllers;

use \yii\web\Controller;
use \yii\web\NotFoundHttpException;
use \common\models\Comment;
use \common\models\User;

class UserController extends Controller
{
    /**
     * @param string $slug
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionView($slug)
    {
        $user = User::find()->forView($slug)->asArray()->one();
        if ($user === null) {
            throw new NotFoundHttpException('Запрошенный пользователь не найден.');
        }

        return $this->render('view', [
            'user' => $user,
            'comments' => Comment::find()->forUser($user['id'])->limit(100)->asArray()->all(),
        ]);
    }
}
