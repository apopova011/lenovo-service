<?php

namespace frontend\controllers;

use common\models\Partners;
use yii\base\InvalidArgumentException;
use yii\web\Controller;

class PartnersController extends Controller
{
    /**
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function actionView()
    {
        $partners = Partners::find()
            ->andWhere(['act' => false])
            ->orderBy([
                'country' => SORT_ASC,
                'region' => SORT_ASC,
            ])
            ->all();

        return $this->render('view', [
            'model' => new Partners(),
            'partners' => $partners,
        ]);
    }
}
