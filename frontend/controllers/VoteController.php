<?php

namespace frontend\controllers;

use Yii;
use \yii\web\BadRequestHttpException;
use \yii\web\Controller;
use \yii\web\ForbiddenHttpException;
use \yii\web\NotFoundHttpException;
use \common\helpers\Url;
use \common\models\Comment;
use \common\models\CommentVote;
use \common\models\Post;
use \common\models\User;

class VoteController extends Controller
{
    /**
     * @param integer $id
     * @return string
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function actionComment($id)
    {
        $request = Yii::$app->getRequest();

        $direction = $request->post('direction');
        if (empty($direction) || in_array($direction, ['up', 'down'], true) === false) {
            throw new BadRequestHttpException('Неверное значение параметра.');
        }

        $comment = Comment::find()->andWhere(['id' => $id])->withPost()->asArray()->one();
        if (empty($comment)) {
            throw new NotFoundHttpException('Комментарий не существует.');
        }

        $result = $this->voteComment($comment, $direction);

        if ($request->getIsAjax()) {
            $response = Yii::$app->getResponse();
            $response->format = $response::FORMAT_JSON;

            return $result;
        } else {
            $route = Post::getUrlParams($comment['post']);
            $route['#'] = 'comment-' . $comment['id'];

            return $this->redirect(Url::toRoute($route));
        }
    }

    /**
     * @param array $comment
     * @param string $direction
     * @return array
     * @throws ForbiddenHttpException
     */
    protected function voteComment($comment, $direction)
    {
        $session = Yii::$app->getSession();
        $session->open();

        $sessionId = $session->getId();
        $request = Yii::$app->getRequest();
        $userId = Yii::$app->getUser()->getId();

        if (empty($userId)) {
            $userAnonym = $session->get('userAnonym');
            if (!empty($userAnonym)) {
                $userId = $userAnonym['id'];
            }

            if (empty($userId)) {
                $email = $request->getQueryParam('email');
                if (!empty($email)) {
                    $userId = User::find()->select('id')->andWhere(['email' => $email])->emailNotVerified()->scalar();
                }
            }
        }

        if (!empty($userId) && $comment['author_id'] === $userId) {
            throw new ForbiddenHttpException('Вы пытаетесь проголосовать за свой комментарий.');
        }

        $condition = ['user_session' => $sessionId];
        if (!empty($userId)) {
            $condition = [
                'OR',
                $condition,
                ['user_id' => $userId],
            ];
        }

        if (CommentVote::find()->andWhere(['comment_id' => $comment['id']])->andWhere($condition)->exists()) {
            throw new ForbiddenHttpException('Вы уже голосовали за этот комментарий, его рейтинг ' . $comment['votes_value'] . '.');
        }

        $voteValue = CommentVote::getVoteValue();
        if ($direction === 'down') {
            $voteValue = -$voteValue;
        }

        $vote = new CommentVote();
        $vote->value = $voteValue;
        $vote->comment_id = $comment['id'];
        $vote->user_id = $userId;
        $vote->user_ip = $request->getUserIP();
        $vote->user_agent = $request->getUserAgent();
        $vote->user_session = $sessionId;

        $vote->save();

        Yii::$app->cacheCommonPost->set('post:' . $comment['post_id'] . ':comments', Comment::find()->forPost($comment['post_id'])->asArray()->all());

        return $comment['votes_value'] + $vote->value;
    }
}
