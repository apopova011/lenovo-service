<?php

use common\helpers\Url;

/**
 * @var $this yii\web\View
 */

$appParams = Yii::$app->params;
$lang = Yii::$app->language;

$pageUrl = Url::home(true);
$this->registerMetaTag([
    'property' => 'og:url',
    'content' => $pageUrl,
]);
$this->registerLinkTag([
    'rel' => 'canonical',
    'href' => $pageUrl,
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => Url::to('@web' . $appParams['defaultOgImage'], true),
]);

$this->title = $appParams['mainTitle_' . $lang];
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $appParams['mainOgTitle_' . $lang],
]);

$this->registerMetaTag([
    'name' => 'description',
    'content' => $appParams['mainMetaDescription_' . $lang],
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content' => $appParams['mainOgDescription_' . $lang],
]);

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $appParams['mainKeywords_' . $lang],
]);
/*
$nextUrl = ;
$this->registerLinkTag([
    'rel' => 'prefetch',
    'href' => $nextUrl,
], 'prefetch');
$this->registerLinkTag([
    'rel' => 'prerender',
    'href' => $nextUrl,
], 'prerender');
*/
?>

Main Page
