<?php

use \yii\widgets\ActiveForm;
use \common\helpers\Html;
use \common\helpers\Url;
use \frontend\assets\FormSimpleAsset;
use \frontend\widgets\Alert;

/**
 * @var yii\web\View $this
 * @var \common\models\Feedback $model
 */

$appParams = Yii::$app->params;
$lang = Yii::$app->language;

$pageUrl = Url::toRoute(['feedback/index'], true);
$this->registerMetaTag([
    'property' => 'og:url',
    'content'  => $pageUrl,
]);
$this->registerLinkTag([
    'rel'  => 'canonical',
    'href' => $pageUrl,
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content'  => Url::to('@web' . $appParams['defaultOgImage'], true),
]);

$this->title = 'Обратная связь';
$this->registerMetaTag([
    'property' => 'og:title',
    'content'  => $appParams['mainOgTitle_' . $lang],
]);

$this->registerMetaTag([
    'name'    => 'description',
    'content' => $appParams['mainMetaDescription_' . $lang],
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content'  => $appParams['mainOgDescription_' . $lang],
]);

$this->registerMetaTag([
    'name'    => 'keywords',
    'content' => $appParams['mainKeywords_' . $lang],
]);
FormSimpleAsset::register($this);
?>
<?= Alert::widget(); ?>
<h1><?= Html::encode($this->title); ?></h1>
<?php $form = ActiveForm::begin([
    'id' => 'feedback-form',
]); ?>
<?= $form->field($model, 'user_title'); ?>
<?= $form->field($model, 'user_email'); ?>
<?= $form->field($model, 'text')->textarea(); ?>
<div class="form-group">
    <?= Html::submitButton('Отправить', ['class' => 'button-submit']); ?>
</div>
<?php ActiveForm::end(); ?>
