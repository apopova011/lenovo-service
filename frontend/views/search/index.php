<?php

use \common\helpers\Html;
use \common\helpers\Url;

/**
 * @var yii\web\View $this
 */

$appParams = Yii::$app->params;
$query = Yii::$app->getRequest()->get('q');

$this->registerMetaTag([
    'property' => 'og:url',
    'content' => Url::toRoute(['search/index', 'q' => $query], true),
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => Url::to('@web' . $appParams['defaultOgImage'], true),
]);

$this->title = $query . ' на ' . $appParams['brandName'];

$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $this->title,
]);

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $this->title,
]);

$googleSearchId = $appParams['googleSearchId'];
$this->registerJs(<<<JS
(function() {
    var cx = '{$googleSearchId}';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
    '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
})();
JS
    , $this::POS_END, 'search-results');
?>
<section class="search-results">
    <h1><?= Html::encode($query); ?></h1>
    <gcse:searchresults-only></gcse:searchresults-only>
</section>
