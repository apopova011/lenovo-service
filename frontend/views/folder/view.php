<?php

use \common\helpers\Url;

/**
 * @var yii\web\View $this
 * @var common\models\Folder $item
 * @var array $posts
 */

$appParams = Yii::$app->params;

$isFirst = Yii::$app->getRequest()->get('year') === null;

if ($isFirst) {
    $this->registerMetaTag([
        'property' => 'og:url',
        'content' => $item->getUrl(true),
    ]);
}

$iconUrl = Url::imageDomain($item->icon);
$this->registerMetaTag([
    'property' => 'og:image',
    'content' => $iconUrl ? $iconUrl . Url::thumbnail($item->icon, 1920, 1080) : Url::to('@web' . $appParams['defaultOgImage'], true),
]);

$value = empty($item->title) ? $item->name : $item->title;
$this->title = empty($item->title_seo) ? $value : $item->title_seo;
if (!$isFirst) {
    $get = Yii::$app->getRequest()->get();
    $this->title .= ' от ' . $get['day'] . '.' . $get['month'] . '.' . $get['year'];
}
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $value,
]);

if ($isFirst && !empty($item->description)) {
    $this->registerMetaTag([
        'name' => 'description',
        'content' => $item->description,
    ]);
    $this->registerMetaTag([
        'property' => 'og:description',
        'content' => $item->description,
    ]);
}

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $item->name,
]);

echo $this->render('/post/_feed', [
    'item'  => $item,
    'posts' => $posts,
]);
