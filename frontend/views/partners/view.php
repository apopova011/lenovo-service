<?php

use common\helpers\Html;
use common\helpers\StringHelper;
use common\helpers\Url;
use common\models\Partners;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var Partners[] $partners
 * @var Partners $model
 */

$appParams = Yii::$app->params;
$lang = Yii::$app->language;

$pageUrl = Url::toRoute(['service/index'], true);
$this->registerMetaTag([
    'property' => 'og:url',
    'content'  => $pageUrl,
]);
$this->registerLinkTag([
    'rel'  => 'canonical',
    'href' => $pageUrl,
]);

$this->registerMetaTag([
    'property' => 'og:image',
    'content'  => Url::to('@web' . $appParams['defaultOgImage'], true),
]);

$this->title = 'Сервисные центры — Accesstyle';
$this->registerMetaTag([
    'property' => 'og:title',
    'content'  => $this->title,
]);

$description = 'Сервисные центры Accesstyle в городах России и ближнем зарубежье';

$this->registerMetaTag([
    'name'    => 'description',
    'content' => $description,
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content'  => $description,
]);

$this->registerMetaTag([
    'name'    => 'keywords',
    'content' => 'Accesstyle, сервисные центры, сервисное обслуживание, где починить',
]);

$this->params['longContent'] = true;
?>

<services-map
    :points='<?= StringHelper::jsonEncodeVue(ArrayHelper::toArray($partners)); ?>'
    :countries='<?= StringHelper::jsonEncodeVue($model::dictionaryCountryRegions()); ?>'
/>
