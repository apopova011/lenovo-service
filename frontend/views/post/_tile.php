<?php

use \common\helpers\Html;
use \common\helpers\Url;
use \common\models\Post;

/**
 * @var yii\web\View $this
 * @var array $post
 * @var string|null $articleHeadingTag
 * @var integer|null $icon_width
 * @var integer|null $icon_height
 * @var array|null $withPicture
 */

if (empty($articleHeadingTag)) {
    $articleHeadingTag = 'h3';
}

$params = Yii::$app->params;

$imgParams = [];
if (array_key_exists('title_seo', $post)) {
    $imgParams = ['alt' => $post['title_seo']];
}
?>
<article>
    <time datetime="<?= date('c', $post['published']); ?>" title="<?= date('H:i', $post['published']); ?>"> <?= Yii::t('common', '{0, date, d MMMM YYYY}', $post['published']); ?></time>
    <?= Html::tag($articleHeadingTag, Html::encode($post['name'])); ?>
    <?php if (!empty($withPicture) && !empty($post['icon'])) { ?>
        <picture class="icon-wrapper">
            <?= Html::thumbnail($post['icon'], $icon_width, $icon_height, $imgParams); ?>
        </picture>
    <?php } else { ?>
        <div class="icon-wrapper">
            <?= Html::thumbnail($post['icon'], $icon_width, $icon_height, $imgParams); ?>
        </div>
    <?php } ?>
    <?php
    if (array_key_exists('description', $post)) { ?>
        <p><?= Html::encode($post['description']); ?></p>
    <?php } ?>
    <a href="<?= Url::toRoute(Post::getUrlParams($post)); ?>">Читать полностью</a>
</article>
