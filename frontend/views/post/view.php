<?php

use \yii\helpers\ArrayHelper;
use \yii\widgets\ActiveForm;
use \common\helpers\Html;
use \common\helpers\Url;
use \common\models\Folder;
use \common\models\Post;
use \common\models\Tag;
use \common\models\User;
use \frontend\assets\PostViewAsset;

/**
 * @var yii\web\View $this
 * @var Post $post
 * @var string $postUrl
 * @var array $postsSimilar
 * @var array $comments
 * @var \frontend\models\CommentForm $commentModel
 */

$postUrl = Url::to($postUrl, true);
$appParams = Yii::$app->params;

$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'article',
]);
$this->registerMetaTag([
    'name' => 'twitter:card',
    'content' => 'summary_large_image',
]);

$this->registerMetaTag([
    'property' => 'og:url',
    'content' => $postUrl,
]);
$this->registerLinkTag([
    'rel' => 'canonical',
    'href' => $postUrl,
]);

$iconUrl = Url::imageDomain($post['icon']);
$postImage = $iconUrl ? $iconUrl . Url::thumbnail($post['icon'], 1920, 1080) : Url::to('@web' . $appParams['defaultOgImage'], true);
$this->registerMetaTag([
    'property' => 'og:image',
    'content' => $postImage,
]);

$this->title = (empty($post['title_seo']) ? $post['name'] : $post['title_seo']);
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $post['name'],
]);

$this->registerMetaTag([
    'name' => 'description',
    'content' => $post['description'],
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content' => $post['description'],
]);

$folders = $post->hasProperty('folders') ? $post['folders'] : [];
$tags    = $post->hasProperty('tags') ? $post['tags'] : [];

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => implode(', ', array_unique(array_merge(
        ArrayHelper::getColumn($folders, 'name'),
        ArrayHelper::getColumn($tags,    'name')
    ))),
]);

PostViewAsset::register($this);

$schemaVideo = !empty($post['video_duration']);
$sixMonthsAgo = mktime(0, 0, 0, date('m') - 6, date('d'), date('Y'));
?>
<div class="article-area">
    <article itemscope itemtype="http://schema.org/<?php if ($schemaVideo) { ?>VideoObject<?php } else { ?>NewsArticle<?php } ?>" id="article">
        <h1 itemprop="<?php if ($schemaVideo) { ?>name<?php } else { ?>headline<?php } ?>" class="article-headline"><?= Html::encode($post['name']); ?></h1>
        <?= Html::tag('meta', '', [
            'itemprop' => 'description',
            'content' => $post['description'],
        ]); ?>
        <meta itemprop="image" content="<?= $postImage; ?>"/>
        <div<?php if (!$schemaVideo) { ?> itemprop="articleBody"<?php } ?> id="article-text" class="article-text">
            <?php
            $content = $post['content'];
            if (strpos($content, 'youtube') > 0) {
                $content = Html::replaceYoutube(
                    $this,
                    $content,
                    $iconUrl ? $iconUrl . Url::thumbnail($post['icon'], 600, 338) : null,
                    $schemaVideo,
                    false
                );
            }
            if (strpos($content, 'image-big') > 0) {
                $content = Html::schemaImage($content, $this->title, !$schemaVideo);
            }

            if (!$schemaVideo) {
                $content = Html::schemaAddItempropToImages($content, 'image');
            }
            ?>
            <!--nospaceless--><?= $content; ?><!--/nospaceless-->
        </div>
        <?php
        if ($post['is_allow_footer']) { ?>
            <footer>
                <?php
                $postTags = [];
                foreach ($folders as $folder) {
                    $postTags[] = [
                        'name'         => $folder['name'],
                        'url'          => Url::toRoute(Folder::getUrlParams($folder)),
                        'isBreadcrumb' => mb_strpos($folder['path'], '.') === false,
                    ];
                }

                foreach ($tags as $tag) {
                    $postTags[] = [
                        'name' => $tag['name'],
                        'url'  => Url::toRoute(Tag::getUrlParams($tag)),
                    ];
                }
                ?>
                <?php if ($postTags) { ?>
                    <ul class="tags" itemscope itemtype="http://schema.org/BreadcrumbList">
                        <?= Html::itemsValue($postTags); ?>
                    </ul>
                <?php } ?>
                <div class="article-info">
                    <?php if ($post['published'] >= $sixMonthsAgo) {
                        $html = 'd MMMM';
                        if (date('Y', $post['published']) !== date('Y')) {
                            $html .= ' YYYY';
                        }
                        ?>
                        <time datetime="<?= date('c', $post['published']); ?>" title="<?= date('H:i', $post['published']) ?>" itemprop="<?php if ($schemaVideo) { ?>uploadDate<?php } else { ?>datePublished<?php } ?>"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
                            <use xlink:href='#icon-calendar'/>
                        </svg><?= Yii::t('common', '{0, date, ' . $html . '}', $post['published']); ?></time>
                    <?php } ?>
                    <?php
                    $author = $post['author']['title'];
                    if ($post['author']['id'] !== $appParams['defaultAuthor']['id']) {
                        $author = Html::a($author, User::getUrlParams($post['author']), ['rel' => 'nofollow']);

                        $this->registerLinkTag([
                            'rel' => 'author',
                            'href' => Url::toRoute(User::getUrlParams($post['author']), true),
                        ]);
                    }
                    ?>
                    <span class="author"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
                        <use xlink:href='#icon-author'/>
                    </svg>Автор: <span itemprop="author"><?= $author; ?></span></span>
                </div>
                <?php if ($schemaVideo) { ?>
                    <meta itemprop="url" content="<?= $postUrl; ?>"/>
                    <meta itemprop="isFamilyFriendly" content="True"/>
                    <meta itemprop="duration" content="<?= $post['video_duration']; ?>"/>
                <?php } ?>
                <div class="likely-not-loaded share">
                    <div class="vkontakte"></div>
                    <div class="facebook"></div>
                    <div class="twitter"></div>
                    <div class="gplus"></div>
                    <div class="odnoklassniki"></div>
                    <div class="telegram"></div>
                </div>
            </footer>
            <?php if ($postsSimilar) { ?>
                <section class="article-similar">
                    <a href="#article-similar-prev" class="article-similar-arrow article-similar-prev" id="article-similar-prev"></a>
                    <a href="#article-similar-next" class="article-similar-arrow article-similar-next" id="article-similar-next"></a>
                    <div><ul id="article-similar">
                        <?php foreach ($postsSimilar as $i => $postSimilar) { ?>
                            <li<?php if ($i === 0) { ?> class="active"<?php } ?>><a href="<?= Url::toRoute(Post::getUrlParams($postSimilar)); ?>">
                                <h2><?= Html::encode($postSimilar['name']); ?></h2>
                                <span><?= date('d.m.y', $postSimilar['published']); ?></span>
                            </a></li>
                        <?php } ?>
                    </ul></div>
                </section>
            <?php } ?>
        <?php } ?>
        <?php if ($post['is_allow_comments']) {
            $commentsCount = count($comments);
            ?>
            <section class="comments" id="comments">
                <?php
                if ($commentsCount > 0) {
                    echo Html::tag('h5', 'Все комментарии');
                    if ($commentsCount > 4) {
                        echo Html::a('Добавить', '#comment-add', [
                            'id' => 'comments-to-form',
                            'class' => 'button-submit'
                        ]);
                    }
                }
                ?>
                <?php
                foreach ($comments as $i => $comment) {
                    $showVoting = $comment['showVoting'];
                    $value = $comment['votes_value'];
                    $class = $comment['level'] > 1 ? 'level' . ($comment['level'] > 2 ? '3' : '2') : '';
                    $statusClass = in_array($comment['status'], [
                        'excellent_by_vote',
                        'excellent_by_moderator'
                    ], true) !== false ? 'excellent' : ($value < 0 ? 'poor' : '');

                    if (!empty($statusClass)) {
                        if (!empty($class)) {
                            $class .= ' ';
                        }

                        $class .= $statusClass;
                    }
                    ?>
                    <article<?php if (!empty($class)) { ?> class="<?= $class; ?>"<?php } ?> id="comment-<?= $comment['id']; ?>">
                        <header>
                            <?php
                            $text  = Html::thumbnail(empty($comment['user_avatar']) ? $comment['author']['avatar'] : $comment['user_avatar'], 50, 50);
                            $text .= Html::encode(   empty($comment['user_title'])  ? $comment['author']['title']  : $comment['user_title']);

                            if ($comment['author']['is_author'] || $comment['author']['comment_visible_count'] > 5) {
                                echo Html::a($text, Url::toRoute(User::getUrlParams($comment['author'])), ['class' => 'author', 'rel' => 'nofollow']);
                            } else {
                                echo Html::tag('span', $text, ['class' => 'author']);
                            }

                            if ($comment['created'] >= $sixMonthsAgo) { ?>
                                <time datetime="<?= date('c', $comment['created']); ?>"><?= date('d.m.y H:i', $comment['created']); ?></time>
                            <?php } ?>
                        </header>
                        <div class="comment-text">
                            <?= nl2br(Html::encode($comment['content'])); ?>
                        </div>
                        <footer>
                            <div class="votes<?= $value > 0 ? ' vote-good' : ($value < 0 ? ' vote-bad' : ''); ?><?= !$showVoting ? ' cant-vote' : ''; ?>" data-comment="<?= $comment['id']; ?>">
                                <?php if ($showVoting) { ?>
                                    <?php ActiveForm::begin([
                                        'action' => [
                                            'vote/comment',
                                            'id' => $comment['id'],
                                        ]
                                    ]); ?>
                                    <button name="direction" value="down" class="vote-arrow down" type="submit"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="8" height="6">
                                        <use xlink:href='#vote-down'/>
                                    </svg></button>
                                <?php } ?>
                                <span class="value"><?= $value; ?></span>
                                <?php if ($showVoting) { ?>
                                    <button name="direction" value="up" class="vote-arrow up" type="submit"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="8" height="6">
                                        <use xlink:href='#vote-up'/>
                                    </svg></button>
                                    <?php ActiveForm::end(); ?>
                                <?php } ?>
                            </div>
                            <a href="#comment-form" class="reply" data-path="<?= $comment['path']; ?>"> <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="30px">
                                <use xlink:href='#reply-arrow'/>
                            </svg>Ответить</a>
                        </footer>
                    </article>
                    <?php
                }
                $isAuthorized = !empty($user);

                $params = [
                    'id' => 'comment-form',
                    'enableAjaxValidation' => true,
                    'encodeErrorSummary' => false,
                ];
                if ($isAuthorized) {
                    $params['options'] = [
                        'class' => 'with-profile authorised',
                    ];
                }
                $form = ActiveForm::begin($params);
                echo $form->field($commentModel, 'reply', [
                    'template' => '{input}',
                ])->hiddenInput();
                echo $form->field($commentModel, 'content', [
                    'template' => '{input}',
                    'inputOptions' => [
                        'class' => 'form-control',
                        'placeholder' => 'Комментарии — это обсуждение, беседа или даже спор на тему статьи, но ни в коем случае не интеллектуальных и моральных качеств собеседника или оппонента. Впрочем, если вы вдруг переборщили, ничего страшного — модераторы всегда наготове ;)',
                    ],
                ])->textarea(['rows' => 4]);
                ?>
                <div class="comment-user" id="comment-user">
                    <div class="vk-auth">
                        <div id="vk-auth"></div>
                    </div>
                    <div class="site-auth">
                        <?php
                        $session = Yii::$app->getSession();
                        if ($session->getHasSessionId()) {
                            $session->open();
                            $userAnonym = $session->get('userAnonym', false);
                        } else {
                            $userAnonym = false;
                        }

                        $inputOptions = [
                            'class' => 'form-control',
                            'placeholder' => $commentModel->getAttributeLabel('title'),
                        ];
                        if ($userAnonym) {
                            $inputOptions['data'] = ['val' => $userAnonym['title']];
                        }
                        echo $form->field($commentModel, 'title', [
                            'template' => '{label}{input}',
                            'inputOptions' => $inputOptions,
                        ])->label('Введите:');

                        $inputOptions['placeholder'] = $commentModel->getAttributeLabel('email');
                        if ($userAnonym) {
                            $inputOptions['data']['val'] = $userAnonym['email'];
                        }
                        echo $form->field($commentModel, 'email', [
                            'template' => '{input}',
                            'inputOptions' => $inputOptions,
                        ]);
                        ?>
                    </div>
                    <div class="profile">Авторизован как: <span id="user-title"><?= $isAuthorized ? $user->title : 'Злобная марсианская печенька'; ?></span>
                        <?php
                        $params = ['rel' => 'nofollow'];
                        if (!$isAuthorized) {
                            $params['id'] = 'logout';
                        }
                        ?>
                        (<?= Html::a('выйти', $isAuthorized ? [
                                'auth/logout',
                                'redirect' => Yii::$app->getRequest()->getAbsoluteUrl()
                            ] : ['#logout'],
                            $params
                        ); ?>)
                    </div>
                </div>
                <?= $form->errorSummary($commentModel, ['header' => '']); ?>
                <a href="#comment-form" class="reply" id="reply"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30px" height="30px">
                    <use xlink:href='#reply-arrow'/>
                </svg>Отменить ответ</a>
                <?= Html::submitButton($commentsCount === 0 ? 'Добавить комментарий' : 'Добавить', [
                    'class' => 'button-submit',
                    'id' => 'comment-form-button'
                ]); ?>
                <?php ActiveForm::end(); ?>
                <svg version="1.1" class="svg-container" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" xml:space="preserve">
                    <symbol id="reply-arrow" viewBox="0 0 100 100">
                        <path d="M48.32,68.813c-0.246,0-0.489-0.091-0.677-0.264L28.269,50.736c-0.206-0.189-0.323-0.457-0.323-0.736 s0.117-0.547,0.323-0.736l19.375-17.813c0.292-0.269,0.717-0.338,1.079-0.179c0.363,0.159,0.598,0.519,0.598,0.916v9.681 c13.224,2.542,22.734,12.235,22.734,23.31c0,0.533-0.027,1.089-0.086,1.748c-0.045,0.509-0.468,0.902-0.979,0.911 c-0.505,0.013-0.946-0.37-1.009-0.878c-0.741-6.029-8.974-9.536-16.13-9.277c-1.596,0.061-3.116,0.24-4.53,0.535v9.595 c0,0.396-0.235,0.756-0.598,0.916C48.593,68.785,48.456,68.813,48.32,68.813z"/>
                    </symbol>
                    <symbol id="vote-down" viewBox="0 0 8.991 6.008">
                        <path d="M4.496,6.008c-0.205,0-0.401-0.092-0.545-0.257L0.226,1.5c-0.301-0.343-0.301-0.9,0-1.243s0.789-0.343,1.089,0 l3.181,3.628l3.18-3.628c0.301-0.343,0.789-0.343,1.09,0c0.301,0.343,0.301,0.9,0,1.243L5.04,5.751 C4.896,5.916,4.7,6.008,4.496,6.008z"/>
                    </symbol>
                    <symbol id="vote-up" viewBox="0 0 8.991 6.008">
                        <path d="M4.495,0C4.7,0,4.896,0.093,5.04,0.257l3.725,4.251c0.301,0.343,0.301,0.9,0,1.243 c-0.301,0.343-0.789,0.343-1.089,0L4.495,2.123l-3.18,3.628c-0.301,0.343-0.789,0.343-1.09,0c-0.301-0.343-0.301-0.9,0-1.243 L3.95,0.257C4.095,0.093,4.291,0,4.495,0z"/>
                    </symbol>
                </svg>
            </section>
            <?php
            $this->registerJs(<<<JS
window.vkApiId = {$appParams['vkApiId']};
JS
                , $this::POS_READY, 'globalVkVars');
            ?>
        <?php } ?>
    </article>
</div>
<div id="vk_api_transport"></div>
