<?php

use \common\helpers\Url;

/**
 * @var yii\web\View $this
 * @var frontend\models\MainPage $item
 * @var array $posts
 */

$appParams = Yii::$app->params;
$lang = Yii::$app->language;
$isFirst = Yii::$app->getRequest()->get('year') === null;

$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'website',
]);

if ($isFirst) {
    $this->registerMetaTag([
        'property' => 'og:url',
        'content' => $item->getUrl(true),
    ]);
}

$this->registerMetaTag([
    'property' => 'og:image',
    'content' => Url::to('@web' . $appParams['defaultOgImage'], true),
]);

if ($isFirst) {
    $this->title = $appParams['mainTitle_' . $lang];
} else {
    $get = Yii::$app->getRequest()->get();
    $this->title = $appParams['mainListTitle_' . $lang] . ' от ' . $get['day'] . '.' . $get['month'] . '.' . $get['year'];
}
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $appParams['mainOgTitle_' . $lang],
]);

if ($isFirst) {
    $this->registerMetaTag([
        'name' => 'description',
        'content' => $appParams['mainMetaDescription_' . $lang],
    ]);
    $this->registerMetaTag([
        'property' => 'og:description',
        'content' => $appParams['mainOgDescription_' . $lang],
    ]);
}

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $appParams['mainKeywords_' . $lang],
]);
?>

<?= $this->render('/post/_feed', [
    'item'  => $item,
    'tag'   => 'h1',
    'posts' => $posts,
]); ?>
