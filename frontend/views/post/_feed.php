<?php

use \yii\widgets\Menu;
use \common\helpers\Html;
use \common\helpers\Url;
use \common\models\FeedNavigation;
use \frontend\assets\PostListAsset;

/**
 * @var yii\web\View $this
 * @var \frontend\models\MainPage|\common\models\Tag|\common\models\Folder $item
 * @var array $posts
 * @var array|null $navigation
 */

PostListAsset::register($this);

if (empty($tag)) {
    $tag = 'h1';
}

$notFirst = Yii::$app->getRequest()->get('year') !== null;

$heading = Html::tag($tag, empty($item->title) ? $item->name : $item->title);
if ($notFirst || $tag !== 'h1') {
    $heading = Html::a($heading, $item->getUrl());
}

$navigation = $item->getNavigation();
?>
<div class="post-feed-area">
    <section>
        <?= $heading; ?>
        <?php if (!empty($item->description)) { ?>
            <p class="description"><?= $item->description; ?></p>
        <?php } ?>
        <?php if (!empty($navigation)) { ?>
            <?= Menu::widget([
                'options' => ['class' => 'feed-nav'],
                'items' => FeedNavigation::forNavigationWidget($navigation),
            ]); ?>
        <?php } ?>
        <div class="post-feed-wrapper">
            <div class="post-feed" id="post-feed">
                <?php
                foreach ($posts as $i => $post) {
                    if ($i === 12) {
                        break;
                    }

                    echo $this->render('_tile', [
                        'post' => $post,
                        'articleHeadingTag' => 'h2',
                        'icon_width' => 600,
                        'icon_height' => 338,
                    ]);
                }
                ?>
            </div>
            <?php
            if (isset($i) && $i === 12) {
                $postUrl = $item->getUrl(false, $post);
                ?>
                <a href="<?= $postUrl; ?>" id="next" class="next" rel="next">Показать ещё</a>
                <?php
                $postUrl = Url::to($postUrl, true);
                $this->registerLinkTag([
                    'rel'  => 'prefetch',
                    'href' => $postUrl,
                ], 'prefetch');
                $this->registerLinkTag([
                    'rel'  => 'prerender',
                    'href' => $postUrl,
                ], 'prerender');
            }
            ?>
        </div>
    </section>
</div>
