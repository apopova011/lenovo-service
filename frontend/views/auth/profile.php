<?php

use \yii\widgets\ActiveForm;
use \yii\widgets\MaskedInput;
use \yii\jui\DatePicker;
use \common\helpers\Html;
use \common\helpers\Url;
use \common\models\User;
use \frontend\assets\FormFullAsset;
use \frontend\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model \common\models\User */

FormFullAsset::register($this);

$this->title = 'Профиль';
?>
<section class="form-common">
    <?= Alert::widget(); ?>
    <h1><?= Html::encode($this->title); ?></h1>
    <?php $form = ActiveForm::begin([
        'id' => 'profile-form',
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>
    <div class="input-group">
        <div class="form-group">
            <?= Html::img(Url::thumbnail($model->avatar, 100, 100),
                ['width' => '100px', 'height' => '100px']); ?>
            <?= $form->field($model, 'UploadFile', ['template' => "{input}\n{hint}\n{error}"])->fileInput(); ?>
        </div>
        <div class="form-group">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]); ?>
            <?= $form->field($model, 'birthday')->widget(DatePicker::className(), [
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                    'class' => 'form-control',
                ],
            ]); ?>
            <?= $form->field($model, 'sex')->radioList(User::dictionarySexLabels(), ['class' => 'user-sex']); ?>
        </div>
    </div>
    <?= $form->field($model, 'city_name')->textInput(['maxlength' => true]); ?>
    <div class="input-group">
        <?= $form->field($model, 'phone_mobile')->widget(MaskedInput::className(), [
            'mask' => '+9-999-999-9999',
            'options' => ['maxlength' => 15, 'class' => 'form-control'],
        ]); ?>
        <?= $form->field($model, 'skype')->textInput(['maxlength' => true]); ?>
    </div>
    <div class="profile-social">
        <?php
        $attributes = [
            'twitter_slug' => [
                'label' => 'https://twitter.com/',
            ],
            'vk_id' => [
                'label' => 'https://vk.com/id',
            ],
            'facebook_id' => [
                'label' => 'https://www.facebook.com/',
            ],
            'instagram_slug' => [
                'label' => 'https://instagram.com/',
            ],
            'google_plus_slug' => [
                'label' => 'https://plus.google.com/',
            ],
        ];
        foreach ($attributes as $attribute => $config) {
            $name = preg_replace('/^(.+)_(.+)$/', '$1', $attribute);
            $name = str_replace('_', '-', $name);
            $svg = <<<HTML
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
    <use xlink:href="#icon-{$name}">
</svg>
HTML;
            $label = Html::tag('span', $svg, ['class' => 'profile-' . $name]) . $config['label'];
            echo $form
                ->field($model, $attribute, [
                    'options' => ['class' => 'form-group profile-social-group'],
                    'labelOptions' => ['label' => $label]
                ])
                ->textInput(['maxlength' => true, 'class' => 'profile-social-input', 'autocomplete' => 'off']);
        }
        ?>
    </div>
    <?= $form->field($model, 'passwordNew')->passwordInput(['maxlength' => true])->hint('Оставьте пустым, если не хотите менять.'); ?>
    <div class="form-group">
        <?= Html::submitButton('Обновить', ['class' => 'button-submit']); ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
