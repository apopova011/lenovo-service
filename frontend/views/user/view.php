<?php

use \common\helpers\Html;
use \common\helpers\StringHelper;
use \common\helpers\Url;
use \common\models\Post;
use \common\models\User;

/**
 * @var yii\web\View $this
 * @var array|User $user
 */

$appParams = Yii::$app->params;

$this->registerMetaTag([
    'property' => 'og:url',
    'content' => Url::to(User::getUrlParams($user), true),
]);

$iconUrl = Url::imageDomain($user['avatar']);
if ($iconUrl) {
    $iconUrl .= Url::thumbnail($user['avatar'], 100, 100);
}
$this->registerMetaTag([
    'property' => 'og:image',
    'content' => $iconUrl ? $iconUrl : Url::to('@web' . $appParams['defaultOgImage'], true),
]);

$this->title = $user['title'];
if ($user['is_author']) {
    $this->title .= ' - Авторы';
} else {
    $this->title .= ' - Пользователи';
}
$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $user['title'],
]);

if (!empty($user['about'])) {
    $this->registerMetaTag([
        'property' => 'og:description',
        'content' => $user['about'],
    ]);
}

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'профиль, автор, ' . $user['title'] . ', социалки, вконтакте, vk, google plus, место рождения, дата рождения, возраст, статьи, новости, комментарии, ответы',
]);
?>

<div class="user-info">
    <?= $this->render('_tile.php', [
        'user' => $user,
        'isPersonalProfile' => true,
    ]); ?>
    <?php if ($user['city_name']) { ?>
        <p class="profile-extend"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20">
            <use xlink:href='#icon-location'/>
        </svg>Место жительства: <?= $user['city_name']; ?></p>
    <?php } ?>
    <?php if ($user['birthday'] && (!$user['is_author'] || (time() - $user['birthday']) / 31536000 >= 24)) { // 60 * 60 * 24 * 365 ?>
        <p class="profile-extend"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16">
            <use xlink:href='#icon-calendar'/>
        </svg>День рождения: <?= Yii::t('common', '{0, date, d MMMM YYYY}', $user['birthday']); ?></p>
    <?php } ?>
    <?php if (!empty($comments)) { ?>
        <section class="profile-comments" id="profile-comments">
            <h3>Комментарии пользователя:</h3>
            <?php
            foreach ($comments as $i => $comment) {
                if ($i === 10) { ?>
                    <a id="profile-more-comments" class="next" rel="next" href="#profile-more-comments">Показать ещё</a>
                <?php } ?>
                <article<?php if ($i >= 10) { ?> class="display-none"<?php } ?>>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20">
                        <use xlink:href='#icon-comment'/>
                    </svg>
                    <time datetime="<?= date(DATE_ISO8601, $comment['created']); ?>" class="profile-comment-info"><?= date('d.m.y H:m', $comment['created']); ?></time>
                    <p><?= StringHelper::truncateAtWordEnd($comment['content'], 255, '…'); ?></p>
                    <p class="profile-comment-info">В статье: <?= Html::a(
                        StringHelper::truncateAtWordEnd($comment['post']['name'], 80, '…'),
                        Url::toRoute(Post::getUrlParams($comment['post'])) . '#comment-' . $comment['id']
                    ); ?></p>
                </article>
            <?php } ?>
        </section>
        <?php
        $this->registerJs(
            <<<JS
$('#profile-more-comments').on('click', function(e) {
    e.preventDefault();
    var comments = $('#profile-comments');
    comments.children('.display-none:lt(10)').removeClass('display-none');
    if (comments.children('.display-none').length) {
        $('#profile-more-comments').appendTo('#profile-comments');
    } else {
        $('#profile-more-comments').remove();
    }
})
JS
            , $this::POS_READY, 'show_more_comments');
    }
    ?>
</div>
