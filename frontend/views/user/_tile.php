<?php

use \common\helpers\Html;
use \common\helpers\Url;
use \common\models\User;

/**
 * @var yii\web\View $this
 * @var array|User $user
 * @var boolean|null $isPersonalProfile
 */
$isPersonalProfile = !empty($isPersonalProfile);

$iconUrl = Url::imageDomain($user['avatar']);
if ($iconUrl) {
    $iconUrl .= Url::thumbnail($user['avatar'], 100, 100);
}
$avatar = Html::img($iconUrl, [
    'alt' => 'Аватар ' . Html::encode(empty($user['title_gen']) ? $user['title'] : $user['title_gen']),
    'width' => 100,
    'height' => 100,
]);
?>
<div class="profile-main">
    <?php
    $html = $user['title'];
    if ($isPersonalProfile) {
        $html = $avatar . Html::tag('h1', $html);
    } else {
        $html = Html::a($avatar . Html::tag('h3', $html), User::getUrlParams($user));
    } ?>
    <?= $html; ?>
    <?php if (!empty($user['about'])) { ?>
        <p><?= $user['about']; ?></p>
    <?php } ?>
    <?php $social = [];
    if (!empty($user['twitter_slug'])) {
        $social[] = [
            'slug' => 'twitter',
            'title' => 'Twitter',
            'url' => 'https://twitter.com/' . $user['twitter_slug'],
        ];
    }
    if (!empty($user['vk_id'])) {
        $social[] = [
            'slug' => 'vk',
            'title' => 'Вконтакте',
            'url' => 'https://vk.com/id' . $user['vk_id'],
        ];
    }
    if (!empty($user['facebook_id'])) {
        $social[] = [
            'slug' => 'facebook',
            'title' => 'Facebook',
            'url' => 'https://www.facebook.com/' . $user['facebook_id'],
        ];
    }
    if (!empty($user['google_plus_slug'])) {
        $social[] = [
            'slug' => 'google-plus',
            'title' => 'Google Plus',
            'url' => 'https://plus.google.com/' . $user['google_plus_slug'],
        ];
    }
    if ($social) { ?>
        <ul>
            <?php foreach ($social as $item) { ?>
                <li>
                    <a href="<?= $item['url']; ?>" target="_blank" rel="<?php if ($isPersonalProfile && $item['slug'] === 'google-plus') { ?>me<?php } else { ?>nofollow<?php } ?>" title="<?= $item['title']; ?>" class="profile-<?= $item['slug']; ?>">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13" height="13">
                            <use xlink:href="#icon-<?= $item['slug']; ?>"/>
                        </svg>
                    </a>
                </li>
            <?php } ?>
        </ul>
    <?php } ?>
</div>
