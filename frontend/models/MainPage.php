<?php

namespace frontend\models;

use \yii\base\Model;
use \common\helpers\Url;
use \common\models\FeedNavigation;
use \common\models\PostForFeed;

/**
 * Virtual model MainPage for unification.
 */
class MainPage extends Model
{
    /**
     * @var string
     */
    public $name = 'Главная';

    /**
     * @var string
     */
    public $title = 'Новости';

    /**
     * @var string
     */
    public $description = '';

    /**
     * @param boolean $scheme
     * @param array|null $postLast
     * @return string
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     */
    public function getUrl($scheme = false, $postLast = null)
    {
        if ($scheme === false && $postLast === null) {
            return '/';
        }

        $route = ['post/index'];
        if (!empty($postLast)) {
            $route['year']   = date('Y', $postLast['published']);
            $route['month']  = date('m', $postLast['published']);
            $route['day']    = date('d', $postLast['published']);
            $route['hour']   = date('H', $postLast['published']);
            $route['minute'] = date('i', $postLast['published']);
            $route['second'] = date('s', $postLast['published']);
        }

        return Url::toRouteFrontend($route, $scheme);
    }

    /**
     * @return string
     */
    public function getUpdated()
    {
        return PostForFeed::find()->getLastPublished();
    }

    /**
     * @return array
     */
    public function getNavigation() {
        $navigation = FeedNavigation::find()->forPage('main_page')->asArray()->all();

        if (!empty($navigation)) {
            return FeedNavigation::activeByUrl($navigation);
        }

        return [];
    }
}
