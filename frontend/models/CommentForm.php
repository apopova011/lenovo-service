<?php

namespace frontend\models;

use Yii;
use \yii\base\Model;
use \yii\helpers\Json;
use \common\helpers\Html;
use \common\models\Comment;
use \common\models\User;

/**
 * CommentForm is the model for frontend comment form.
 */
class CommentForm extends Model
{
    /**
     * @var string
     */
    public $content;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $user;

    /**
     * @var string
     */
    public $reply;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [['content', 'title', 'email'], 'filter', 'filter' => 'trim'],
            [['email'], 'filter', 'filter' => 'mb_strtolower'],
            [['content', 'title', 'email'], 'required'],
            [['user'], 'required', 'message' => 'Включите javascript, чтобы комментировать.'],
            [['email'], 'email', 'enableIDN' => true],
            [['reply'], 'exist', 'targetClass' => 'common\models\Comment', 'targetAttribute' => 'path', 'message' => 'Невозможно ответить на этот комментарий.'],
        ];

        if (empty(Yii::$app->getUser()->getIdentity())) {
            $rules[] = [['title'], 'unique', 'targetClass' => 'common\models\User', 'filter' => function ($query) {
                /* @var \common\models\UserQuery $query */
                $query->emailVerified();
            }, 'message' => 'Имя «{value}» занято зарегистрированным пользователем.'];
            $rules[] = [['email'], 'unique', 'targetClass' => 'common\models\User', 'filter' => function ($query) {
                /* @var \common\models\UserQuery $query */
                $query->emailVerified();
            }, 'message' => '{attribute} «{value}» занят зарегистрированным пользователем.'];
        }

        return $rules;
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'content' => 'Ваше сообщение',
            'title'   => 'Имя',
            'email'   => 'Email',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        $titleError = $this->getErrors('title');
        $emailError = $this->getErrors('email');
        $hasEmailError = $emailError ? mb_strpos($emailError[0], 'зарегистрированным пользователем') !== false : false;
        $hasTitleError = $titleError ? mb_strpos($titleError[0], 'зарегистрированным пользователем') !== false : false;

        if ($hasEmailError || $hasTitleError) {
            if (!$hasEmailError) {
                $attribute = 'title';
                $errorText = $titleError[0];
                $attributeText = 'ваш логин';
            } else {
                $attribute = 'email';
                $errorText = $emailError[0];
                $attributeText = $hasTitleError ? 'ваш аккаунт' : 'ваша почта';
            }

            $registeredText = $errorText . '<br> Если это ' . $attributeText . ', то ' . Html::a('авторизуйтесь здесь', ['auth/login'], ['target' => '_blank']);
            $registeredText .= '. A если вы забыли пароль, то ' . Html::a('запросите новый', ['auth/request-password-new'], ['target' => '_blank']) . '.';

            $this->clearErrors($attribute);
            $this->addError($attribute, $registeredText);
        }

        parent::afterValidate();
    }

    /**
     * @param integer $postId
     * @return boolean
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function save($postId)
    {
        if ($this->validate() === false) {
            return false;
        }

        $authUser = Yii::$app->getUser()->getIdentity();
        $session = Yii::$app->getSession();
        if (!empty($authUser)) {
            $user = $authUser;

            $sessionId = $session->getId();
        } else if ($this->user === '{}') {
            $user = User::find()->andWhere(['email' => mb_strtolower($this->email, Yii::$app->charset)])->one();
            if ($user === null) {
                $user = new User;
                $user->role = 'guest';
                $user->email = $this->email;

                $doSave = true;
            } elseif ($user->title !== $this->title) {
                $doSave = true;
            } else {
                $doSave = false;
            }
            if ($doSave) {
                $user->setScenario('commentFormGuest');
                $user->title = $this->title;

                $ret = $user->save();
                if ($ret === false) {
                    $this->addError('email', 'Ошибка сохранения пользователя.');
                    Yii::error($user->getErrors(), 'comment\saveGuest');

                    return false;
                }
            }

            $session->open();
            $session->set('userAnonym', [
                'id'    => $user->id,
                'email' => $user->email,
                'title' => $user->title,
            ]);
            $sessionId = $session->getId();
        } else {
            $data = '';
            foreach (explode('!', substr($this->user, 0, -1)) as $value) {
                $data .= html_entity_decode('&#' . ($value - 99) . ';', ENT_QUOTES, Yii::$app->charset);
            }
            $data = Json::decode($data);

            $user = array_key_exists('hash', $data['session']);
            if (
                ($user && $data['session']['hash'] !== md5(Yii::$app->params['vkApiId'] . $data['session']['mid'] . Yii::$app->params['vkSecretKey']))
                || (
                    !$user && (
                        $data['session']['sig'] !== md5(
                            'expire=' . $data['session']['expire'] .
                            'mid='    . $data['session']['mid'] .
                            'secret=' . $data['session']['secret'] .
                            'sid='    . $data['session']['sid'] .
                            Yii::$app->params['vkSecretKey']
                        ))
                )
            ) {
                $this->addError('user', 'Сервер vk не подтвердил авторизацию.');
                Yii::error($data, 'comment\checkAuthorization');

                return false;
            }

            $user = User::find()->fromVk()->andWhere(['vk_id' => $data['session']['mid']])->one();
            if ($user === null) {
                $user = new User;
                $user->title = $this->title;
                $user->email = $this->email;
                $user->role = 'registered';
                $user->vk_id = $data['session']['mid'];
            }
            $user->setScenario('commentFormVk');
            $user->loadFromVk($data);

            $ret = $user->save();
            if ($ret === false) {
                $this->addError('user', 'Ошибка сохранения пользователя.');
                Yii::error($user->getErrors(), 'comment\saveVk');

                return false;
            }
        }

        $comment = new Comment();
        $comment->setScenario('commentForm');
        $comment->content = $this->content;
        $comment->status = 'new';
        $comment->PathParent = $this->reply;
        $comment->user_title  = $user->title;
        $comment->user_avatar = $user->avatar;
        $comment->author_id   = $user->id;
        $comment->post_id = $postId;
        if (!empty($sessionId)) {
            $comment->user_session = $sessionId;
        }

        $ret = $comment->save();
        if ($ret === false) {
            $contentErrors = $comment->getErrors('content');
            $this->addError('content', $contentErrors ? $contentErrors[0] : 'Ошибка сохранения комментария.');
            Yii::error($comment->getErrors(), 'comment\save');

            return false;
        }

        return $comment->id;
    }
}
