<?php
return [
    'defaultOgImage'            => '/icon310.png',

    'mainTitle_en-US'           => 'Main Page',
    'mainOgTitle_en-US'         => 'Main Page',
    'mainMetaDescription_en-US' => 'Description.',
    'mainOgDescription_en-US'   => 'Description.',
    'mainKeywords_en-US'        => 'words',

    'mainTitle_ru-RU'           => 'Главная страница',
    'mainOgTitle_ru-RU'         => 'Главная страница',
    'mainListTitle_ru-RU'       => 'Список',
    'mainMetaDescription_ru-RU' => 'Описание.',
    'mainOgDescription_ru-RU'   => 'Описание.',
    'mainKeywords_ru-RU'        => 'ключевые слова',

    //'yaCounterId' => '',
    //'googleAnalyticsId' => '',
    //'googleSearchId' => ''
];
