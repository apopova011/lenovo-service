<?php
/**
 * Configuration file for the "yii asset" console command.
 * Note that in the console environment, some path aliases like '@webroot' and '@web' may not exist.
 * Please define these missing path aliases.
 */
$basePath = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'web';
return [
    'jsCompressor' => 'yii compress/js  {from} {to}',
    'cssCompressor' => 'yii compress/css {from} {to}',
    'bundles' => [
        'frontend\assets\PostListAsset',
        'frontend\assets\PostViewAsset',
        'frontend\assets\FormFullAsset',
    ],
    'targets' => [
        'frontend\assets\AppCompressedAsset' => [
            'basePath' => $basePath,
            'baseUrl' => '',
            'js' => 'assets/app-{hash}.js',
            'css' => 'assets/app-{hash}.css',
            'depends' => [
                'yii\web\JqueryAsset',
                'yii\web\YiiAsset',
                'frontend\assets\AppAsset',
            ],
        ],
        'frontend\assets\FormSimpleCompressedAsset' => [
            'basePath' => $basePath,
            'baseUrl' => '',
            'js' => 'assets/form-simple-{hash}.js',
            'css' => 'assets/form-simple-{hash}.css',
            'depends' => [
                'yii\widgets\ActiveFormAsset',
                'yii\validators\ValidationAsset',
                'yii\validators\PunycodeAsset',
                'frontend\assets\FormSimpleAsset',
            ],
        ],
        'frontend\assets\FormFullCompressedAsset' => [
            'basePath' => $basePath,
            'baseUrl' => '',
            'js' => 'assets/form-full-{hash}.js',
            'css' => 'assets/form-full-{hash}.css',
            'depends' => [
                'yii\jui\JuiAsset',
                'yii\widgets\MaskedInputAsset',
                'frontend\assets\FormFullAsset',
            ],
        ],
        'frontend\assets\PostListCompressedAsset' => [
            'basePath' => $basePath,
            'baseUrl' => '',
            'js' => 'assets/post_list-{hash}.js',
            'css' => 'assets/post_list-{hash}.css',
            'depends' => [
                'frontend\assets\PostListAsset',
            ],
        ],
        'frontend\assets\PostViewCompressedAsset' => [
            'basePath' => $basePath,
            'baseUrl' => '',
            'js' => 'assets/post_view-{hash}.js',
            'css' => 'assets/post_view-{hash}.css',
            'depends' => [
                'frontend\assets\PostViewAsset',
            ],
        ],
    ],
    'assetManager' => [
        'basePath' => $basePath . DIRECTORY_SEPARATOR . 'assets',
        'baseUrl' => '/assets',
        'linkAssets' => strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN',
    ],
];
