jQuery(document).ready(function () {
    var notLoaded   = $('.likely-not-loaded'),
        comments    = $('#comments'),
        commentForm = $('#comment-form'),
        postSlider  = $('#article-similar');

    if (notLoaded.length > 0) {
        $.each(notLoaded, function (i, item) {
            $(item).removeClass('likely-not-loaded').addClass('likely');
            window.likely(item);
        });
    }

    if ($('.image-big').length > 0) {
        $('#article-text').lightGallery({
            selector: '.image-big',
            download: false
        });
    }

    var articleWidth = $('#article-text').width();
    var articleHeight = articleWidth * 338 / 600;

    $.each($('.video-js'), function(i, el) {
        var playerId = $(el).attr('id'),
            youtube_id = playerId.replace('player', ''),
            videojsOptions = {
                "techOrder": ["youtube"],
                "sources": [
                    {
                        "type": "video/youtube",
                        "src": "http://www.youtube.com/watch?v=" + youtube_id
                    }
                ],
                youtube: {
                    "ytControls": true
                },
                children: ['mediaLoader', 'posterImage', 'controlBar', 'loadingSpinner'],
                "forceSSL": false
            };

        $(el).width(articleWidth);
        $(el).height(articleHeight);

        if (i == 0 && window.isAllowPreroll) {
            videojsOptions.plugins = {
                "ads": {},
                "vast": {
                    "url": "/video-js/preroll.xml"
                }
            };
        }

        videojs(playerId, videojsOptions);
    });

    if (comments.length > 0) {
        window.vkAsyncInit = function () {
            VK.init({apiId: window.vkApiId});
        };

        setTimeout(function () {
            var el = document.createElement("script");
            el.type = "text/javascript";
            el.src = "//vk.com/js/api/openapi.js";
            el.async = true;
            document.getElementById("vk_api_transport").appendChild(el);
        }, 0);

        var commentField        = $('#commentform-content'),
            commentFieldPadding = parseInt(commentField.css('padding-bottom')) + parseInt(commentField.css('padding-top')),
            commentFieldHeight  = commentField.height();

        commentField
            .on('keyup', function () {
                $(this).height(commentFieldHeight);
                $(this).height(this.scrollHeight - commentFieldPadding);
            })
            .on('paste', function () {
                setTimeout(function () {
                    var txt = commentField.val();
                    if (txt != '') {
                        txt = txt.replace(/\s+\u041f\u043e\u0434\u0440\u043e\u0431\u043d\u0435\u0435: http\:\/\/[a-z0-9\/\._-]+/g, '');
                        commentField.val(txt);
                    }
                }, 0);
            })
            .on('keydown', function (e) {
                if ((e.keyCode == 10 || e.keyCode == 13) && e.ctrlKey) {
                    $('#comment-form-button').click();
                }
            });

        $('#comment-user').hide()
            .prev().hide();

        $('#comments-to-form').click(function (e) {
            e.preventDefault();

            if ($('#comment-user').is(':hidden')) {
                showCommentForm();
            }

            scrollCommentForm();
        });

        function scrollCommentForm() {
            $.fn.scrollToBottom('#comment-form', function () {
                $('#commentform-content').focus();
            });
        }

        function showCommentForm() {
            $('#comment-form-button').html('\u0414\u043e\u0431\u0430\u0432\u0438\u0442\u044c'); // ��������

            $('#comment-user').removeAttr('style')
                .prev().removeAttr('style');

            if ($('#comment-form').hasClass('authorised')) {
                return;
            }
            if (typeof VK !== 'undefined') {
                VK.Auth.getLoginStatus(function (response) {
                    if (response.session) {
                        showProfile(response.session);
                    } else {
                        showVkLogin();
                    }
                });
            }
        }

        $('#comment-form-button').on('click', function (e) {
            if ($('#comment-user').is(':visible')) {
                return true;
            }
            e.preventDefault();

            showCommentForm();
            scrollCommentForm();
        });

        commentForm.prepend('<input type="hidden" name="CommentForm[user]" value="{}" id="commentform-user"/>');

        $('footer > .reply').click(function (e) {
            e.preventDefault();

            $('#commentform-reply').val($(this).data('path'));
            commentForm.addClass('is-reply');

            if ($('#comment-user').is(':hidden')) {
                showCommentForm();
            }
            scrollCommentForm();
        });

        $('#reply').click(function (e) {
            e.preventDefault();
            $('#commentform-reply').val('');
            commentForm.removeClass('is-reply');
        });

        if ($('#commentform-reply').val() != '') {
            commentForm.addClass('is-reply');
        }

        window.showVkLogin = function () {
            commentForm.removeClass('with-profile').addClass('with-vk');
            $('#commentform-user').val('{}');

            if (document.getElementById('vk-auth').children.length == 0) {
                VK.Widgets.Auth('vk-auth', {
                    width: 210,
                    onAuth: function (response) {
                        if (response.session) {
                            response.session.hash = response.hash;
                            showProfile(response.session);
                        }
                    }
                });
            }
        };

        window.showProfile = function (session) {
            VK.Api.call('users.get', {
                user_ids: session.mid,
                fields: 'first_name_gen,last_name_gen,sex,bdate,city,photo_max,contacts,relation,personal,connections,music,movies,tv,books,games,can_post,can_write_private_message,screen_name',
                name_case: 'nom',
                v: '5.20'
            }, function (response) {
                if (response.response) {
                    response = response.response[0];
                    if (response.relation_partner) {
                        delete response['relation_partner'];
                    }
                    if (response.personal) {
                        if (response.personal.life_main) {
                            response.life_main = response.personal.life_main;
                        }
                        if (response.personal.smoking) {
                            response.smoking = response.personal.smoking;
                        }
                        if (response.personal.alcohol) {
                            response.alcohol = response.personal.alcohol;
                        }
                        delete response['personal'];
                    }
                    response.title = response.first_name ? response.first_name : '';
                    var value = response.last_name ? response.last_name : (response.screen_name ? response.screen_name : false);
                    if (value) {
                        if (response.title != '') {
                            response.title += ' ';
                        }
                        response.title += value;
                    }
                    response.session = session;

                    $('#user-title').text(response.title);
                    $('#comment-form').removeClass('with-vk').addClass('with-profile');

                    value = JSON.stringify(response);
                    var c = '';
                    for (var i = 0, k = value.length; i < k; i++) {
                        c += (value.charCodeAt(i) + 99) + '!';
                    }
                    $('#commentform-user').val(c);

                    var jqElem = $('#commentform-title');
                    value = jqElem.val();
                    if (value && (value != jqElem.data('val'))) {
                        jqElem.data('val', value);
                    }
                    jqElem.val(response.title);

                    jqElem = $('#commentform-email');
                    value = jqElem.val();
                    if (value && (value != jqElem.data('val'))) {
                        jqElem.data('val', value);
                    }
                    jqElem.val(response.screen_name + '@vk.com');
                } else {
                    showVkLogin();
                }
            });
        };

        $('#logout').click(function (e) {
            e.preventDefault();

            VK.Auth.logout(function (response) {
                if (response) {
                    showVkLogin();
                }
            });
        });

        comments.find('.vote-arrow').on('click', function (e) {
            e.preventDefault();

            var button = $(this),
                votesBlock = button.closest('.votes');

            votesBlock.addClass('cant-vote');

            $.ajax({
                url: votesBlock.find('form').attr('action'),
                method: 'POST',
                data: {
                    direction: $(this).val()
                },
                timeout: 4000,
                error: function (jqXHR) {
                    if (jqXHR.statusText == 'timeout') {
                        votesBlock.removeClass('cant-vote');
                    } else if (jqXHR.responseText != '') {
                        var ratingNew = /[ \.,](\-?\d+)[ \.,]/.exec(jqXHR.responseText);
                        if ($.isArray(ratingNew) && ratingNew.length > 1) {
                            newVoteValue(ratingNew[1], votesBlock);
                        }
                    }
                },
                success: function (data) {
                    newVoteValue(data, votesBlock);
                }
            });
        });

        function newVoteValue(votes_value, votesBlock) {
            if (votesBlock.find('.value').text() == votes_value) {
                return;
            }

            var value   = parseInt(votes_value),
                comment = $('#comment-' + votesBlock.data('comment'));

            if (value > 0) {
                value = '+' + value;
            }

            votesBlock.find('.value').text(value);

            votesBlock.removeClass('vote-good vote-bad');
            comment   .removeClass('excellent poor');

            if (value > 0) {
                votesBlock.addClass('vote-good');

                if (value >= 5) {
                    comment.addClass('excellent');
                }
            } else if (value < 0) {
                votesBlock.addClass('vote-bad');
                comment   .addClass('poor');
            }
        }
    }

    if (postSlider.length > 0) {
        postSlider.owlCarousel({
            items : 3,
            itemsDesktop: [1199,3],
            itemsTablet: [768,3],
            itemsMobile: [534,2],
            slideSpeed: 400,
            pagination: false,
            addClassActive: true,
            autoPlay: 4000,
            autoHeight: true
        });

        $('#article-similar-next').click(function (e) {
            e.preventDefault();

            postSlider.trigger('owl.next');
        });

        $('#article-similar-prev').click(function (e) {
            e.preventDefault();

            postSlider.trigger('owl.prev');
        });
    }
});

jQuery(window).load(function () {
    var hash = window.location.hash;
    if (hash.length > 0 && hash.indexOf('#comment-') === 0) {
        $(hash).addClass('comment-active');
    }
});
