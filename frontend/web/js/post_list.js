jQuery(document).ready(function () {
    var next = $('#next');

    if (next.length === 1) {
        window.pageCount = 0;
        window.pageNext = next.click(function () {
            if (window.pageNext.hasClass('page-loading')) {
                return false;
            }

            if (window.pageCount > 1) {
                return true;
            }

            window.pageNext.text('\u0414\u043e\u0433\u0440\u0443\u0437\u043a\u0430\u2026').addClass('page-loading');

            $.ajax({
                url: window.pageNext.attr('href'),
                error: function () {
                    location.assign(window.pageNext.attr('href'));
                },
                success: function (data) {
                    data = $('.post-feed-wrapper', data);

                    if (data.length === 0) {
                        location.assign(window.pageNext.attr('href'));
                    }

                    window.pageCount++;

                    var next = data.children('#next');

                    if (next.length === 0) {
                        window.pageNext.hide().unbind('click');
                    } else {
                        window.pageNext
                            .attr('href', next.attr('href'))
                            .removeClass('page-loading')
                            .text('\u041f\u043e\u043a\u0430\u0437\u0430\u0442\u044c \u0435\u0449\u0451');
                    }

                    $('#post-feed').append(data.find('article'));

                    window.autoloadCalculate();
                }
            });
            return false;
        });

        $(window).scroll(function () {
            var scrollPos = Math.ceil($(window).scrollTop());

            if ((typeof window.pageNext != 'undefined') && window.pageNext.is(":visible") && window.pageCount < 2 && (scrollPos > window.autoloadPos)) {
                window.pageNext.click();
            }
        });
    }

    window.autoloadCalculate = function () {
        window.autoloadPos = $('.content').innerHeight() - (window.innerHeight || 600);
    };

    window.autoloadCalculate();
});

jQuery(window).load(function () {

});
