jQuery(document).ready(function () {
    var feedbackForm = $('#feedback-form');
    if (feedbackForm.length > 0) {
        if (window.location.hash == '#sended') {
            feedbackForm.after('<a href="#feedback-form" class="button-submit" id="new-feedback-message">\u041d\u0430\u043f\u0438\u0441\u0430\u0442\u044c \u0435\u0449\u0435</a>'); // Написать еще
            feedbackForm.after('<h3 id="feedback-message-sent">Ваше сообщение отправлено</h3>');
            $('#feedback-form').hide();
        }

        $('#new-feedback-message').on('click', function (e) {
            e.preventDefault();

            $('#feedback-message-sent').remove();
            $(this).remove();
            $('#feedback-form').show();
        });
    }
});

jQuery(window).on('load', function () {

});
