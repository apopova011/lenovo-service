<?php

namespace frontend\assets;

class FormFullAsset extends AppAssetBundle
{
    public $css = [];
    public $js = [];
    public $depends = [
        'frontend\assets\FormSimpleAsset',
        'yii\jui\JuiAsset',
        'yii\widgets\MaskedInputAsset',
    ];
}
