<?php
namespace frontend\assets;

class PostListAsset extends AppAssetBundle
{
    public $css = [
    ];
    public $js = [
        'js/post_list.js',
    ];
    public $depends = [
        'frontend\assets\AppAsset',
    ];
}
