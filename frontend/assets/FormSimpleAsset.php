<?php

namespace frontend\assets;

class FormSimpleAsset extends AppAssetBundle
{
    public $css = [
        'css/alert.css',
    ];
    public $js = [
        'js/alert.js',
    ];
    public $depends = [
        'frontend\assets\AppAsset',
        'yii\widgets\ActiveFormAsset',
        'yii\validators\ValidationAsset',
        'yii\validators\PunycodeAsset',
    ];
}
