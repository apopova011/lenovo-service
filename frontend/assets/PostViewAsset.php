<?php
namespace frontend\assets;

class PostViewAsset extends AppAssetBundle
{
    public $css = [
        'css/likely.css',
        'css/owl.carousel.css',
        'css/lightgallery.css',
    ];
    public $js = [
        'js/likely.js',

        'js/owl.carousel.js',

        'js/lightgallery.js',
        'js/lg-fullscreen.js',
        'js/lg-hash.js',
        'js/lg-thumbnail.js',

        'js/jquery.scroll.js',

        'video-js/5.11.6.js',
        'video-js/youtube.js',
        'video-js/ads.js',
        'video-js/vast-client.js',
        'video-js/vast.js',

        'js/post_view.js',
    ];
    public $depends = [
        'frontend\assets\AppAsset',
    ];
}
