<?php

namespace frontend\components;

use yii\web\UrlManager;

/**
 * FrontendUrlManager is UrlManager for Frontend.
 */
class FrontendUrlManager extends UrlManager
{
    /**
     * @inheritdoc
     */
    public $enablePrettyUrl = true;

    /**
     * @inheritdoc
     */
    public $enableStrictParsing = true;

    /**
     * @inheritdoc
     */
    public $showScriptName = false;

    /**
     * @inheritdoc
     */
    public $cache = 'cacheCommon';

    /**
     * @inheritdoc
     */
    public $rules = [
        '' => 'site/index',
        ['pattern' => 'dadata/<method:address>', 'route' => 'site/dadata'],
        ['pattern' => 'partners', 'route' => 'partners/view', 'suffix' => '/'],
        ['pattern' => 'timeline-<year:20\d\d><month:(0[1-9]|1[0-2])><day:(0[1-9]|[12][0-9]|3[01])>-<hour:([01][0-9]|2[0-3])><minute:([0-5][0-9])><second:([0-5][0-9])>', 'route' => 'post/index', 'suffix' => '.html'],
        ['pattern' => 'themes/<slug:[a-z0-9-]+>/timeline-<year:20\d\d><month:(0[1-9]|1[0-2])><day:(0[1-9]|[12][0-9]|3[01])>-<hour:([01][0-9]|2[0-3])><minute:([0-5][0-9])><second:([0-5][0-9])>', 'route' => 'tag/view', 'suffix' => '.html'],
        ['pattern' => 'themes/<slug:[a-z0-9-]+>', 'route' => 'tag/view', 'suffix' => '/'],
        ['pattern' => 'auth/<action:(login|logout|signup|profile|request\-password\-new|confirm)>', 'route' => 'auth/<action>'],
        ['pattern' => 'users/<slug:[a-z0-9-]+>', 'route' => 'user/view', 'suffix' => '/'],
        ['pattern' => 'search', 'route' => 'search/index', 'suffix' => '/'],
        ['pattern' => 'vote/<id:\d+>', 'route' => 'vote/comment'],
        ['pattern' => 'feedback', 'route' => 'feedback/index'],
        ['pattern' => 'media/<year:20\d\d>-<type:(post-icon|folder-icon)>/<name:[a-zA-Z0-9_\.-]+>-<size:(240x135|600x338|1920x1080)>.<extension:(jpe?g|png|gif)>', 'route' => 'thumbnail/view'],
        ['pattern' => 'media/<year:20\d\d>-<type:user-avatar>/<name:[a-zA-Z0-9_\.-]+>-<size:(50x50|100x100)>.<extension:(jpe?g|png|gif)>', 'route' => 'thumbnail/view'],
        ['pattern' => 'media/<year:20\d\d>-<type:(0[1-9]|1[0-2])>/<name:[a-zA-Z0-9_\.-]+>-<size:(\d+x\d+)>.<extension:(jpe?g|png|gif)>', 'route' => 'thumbnail/view'],
        ['pattern' => 'rss/<action:(all|yandex)>', 'route' => 'rss/<action>', 'suffix' => '.xml'],
        ['pattern' => 'sitemap<action:(tag|post|tag-timeline|post-timeline|folder-timeline)><page:\d+>', 'route' => 'sitemap/<action>', 'suffix' => '.xml'],
        ['pattern' => 'sitemap<action:(index|page|folder|news)>', 'route' => 'sitemap/<action>', 'suffix' => '.xml'],
        ['pattern' => '<path:([\\a-z0-9-]+/)?[a-z0-9-]+>/timeline-<year:20\d\d><month:(0[1-9]|1[0-2])><day:(0[1-9]|[12][0-9]|3[01])>-<hour:([01][0-9]|2[0-3])><minute:([0-5][0-9])><second:([0-5][0-9])>', 'route' => 'folder/view', 'encodeParams' => false, 'suffix' => '.html'],
        ['pattern' => '<folder_slug:[a-z0-9-]+>/<post_slug:[a-z0-9_-]+>-<post_id:\d+>', 'route' => 'post/view', 'suffix' => '.html'],
        ['pattern' => '<path:([\\a-z0-9-]+/)?[a-z0-9-]+>', 'route' => 'folder/view', 'encodeParams' => false, 'suffix' => '/'],
        ['pattern' => '<slug:[a-z0-9_-]+>', 'route' => 'post/view-by-slug'],
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->setBaseUrl('');
        $this->setScriptUrl('/index.php');

        $appParams = \Yii::$app->params;
        $this->setHostInfo($appParams['frontendScheme'] . '://' . $appParams['frontendDomain']);

        parent::init();
    }
}
