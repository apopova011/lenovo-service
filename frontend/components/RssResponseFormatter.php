<?php

namespace frontend\components;

use \yii\base\Arrayable;
use \yii\web\XmlResponseFormatter;
use \DOMCdataSection;
use \DOMDocument;
use \DOMElement;
use \DOMText;
use \common\helpers\StringHelper;

/**
 * RssResponseFormatter formats the given data into an RSS.
 *
 * It is used by [[Response]] to format response data.
 *
 * @author Kostya M <primat@list.ru>
 */
class RssResponseFormatter extends XmlResponseFormatter
{
    /**
     * @inheritdoc
     */
    public $rootTag = 'rss';

    /**
     * @var array
     */
    public $rootAttributes = [
        'version' => '2.0',
    ];

    /**
     * @var array
     */
    public $rootNs = [];

    /**
     * @var string
     */
    public $rootSubTag = 'channel';

    /**
     * Namespaces catalog.
     * @var array
     */
    public $ns = [
        'yandex' => [
            'namespaceURI' => 'http://www.w3.org/2000/xmlns/',
            'qualifiedName' => 'xmlns:yandex',
            'value' => 'http://news.yandex.ru'
        ],
        'media' => [
            'namespaceURI' => 'http://www.w3.org/2000/xmlns/',
            'qualifiedName' => 'xmlns:media',
            'value' => 'http://search.yahoo.com/mrss/'
        ],
        'atom' => [
            'namespaceURI' => 'http://www.w3.org/2000/xmlns/',
            'qualifiedName' => 'xmlns:atom',
            'value' => 'http://www.w3.org/2005/Atom'
        ],
        'content' => [
            'namespaceURI' => 'http://www.w3.org/2000/xmlns/',
            'qualifiedName' => 'xmlns:content',
            'value' => 'http://purl.org/rss/1.0/modules/content/'
        ],
        'dc' => [
            'namespaceURI' => 'http://www.w3.org/2000/xmlns/',
            'qualifiedName' => 'xmlns:dc',
            'value' => 'http://purl.org/dc/elements/1.1/'
        ],
        'slash' => [
            'namespaceURI' => 'http://www.w3.org/2000/xmlns/',
            'qualifiedName' => 'xmlns:slash',
            'value' => 'http://purl.org/rss/1.0/modules/slash/'
        ],
    ];

    /**
     * @var array
     */
    public $cdataTags = [
        'content:encoded',
        'full-text',
    ];

    /**
     * @var DOMDocument
     */
    protected $domDocument;

    /**
     * @inheritdoc
     */
    public function format($response)
    {
        $response->getHeaders()->set('Content-Type', $this->contentType);
        $this->domDocument = new DOMDocument($this->version, $this->encoding === null ? $response->charset : $this->encoding);

        $root = new DOMElement($this->rootTag);
        $this->domDocument->appendChild($root);
        foreach ($this->rootAttributes as $attribute => $value) {
            $root->setAttribute($attribute, $value);
        }

        $rootSub = new DOMElement($this->rootSubTag);
        $root->appendChild($rootSub);

        $this->buildXml($rootSub, $response->data);
        foreach ($this->rootNs as $ns) {
            $root->setAttributeNS($this->ns[$ns]['namespaceURI'], $this->ns[$ns]['qualifiedName'], $this->ns[$ns]['value']);
        }

        $response->content = $this->domDocument->saveXML();
    }

    /**
     * @param DOMElement $parentElement
     * @param string|int $name
     * @return DOMElement
     */
    protected function appendNewElement(&$parentElement, $name)
    {
        if (is_int($name)) {
            $name = $this->itemTag;
        } else {
            $ns = strpos($name, ':');
            if ($ns) {
                $this->rootNs[] = substr($name, 0, $ns);
            }
        }

        $element = $this->domDocument->createElement($name);
        $parentElement->appendChild($element);

        return $element;
    }

    /**
     * @param DOMElement $element
     * @param mixed $data
     */
    protected function buildXml($element, $data)
    {
        if (is_object($data)) {
            $child = new DOMElement(StringHelper::basename(get_class($data)));
            $element->appendChild($child);
            if ($data instanceof Arrayable) {
                $this->buildXml($child, $data->toArray());
            } else {
                $array = [];
                foreach ($data as $name => $value) {
                    $array[$name] = $value;
                }
                $this->buildXml($child, $array);
            }
        } elseif (is_array($data)) {
            foreach ($data as $name => $value) {
                $valueIsObject = is_object($value);
                if (is_int($name) && $valueIsObject) {
                    // элемент из одинаковых тегов заподряд
                    $this->buildXml($element, $value);
                } elseif (is_array($value) || $valueIsObject) {
                    if ($name === 'category' || $name === 'yandex:logo' || $name === 'enclosure') {
                        // несколько одинаковых тегов заподряд
                        $this->itemTag = $name;
                        $this->buildXml($element, $value);
                        $this->itemTag = 'item';
                    } else {
                        // теги с аттрибутами
                        $child = $this->appendNewElement($element, $name);

                        /** @var boolean $withoutValue одиночный закрытый тег */
                        $withoutValue = $name === 'atom:link' || $this->itemTag === 'enclosure';
                        if ($withoutValue || $name === 'guid' || $this->itemTag === 'yandex:logo') {
                            foreach ($value as $attribute => $text) {
                                if (is_numeric($attribute)) {
                                    continue;
                                }

                                $child->setAttribute($attribute, $text);
                            }

                            if (!$withoutValue) {
                                $value = $value[0];
                            }
                        }
                        if (!$withoutValue) {
                            $this->buildXml($child, $value);
                        }
                    }
                } else {
                    $child = $this->appendNewElement($element, $name);

                    $value = (string)$value;
                    $value = in_array($name, $this->cdataTags, true) ? new DOMCdataSection($value) : new DOMText($value);
                    $child->appendChild($value);
                }
            }
        } else {
            $element->appendChild(new DOMText((string)$data));
        }
    }
}
