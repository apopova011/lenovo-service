<?php

namespace frontend\components;

use \yii\base\Arrayable;
use \yii\web\XmlResponseFormatter;
use \DOMDocument;
use \DOMElement;
use \DOMText;
use \common\helpers\StringHelper;

/**
 * SitemapResponseFormatter formats the given data into an sitemap.
 *
 * It is used by [[Response]] to format response data.
 *
 * @author Kostya M <primat@list.ru>
 */
class SitemapResponseFormatter extends XmlResponseFormatter
{
    /**
     * @inheritdoc
     */
    public $rootTag = 'urlset';

    /**
     * @var array
     */
    public $rootAttributes = [
        'xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9',
    ];

    /**
     * @var string the Content-Type header for the response
     */
    public $itemTag = 'url';

    /**
     * Namespaces catalog.
     * @var array
     */
    public $ns = [
        'news' => [
            'namespaceURI' => 'http://www.w3.org/2000/xmlns/',
            'qualifiedName' => 'xmlns:news',
            'value' => 'http://www.google.com/schemas/sitemap-news/0.9'
        ],
    ];

    /**
     * @var array
     */
    public $rootNs = [];

    /**
     * @var DOMDocument
     */
    protected $domDocument;

    /**
     * @inheritdoc
     */
    public function format($response)
    {
        $charset = $this->encoding === null ? $response->charset : $this->encoding;
        if (stripos($this->contentType, 'charset') === false) {
            $this->contentType .= '; charset=' . $charset;
        }
        $response->getHeaders()->set('Content-Type', $this->contentType);
        $this->domDocument = new DOMDocument($this->version, $this->encoding === null ? $response->charset : $this->encoding);

        $root = new DOMElement($this->rootTag);
        $this->domDocument->appendChild($root);
        foreach ($this->rootAttributes as $attribute => $value) {
            $root->setAttribute($attribute, $value);
        }

        $this->buildXml($root, $response->data);
        foreach ($this->rootNs as $ns) {
            $root->setAttributeNS($this->ns[$ns]['namespaceURI'], $this->ns[$ns]['qualifiedName'], $this->ns[$ns]['value']);
        }

        $response->content = $this->domDocument->saveXML();
    }

    /**
     * @param DOMElement $parentElement
     * @param string|int $name
     * @return DOMElement
     */
    protected function appendNewElement(&$parentElement, $name)
    {
        if (is_int($name)) {
            $name = $this->itemTag;
        } else {
            $ns = strpos($name, ':');
            if ($ns) {
                $this->rootNs[] = substr($name, 0, $ns);
            }
        }

        $element = $this->domDocument->createElement($name);
        $parentElement->appendChild($element);

        return $element;
    }

    /**
     * @param DOMElement $element
     * @param mixed $data
     */
    protected function buildXml($element, $data)
    {
        if (is_object($data)) {
            $child = new DOMElement(StringHelper::basename(get_class($data)));
            $element->appendChild($child);
            if ($data instanceof Arrayable) {
                $this->buildXml($child, $data->toArray());
            } else {
                $array = [];
                foreach ($data as $name => $value) {
                    $array[$name] = $value;
                }
                $this->buildXml($child, $array);
            }
        } else if (is_array($data)) {
            foreach ($data as $name => $value) {
                $valueIsObject = is_object($value);
                if (is_int($name) && $valueIsObject) {
                    $this->buildXml($element, $value);
                } else if (is_array($value) || $valueIsObject) {
                    $child = $this->appendNewElement($element, $name);
                    $element->appendChild($child);
                    $this->buildXml($child, $value);
                } else {
                    $child = $this->appendNewElement($element, $name);
                    $element->appendChild($child);
                    $child->appendChild(new DOMText((string)$value));
                }
            }
        } else {
            $element->appendChild(new DOMText((string)$data));
        }
    }
}
