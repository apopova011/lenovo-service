#!/bin/sh
export PGUSER=postgres
export PGPASSWORD="postgres"
cd /var/www/accesstyle/common/data/
/usr/bin/find ./db/ -type f -name '*.backup' -mtime +7 -delete
/usr/bin/pg_dump --format custom --blobs accesstyle > ./db/$(date +%Y_%m_%d_%H_%M).backup