<?php
use common\helpers\Url;

/**
 * @var $this yii\web\View
 * @var $username string
 * @var $token    string
 * @var $toFrontend bool
 */

$appName         = $toFrontend ? Yii::$app->params['brandName'] : Yii::$app->name;
$toRouteFunction = $toFrontend ? 'toRouteFrontend' : 'toRoute';
?>

<?= Yii::t('common\email', 'Hello, {username}!', ['username' => $username]); ?> <?= Yii::t('common\email', 'You were invited to the {appName}.', ['appName' => $appName]); ?>

<?= Yii::t('common\email', 'To get a password, click here:'); ?> <?= Url::$toRouteFunction(['auth/confirm', 'token' => $token], true); ?>

<?= Yii::t('common\email', 'If you do not have any relationship to this site, ignore this email.'); ?>
