<?php

/**
 * @var yii\web\View $this
 * @var string $text
 */

?>
<div>
    <p><?= nl2br($text); ?></p>
</div>
