<?php
$subDomainFrontend = '';
$subDomainBackend  = 'admin';
$subDomainApi      = 'api';

if (array_key_exists('SERVER_NAME', $_SERVER)) {
    $domain = $_SERVER['SERVER_NAME'];
    if ($subDomainFrontend && strpos($domain, $subDomainFrontend) === 0) {
        $domain = substr($domain, strlen($subDomainFrontend) + 1);
    } elseif (strpos($domain, $subDomainBackend) === 0) {
        $domain = substr($domain, strlen($subDomainBackend) + 1);
    }
} else {
    $domain = 'lenovo-service.com';
}

$frontendDomain = $domain;
if ($subDomainFrontend) {
    $frontendDomain = $subDomainFrontend . '.' . $frontendDomain;
}

return [
    'frontendScheme' => 'https',
    'frontendDomain' => $frontendDomain,
    'backendDomain'  => $subDomainBackend . '.' . $domain,
    'apiDomain'      => $subDomainApi     . '.' . $domain,
    'urlMedia' => '/media',
    'brandName' => 'LenovoService',
    'emailInfo'    =>    'info@' . $domain,
    'emailSupport' => 'support@' . $domain,
    'emailBugs'    =>    'bugs@' . $domain,

    'directoryAvatars' => 'avatars',
    'backgrounds' => [
        '0' => 'FFACC5',
        '1' => 'FF944D',
        '2' => '71B84D',
        '3' => '4D94DB',
        '4' => '4D71DB',
        '5' => 'FF7194',
        '6' => '6AD19F',
        '7' => 'DB4D4D',
        '8' => 'B871B8',
        '9' => 'FFB84D',
        'a' => 'FFDB71',
        'b' => 'DB94DB',
        'c' => '4DB8DB',
        'd' => 'B8DB4D',
        'e' => '4DB8B8',
        'f' => '9471DB',
    ],
    'defaultAuthor' => [
        'id' => 1,
        'title' => 'Author'
    ],
    'postsUrlReplace' => [],
    'postMinShowCount' => 10,
];
