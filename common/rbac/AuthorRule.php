<?php

namespace common\rbac;

use \yii\rbac\Rule;

class AuthorRule extends Rule
{
    /**
     * @inheritdoc
     */
    public $name = 'author';

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        $hasAuthor = false;

        if (is_object($params)) {
            /** @var \yii\db\ActiveRecord $params */
            $hasAuthor = $params->hasAttribute('author_id');
        } else if (is_array($params)) {
            $hasAuthor = array_key_exists('author_id', $params);
        }

        return $hasAuthor && $params['author_id'] === $user;
    }
}