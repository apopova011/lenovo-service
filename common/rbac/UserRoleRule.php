<?php

namespace common\rbac;

use Yii;
use \yii\rbac\Rule;
use \common\models\User;

class UserRoleRule extends Rule
{
    /**
     * @inheritdoc
     */
    public $name = 'userRole';

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        $authUser = Yii::$app->getUser();
        if (!$authUser->getIsGuest() && $user === $authUser->getIdentity()->id) {
            $user = $authUser->getIdentity();
        } else {
            $user = User::findIdentity($user);
        }

        if ($user === null) {
            return false;
        }

        $userRole = $user->role;
        $itemName = $item->name;

        $roles = ['guest', 'registered', 'verified', 'writer', 'contributor', 'editor', 'administrator']; // without moderator
        $userWeight = array_search($userRole, $roles, true);
        $itemWeight = array_search($itemName, $roles, true);
        if ($userWeight !== false && $itemWeight !== false) {
            return $userWeight >= $itemWeight;
        } elseif ($userRole === 'moderator') {
            return $itemName === 'guest' || $itemName === 'registered' || $itemName === 'verified' || $itemName === 'moderator';
        } elseif ($itemName === 'moderator') {
            return $userRole === 'administrator' || $userRole === 'editor' || $userRole === 'contributor' || $userRole === 'moderator';
        }
        return false;
    }
}
