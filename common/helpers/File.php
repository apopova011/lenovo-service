<?php

namespace common\helpers;

use Yii;
use \yii\helpers\BaseFileHelper;
use \lsolesen\pel\PelDataWindow;
use \lsolesen\pel\PelEntryAscii;
use \lsolesen\pel\PelExif;
use \lsolesen\pel\PelIfd;
use \lsolesen\pel\PelInvalidDataException;
use \lsolesen\pel\PelJpeg;
use \lsolesen\pel\PelJpegInvalidMarkerException;
use \lsolesen\pel\PelTag;
use \lsolesen\pel\PelTiff;
use \common\models\Post;
use \common\models\PostFolder;
use \common\models\PostTag;
use \common\models\Tag;
use \frontend\controllers\SitemapController;

class File extends BaseFileHelper
{
    /**
     * @var int
     */
    protected static $pageLimit = SitemapController::PAGE_LIMIT;

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public static function deleteSitemapTag()
    {
        $last = ceil(Tag::find()->count() / static::$pageLimit);
        for ($i = 1; $i <= $last; ++$i) {
            static::deleteXmlAndGzByPath('sitemap' . DIRECTORY_SEPARATOR . 'sitemaptag' . $i);
        }

        $last = ceil(PostTag::find()->count() / static::$pageLimit);

        for ($i = 1; $i <= $last; ++$i) {
            static::deleteXmlAndGzByPath('sitemap' . DIRECTORY_SEPARATOR . 'sitemaptag-timeline' . $i);
        }
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public static function deleteSitemapFolder()
    {
        static::deleteXmlAndGzByPath('sitemap' . DIRECTORY_SEPARATOR . 'sitemapfolder');

        $last = ceil(PostFolder::find()->count() / static::$pageLimit);

        for ($i = 1; $i <= $last; ++$i) {
            static::deleteXmlAndGzByPath('sitemap' . DIRECTORY_SEPARATOR . 'sitemapfolder-timeline' . $i);
        }
    }

    /**
     * @param null|integer $postId
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public static function deleteSitemapPost($postId = null)
    {
        $sitemapTypes = [
            'sitemappost',
            'sitemappost-timeline',
        ];

        $sitemapPostFileFrom = is_numeric($postId) ? ceil(Post::find()->isPublished()->andWhere('id <= ' . $postId)->count() / static::$pageLimit) : 1;
        $sitemapPostFileLast = ceil(Post::find()->isPublished()->count() / static::$pageLimit);

        foreach ($sitemapTypes as $sitemapType) {
            for ($i = $sitemapPostFileFrom; $i <= $sitemapPostFileLast; ++$i) {
                static::deleteXmlAndGzByPath('sitemap' . DIRECTORY_SEPARATOR . $sitemapType . $i);
            }
        }
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public static function deleteRss()
    {
        $rssTypes = [
            'all',
            'yandex',
        ];

        foreach ($rssTypes as $rssType) {
            static::deleteXmlAndGzByPath('rss' . DIRECTORY_SEPARATOR . $rssType);
        }

        // там содержится главная страница сайта
        static::deleteXmlAndGzByPath('sitemap' . DIRECTORY_SEPARATOR . 'sitemappage');
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public static function deleteNews()
    {
        static::deleteXmlAndGzByPath('sitemap' . DIRECTORY_SEPARATOR . 'sitemapnews');
    }

    /**
     * @param string $path
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public static function deleteXmlAndGzByPath($path)
    {
        $path = Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $path . '.xml';
        Yii::trace('Delete from static cache ' . $path, 'StaticCache');
        if (file_exists($path)) {
            unlink($path);
            unlink($path . '.gz');
        }
    }

    /**
     * @param string $filename
     * @param \common\models\Photo|array $photo
     * @return bool
     * @throws PelInvalidDataException
     * @throws PelJpegInvalidMarkerException
     */
    public static function addJpegMetadata($filename, $photo)
    {
        $params = Yii::$app->params;

        $creator = $params['brandName'];
        $email = $params['emailInfo'];
        $site = 'http://' . $params['frontendDomain'];
        $description = $photo['post_content_img_alt'];
        $copyrightYear = empty($photo['post_published']) ? date('Y') : date('Y', strtotime($photo['post_published']));
        $copyright = '©' . $copyrightYear . ' ' . $creator;

        $iptcData = [
            '1#090' => "\x1B%G",
            '2#080' => $creator,
            '2#116' => $copyright,
        ];

        $exifData = [
            PelTag::ARTIST => $creator,
            PelTag::COPYRIGHT => $copyright,
        ];

        $xmpData = [
            'brandName' => $creator,
            'copyright' => $copyright,
            'site' => $site,
            'email' => $email,
        ];

        if (!empty($description)) {
            $exifData[PelTag::IMAGE_DESCRIPTION] = $description;
            $iptcData['2#120'] = $description;
            $xmpData['description'] = $description;
        }

        $xmpData = static::prepareXmp($xmpData);

        $memoryLimit = ini_get('memory_limit');
        ini_set('memory_limit', '96M');

        $result = static::addExif($filename, $exifData) && static::addXmp($filename, $xmpData) && static::addIptc($filename, $iptcData);

        ini_set('memory_limit', $memoryLimit);

        return $result;
    }

    /**
     * @param string $filename
     * @param array $exifData
     * @return bool
     * @throws PelInvalidDataException
     * @throws PelJpegInvalidMarkerException
     */
    public static function addExif($filename, $exifData = [])
    {
        $data = new PelDataWindow(file_get_contents($filename));
        $jpeg = $file = new PelJpeg();

        $jpeg->load($data);
        $exif = $jpeg->getExif();

        if ($exif === null) {
            $exif = new PelExif();
            $jpeg->setExif($exif);
            $tiff = new PelTiff();
            $exif->setTiff($tiff);
        } else {
            $tiff = $exif->getTiff();
        }

        $ifd0 = $tiff->getIfd();

        if ($ifd0 === null) {
            $ifd0 = new PelIfd(PelIfd::IFD0);
            $tiff->setIfd($ifd0);
        }

        foreach ($exifData as $tag => $string) {
            static::setExifValue($tag, $string, $ifd0);
        }

        $file->saveFile($filename);

        return true;
    }

    /**
     * @param string $filename
     * @param array $iptcData
     * @return bool
     */
    public static function addIptc($filename, $iptcData = [])
    {
        $data = '';

        foreach ($iptcData as $tag => $string) {
            $rec = $tag[0];
            $tag = substr($tag, 2);
            $data .= static::iptMakeTag($rec, $tag, $string);
        }

        $content = iptcembed($data, $filename);

        $fp = fopen($filename, 'wb');
        fwrite($fp, $content);
        fclose($fp);

        return true;
    }

    /**
     * @param string $filename
     * @param string $xmpData
     * @return bool
     */
    public static function addXmp($filename, $xmpData)
    {
        $oldJpegHeaderData = static::getJpegHeaderData($filename);

        if (empty($oldJpegHeaderData)) {
            return false;
        }

        $newJpegHeaderData = static::putXmpText($oldJpegHeaderData, $xmpData);

        if (empty($newJpegHeaderData)) {
            return false;
        }

        return static::putJpegHeaderData($filename, $newJpegHeaderData);
    }

    /**
     * @param PelTag|integer $tag
     * @param string $value
     * @param PelIfd $ifd0
     */
    public static function setExifValue($tag, $value, &$ifd0)
    {
        $desc = $ifd0->getEntry($tag);
        if ($desc === null) {
            $desc = new PelEntryAscii($tag, $value);
            $ifd0->addEntry($desc);
        } else {
            $desc->setValue($value);
        }
    }

    /**
     * @param array $data
     * @return string
     */
    public static function prepareXmp($data)
    {
        $descriptionText = '';

        if (!empty($data['description'])) {
            $descriptionText = <<<XML
<dc:description>
    <rdf:Alt>
        <rdf:li xml:lang="x-default">{$data['description']}</rdf:li>
    </rdf:Alt>
</dc:description>
XML;
        }

        return <<<XML
<?xpacket begin="?" id="W5M0MpCehiHzreSzNTczkc9d"?>
<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.5-c021 79.154911, 2013/10/29-11:47:16        ">
    <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
        <rdf:Description rdf:about="" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:Iptc4xmpCore="http://iptc.org/std/Iptc4xmpCore/1.0/xmlns/" xmlns:xmpRights="http://ns.adobe.com/xap/1.0/rights/">
            <dc:format>image/jpeg</dc:format>
            <dc:creator>
                <rdf:Seq>
                    <rdf:li>{$data['brandName']}</rdf:li>
                </rdf:Seq>
            </dc:creator>
            <dc:rights>
                <rdf:Alt>
                    <rdf:li xml:lang="x-default">{$data['copyright']}</rdf:li>
                </rdf:Alt>
            </dc:rights>
            {$descriptionText}
            <Iptc4xmpCore:CreatorContactInfo rdf:parseType="Resource">
                <Iptc4xmpCore:CiEmailWork>{$data['email']}</Iptc4xmpCore:CiEmailWork>
                <Iptc4xmpCore:CiUrlWork>{$data['site']}</Iptc4xmpCore:CiUrlWork>
            </Iptc4xmpCore:CreatorContactInfo>
            <xmpRights:Marked>True</xmpRights:Marked>
        </rdf:Description>
    </rdf:RDF>
</x:xmpmeta>
XML;
    }

    /**
     * @param integer $rec
     * @param $data
     * @param char string $value
     * @return string
     */
    public static function iptMakeTag($rec, $data, $value)
    {
        $length = strlen($value);
        $val = chr(0x1C) . chr($rec) . chr($data);

        if ($length < 0x8000) {
            $val .= chr($length >> 8) . chr($length & 0xFF);
        } else {
            $val .= chr(0x80) .
                chr(0x04) .
                chr(($length >> 24) & 0xFF) .
                chr(($length >> 16) & 0xFF) .
                chr(($length >>  8) & 0xFF) .
                chr( $length        & 0xFF);
        }

        return $val . $value;
    }

    /**
     * @param $filename
     * @return array|bool
     */
    public static function getJpegHeaderData($filename)
    {
        $file = fopen($filename, 'rb');
        $headerData = [];
        $data = fread($file, 2);

        if ($data !== "\xFF\xD8") {
            fclose($file);
            return false;
        }

        $data = fread($file, 2);

        if ($data[0] !== "\xFF") {
            fclose($file);
            return false;
        }

        $hitCompressedImageData = false;

        while (!empty($data) && $data[1] !== "\xD9" && !$hitCompressedImageData && !feof($file)) {
            $ordData = ord($data[1]);

            if ($ordData < 0xD0 || $ordData > 0xD7) {
                $decodedSize = unpack('nsize', fread($file, 2));
                $headerData[] = [
                    'SegType' => ord($data[1]),
                    'SegDataStart' => ftell($file),
                    'SegData' => fread($file, $decodedSize['size'] - 2)
                ];
            }

            if ($data[1] === "\xDA") {
                $hitCompressedImageData = true;
            } else {
                $data = fread($file, 2);

                if (!empty($data) && $data[0] !== "\xFF") {
                    fclose($file);
                    return false;
                }
            }
        }

        fclose($file);

        return $headerData;
    }

    /**
     * @param array $jpegHeaderData
     * @param string $newXMP
     * @return mixed
     */
    public static function putXmpText($jpegHeaderData = [], $newXMP)
    {
        if (empty($jpegHeaderData)) {
            return false;
        }

        foreach ($jpegHeaderData as &$data) {
            if (strcmp($data['SegType'], 0xE1) === 0 && mb_strpos($data['SegData'], "http://ns.adobe.com/xap/1.0/\x00") === 0) {
                $data['SegData'] = "http://ns.adobe.com/xap/1.0/\x00" . $newXMP;
                return $jpegHeaderData;
            }
        }

        $i = 0;

        while ($jpegHeaderData[$i]['SegType'] === 0xE0 || $jpegHeaderData[$i]['SegType'] === 0xE1) {
            $i++;
        }

        array_splice($jpegHeaderData, $i, 0, [
            [
                'SegType' => 0xE1,
                'SegData' => "http://ns.adobe.com/xap/1.0/\x00" . $newXMP
            ]
        ]);

        return $jpegHeaderData;
    }

    /**
     * @param string $filename
     * @return bool|string|void
     */
    public static function getJpegImageData($filename)
    {
        $file = fopen($filename, 'rb');
        $data = fread($file, 2);

        if ($data !== "\xFF\xD8") {
            fclose($file);
            return false;
        }

        $data = fread($file, 2);

        if ($data[0] !== "\xFF") {
            fclose($file);
            return false;
        }

        $compressedData = '';
        $hitCompressedImageData = false;

        while ($data[1] !== "\xD9" && !$hitCompressedImageData && !feof($file)) {
            $ordData = ord($data[1]);
            if ($ordData < 0xD0 || $ordData > 0xD7) {
                $decodedSize = unpack('nsize', fread($file, 2));
                fread($file, $decodedSize['size'] - 2);
            }

            if ($data[1] === "\xDA") {
                $hitCompressedImageData = true;

                $compressedData = '';
                do {
                    $compressedData .= fread($file, 1048576);
                } while (!feof($file));

                $eoiPos = strpos($compressedData, "\xFF\xD9");
                $compressedData = substr($compressedData, 0, $eoiPos);
            } else {
                $data = fread($file, 2);

                if ($data[0] !== "\xFF") {
                    fclose($file);
                    return false;
                }
            }
        }

        fclose($file);

        if ($hitCompressedImageData) {
            return $compressedData;
        } else {
            return false;
        }
    }

    /**
     * @param string $filename
     * @param array $jpegHeaderData
     * @return bool
     */
    public static function putJpegHeaderData($filename, $jpegHeaderData)
    {
        $compressedImageData = static::getJpegImageData($filename);

        if (empty($compressedImageData)) {
            return false;
        }

        foreach ($jpegHeaderData as $segNo => $segment) {
            if (strlen($segment['SegData']) > 0xFFFD) {
                return false;
            }
        }

        $segments = '';
        foreach ($jpegHeaderData as $segNo => $segment) {
            $segments .= sprintf("\xFF%c", $segment['SegType']);
            $segments .= pack('n', strlen($segment['SegData']) + 2);
            $segments .= $segment['SegData'];
        }

        $newFile = fopen($filename, 'wb');
        fwrite($newFile, "\xFF\xD8" . $segments . $compressedImageData . "\xFF\xD9");
        fclose($newFile);

        return true;
    }
}
