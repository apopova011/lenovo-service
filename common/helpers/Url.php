<?php

namespace common\helpers;

use Yii;
use \yii\helpers\BaseUrl;
use \yii\base\InvalidConfigException;

/**
 * Url is helper extends BaseUrl.
 *
 * @author Kostya M <iamF13@ya.ru>
 */
class Url extends BaseUrl
{
    /**
     * @return \yii\web\UrlManager
     * @throws InvalidConfigException
     */
    protected static function getUrlManager()
    {
        $app = Yii::$app;
        if ($app->has('urlManager', true)) {
            return $app->get('urlManager');
        } elseif ($app->has('frontendUrlManager')) {
            return $app->get('frontendUrlManager');
        } else {
            throw new InvalidConfigException('Url manager is not found');
        }
    }

    /**
     * @return \yii\web\UrlManager
     * @throws InvalidConfigException
     */
    protected static function getUrlManagerFrontend()
    {
        $app = Yii::$app;
        if ($app->has('frontendUrlManager')) {
            return $app->get('frontendUrlManager');
        } elseif ($app->has('urlManager', true)) {
            return $app->get('urlManager');
        } else {
            throw new InvalidConfigException('Frontend url manager is not found');
        }
    }

    /**
     * @param \yii\web\UrlManager $urlManager
     * @param string|array $route
     * @param boolean|string $scheme
     * @return string
     * @throws \yii\base\InvalidParamException
     */
    protected static function toRouteByUrlManager($urlManager, $route, $scheme = false)
    {
        // code from BaseUrl::toRoute
        $route = (array) $route;
        $route[0] = static::normalizeRoute($route[0]);

        return $scheme ?
            $urlManager->createAbsoluteUrl($route, is_string($scheme) ? $scheme : null) :
            $urlManager->createUrl($route);
    }

    /**
     * @param \yii\web\UrlManager $urlManager
     * @param boolean|string $scheme
     * @return string
     * @throws InvalidConfigException
     */
    public static function baseByUrlManager($urlManager, $scheme = false)
    {
        // code from BaseUrl::base
        $url = $urlManager->getBaseUrl();
        if ($scheme) {
            $url = $urlManager->getHostInfo() . $url;
            if (is_string($scheme) && ($pos = strpos($url, '://')) !== false) {
                $url = $scheme . substr($url, $pos);
            }
        }
        return $url;
    }

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public static function toRoute($route, $scheme = false)
    {
        return static::toRouteByUrlManager(
            static::getUrlManager(),
            $route,
            $scheme
        );
    }

    /**
     * @param string|array $route
     * @param boolean|string $scheme
     * @return string
     * @throws InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public static function toRouteFrontend($route, $scheme = false)
    {
        return static::toRouteByUrlManager(
            static::getUrlManagerFrontend(),
            $route,
            $scheme
        );
    }

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public static function base($scheme = false)
    {
        return static::baseByUrlManager(
            static::getUrlManager(),
            $scheme
        );
    }

    /**
     * @param boolean|string $scheme
     * @return string
     * @throws InvalidConfigException
     */
    public static function baseFrontend($scheme = false)
    {
        return static::baseByUrlManager(
            static::getUrlManagerFrontend(),
            $scheme
        );
    }

    /**
     * @param string $urlFull
     * @return string|boolean
     */
    public static function getSubUrlFromFull($urlFull)
    {
        $urlMedia = Yii::$app->params['urlMedia'];
        if (strpos($urlFull, $urlMedia) === 0) {
            return substr($urlFull, strlen($urlMedia));
        }

        if (strpos($urlMedia, 'http:') === 0) {
            $urlMedia = substr($urlMedia, 5);

            if (strpos($urlFull, $urlMedia) === 0) {
                return substr($urlFull, strlen($urlMedia));
            }
        }

        if (strpos($urlFull, 'http:') === 0) {
            $urlFull = substr($urlFull, 5);

            if (strpos($urlFull, $urlMedia) === 0) {
                return substr($urlFull, strlen($urlMedia));
            }
        }

        if (strpos($urlFull, '//') === 0) {
            $pos = strpos($urlFull, '/', 2);
            if ($pos === false) {
                return false;
            }
            $urlFull = substr($urlFull, $pos);
        }

        if (strpos($urlFull, $urlMedia) === 0) {
            return substr($urlFull, strlen($urlMedia));
        }

        return false;
    }

    /**
     * @param string $url
     * @param integer|boolean $width
     * @param integer|boolean $height
     * @return string
     */
    public static function thumbnail($url, $width = false, $height = false)
    {
        if (($width || $height) && strpos($url, Yii::$app->params['directoryAvatars']) !== 1) {
            $suffix = '-' . ($width ?: '0') . 'x' . ($height ?: '0');

            $pos = strrpos($url, '.');
            if ($pos === false) {
                $url .= $suffix;
            } else {
                $url = substr($url, 0, $pos) . $suffix . substr($url, $pos);
            }
        }

        return Yii::$app->params['urlMedia'] . $url;
    }

    /**
     * @param string|boolean|null $url true if check is not needed
     * @return string|boolean
     */
    public static function imageDomain($url = true)
    {
        if (empty($url)) {
            return false;
        }

        $url = Yii::$app->params['urlMedia'];
        if (strpos($url, 'http') === 0) {
            return '';
        }

        $domain = 'http:';
        if (strpos($url, '//') !== 0) {
            $domain .= '//' . Yii::$app->params['frontendDomain'];
        }

        return $domain;
    }
}
