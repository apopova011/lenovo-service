<?php

namespace common\models;

use \yii\db\ActiveQuery;

/**
 * UserQuery is ActiveQuery with user scopes.
 *
 * @method User|array|null one($db = null)
 * @method User[]|array all($db = null)
 */
class UserQuery extends ActiveQuery
{
    /**
     * @param string $column
     * @return string
     */
    public static function getAsTimestamp($column)
    {
        return 'EXTRACT(EPOCH FROM ' . $column . ' AT TIME ZONE current_setting(\'TIMEZONE\'))';
    }

    /**
     * @return $this
     */
    public function emailVerified()
    {
        $this->andWhere(['role' => User::$rolesEmailVerified]);
        return $this;
    }

    /**
     * @return UserQuery
     */
    public function emailNotVerified()
    {
        $this->andWhere(['not in', 'role', User::$rolesEmailVerified]);
        return $this;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function tokenByEmail($email)
    {
        $this
            ->select(['token' => User::COLUMN_TOKEN])
            ->andWhere(['email' => $email]);
        return $this;
    }

    /**
     * @return UserQuery
     */
    public function onlyAuthors()
    {
        $this->andWhere(['role' => User::$rolesAuthors]);
        return $this;
    }

    /**
     * @return UserQuery
     */
    public function authors()
    {
        $this->onlyAuthors()->orderBy('title');
        return $this;
    }

    /**
     * @return UserQuery
     */
    public function fromVk()
    {
        $this
            ->andWhere('vk_id IS NOT NULL')
            ->andWhere('password_hash IS NULL');
        return $this;
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @return UserQuery
     */
    public function createdBetween($startDate, $endDate)
    {
        $this->andWhere(['between', 'created', $startDate, $endDate]);
        return $this;
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @return CommentQuery
     */
    public function createdBetweenByWeek($startDate, $endDate)
    {
        $this->createdBetween($startDate, $endDate);
        $this->groupBy(['week' => 'date_trunc(\'week\', created)']);
        $this->orderBy(['date_trunc(\'week\', created)' => SORT_DESC]);
        return $this;
    }

    /**
     * @return UserQuery
     */
    public function forList()
    {
        $this->select([
            'id',
            'title',
            'slug',
            'is_author' => '"role" IN (\'' . implode('\',\'', User::$rolesAuthors) . '\')',
        ]);
        return $this;
    }

    /**
     * @param string $slug
     * @return UserQuery
     */
    public function forView($slug)
    {
        $this
            ->select([
                'id',
                'slug',
                'title',
                'avatar',
                'about',
                'created' => static::getAsTimestamp('created'),
                'title_gen',
                'birthday' => static::getAsTimestamp('birthday'),
                'city_name',
                'twitter_slug',
                'instagram_slug',
                'google_plus_slug',
                'facebook_id',
                'vk_id',
                'is_author' => '"role" IN (\'' . implode('\',\'', User::$rolesAuthors) . '\')',
            ])
            ->andWhere(['slug' => $slug]);

        return $this;
    }

    /**
     * @return UserQuery
     */
    public function authorForRss()
    {
        $this->select(['id', 'title', 'first_name', 'last_name']);
        return $this;
    }
}
