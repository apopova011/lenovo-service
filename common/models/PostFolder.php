<?php

namespace common\models;

use \yii\db\ActiveRecord;

/**
 * This is the model class for table "post_folder".
 *
 * @property integer $post_id
 * @property integer $folder_id
 *
 * @property Folder $folder
 * @property Post $post
 */
class PostFolder extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_folder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'folder_id'], 'required'],
            [['post_id', 'folder_id'], 'integer'],
            [['folder_id'], 'exist', 'skipOnError' => true, 'targetClass' => Folder::className(), 'targetAttribute' => ['folder_id' => 'id']],
            [['post_id'],   'exist', 'skipOnError' => true, 'targetClass' =>   Post::className(), 'targetAttribute' => ['post_id'   => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'post_id' => 'ID материала',
            'folder_id' => 'ID директории',
        ];
    }

    /**
     * @return FolderQuery
     */
    public function getFolder()
    {
        return $this->hasOne(Folder::className(), ['id' => 'folder_id']);
    }

    /**
     * @return PostQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }

    /**
     * @inheritdoc
     * @return PostFolderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PostFolderQuery(get_called_class());
    }
}
