<?php

namespace common\models;

use Yii;
use \yii\db\ActiveRecord;
use \common\validators\IpValidator;

/**
 * This is the model class for table "comment_vote".
 *
 * @property integer $id
 * @property integer $value
 * @property integer $comment_id
 * @property string $user_session
 * @property integer|null $user_id
 * @property string|null $user_ip
 * @property string|null $user_agent
 *
 * @property Comment $comment
 * @property User $user
 */
class CommentVote extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment_vote';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value', 'comment_id', 'user_session'], 'required'],
            [['value', 'comment_id', 'user_id'], 'integer'],
            [['user_session'], 'string', 'max' => 32],
            [['user_agent'], 'string', 'max' => 255],
            [['user_ip'], IpValidator::className()],
            [['user_id', 'user_ip', 'user_agent'], 'default', 'value' => null],
            [['user_id'],    'exist', 'targetClass' => 'common\models\User',    'targetAttribute' => 'id'],
            [['comment_id'], 'exist', 'targetClass' => 'common\models\Comment', 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Значение',
            'comment_id' => 'Комментарий',
            'user_session' => 'Сессия',
            'user_id' => 'Пользователь',
            'user_ip' => 'IP',
            'user_agent' => 'User-Agent',
        ];
    }

    /**
     * @return CommentQuery
     */
    public function getComment()
    {
        return $this->hasOne(Comment::className(), ['id' => 'comment_id']);
    }

    /**
     * @return UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return int
     */
    public static function getVoteValue()
    {
        $user = Yii::$app->getUser();
        if ($user->getIsGuest()) {
            return 1;
        }
        if ($user->can('commentMaxVoteValue')) {
            return 3;
        }
        return 2;
    }
}
