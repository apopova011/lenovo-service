<?php

namespace common\models;

use \yii\db\ActiveRecord;
use \common\validators\DatePgValidator;

/**
 * This is the model class for table "photo".
 *
 * @property integer $id
 * @property string $file
 * @property string|null $content_type
 * @property string|null $exif_updated
 * @property integer $post_id
 * @property string|null $post_published
 * @property integer $author_id
 * @property string $author_title
 * @property string $author_email
 * @property string|null $post_content_img_alt
 *
 * @property Post $post
 * @property User $author
 */
class Photo extends ActiveRecord
{
    /**
     * User friendly russian label.
     */
    const LABEL_SINGULAR_ru_RU = 'Фото';

    /**
     * User friendly russian label.
     */
    const LABEL_PLURAL_ru_RU = 'Фото';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file', 'post_id', 'author_id', 'author_title', 'author_email'], 'required'],
            [['file', 'author_email', 'post_content_img_alt'], 'string', 'max' => 255],
            [['author_title'],                                 'string', 'max' => 95],
            [['content_type'],                                 'string', 'max' => 15],
            [['post_id', 'author_id'], 'integer'],
            [['exif_updated', 'post_published'], DatePgValidator::className()],
            [['exif_updated', 'content_type', 'post_content_img_alt'], 'default', 'value' => null],
            [['post_id'],   'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $auto = parent::scenarios();
        return [
            'administrator' => $auto[static::SCENARIO_DEFAULT],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file' => 'Файл',
            'content_type' => 'Content Type',
            'exif_updated' => 'Exif обновлен',
            'post_id' => 'Материал',
            'post_published' => 'Материал опубликован',
            'author_id' => 'Автор',
            'author_title' => 'Подпись автора',
            'author_email' => 'Email автора',
            'post_content_img_alt' => 'Alt фото',
        ];
    }

    /**
     * @return PostQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }

    /**
     * @return UserQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @inheritdoc
     * @return PhotoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PhotoQuery(get_called_class());
    }
}
