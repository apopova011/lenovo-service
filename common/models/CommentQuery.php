<?php

namespace common\models;

use Yii;
use \yii\db\ActiveQuery;
use \common\behaviors\TreeQueryTrait;

/**
 * CommentQuery is ActiveQuery with comment scopes.
 *
 * @method Comment|array|null one($db = null)
 * @method Comment[]|array all($db = null)
 */
class CommentQuery extends ActiveQuery
{
    use TreeQueryTrait;

    /**
     * @param string $column
     * @return string
     */
    protected static function getAsTimestamp($column)
    {
        return 'EXTRACT(EPOCH FROM ' . $column . ' AT TIME ZONE current_setting(\'TIMEZONE\'))';
    }

    /**
     * @return CommentQuery
     */
    public function excellent()
    {
        $this->andWhere(['status' => ['excellent_by_vote', 'excellent_by_moderator']]);
        return $this;
    }

    /**
     * @return CommentQuery
     */
    public function showed()
    {
        $this->andWhere(['status' => Comment::$showedStatuses]);
        return $this;
    }

    /**
     * @return CommentQuery
     */
    public function banned()
    {
        $this->andWhere(['status' => ['banned_by_vote', 'banned_by_moderator']]);
        return $this;
    }

    /**
     * @param int|string $authorId
     * @return CommentQuery
     */
    public function byAuthor($authorId)
    {
        $this->andWhere(['author_id' => $authorId]);
        return $this;
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @return CommentQuery
     */
    public function createdBetween($startDate, $endDate)
    {
        $this->andWhere(['between', 'created', $startDate, $endDate]);
        return $this;
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @return CommentQuery
     */
    public function createdBetweenByWeek($startDate, $endDate)
    {
        $this->createdBetween($startDate, $endDate);
        $this->groupBy(['week' => 'date_trunc(\'week\', created)']);
        $this->orderBy(['date_trunc(\'week\', created)' => SORT_DESC]);
        return $this;
    }

    /**
     * @return CommentQuery
     */
    public function withPost()
    {
        $this->with([
            'post' => function ($query) {
                /* @var PostQuery $query */
                $query->forList();
            }
        ]);

        return $this;
    }

    /**
     * @param int $userId
     * @return CommentQuery
     */
    public function usefulForUser($userId)
    {
        $this->andWhere([
            'or',
            'path <@ (SELECT array_agg(path) FROM "comment" WHERE author_id = ' . $userId . ')',
            'post_id IN (SELECT id FROM post WHERE author_id = ' . $userId . ')',
        ]);

        return $this;
    }

    /**
     * @return CommentQuery
     */
    public function usefulForMe()
    {
        return $this->usefulForUser(Yii::$app->getUser()->id);
    }

    /**
     * @param string|int $postId
     * @return CommentQuery
     */
    public function forPost($postId)
    {
        $commentTable = Comment::tableName();

        $this->select([
            'id',
            'content',
            'status',
            'created' => static::getAsTimestamp('created'),
            'level' => 'nlevel(path)',
            'author_id',
            'user_title',
            'user_avatar',
            'user_session',
            'path',
            'votes_value' => 'COALESCE(votes_value, 0)',
        ])->with([
            'author' => function ($query) {
                /* @var UserQuery $query */
                $query
                    ->forList()
                    ->addSelect('comment_visible_count');
            },
        ])->orderBy([
            'ltree2text(subpath(path, 0, 1))::INT' => SORT_ASC,
            'path' => SORT_ASC,
        ])->andWhere(<<<SQL
path <@ ARRAY (
    SELECT path
    FROM {$commentTable}
    WHERE
        (post_id = :post_id)
        AND nlevel(path) = 1
    ORDER BY id DESC
    LIMIT 66
)
SQL
            , [':post_id' => $postId]
        );

        return $this;
    }

    /**
     * @param integer $authorId
     * @return CommentQuery
     */
    public function forUser($authorId)
    {
        $this
            ->select([
                'id',
                'content',
                'created' => static::getAsTimestamp('created'),
                'post_id',
            ])
            ->with([
                'post' => function ($query) {
                    /* @var PostQuery $query */
                    $query->forList();
                }
            ])
            ->andWhere(['author_id' => $authorId])
            ->andWhere(['status' => Comment::$showedStatuses])
            ->orderBy(['created' => SORT_DESC]);

        return $this;
    }
}
