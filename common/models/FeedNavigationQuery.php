<?php

namespace common\models;

use \yii\db\ActiveQuery;

/**
 * FeedNavigationQuery is ActiveQuery with feed navigation scopes.
 *
 * @method FeedNavigation|array|null one($db = null)
 * @method FeedNavigation[]|array all($db = null)
 */
class FeedNavigationQuery extends ActiveQuery
{
    /**
     * @param string $resourceType
     * @param integer|null $resourceId
     * @return FeedNavigationQuery $this
     */
    public function forPage($resourceType, $resourceId = null)
    {
        $this
            ->select([
                'url',
                'label' => 'name',
            ])
            ->andWhere(['page_resource_type' => $resourceType])
            ->orderBy(['order' => SORT_ASC]);

        if (!empty($resourceId)) {
            $this->andWhere(['page_resource_id' => $resourceId]);
        }

        return $this;
    }
}
