<?php

namespace common\models;

use \yii\db\ActiveRecord;

/**
 * This is the model class for table "link".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 */
class Link extends ActiveRecord
{
    /**
     * User friendly russian label.
     */
    const LABEL_SINGULAR_ru_RU = 'Ссылка';

    /**
     * User friendly russian label.
     */
    const LABEL_PLURAL_ru_RU = 'Ссылки';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'filter', 'filter' => 'trim'],
            [['name', 'url'], 'required'],
            [['name'], 'string', 'max' =>  31],
            [['url'],  'string', 'max' => 255],
            [['url'], 'url', 'enableIDN' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $auto = parent::scenarios();
        return [
            'administrator' => $auto[static::SCENARIO_DEFAULT],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Текст',
            'url' => 'Адрес',
        ];
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
}
