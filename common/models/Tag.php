<?php

namespace common\models;

use Yii;
use \yii\db\ActiveRecord;
use \common\helpers\File;
use \common\helpers\Inflector;
use \common\helpers\Url;
use \common\validators\DatePgValidator;
use \common\validators\SlugValidator;

/**
 * This is the model class for table "tag".
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string|null $title
 * @property string|null $title_seo
 * @property string|null $description
 * @property string $updated
 * @property integer|null $group_id
 * @property integer $post_count
 *
 * @property Group $group
 * @property PostTag $postTag
 * @property Post[] $posts
 */
class Tag extends ActiveRecord
{
    /**
     * User friendly russian label.
     */
    const LABEL_SINGULAR_ru_RU = 'Тэг';

    /**
     * User friendly russian label.
     */
    const LABEL_PLURAL_ru_RU = 'Тэги';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slug', 'name', 'title', 'title_seo', 'description'], 'filter', 'filter' => 'trim'],
            [['slug', 'name'], 'required'],
            [['slug'],        'string', 'max' =>   31, 'min' => 2],
            [['name'],        'string', 'max' =>   63],
            [['title'],       'string', 'max' =>   95],
            [['title_seo'],   'string', 'max' =>  255],
            [['description'], 'string', 'max' => 1023],
            [['group_id', 'post_count'], 'integer'],
            [['slug'],      SlugValidator::className()],
            [['updated'], DatePgValidator::className()],
            [['slug'], 'unique'],
            [['name'], 'unique'],
            [['group_id'], 'exist', 'targetClass' => 'common\models\Group', 'targetAttribute' => 'id'],
            [['post_count'], 'default', 'value' => 0],
            [['title', 'title_seo', 'description', 'group_id'], 'default', 'value' => null],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $auto = parent::scenarios();
        return [
            'administrator' => $auto[static::SCENARIO_DEFAULT],
            'administrator_auto' => ['slug', 'name'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Для url',
            'name' => 'Текст ссылки',
            'title' => 'Заголовок',
            'title_seo' => 'Подпись браузера',
            'description' => 'Описание',
            'updated' => 'Обновлён',
            'group_id' => 'Группа',
            'post_count' => 'Материалов',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostTag()
    {
        return $this->hasOne(PostTag::className(), ['tag_id' => 'id']);
    }

    /**
     * @return PostQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['id' => 'post_id'])->viaTable('post_tag', ['tag_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return TagQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TagQuery(get_called_class());
    }

    /**
     * @param array|Tag $tag
     * @param array|null $postLast
     * @return array
     */
    public static function getUrlParams($tag, $postLast = null)
    {
        $route = ['tag/view', 'slug' => $tag['slug']];
        if (!empty($postLast)) {
            $route['year']   = date('Y', $postLast['published']);
            $route['month']  = date('m', $postLast['published']);
            $route['day']    = date('d', $postLast['published']);
            $route['hour']   = date('H', $postLast['published']);
            $route['minute'] = date('i', $postLast['published']);
            $route['second'] = date('s', $postLast['published']);
        }

        return $route;
    }

    /**
     * @param boolean $scheme
     * @param array|null $postLast
     * @return string
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     */
    public function getUrl($scheme = false, $postLast = null)
    {
        return Url::toRouteFrontend(static::getUrlParams($this, $postLast), $scheme);
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        $navigation = FeedNavigation::find()->forPage('tag', $this->id)->asArray()->all();

        if (!empty($navigation)) {
            return FeedNavigation::activeByUrl($navigation);
        }

        return [];
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($changedAttributes) {
            File::deleteSitemapTag();
        }
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function afterDelete()
    {
        parent::afterDelete();

        File::deleteSitemapTag();
    }

    /**
     * Select ids. Insert if tag do not exist.
     * @param array $names
     * @return array
     * @throws \yii\db\Exception
     */
    public static function selectOrInsert($names)
    {
        $namesClear = [];
        $ids = [];

        foreach ($names as $tag) {
            $namesClear[static::textAlphanumeric($tag)] = $tag;
        }

        foreach (
            static::find()
                ->select(['id', 'name' => 'text_alphanumeric("name")'])
                ->andWhere(['text_alphanumeric("name")' => array_keys($namesClear)])
                ->asArray()
                ->all() as $tag
        ) {
            $ids[] = $tag['id'];

            unset($namesClear[$tag['name']]);
        }

        foreach ($namesClear as $tag) {
            $slug = mb_substr(Inflector::slug($tag), 0, 31, Yii::$app->charset);

            $id = static::find()->select('id')->andWhere(['slug' => $slug])->scalar();
            if (!empty($id)) {
                $ids[] = $id;
                continue;
            }

            $ar = new Tag();
            $ar->setScenario('administrator_auto');
            $ar->name = $tag;
            $ar->slug = $slug;
            if ($ar->save()) {
                $ids[] = $ar->id;
            } else {
                $message = $ar->getFirstErrors();
                if (empty($message)) {
                    $message = '.';
                } else {
                    $message = ': ' . implode(' ', array_values($message));
                }

                Yii::warning('Tag «' . $tag . '» is not saved' . $message, 'TagInsertAutoFromText');
            }
        }

        return array_unique($ids, SORT_NUMERIC);
    }

    /**
     * @param string $text
     * @return string
     * @throws \yii\db\Exception
     */
    public static function textAlphanumeric($text)
    {
        return Yii::$app->getDb()->createCommand('SELECT text_alphanumeric(\'' . $text . '\');')->queryScalar();
    }
}
