<?php

namespace common\models;

use Yii;
use \yii\db\ActiveQuery;

/**
 * PostQuery is ActiveQuery with post scopes.
 *
 * @method Post|array|null one($db = null)
 * @method Post[]|array all($db = null)
 */
class PostQuery extends ActiveQuery
{
    /**
     * @param string $column
     * @return string
     */
    public static function getAsTimestamp($column)
    {
        return 'EXTRACT(EPOCH FROM ' . $column . ' AT TIME ZONE current_setting(\'TIMEZONE\'))';
    }

    /**
     * @return PostQuery
     */
    public function isPublished()
    {
        $this->andWhere('published IS NOT NULL')->andWhere('published <= CURRENT_TIMESTAMP');
        return $this;
    }

    /**
     * @return PostQuery
     */
    public function notPublishedOrInFuture()
    {
        $this->andWhere(['or', ['published' => null], 'published > CURRENT_TIMESTAMP']);
        return $this;
    }

    /**
     * @param integer $days
     * @return PostQuery
     */
    public function publishedInLastDay($days)
    {
        $this->andWhere('"published" >= NOW() - interval \'' . $days . ' days\'');
        return $this;
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @return PostQuery
     */
    public function publishedBetween($startDate, $endDate)
    {
        $this->isPublished();
        $this->andWhere(['between', 'published', $startDate, $endDate]);
        return $this;
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @return PostQuery
     */
    public function publishedBetweenByWeek($startDate, $endDate)
    {
        $this->publishedBetween($startDate, $endDate);
        $this->groupBy(['week' => 'date_trunc(\'week\', published)']);
        $this->orderBy(['date_trunc(\'week\', published)' => SORT_DESC]);

        return $this;
    }

    /**
     * @return PostQuery
     */
    public function inCommonFeed()
    {
        $this->andWhere(['is_in_common_feed' => true]);
        return $this;
    }

    /**
     * @param boolean $withFuture
     * @return bool|string
     */
    public function getLastPublished($withFuture = false)
    {
        if ($withFuture) {
            $this->andWhere('published IS NOT NULL');
        } else {
            $this->isPublished();
        }

        return $this
            ->select([
                'published' => static::getAsTimestamp('published'),
            ])
            ->orderBy([Post::tableName() . '.published' => SORT_DESC])
            ->limit(1)
            ->scalar();
    }

    /**
     * @return PostQuery
     */
    public function notUrlReplaced()
    {
        $this->andWhere(['NOT IN', 'id', array_keys(Yii::$app->params['postsUrlReplace'])]);
        return $this;
    }

    /**
     * @param int|string $folderId
     * @return PostQuery
     */
    public function byFolder($folderId)
    {
        $this->andWhere(['id' => PostFolder::find()->select('post_id')->andWhere(['folder_id' => $folderId])]);
        return $this;
    }

    /**
     * @param int|string $tagId
     * @return PostQuery
     */
    public function byTag($tagId)
    {
        $this->andWhere(['id' => PostTag::find()->select('post_id')->andWhere(['tag_id' => $tagId])]);
        return $this;
    }

    /**
     * @param int|string $authorId
     * @return PostQuery
     */
    public function byAuthor($authorId)
    {
        $this->andWhere(['author_id' => $authorId]);
        return $this;
    }

    /**
     * @return PostQuery
     */
    public function forWeeksList()
    {
        $this
            ->select([
                'title'     => 'to_char(date_trunc(\'week\', published), \'YY.MM.W\')',
                'timestamp' => 'extract(epoch from date_trunc(\'week\', published) at time zone current_setting(\'TIMEZONE\'))',
            ])
            ->andWhere('published >= \'2008-09-08\'')
            ->andWhere(
                'published < :monday_plus_2_weeks',
                [':monday_plus_2_weeks' => date('Y-m-d H:i:s', 1209600 + strtotime('Monday this week 00:00:00'))]
            )
            ->groupBy(['date_trunc(\'week\', published)'])
            ->orderBy(['date_trunc(\'week\', published)' => SORT_DESC]);

        return $this;
    }

    /**
     * @return PostQuery
     */
    public function forList()
    {
        $postTable = Post::tableName();

        $this
            ->select([
                $postTable . '.id',
                $postTable . '.slug',
                $postTable . '.name',
                $postTable . '.folder_id_main',
                $postTable . '.type',
            ])
            ->with([
                'folderMain' => function ($query) {
                    /* @var FolderQuery $query */
                    $query->forPostList();
                },
            ])
            ->isPublished()
            ->orderBy([$postTable . '.published' => SORT_DESC]);

        return $this;
    }

    /**
     * @return PostQuery
     */
    public function forListWithIconAndFolders()
    {
        $this->forList();
        $this->select[] = 'icon';
        $this->select[] = 'title_seo';
        $this->with['folders'] = function ($query) {
            /* @var FolderQuery $query */
            $query->forPostList();
        };
        return $this;
    }

    /**
     * @param array $datetimeFrom
     * @return PostQuery
     */
    public function forFeed(array $datetimeFrom = [])
    {
        $this->forListWithIconAndFolders();
        $this->addSelect([
            'description',
            'is_contain_video',
            'published' => static::getAsTimestamp('published'),
        ]);

        if (array_key_exists('year', $datetimeFrom)) {
            $datetimeFromStr = $datetimeFrom['year'] . '-' .
                (array_key_exists('month',  $datetimeFrom) ? $datetimeFrom['month']  : '01') . '-' .
                (array_key_exists('day',    $datetimeFrom) ? $datetimeFrom['day']    : '01') . ' ' .
                (array_key_exists('hour',   $datetimeFrom) ? $datetimeFrom['hour']   : '00') . ':' .
                (array_key_exists('minute', $datetimeFrom) ? $datetimeFrom['minute'] : '00') . ':' .
                (array_key_exists('second', $datetimeFrom) ? $datetimeFrom['second'] : '00');

            $this->andWhere('published <= :published_from', [':published_from' => $datetimeFromStr]);
        }

        $this->limit(13);

        return $this;
    }

    /**
     * @return PostQuery
     */
    public function forView()
    {
        $this
            ->select([
                'id',
                'slug',
                'name',
                'title_seo',
                'description',
                'icon',
                'content',
                'published' => static::getAsTimestamp('published'),
                'author_id',
                'folder_id_main',
                'is_allow_comments',
                'is_allow_footer',
                'extract_iso8601(video_duration) as video_duration',
            ])
            ->with([
                'author' => function ($query) {
                    /* @var UserQuery $query */
                    $query->forList();
                },
            ])
            ->andWhere('published IS NOT NULL');

        return $this;
    }

    /**
     * @param Post $post
     * @return PostQuery
     */
    public function similar($post)
    {
        $this->forList();
        $this->addSelect(['published' => static::getAsTimestamp('published')]);
        $this->andWhere(['!=', 'id', $post['id']]);
        $this->andWhere([ '>', 'published', date('Y-m-d', mktime(0, 0, 0, date('m') - 4, date('d'), date('Y')))]);

        $query = empty($post['title_seo']) ? $post['name'] : $post['title_seo'];
        if ($post->hasProperty('tags')) {
            foreach ($post['tags'] as $tag) {
                $query .= ' ' . (empty($tag['title']) ? $tag['name'] : $tag['title']);
            }
        }
        if ($post->hasProperty('folders')) {
            foreach ($post['folders'] as $folder) {
                $query .= ' ' . (empty($folder['title']) ? $folder['name'] : $folder['title']);
            }
        }

        $query = preg_replace('/[\s]+/u', '|', trim(preg_replace('/[^\w\s]/u', ' ', $query)));
        $this->orderBy(['ts_rank_cd(ARRAY[0.5, 0.7, 0.8, 1.0], similar_tsv, to_tsquery(\'text_search_config\', \'' . $query . '\'), 4)' => SORT_DESC]);

        return $this;
    }

    /**
     * @return PostQuery
     */
    public function forSitemap()
    {
        $this
            ->select([
                'id',
                'slug',
                'updated' => static::getAsTimestamp('updated'),
                'folder_id_main',
                'type',
            ])
            ->with([
                'folderMain' => function ($query) {
                    /* @var FolderQuery $query */
                    $query->select(['id', 'slug']);
                },
            ])
            ->isPublished()
            ->notUrlReplaced()
            ->orderBy('id');

        return $this;
    }

    /**
     * @return PostQuery
     */
    public function forSitemapTimeline()
    {
        $maxPublished = PostForFeed::find()
            ->select('published')
            ->isPublished()
            ->inCommonFeed()
            ->notUrlReplaced()
            ->orderBy(['published' => SORT_DESC])
            ->limit(1);

        $this
            ->forSitemap()
            ->inCommonFeed()
            ->addSelect(['published' => static::getAsTimestamp('published')])
            ->andWhere(['!=', 'published', $maxPublished]);

        return $this;
    }

    /**
     * @return PostQuery
     */
    public function forNewsSitemap()
    {
        $now = time();

        $this
            ->select([
                'id',
                'slug',
                'name',
                'tags_name',
                'updated' => static::getAsTimestamp('updated'),
                'folder_id_main',
            ])
            ->with([
                'folderMain' => function ($query) {
                    /* @var FolderQuery $query */
                    $query->select(['id', 'slug']);
                },
                'folders' => function ($query) {
                    /* @var FolderQuery $query */
                    $query->select(['id', 'name']);
                },
            ])
            ->inCommonFeed()
            ->notUrlReplaced()
            ->andWhere('published IS NOT NULL')
            ->andWhere([
                'between',
                'published',
                date('Y-m-d H:i:s', $now - 172800), // -(60 * 60 * 24 * 2)
                date('Y-m-d H:i:s', $now +    300) // +(60 * 5)
            ])
            ->orderBy(['published' => SORT_DESC]);

        return $this;
    }

    /**
     * @return PostQuery
     */
    public function forRss()
    {
        $this
            ->forList()
            ->addSelect([
                'description',
                'content',
                'author_id',
                'icon',
                'published' => static::getAsTimestamp('published'),
                'type',
            ])
            ->inCommonFeed()
            ->with([
                'author' => function ($query) {
                    /* @var UserQuery $query */
                    $query->authorForRss();
                },
            ]);

        return $this;
    }

    /**
     * @param integer $days
     * @param integer $limit
     * @return PostQuery
     */
    public function popularInLastDay($days, $limit = 10)
    {
        $stat = StatisticPost::find()
            ->select(['post_id', 'show_count' => 'SUM("show_count")'])
            ->andWhere('"date" >= NOW() - interval \'' . $days . ' days\'')
            ->groupBy('post_id')
            ->orderBy(['SUM("show_count")' => SORT_DESC]);

        $this
            ->addSelect([
                'id',
                'slug',
                'name',
                'author_id',
                'folder_id_main',
                'show_count' => 'popular.show_count'
            ])
            ->with([
                'folderMain' => function ($query) {
                    /* @var FolderQuery $query */
                    $query->forPostList();
                },
            ])
            ->innerJoin(['popular' => $stat], Post::tableName() . '.id = popular.post_id')
            ->limit($limit)
            ->orderBy(['show_count' => SORT_DESC]);

        return $this;
    }

    /**
     * @return PostQuery
     */
    public function notInteresting()
    {
        $postTable = Post::tableName();
        $statisticPostTable = StatisticPost::tableName();

        $this
            ->forList()
            ->addSelect([
                'show_count' => 'SUM(COALESCE(' . $statisticPostTable . '.show_count, 0))',
                'period' => 'date_trunc(\'second\', NOW() - ' . $postTable . '.published)',
                'author_id'
            ])
            ->joinWith('statisticPost', false)
            ->andWhere($postTable . '.published BETWEEN date_trunc(\'day\', NOW() - interval \'6 days\') AND (NOW() - interval \'12 hours\')')
            ->groupBy($postTable . '.id')
            ->orderBy('SUM(COALESCE(' . $statisticPostTable . '.show_count, 0)) / EXTRACT(EPOCH FROM (NOW() - ' . $postTable . '.published))');

        return $this;
    }
}
