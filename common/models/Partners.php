<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "partners".
 * @package common\models
 *
 * @property int $id
 * @property string $country
 * @property string $region
 * @property string $city
 * @property string $address
 * @property string $coords
 * @property string $name
 * @property string|null $site
 * @property string|null $description
 * @property string|null $phones
 * @property boolean $act
 */
class Partners extends ActiveRecord
{
    /**
     * User friendly russian label.
     */
    public const LABEL_SINGULAR_ru_RU = 'Сервисный центр';

    /**
     * User friendly russian label.
     */
    public const LABEL_PLURAL_ru_RU = 'Сервисные центры';

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'partners';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['country', 'region', 'city', 'address', 'name', 'act'], 'required'],
            [['country', 'region', 'city', 'address', 'phones', 'name', 'site', 'description', 'coords'], 'string'],

            [['country', 'region', 'city', 'address', 'name'], 'filter', 'filter' => 'trim'],

            [['act'], 'boolean'],
            [['coords'], 'default', 'value' => '(0, 0)'],
            [['act'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        $auto = parent::scenarios();

        return [
            'administrator' => $auto[static::SCENARIO_DEFAULT],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'country' => 'Страна',
            'region' => 'Регион',
            'city' => 'Город',
            'address' => 'Адрес',
            'phones' => 'Телефоны',
            'coords' => 'Координаты',
            'name' => 'Название',
            'site' => 'Адрес сайта',
            'description' => 'Дополнительная информация',
            'act' => 'Скрыть',
        ];
    }

    /**
     * @inheritdoc
     * @return PartnersQuery the active query used by this AR class.
     */
    public static function find(): PartnersQuery
    {
        return new PartnersQuery(get_called_class());
    }

    /**
     * @return array|false
     */
    public static function dictionaryCountryRegions()
    {
        $addresses = static::find()
            ->andWhere(['act' => false])
            ->orderBy(['country' => SORT_ASC, 'region' => SORT_ASC])
            ->all();

        $result = [];
        $rusNoCapitals = [];
        $noRussia = [];

        foreach ($addresses as $address) {
            if ($address->country === 'Россия') {
                if ($address->region === 'г Москва' || $address->region === 'г Санкт-Петербург') {
                    $result[$address->country][] = $address->region;
                } else {
                    $rusNoCapitals[] = $address->region;
                }
            }

            if ($address->country !== 'Россия') {
                $noRussia[$address->country][] = $address->region;
            }
        }

        $result['Россия'] = array_merge($result['Россия'], $rusNoCapitals);
        $result = array_merge($result, $noRussia);

        return $result;
    }
}
