<?php

namespace common\models;

use Yii;
use \yii\base\Event;
use \yii\db\ActiveRecord;
use \yii\db\AfterSaveEvent;
use \common\helpers\File;

/**
 * FolderTrait provides methods for work with model's folders.
 *
 * @property array $folderIds
 *
 * @property PostFolder[] $postFolder
 * @property Folder[]     $folders
 */
trait FolderTrait
{
    /**
     * @var array
     */
    protected $_folderIds;

    /**
     * @var array
     */
    protected $oldFolderIds;

    /**
     * @var boolean
     */
    protected $foldersChanged = false;

    /**
     * Attaches an event handlers.
     */
    public function attachFolderEvents()
    {
        $this->on(ActiveRecord::EVENT_AFTER_INSERT,  [$this, 'afterInsertOrUpdateFolderOwner']);
        $this->on(ActiveRecord::EVENT_AFTER_UPDATE,  [$this, 'afterInsertOrUpdateFolderOwner']);
        $this->on(ActiveRecord::EVENT_BEFORE_DELETE, [$this, 'beforeDeleteFolderOwner']);
        $this->on(ActiveRecord::EVENT_AFTER_DELETE,  [$this, 'afterDeleteFolderOwner']);
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules[] = [['folderIds'], 'safe'];

        return $rules;
    }

    /**
     * @return PostFolderQuery
     */
    public function getPostFolder()
    {
        return $this->hasMany(PostFolder::className(), ['post_id' => 'id']);
    }

    /**
     * @return FolderQuery
     */
    public function getFolders()
    {
        return $this->hasMany(Folder::className(), ['id' => 'folder_id'])->viaTable('post_folder', ['post_id' => 'id']);
    }

    /**
     * Getter for _folderIds
     * @return array|null
     */
    public function getFolderIds()
    {
        if ($this->_folderIds === null && !$this->getIsNewRecord()) {
            $this->_folderIds = $this->getFolders()->select('id')->column();
        }

        return $this->_folderIds;
    }

    /**
     * Setter for _folderIds
     * @param array|null $value
     */
    public function setFolderIds($value)
    {
        if (!is_array($value)) {
            $value = [];
        }

        array_walk($value, function (&$value) {
            $value = (int)$value;
        });

        $this->_folderIds = $value;
    }

    /**
     * @param array|Post $post
     * @param bool $byType
     * @return array
     */
    public static function getUrlParams($post, $byType = false)
    {
        $postsUrlReplace = Yii::$app->params['postsUrlReplace'];
        if (array_key_exists($post['id'], $postsUrlReplace)) {
            $params = $postsUrlReplace[$post['id']];

            if ($params[0] === 'post/view-by-slug') {
                $params['slug'] = $post['slug'];
            }
        } else if ($post['folderMain']) {
            $params = [
                'post/view',
                'folder_slug' => $post['folderMain']['slug'],
                'post_slug'   => $post['slug'],
                'post_id'     => $post['id'],
            ];
        } else {
            $params = parent::getUrlParams($post, $byType);
        }

        return $params;
    }

    /**
     * @param AfterSaveEvent $event
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    protected function afterInsertOrUpdateFolderOwner($event)
    {
        $oldFolderIds = PostFolder::find()
            ->select('folder_id')
            ->andWhere(['post_id' => $this->id])
            ->column();

        $this->foldersChanged = $this->_folderIds !== $oldFolderIds;

        if ($this->foldersChanged) {
            $this->updateRelation('Folder', $oldFolderIds, $this->_folderIds);
        }

        $changedAttributes = $event->changedAttributes;
        if (!$changedAttributes) {
            // нет изменений
            return;
        }

        $now = time();
        $isHiddenNew = empty($this->published);
        if (!$isHiddenNew) {
            $timestampNew = strtotime($this->published);
            if ($timestampNew > $now) {
                $isHiddenNew = true;
            }
        }
        $isHiddenOld = empty($changedAttributes['published']);
        if (!$isHiddenOld) {
            $timestampOld = strtotime($changedAttributes['published']);
            if ($timestampOld > $now) {
                $isHiddenOld = true;
            }
        }
        if (($isHiddenNew && $isHiddenOld) || ($this->getIsNewRecord() && $isHiddenNew)) {
            // сохраняем скрытый
            return;
        }

        if ($isHiddenNew && !$isHiddenOld) {
            static::updateUpdatedOnRelationByNow('Folder', $oldFolderIds);
        } else if (!$isHiddenNew && $isHiddenOld) {
            static::updateUpdatedOnRelationByNow('Folder', $this->_folderIds);
        } else {
            static::updateUpdatedOnRelationByNow('Folder', array_unique(array_merge($this->_folderIds, $oldFolderIds)));
        }

        File::deleteSitemapFolder();
    }

    /**
     * @param Event $event
     */
    protected function beforeDeleteFolderOwner($event)
    {
        $this->oldFolderIds = PostFolder::find()
            ->select('folder_id')
            ->andWhere(['post_id' => $this->id])
            ->column();
    }

    /**
     * @param Event $event
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    protected function afterDeleteFolderOwner($event)
    {
        if (empty($this->published) || strtotime($this->published) > time()) {
            // невидимый или отложенная публикация
            return;
        }

        static::updateUpdatedOnRelationByNow('Folder', $this->oldFolderIds);
        File::deleteSitemapFolder();
    }
}
