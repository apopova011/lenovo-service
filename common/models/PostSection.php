<?php

namespace common\models;

class PostSection extends Post
{
    /**
     * User friendly russian label.
     */
    const LABEL_SINGULAR_ru_RU = 'Текст';

    /**
     * User friendly russian label.
     */
    const LABEL_PLURAL_ru_RU = 'Тексты';

    /**
     * Type of the post.
     */
    const POST_TYPE = 'section';

    /**
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->type = static::POST_TYPE;
        $this->is_allow_footer = false;
        $this->is_allow_comments = false;
    }

    /**
     * @inheritdoc
     * @return PostQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new PostQuery(get_called_class());
        return $query->andWhere(['type' => static::POST_TYPE]);
    }
}