<?php

namespace common\models;

use Yii;
use \yii\db\ActiveRecord;
use \common\behaviors\TreeBehavior;
use \common\behaviors\UploadBehavior;
use \common\helpers\File;
use \common\helpers\Url;
use \common\validators\DatePgValidator;
use \common\validators\SlugValidator;

/**
 * This is the model class for table "folder".
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 * @property string|null $title
 * @property string|null $title_seo
 * @property string $description
 * @property string|null $icon
 * @property string $updated
 * @property string $path
 * @property integer|null $group_id
 * @property integer $post_count
 *
 * @property string $UploadFile
 * @property string $UploadExternalUrl
 * @property string $UploadUrl
 * @property string $PathParent
 *
 * @property Folder $parent
 * @property Group $group
 * @property PostFolder $postFolder
 * @property Post[] $posts
 *
 * @method string getUploadUrl()
 */
class Folder extends ActiveRecord
{
    /**
     * User friendly russian label.
     */
    const LABEL_SINGULAR_ru_RU = 'Директория';

    /**
     * User friendly russian label.
     */
    const LABEL_PLURAL_ru_RU = 'Директории';

    /**
     * Cache key for cache by id.
     */
    const CACHE_KEY_BY_ID = 'folder_by_id';

    /**
     * @var array
     */
    protected static $cachedById = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'folder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [['slug', 'name', 'title', 'title_seo', 'description'], 'filter', 'filter' => 'trim'],
            [['slug', 'name', 'description'], 'required'],
            [['slug', 'name'],      'string', 'max' => 31, 'min' => 2],
            [['title'],             'string', 'max' => 95],
            [['title_seo', 'icon'], 'string', 'max' => 255],
            [['description'],       'string', 'max' => 1023],
            [['group_id', 'post_count'], 'integer'],
            [['slug'],      SlugValidator::className()],
            [['updated'], DatePgValidator::className()],
            [['path'], 'unique'],
            [['group_id'], 'exist', 'targetClass' => 'common\models\Group', 'targetAttribute' => 'id'],
            [['post_count'], 'default', 'value' => 0],
            [['title', 'title_seo', 'icon', 'group_id'], 'default', 'value' => null],
        ];
        if ($this->getBehavior('upload') !== null) {
            $rules[] = [['UploadFile'], 'image', 'minWidth' => 600, 'minHeight' => 338, 'maxWidth' => 3840, 'maxHeight' => 3840];
            $rules[] = [['UploadExternalUrl'], 'url', 'enableIDN' => true];
        }
        if ($this->getBehavior('tree') !== null) {
            $rules[] = [['PathParent'], 'exist', 'targetAttribute' => 'path'];
        }
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $auto = parent::scenarios();
        return [
            'administrator' => $auto[static::SCENARIO_DEFAULT],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Для url',
            'name' => 'Текст ссылки',
            'title' => 'Заголовок',
            'title_seo' => 'Подпись браузера',
            'description' => 'Описание',
            'icon' => 'Иконка',
            'updated' => 'Обновлена',
            'path' => 'Путь',
            'UploadFile' => 'локальную',
            'UploadExternalUrl' => 'из интернета',
            'PathParent' => 'Родитель',
            'group_id' => 'Группа',
            'post_count' => 'Материалов',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'upload' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'icon',
            ],
            'tree' => [
                'class' => TreeBehavior::className(),
                'attributeTreeElement' => 'slug',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostFolder()
    {
        return $this->hasOne(PostFolder::className(), ['folder_id' => 'id']);
    }

    /**
     * @return PostQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['folder_id_main' => 'id']);
    }

    /**
     * @return FolderQuery
     */
    public function getParent()
    {
        return $this->hasOne(Folder::className(), ['path' => 'path_parent']);
    }

    /**
     * @return FolderQuery
     */
    public static function find()
    {
        return new FolderQuery(get_called_class());
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        Yii::$app->cacheCommon->delete(static::CACHE_KEY_BY_ID);

        if ($changedAttributes) {
            File::deleteSitemapFolder();
        }
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function afterDelete() {
        parent::afterDelete();

        File::deleteSitemapFolder();
    }

    /**
     * @param array|Folder $folder
     * @param array|null $postLast
     * @return array
     */
    public static function getUrlParams($folder, $postLast = null)
    {
        $route = [
            'folder/view',
            'path' => TreeBehavior::convertLtreeToUrl($folder['path']),
        ];
        if (!empty($postLast)) {
            $route['year']   = date('Y', $postLast['published']);
            $route['month']  = date('m', $postLast['published']);
            $route['day']    = date('d', $postLast['published']);
            $route['hour']   = date('H', $postLast['published']);
            $route['minute'] = date('i', $postLast['published']);
            $route['second'] = date('s', $postLast['published']);
        }

        return $route;
    }

    /**
     * @param boolean $scheme
     * @param array|null $postLast
     * @return string
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     */
    public function getUrl($scheme = false, $postLast = null)
    {
        return Url::toRouteFrontend(static::getUrlParams($this, $postLast), $scheme);
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        $navigation = FeedNavigation::find()->forPage('folder', $this->id)->asArray()->all();

        if (!empty($navigation)) {
            return FeedNavigation::activeByUrl($navigation);
        }

        return [];
    }

    /**
     * @param string|null $id
     * @return array|null
     */
    public static function getById($id = null)
    {
        $cachedById = static::$cachedById;
        if ($cachedById === []) {
            $cachedById = Yii::$app->cacheCommon->get(static::CACHE_KEY_BY_ID);
            if ($cachedById === false) {
                $cachedByIdData = static::find()->select([
                    'id',
                    'slug',
                    'name',
                    'path'        => 'REPLACE(REPLACE(ltree2text(path), \'.\', \'/\'), \'_\', \'-\')',
                    'path_parent' => 'subltree(path, 0, nlevel(path) - 1)',
                ])->with([
                    'parent' => function ($query) {
                        /* @var FolderQuery $query */
                        $query->select(['id', 'path']);
                    },
                ])->asArray()->all();

                $cachedById = [];
                if (!empty($cachedByIdData)) {
                    /** @var array $cachedById */
                    $cachedById = [];
                    foreach ($cachedByIdData as $item) {
                        $cachedById[$item['id']] = [
                            'slug'      => $item['slug'],
                            'name'      => $item['name'],
                            'path'      => $item['path'],
                            'parent_id' => $item['parent']['id'],
                        ];
                    }

                    Yii::$app->cacheCommon->set(static::CACHE_KEY_BY_ID, $cachedById);
                }
            }
            if (!empty($cachedById)) {
                static::$cachedById = $cachedById;
            }
        }

        if (is_numeric($id)) {
            if (array_key_exists($id, $cachedById)) {
                return $cachedById[$id];
            } else {
                return null;
            }
        }

        return $cachedById;
    }
}
