<?php

namespace common\models;

use \yii\base\Event;
use \yii\db\ActiveRecord;
use \yii\db\AfterSaveEvent;
use \common\helpers\File;

/**
 * TagTrait provides methods for work with model's tags.
 *
 * @property PostTag[] $postTag
 * @property Tag[]     $tags
 */
trait TagTrait
{
    /**
     * @var array
     */
    protected $oldTagIds;

    /**
     * @var boolean
     */
    protected $tagsChanged = false;

    /**
     * Attaches an event handlers.
     */
    public function attachTagEvents()
    {
        $this->on(ActiveRecord::EVENT_AFTER_INSERT,  [$this, 'afterInsertOrUpdateTagOwner']);
        $this->on(ActiveRecord::EVENT_AFTER_UPDATE,  [$this, 'afterInsertOrUpdateTagOwner']);
        $this->on(ActiveRecord::EVENT_BEFORE_DELETE, [$this, 'beforeDeleteTagOwner']);
        $this->on(ActiveRecord::EVENT_AFTER_DELETE,  [$this, 'afterDeleteTagOwner']);
    }

    /**
     * @return PostTagQuery
     */
    public function getPostTag()
    {
        return $this->hasMany(PostTag::className(), ['post_id' => 'id']);
    }

    /**
     * @return TagQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('post_tag', ['post_id' => 'id']);
    }

    /**
     * @throws \yii\db\Exception
     */
    public function syncTagsName()
    {
        $tagsName = $this
            ->getPostTag()
            ->innerJoinWith('tag')
            ->select('name')
            ->orderBy(['idx((SELECT string_to_array(text_alphanumeric(tags_name), \',\') FROM post WHERE id = ' . $this->id . '), text_alphanumeric("name"))' => SORT_ASC])
            ->column();
        $tagsName = empty($tagsName) ? null : implode(', ', $tagsName);

        if ($this->tags_name !== $tagsName) {
            // нет валидаций и событий
            $this->updateAttributes(['tags_name' => $tagsName]);
        }
    }

    /**
     * @param AfterSaveEvent $event
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    protected function afterInsertOrUpdateTagOwner($event)
    {
        $oldTagIds = PostTag::find()
            ->select('tag_id')
            ->andWhere(['post_id' => $this->id])
            ->column();

        $changedAttributes = $event->changedAttributes;
        $this->tagsChanged = array_key_exists('tags_name', $changedAttributes);

        if ($this->tagsChanged) {
            if ($this->tags_name) {
                $tagsName = explode(', ', $this->tags_name);
                $tagIds = Tag::selectOrInsert($tagsName);
            } else {
                $tagIds = [];
            }

            $this->updateRelation('Tag', $oldTagIds, $tagIds);

            $this->syncTagsName();
        } else {
            $tagIds = $oldTagIds;
        }

        if (!$changedAttributes) {
            // нет изменений
            return;
        }

        $now = time();
        $isHiddenNew = empty($this->published);
        if (!$isHiddenNew) {
            $timestampNew = strtotime($this->published);
            if ($timestampNew > $now) {
                $isHiddenNew = true;
            }
        }
        $isHiddenOld = empty($changedAttributes['published']);
        if (!$isHiddenOld) {
            $timestampOld = strtotime($changedAttributes['published']);
            if ($timestampOld > $now) {
                $isHiddenOld = true;
            }
        }
        if (($isHiddenNew && $isHiddenOld) || ($this->getIsNewRecord() && $isHiddenNew)) {
            // сохраняем скрытый
            return;
        }

        if ($isHiddenNew && !$isHiddenOld) {
            static::updateUpdatedOnRelationByNow('Tag', $oldTagIds);
        } else if (!$isHiddenNew && $isHiddenOld) {
            static::updateUpdatedOnRelationByNow('Tag', $tagIds);
        } else {
            static::updateUpdatedOnRelationByNow('Tag', array_unique(array_merge($tagIds, $oldTagIds)));
        }

        File::deleteSitemapTag();
    }

    /**
     * @param Event $event
     */
    protected function beforeDeleteTagOwner($event)
    {
        $this->oldTagIds = PostTag::find()
            ->select('tag_id')
            ->andWhere(['post_id' => $this->id])
            ->column();
    }

    /**
     * @param Event $event
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    protected function afterDeleteTagOwner($event)
    {
        if (empty($this->published) || strtotime($this->published) > time()) {
            // невидимый или отложенная публикация
            return;
        }

        static::updateUpdatedOnRelationByNow('Tag', $this->oldTagIds);
        File::deleteSitemapTag();
    }
}
