<?php

namespace common\models;

use \yii\db\ActiveQuery;

/**
 * FeedbackQuery is ActiveQuery with Feedback scopes.
 *
 * @method Feedback|array|null one($db = null)
 * @method Feedback[]|array all($db = null)
 */
class FeedbackQuery extends ActiveQuery
{
}
