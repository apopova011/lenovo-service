<?php

namespace common\models;

use Yii;
use \yii\db\ActiveRecord;
use \yii\db\Expression;
use \common\behaviors\HtmlImageBehavior;
use \common\behaviors\HtmlLinkExternalBehavior;
use \common\behaviors\HtmlLinkTargetBehavior;
use \common\behaviors\SpacelessBehavior;
use \common\behaviors\UploadBehavior;
use \common\helpers\File;
use \common\helpers\Inflector;
use \common\helpers\Url;
use \common\validators\DatePgValidator;
use \common\validators\SlugSiteValidator;
use \common\validators\TimeValidator;

/**
 * This is the model class for table "post".
 *
 * @property integer     $id
 * @property string      $slug
 * @property string      $name
 * @property string|null $title_seo
 * @property string      $description
 * @property string|null $icon
 * @property string|null $content
 * @property integer     $author_id
 * @property integer     $folder_id_main
 * @property string|null $tags_name
 * @property string|null $published
 * @property string      $updated
 * @property boolean     $is_sent_to_index
 * @property boolean     $is_in_common_feed
 * @property boolean     $is_allow_footer
 * @property boolean     $is_allow_comments
 * @property boolean     $is_contain_video
 * @property string      $video_duration
 * @property integer     $show_count
 * @property integer     $symbol_count
 * @property integer     $comment_visible_count
 * @property string      $type
 *
 * @property string $UploadFile
 * @property string $UploadExternalUrl
 *
 * @property User            $author
 * @property Comment[]       $comments
 * @property Photo[]         $photos
 * @property StatisticPost[] $statisticPost
 * @property Folder          $folderMain
 */
class Post extends ActiveRecord
{
    /**
     * User friendly russian label.
     */
    const LABEL_SINGULAR_ru_RU = 'Материал';

    /**
     * User friendly russian label.
     */
    const LABEL_PLURAL_ru_RU = 'Материалы';

    /**
     * Default name value.
     */
    const DEFAULT_NAME = 'Название';

    /**
     * @var integer
     */
    public $popularity;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function rules()
    {
        $rules = [
            [['slug', 'name', 'title_seo', 'description', 'content'], 'filter', 'filter' => 'trim'],
            [['tags_name'],                                           'filter', 'filter' => function ($value) {
                $value = trim($value, " \t\n\r\0\x0B,");

                $value = explode(',', $value);
                array_walk($value, function(&$str) { $str = trim($str); });
                return implode(', ', $value);
            }],
            [['name', 'description'],                                 'filter', 'filter' => function ($value) {
                return preg_replace('/[ ]{2,}/', ' ', str_replace(["\n", "\r"], ' ', $value));
            }],
            [['slug', 'name', 'description', 'author_id', 'type'], 'required'],
            [['slug'],                           'string', 'max' => 31, 'min' => 2],
            [['name'],                           'string', 'max' => 95],
            [['title_seo', 'icon', 'tags_name'], 'string', 'max' => 255],
            [['description'],                    'string', 'max' => 1023],
            [['content', 'type'],                'string'],
            [['author_id', 'folder_id_main', 'show_count', 'symbol_count', 'comment_visible_count'], 'integer'],
            [['is_sent_to_index', 'is_in_common_feed', 'is_allow_footer', 'is_allow_comments', 'is_contain_video'], 'boolean'],
            [['published'], 'date', 'format' => 'php:Y-m-d| H:i'],
            [['slug'],       SlugSiteValidator::className()],
            [['updated'],      DatePgValidator::className()],
            [['video_duration'], TimeValidator::className()],

            [['published'], 'unique', 'targetAttribute' => 'date_trunc(\'minute\', published)', 'message' => 'На эту минуту уже назначена другая публикация.'],
            ['slug', 'unique', 'filter' => function($query) {
                /* @var \common\models\PostQuery $query */
                $query->andWhere(['type' => 'section']);
            }],
            [['author_id'], 'exist', 'targetClass' => 'common\models\User',   'targetAttribute' => 'id', 'filter' => function($query) {
                /* @var UserQuery $query */
                $query->onlyAuthors();
            }],
            [['folder_id_main'], 'exist', 'targetClass' => 'common\models\Folder', 'targetAttribute' => 'id', 'filter' => function($query) {
                /* @var FolderQuery $query */
                $query->parents();
            }],

            [['type'], 'in', 'range' => static::dictionaryType()],

            ['tags_name', function ($attribute, $params) {
                $tagsClear = $this->{$attribute};
                $tagsClear = Tag::textAlphanumeric($tagsClear);
                $tagsClear = explode(',', $tagsClear);

                if (Folder::find()->andWhere(['text_alphanumeric(name)' => $tagsClear])->exists()) {
                    $this->addError(
                        $attribute,
                        Yii::$app->getI18n()->format(
                            '«{attribute}» не может содержать разделы.',
                            ['attribute' => $this->getAttributeLabel($attribute)],
                            Yii::$app->language
                        )
                    );
                }
            }],
            [['title_seo', 'icon', 'content', 'published', 'tags_name', 'video_duration', 'folder_id_main'], 'default', 'value' => null],
            [['is_in_common_feed', 'is_allow_footer', 'is_allow_comments'],                                  'default', 'value' => true],
            [['is_sent_to_index', 'is_contain_video'],                                                       'default', 'value' => false],
            [['show_count', 'symbol_count', 'comment_visible_count'],                                        'default', 'value' => 0],
        ];

        if ($this->getBehavior('upload') !== null) {
            $rules[] = [['UploadFile'], 'image', 'minWidth' => 600, 'minHeight' => 338, 'maxWidth' => 3840, 'maxHeight' => 3840];
            $rules[] = [['UploadExternalUrl'], 'url', 'enableIDN' => true];
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $auto = parent::scenarios();
        return [
            'administrator' => $auto[static::SCENARIO_DEFAULT],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                    => 'ID',
            'slug'                  => 'Для url',
            'name'                  => 'Название',
            'title_seo'             => 'Заголовок',
            'description'           => 'Описание',
            'icon'                  => 'Иконка',
            'content'               => 'Содержимое',
            'author_id'             => 'Автор',
            'folder_id_main'        => 'Главная директория',
            'tags_name'             => 'Тэги',
            'published'             => 'Опубликован',
            'updated'               => 'Обновлён',
            'is_sent_to_index'      => 'Отправлен на индексацию',
            'is_in_common_feed'     => 'Отображать в общих лентах?',
            'is_allow_footer'       => 'Показывать футер?',
            'is_allow_comments'     => 'Разрешить комментирование?',
            'is_contain_video'      => 'Содержит видео?',
            'video_duration'        => 'Продолжительность авторского видео',
            'show_count'            => 'Показов',
            'symbol_count'          => 'Символов',
            'comment_visible_count' => 'Комментариев',
            'type'                  => 'Тип',
            'UploadFile'            => 'локальную',
            'UploadExternalUrl'     => 'из интернета',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'upload'           => [
                'class'     => UploadBehavior::className(),
                'attribute' => 'icon',
            ],
            'spaceless'        => [
                'class' => SpacelessBehavior::className(),
            ],
            'htmlImage'        => [
                'class' => HtmlImageBehavior::className(),
            ],
            'htmlLinkTarget'   => [
                'class' => HtmlLinkTargetBehavior::className(),
            ],
            'htmlLinkExternal' => [
                'class' => HtmlLinkExternalBehavior::className(),
            ],
        ];
    }

    /**
     * @return FolderQuery
     */
    public function getFolderMain()
    {
        return $this->hasOne(Folder::className(), ['id' => 'folder_id_main']);
    }

    /**
     * @return CommentQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['post_id' => 'id']);
    }

    /**
     * @return UserQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatisticPost()
    {
        return $this->hasMany(StatisticPost::className(), ['post_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return PostQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PostQuery(get_called_class());
    }

    /**
     * @param array|Post $post
     * @param bool $byType
     * @return array
     */
    public static function getUrlParams($post, $byType = true)
    {
        if (
            $byType
            && ((is_array($post) && array_key_exists('type', $post)) || (is_object($post) && $post->hasAttribute('type')))
        ) {
            /** @var Post $class */
            $class = static::getClassByType($post['type']);
            return $class::getUrlParams($post, false);
        }

        return [
            'post/view-by-slug',
            'slug' => $post['slug'],
        ];
    }

    /**
     * @param string $type
     * @return string
     */
    public static function getClassByType($type)
    {
        return Post::className() . Inflector::camelize($type);
    }

    /**
     * @param boolean $scheme
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function getUrl($scheme = false)
    {
        return Url::toRouteFrontend(static::getUrlParams($this), $scheme);
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (
            array_key_exists('name', $changedAttributes) ||
            array_key_exists('title_seo', $changedAttributes) ||
            ($this->hasProperty('foldersChanged') && $this->foldersChanged) ||
            ($this->hasProperty('tagsChanged') && $this->tagsChanged)
        ) {
            Yii::$app->cacheCommonPost->set('post:' . $this->id . ':similar', static::find()->similar($this)->limit(6)->asArray()->all());
        }

        if (empty($changedAttributes)) {
            // нет изменений
            return;
        }

        $now = time();
        $isHiddenNew = empty($this->published);
        if ($isHiddenNew) {
            $timestampNew = 0;
        } else {
            $timestampNew = strtotime($this->published);
            if ($timestampNew > $now) {
                $timestampNew = 0;

                $isHiddenNew = true;
            }
        }
        $isHiddenOld = empty($changedAttributes['published']);
        if ($isHiddenOld) {
            $timestampOld = 0;
        } else {
            $timestampOld = strtotime($changedAttributes['published']);
            if ($timestampOld > $now) {
                $timestampOld = 0;

                $isHiddenOld = true;
            }
        }

        if (($isHiddenNew && $isHiddenOld) || ($this->getIsNewRecord() && $isHiddenNew)) {
            // сохраняем скрытый
            return;
        }

        $this->deleteStaticCache(max($timestampNew, $timestampOld));
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function afterDelete()
    {
        parent::afterDelete();

        Yii::$app->cacheCommonPost->delete('post:' . $this->id . ':similar');
        Yii::$app->cacheCommonPost->delete('post:' . $this->id . ':comments');

        if (empty($this->published)) {
            // невидимый
            return;
        }
        $timestamp = strtotime($this->published);
        if ($timestamp > time()) {
            // отложенная публикация
            return;
        }

        $this->deleteStaticCache($timestamp);
    }

    /**
     * Sets the default values on create form.
     */
    public function defaultOnCreate()
    {
        $this->loadDefaultValues();

        $this->slug = Inflector::slug(static::DEFAULT_NAME) . mt_rand(0, 99999);
        $this->name = static::DEFAULT_NAME;
        $this->description = 'Описание';
        $this->published = date('Y-m-d H:i', max(
            strtotime('+1 year'),
            static::find()->getLastPublished(true) + 300
        ));
        $this->updated = date('Y-m-d H:i:s');
        $this->author_id = Yii::$app->getUser()->id;
    }

    /**
     * @param array|Post $post
     * @return array
     */
    public static function loadSimilarity($post)
    {
        $cacheCommonPost = Yii::$app->cacheCommonPost;
        $cacheKey = 'post:' . $post['id'] . ':similar';

        $result = $cacheCommonPost->get($cacheKey);
        if ($result === false) {
            $result = static::find()->similar($post)->limit(6)->asArray()->all();

            $cacheCommonPost->set($cacheKey, $result);
        }

        return $result;
    }

    /**
     * @param integer $timestamp
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    protected function deleteStaticCache($timestamp = null)
    {
        if (!empty($timestamp)) {
            if ($timestamp > strtotime('-2 days')) {
                File::deleteNews();
            }
            if ($timestamp > strtotime('-1 week')) {
                File::deleteRss();
            }
        }

        File::deleteSitemapPost($this->id);
    }

    /**
     * @param string $arName
     * @param array $oldIds
     * @param array $newIds
     * @throws \yii\db\Exception
     */
    public function updateRelation($arName, $oldIds, $newIds)
    {
        $postId = $this->id;
        $arNameLower = mb_strtolower($arName, Yii::$app->charset);
        $columnName  = $arNameLower . '_id';

        if (!is_array($oldIds)) {
            $oldIds = [];
        }

        if (!is_array($newIds)) {
            $newIds = [];
        }

        $deletedIds = array_diff($oldIds, $newIds);
        $addedIds   = array_diff($newIds, $oldIds);

        if ($deletedIds) {
            $arClass = '\common\models\Post' . $arName;
            /** @var ActiveRecord $arClass */
            $arClass::deleteAll(['AND', ['post_id' => $postId], [$columnName => $deletedIds]]);
        }

        if ($addedIds) {
            $rows = [];
            foreach ($addedIds as $addedId) {
                $rows[] = [$postId, $addedId];
            }

            static::getDb()->createCommand()->batchInsert('post_' . $arNameLower, ['post_id', $columnName], $rows)->execute();
        }
    }

    /**
     * @param string $arName
     * @param int[] $arIds
     */
    public static function updateUpdatedOnRelationByNow($arName, $arIds)
    {
        Yii::trace('Update "updated" to now on ' . $arName . ' ' . implode(', ', $arIds) . '.', 'UpdatedAuto');

        if (empty($arIds)) {
            return;
        }

        $arName = '\common\models\\' . $arName;
        /** @var ActiveRecord $arName */
        $arName::updateAll(
            ['updated' => new Expression('NOW()')],
            ['id' => $arIds]
        );
    }

    /**
     * @param string $arName
     * @param int[] $arIds
     * @throws \yii\db\Exception
     */
    public static function updateUpdatedOnRelationByPublished($arName, $arIds)
    {
        Yii::trace('Update "updated" to max published on ' . $arName . ' ' . implode(', ', $arIds) . '.', 'UpdatedAuto');

        if (empty($arIds)) {
            return;
        }

        $columnName = strtolower($arName) . '_id';

        $arRelationName = '\common\models\Post' . $arName;
        /** @var ActiveRecord $arRelationName */
        $items = $arRelationName::find()
            ->select([
                $columnName,
                'post_published_max' => 'MAX(' . static::tableName() . '.published)',
            ])
            ->innerJoinWith([
                'post' => function ($query) {
                    /** @var PostQuery $query */
                    $query->isPublished();
                },
            ])
            ->andWhere([$columnName => $arIds])
            ->groupBy($columnName)
            ->asArray()
            ->all();

        $arName = '\common\models\\' . $arName;
        /** @var ActiveRecord $arName */
        $command = Yii::$app->getDb()->createCommand(
            'UPDATE ' . $arName::tableName() .
            ' SET [[updated]] = :updated WHERE [[id]] = :item_id AND [[updated]] < :updated'
        );
        foreach ($items as $item) {
            // в первом запросе можем джойнить $arName.updated и кидать UPDATE только если post_published_max больше
            $command
                ->bindValues([
                    ':item_id' => $item[$columnName],
                    ':updated' => $item['post_published_max'],
                ])
                ->execute();
        }
    }

    /**
     * @param array $postsId
     * @return bool
     * @throws \yii\db\Exception
     */
    public static function updateUpdatedOnRelationsByPublished($postsId)
    {
        foreach (['Folder', 'Tag'] as $arName) {
            $columnName = strtolower($arName) . '_id';
            $arRelationName = '\common\models\Post' . $arName;

            /** @var ActiveRecord $arRelationName */
            $arIds = $arRelationName::find()
                ->select($columnName)
                ->andWhere(['post_id' => $postsId])
                ->groupBy($columnName)
                ->column();

            if (!empty($arIds)) {
                static::updateUpdatedOnRelationByPublished($arName, $arIds);
            }
        }

        return true;
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public static function dictionaryType()
    {
        return Yii::$app->getDb()->createCommand('SELECT unnest(enum_range(NULL::post_types))')->queryColumn();
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public static function dictionaryTypeLabels()
    {
        $types = static::dictionaryType();
        return array_combine($types, $types);
    }
}
