<?php

namespace common\models;

use \yii\db\ActiveQuery;

/**
 * PhotoQuery is ActiveQuery with photo scopes.
 *
 * @method Photo|array|null one($db = null)
 * @method Photo[]|array all($db = null)
 */
class PhotoQuery extends ActiveQuery
{
    /**
     * @param int|string $authorId
     * @return PhotoQuery
     */
    public function byAuthor($authorId)
    {
        $this->andWhere(['author_id' => $authorId]);
        return $this;
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @return PhotoQuery
     */
    public function postPublishedBetween($startDate, $endDate)
    {
        $this->andWhere(['between', 'post_published', $startDate, $endDate]);
        return $this;
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @return PhotoQuery
     */
    public function postPublishedBetweenByWeek($startDate, $endDate)
    {
        $this->postPublishedBetween($startDate, $endDate);
        $this->groupBy(['week' => 'date_trunc(\'week\', post_published)']);
        $this->orderBy(['date_trunc(\'week\', post_published)' => SORT_DESC]);
        return $this;
    }
}
