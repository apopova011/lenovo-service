<?php

namespace common\models;

use \yii\db\ActiveQuery;

/**
 * TagQuery is ActiveQuery with tag scopes.
 *
 * @method Tag|array|null one($db = null)
 * @method Tag[]|array all($db = null)
 */
class TagQuery extends ActiveQuery
{
    /**
     * @return TagQuery
     */
    public function hasSeo()
    {
        $tableName = Tag::tableName();

        $this->andWhere($tableName . '.title_seo   IS NOT NULL');
        $this->andWhere($tableName . '.title       IS NOT NULL');
        $this->andWhere($tableName . '.description IS NOT NULL');

        return $this;
    }

    /**
     * @return TagQuery
     */
    public function forListOfLink()
    {
        $this->select(['id', 'slug', 'name', 'title']);
        return $this;
    }

    /**
     * @param string $slug
     * @return TagQuery
     */
    public function forView($slug)
    {
        $this
            ->select([
                'id',
                'slug',
                'name',
                'title',
                'title_seo',
                'description',
            ])
            ->andWhere(['slug' => $slug]);

        return $this;
    }

    /**
     * @return TagQuery
     */
    public function forSitemap()
    {
        $this
            ->select([
                'slug',
                'updated' => 'EXTRACT(EPOCH FROM updated AT TIME ZONE current_setting(\'TIMEZONE\'))',
                'title_seo' => '(title_seo IS NOT NULL AND title IS NOT NULL AND description IS NOT NULL)',
            ])
            ->orderBy('id');

        return $this;
    }
}
