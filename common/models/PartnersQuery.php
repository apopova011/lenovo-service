<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * ServiceQuery is ActiveQuery with Service scopes.
 *
 * @method Partners|array|null one($db = null)
 * @method Partners[]|array all($db = null)
 */
class PartnersQuery extends ActiveQuery
{
}
