<?php

namespace common\models;

use Yii;
use \yii\db\ActiveRecord;
use \yii\db\Expression;
use \common\behaviors\TreeBehavior;
use \common\helpers\File;
use \common\validators\DatePgValidator;
use \common\validators\IpValidator;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property string $content
 * @property string $status
 * @property string $created
 * @property string $user_title
 * @property string $user_avatar
 * @property string|null $user_ip
 * @property string|null $user_agent
 * @property string|null $user_session
 * @property string $path
 * @property integer $post_id
 * @property integer $author_id
 * @property integer|null $votes_value
 *
 * @property string  $PathParent
 * @property boolean $isChild
 * @property string  $parentKey
 *
 * @property Post $post
 * @property User $author
 * @property Comment $parent
 * @property CommentVote[] $commentVote
 */
class Comment extends ActiveRecord
{
    /**
     * User friendly russian label.
     */
    const LABEL_SINGULAR_ru_RU = 'Комментарий';

    /**
     * User friendly russian label.
     */
    const LABEL_PLURAL_ru_RU = 'Комментарии';

    /**
     * Default status.
     */
    const STATUS_DEFAULT = 'new';

    /**
     * array
     */
    public static $voteStatuses = [
        'banned_by_vote',
        'excellent_by_vote',
    ];

    /**
     * array
     */
    public static $showedStatuses = [
        'new',
        'approved',
        'approved_and_not_excellent',
        'excellent_by_vote',
        'excellent_by_moderator'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function rules()
    {
        $rules = [
            [['content'], 'filter', 'filter' => 'trim'],
            [['content', 'created', 'user_title', 'user_avatar', 'post_id', 'author_id'], 'required'],
            [['content'],                           'string'],
            [['path', 'user_avatar', 'user_agent'], 'string', 'max' => 255],
            [['user_session'],                      'string', 'max' => 32],
            [['status'], 'in', 'range' => static::dictionaryStatus()],
            [['post_id', 'author_id', 'votes_value'], 'integer'],
            [['created'], DatePgValidator::className()],
            [['user_ip'],     IpValidator::className()],
            [['path'], 'unique'],
            [['author_id'],  'exist', 'targetClass' => 'common\models\User', 'targetAttribute' => 'id'],
            [['post_id'],    'exist', 'targetClass' => 'common\models\Post', 'targetAttribute' => 'id'],
            [['content', 'user_ip', 'user_agent', 'user_session', 'votes_value'], 'default', 'value' => null],
            [['content'], 'unique', 'targetAttribute' => ['content', 'author_id'], 'targetClass' => 'common\models\Comment', 'filter' => function ($query) {
                /* @var CommentQuery $query */
                $query->andWhere(['>', 'created', date('Y-m-d H:i:s', time() - 900)]);
            }, 'message' => 'Такое сообщение уже добавлено.'],
        ];

        if ($this->getBehavior('tree') !== null) {
            $rules[] = [['PathParent'], 'exist', 'targetAttribute' => 'path'];
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $auto = parent::scenarios();
        return [
            'administrator' => $auto[static::SCENARIO_DEFAULT],
            'commentForm'   => ['content', 'user_title', 'user_avatar'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Содержимое',
            'status' => 'Статус',
            'created' => 'Создан',
            'user_title' => 'Подпись',
            'user_avatar' => 'Аватар',
            'user_ip' => 'IP',
            'user_agent' => 'User-Agent',
            'user_session' => 'Сессия',
            'path' => 'Путь',
            'post_id' => 'Материал',
            'author_id' => 'Автор',
            'votes_value' => 'Рейтинг',
            'PathParent' => 'Ответ на',
        ];
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function setScenario($value)
    {
        parent::setScenario($value);

        if ($value === 'commentForm') {
            $sequence = static::getTableSchema()->sequenceName;
            if (strpos($sequence, '.') !== false) {
                $sequence = str_replace('.', '\'.\'', $sequence);
            }
            $this->id = static::getDb()->createCommand('SELECT NEXTVAL(\'' . $sequence . '\')')->queryScalar();
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert && empty($this->user_ip)) {
                $request = Yii::$app->getRequest();
                if (method_exists($request, 'getUserIp')) {
                    $this->user_ip    = $request->getUserIP();
                    $this->user_agent = $request->getUserAgent();
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (empty($changedAttributes)) {
            return;
        }

        $showedStatuses = static::$showedStatuses;
        $isHiddenNew = empty(               $this->status) || !in_array(               $this->status, $showedStatuses, true);
        $isHiddenOld = empty($changedAttributes['status']) || !in_array($changedAttributes['status'], $showedStatuses, true);

        if (($isHiddenNew && $isHiddenOld) || ($this->getIsNewRecord() && $isHiddenNew)) {
            // сохраняем скрытый
            return;
        }

        $this->onChangeVisible();
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function afterDelete()
    {
        parent::afterDelete();

        if (empty($this->status) || !in_array($this->status, static::$showedStatuses, true)) {
            // невидимый
            return;
        }

        $this->onChangeVisible();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'tree' => [
                'class' => TreeBehavior::className(),
                'attributeTreeElement' => 'id',
            ],
        ];
    }

    /**
     * @return PostQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }

    /**
     * @return UserQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommentVote()
    {
        return $this->hasMany(CommentVote::className(), ['comment_id' => 'id']);
    }

    /**
     * @return CommentQuery
     */
    public function getParent()
    {
        return $this->hasOne(Comment::className(), ['path' => 'pathParent']);
    }

    /**
     * @inheritdoc
     * @return CommentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CommentQuery(get_called_class());
    }

    /**
     * Sets updated and updates cashed comments
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    protected function onChangeVisible()
    {
        User::updateAll(
            ['updated' => new Expression('NOW()')],
            ['id' => $this->author_id]
        );

        $postId = $this->post_id;

        Post::updateAll(
            ['updated' => new Expression('NOW()')],
            ['id' => $postId]
        );

        File::deleteSitemapPost($postId);

        Yii::$app->cacheCommonPost->set('post:' . $postId . ':comments', static::find()->forPost($postId)->asArray()->all());
    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function dictionaryStatus()
    {
        return Yii::$app->getDb()->createCommand('SELECT unnest(enum_range(NULL::comment_statuses))')->queryColumn();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function dictionaryStatusLabels()
    {
        $statuses = static::dictionaryStatus();
        return array_combine($statuses, $statuses);
    }

    /**
     * @param integer $postId
     * @return array
     */
    public static function loadByPost($postId)
    {
        $cacheCommonPost = Yii::$app->cacheCommonPost;
        $cacheKey = 'post:' . $postId . ':comments';

        $result = $cacheCommonPost->get($cacheKey);
        if ($result === false) {
            $result = static::find()->forPost($postId)->asArray()->all();

            $cacheCommonPost->set($cacheKey, $result);
        }

        return $result;
    }
}
