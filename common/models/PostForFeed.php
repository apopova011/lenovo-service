<?php

namespace common\models;

class PostForFeed extends Post
{
    use FolderTrait, TagTrait;

    /**
     * Type of the post.
     */
    const POST_TYPE = 'for_feed';

    /**
     * Default folder id value.
     */
    const DEFAULT_FOLDER_ID = 1;

    /**
     * @inheritdoc
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->type = static::POST_TYPE;

        $this->attachFolderEvents();
        $this->attachTagEvents();
    }

    /**
     * @inheritdoc
     * @return PostQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new PostQuery(get_called_class());
        return $query->andWhere(['type' => static::POST_TYPE]);
    }

    /**
     * @inheritdoc
     */
    public function defaultOnCreate()
    {
        parent::defaultOnCreate();

        $this->folder_id_main = static::DEFAULT_FOLDER_ID;
        $this->folderIds = [static::DEFAULT_FOLDER_ID];
    }
}