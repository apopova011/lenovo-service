<?php

namespace common\models;

use \yii\db\ActiveQuery;

/**
 * PostTagQuery is ActiveQuery with PostTag scopes.
 *
 * @method PostTag|array|null one($db = null)
 * @method PostTag[]|array all($db = null)
 */
class PostTagQuery extends ActiveQuery
{
    /**
     * @return PostTagQuery
     */
    public function innerJoinPostPublished()
    {
        $this->innerJoinWith([
            'post' => function ($query) {
                /** @var $query PostQuery */
                $query
                    ->select([
                        'id',
                        'published' => $query::getAsTimestamp('published'),
                    ])
                    ->isPublished();
            },
        ]);

        return $this;
    }

    /**
     * @return PostTagQuery
     */
    public function forSitemapTimeline()
    {
        $this
            ->innerJoinPostPublished()
            ->innerJoinWith([
                'tag' => function ($query) {
                    /** @var $query TagQuery */
                    $query->select([
                        'id',
                        'slug',
                    ]);
                },
            ])
            ->orderBy([
                'post_id' => SORT_ASC,
                'tag_id'  => SORT_ASC,
            ]);

        return $this;
    }
}
