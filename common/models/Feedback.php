<?php

namespace common\models;

use \yii\behaviors\BlameableBehavior;
use \yii\db\ActiveRecord;
use \common\validators\DatePgValidator;

/**
 * This is the model class for table "feedback".
 *
 * @property integer     $id
 * @property string      $user_title
 * @property string      $user_email
 * @property integer     $user_id
 * @property string      $text
 * @property string      $created
 * @property string|null $sent
 *
 * @property User $user
 */
class Feedback extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'user' => [
                'class'      => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['user_id'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_title', 'user_email', 'text'], 'filter', 'filter' => 'trim'],
            [['user_email'],                       'filter', 'filter' => 'mb_strtolower'],

            [['user_title', 'user_email', 'text'], 'required'],

            [['user_title'], 'string', 'max' =>  95],
            [['user_email'], 'string', 'max' => 255],
            [['text'],       'string'],
            [['user_id'], 'integer'],

            [['created', 'sent'], DatePgValidator::className()],

            [['user_email'], 'email', 'enableIDN' => true],

            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $auto = parent::scenarios();
        $rules = [
            'administrator' => $auto[static::SCENARIO_DEFAULT],
            'send'          => ['user_title', 'user_email', 'text'],
        ];

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'user_title' => 'Имя',
            'user_email' => 'Email для ответа',
            'user_id'    => 'Пользователь',
            'text'       => 'Текст вопроса',
            'created'    => 'Создано',
            'sent'       => 'Отправлено',
        ];
    }

    /**
     * @return UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return FeedbackQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FeedbackQuery(get_called_class());
    }
}
