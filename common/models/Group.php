<?php

namespace common\models;

use \yii\db\ActiveRecord;
use \common\validators\SlugValidator;

/**
 * This is the model class for table "group".
 *
 * @property integer $id
 * @property string $slug
 * @property string $name
 *
 * @property Tag[] $tags
 * @property Folder[] $folders
 */
class Group extends ActiveRecord
{
    /**
     * User friendly russian label.
     */
    const LABEL_SINGULAR_ru_RU = 'Группа';

    /**
     * User friendly russian label.
     */
    const LABEL_PLURAL_ru_RU = 'Группы';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slug', 'name'], 'filter', 'filter' => 'trim'],
            [['slug', 'name'], 'required'],
            [['slug'], 'string', 'max' => 31, 'min' => 2],
            [['name'], 'string', 'max' => 63],
            [['slug'], SlugValidator::className()],
            [['slug', 'name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $auto = parent::scenarios();
        return [
            'administrator' => $auto[static::SCENARIO_DEFAULT],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'   => 'ID',
            'slug' => 'Для url',
            'name' => 'Название',
        ];
    }

    /**
     * @return FolderQuery
     */
    public function getFolders()
    {
        return $this->hasMany(Folder::className(), ['group_id' => 'id']);
    }

    /**
     * @return TagQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['group_id' => 'id']);
    }
}
