<?php

namespace common\models;

use \yii\db\ActiveRecord;

/**
 * This is the model class for table "statistic_post".
 *
 * @property integer $id
 * @property string $date
 * @property integer $post_id
 * @property integer $show_count
 * @property string|null $show_count_by_url
 *
 * @property Post $post
 */
class StatisticPost extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statistic_post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'post_id'], 'required'],
            [['date'], 'safe'],
            [['post_id', 'show_count'], 'integer'],
            [['show_count_by_url'], 'string'],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'post_id' => 'Post ID',
            'show_count' => 'Show Count',
            'show_count_by_url' => 'Show Count By Url',
        ];
    }

    /**
     * @return PostQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }
}
