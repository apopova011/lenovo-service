<?php

namespace common\models;

use Yii;
use \yii\base\NotSupportedException;
use \yii\db\ActiveRecord;
use \yii\helpers\ArrayHelper;
use \yii\web\IdentityInterface;
use \common\behaviors\UploadBehavior;
use \common\helpers\Inflector;
use \common\helpers\StringHelper;
use \common\helpers\Url;
use \common\validators\DatePgValidator;
use \common\validators\SlugGooglePlusValidator;
use \common\validators\SlugValidator;

/**
 * This is the model class for table "user".
 * @package common\models
 *
 * @property integer             $id
 * @property string              $role
 * @property string              $slug
 * @property string              $title
 * @property string              $avatar
 * @property string|null         $first_name
 * @property string|null         $last_name
 * @property string|null         $timezone
 * @property string|null         $email
 * @property string|null         $title_gen
 * @property string|null         $avatar_original
 * @property integer|null        $sex
 * @property string|null         $birthday
 * @property string|null         $phone_mobile
 * @property string|null         $phone_home
 * @property string|null         $city_name
 * @property string|null         $skype
 * @property string|null         $twitter_slug
 * @property string|null         $instagram_slug
 * @property string|integer|null $facebook_id
 * @property string|null         $facebook_name
 * @property string|integer|null $vk_id
 * @property string|null         $vk_slug
 * @property string|null         $google_plus_slug
 * @property boolean|null        $vk_can_post
 * @property boolean|null        $vk_can_write_private_message
 * @property integer|null        $relation
 * @property integer|null        $life_main
 * @property integer|null        $smoking
 * @property integer|null        $alcohol
 * @property string|null         $music
 * @property string|null         $movies
 * @property string|null         $tv
 * @property string|null         $books
 * @property string|null         $games
 * @property string|null         $about
 * @property string|null         $password_hash
 * @property string|null         $auth_key
 * @property string|null         $created
 * @property string|null         $updated
 * @property integer             $comment_visible_count
 * @property integer             $post_count
 *
 * @property string $password write-only password
 * @property string $passwordNew
 * @property string $UploadFile
 * @property string $UploadExternalUrl
 * @property string $UploadUrl
 *
 * @property Post[]    $posts
 * @property Comment[] $comments
 * @property Photo[]   $photos
 *
 * @method string getUploadUrl()
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * User friendly english label.
     */
    const LABEL_SINGULAR_en_US = 'User';

    /**
     * User friendly russian label.
     */
    const LABEL_SINGULAR_ru_RU = 'Пользователь';

    /**
     * User friendly english label.
     */
    const LABEL_PLURAL_en_US = 'Users';

    /**
     * User friendly russian label.
     */
    const LABEL_PLURAL_ru_RU = 'Пользователи';

    /**
     * @var array
     */
    public static $rolesEmailVerified = [
        'verified',
        'moderator',
        'writer',
        'contributor',
        'editor',
        'administrator',
    ];

    /**
     * @var array
     */
    public static $rolesBackend = [
        'moderator',
        'writer',
        'contributor',
        'editor',
        'administrator',
    ];

    /**
     * @var array
     */
    public static $rolesAuthors = [
        'writer',
        'contributor',
        'editor',
        'administrator',
    ];

    /**
     * SQL expression for token.
     */
    const COLUMN_TOKEN = 'sha256(id::TEXT || date_part(\'epoch\', updated AT TIME ZONE \'UTC\') || \'_\' || COALESCE(password_hash, \'\'))';

    /**
     * @var string|null
     */
    public $token;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function rules()
    {
        $rules = [
            [
                [
                    'slug',
                    'title',
                    'about',
                    'title_gen',
                    'avatar_original',
                    'first_name',
                    'last_name',
                    'phone_mobile',
                    'phone_home',
                    'city_name',
                    'email',
                    'skype',
                    'twitter_slug',
                    'instagram_slug',
                    'google_plus_slug',
                    'facebook_name',
                    'vk_slug',
                    'music',
                    'movies',
                    'tv',
                    'books',
                    'games'
                ],
                'filter',
                'filter' => 'trim'
            ],
            [['email'], 'filter', 'filter' => 'mb_strtolower'],
            [
                ['title', 'first_name', 'last_name'],
                'filter',
                'filter' => function ($value) {
                    /* @var string $value */
                    return StringHelper::filterFromHtml($value);
                }
            ],

            [['role', 'slug', 'title'], 'required'],

            [['slug', 'phone_mobile', 'phone_home', 'timezone'], 'string', 'max' => 31, 'min' => 2],
            [['auth_key'],                                       'string', 'max' => 32],
            [['first_name', 'last_name', 'city_name'],           'string', 'max' => 63],
            [['passwordNew'],                                    'string', 'max' => 72, 'min' => 6],
            [['title', 'title_gen'],                             'string', 'max' => 95],
            [
                [
                    'avatar',
                    'avatar_original',
                    'email',
                    'skype',
                    'twitter_slug',
                    'instagram_slug',
                    'google_plus_slug',
                    'facebook_name',
                    'vk_slug',
                    'password_hash'
                ],
                'string',
                'max' => 255
            ],
            [['about', 'music', 'movies', 'tv', 'books', 'games'], 'string', 'max' => 1023],
            [
                [
                    'facebook_id',
                    'vk_id',
                    'sex',
                    'relation',
                    'life_main',
                    'smoking',
                    'alcohol',
                    'comment_visible_count',
                    'post_count'
                ],
                'integer'
            ],
            [['vk_can_post', 'vk_can_write_private_message'], 'boolean'],
            [['email'], 'email', 'enableIDN' => true],
            [['slug', 'skype', 'twitter_slug', 'instagram_slug', 'vk_slug'], SlugValidator::className()],
            [['google_plus_slug'],                                 SlugGooglePlusValidator::className()],
            [['created', 'updated', 'birthday'],                           DatePgValidator::className()],

            [['role'],      'in', 'range' => static::dictionaryRole()],
            [['sex'],       'in', 'range' => array_keys(static::dictionarySexLabels())],
            [['relation'],  'in', 'range' => array_keys(static::dictionaryRelationLabels())],
            [['life_main'], 'in', 'range' => array_keys(static::dictionaryLifeMainLabels())],
            [['smoking'],   'in', 'range' => array_keys(static::dictionarySmokingLabels())],
            [['alcohol'],   'in', 'range' => array_keys(static::dictionaryAlcoholLabels())],

            [['email', 'slug'], 'unique'],
            [
                ['vk_id'],
                'unique',
                'targetClass' => 'common\models\User',
                'filter'      => function ($query) {
                    /* @var \common\models\UserQuery $query */
                    $query->fromVk();
                }
            ],
            [
                'title',
                'unique',
                'filter' => function ($query) {
                    /* @var \common\models\UserQuery $query */
                    $query->emailVerified();
                },
            ],

            [['comment_visible_count', 'post_count'], 'default', 'value' => 0],
            [['role'], 'default', 'value' => 'guest'],
            [
                [
                    'about',
                    'title_gen',
                    'avatar_original',
                    'first_name',
                    'last_name',
                    'timezone',
                    'sex',
                    'birthday',
                    'phone_mobile',
                    'phone_home',
                    'city_name',
                    'email',
                    'skype',
                    'twitter_slug',
                    'instagram_slug',
                    'google_plus_slug',
                    'facebook_id',
                    'facebook_name',
                    'vk_id',
                    'vk_slug',
                    'vk_can_post',
                    'vk_can_write_private_message',
                    'relation',
                    'life_main',
                    'smoking',
                    'alcohol',
                    'music',
                    'movies',
                    'tv',
                    'books',
                    'games',
                    'auth_key',
                    'password_hash',
                ],
                'default',
                'value' => null
            ],
        ];

        if ($this->getBehavior('upload') !== null) {
            $rules[] = [
                ['UploadFile'],
                'image',
                'minWidth'  => 50,
                'minHeight' => 50,
                'maxWidth'  => 3840,
                'maxHeight' => 3840
            ];
            $rules[] = [['UploadExternalUrl'], 'url', 'enableIDN' => true];
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $auto = parent::scenarios();
        $rules = [
            'administrator'    => $auto[static::SCENARIO_DEFAULT],
            'signup'           => ['password_hash', 'role', 'email', 'title'],
            'profile'          => [
                'title',
                'phone_mobile',
                'city_name',
                'skype',
                'twitter_slug',
                'instagram_slug',
                'google_plus_slug',
                'facebook_id',
                'vk_id',
                'sex',
                'birthday',
                'UploadFile',
                'passwordNew',
            ],
            'commentFormGuest' => ['email', 'title'],
            'commentFormVk'    => [
                'title',
                'email',
                'title_gen',
                'avatar_original',
                'first_name',
                'last_name',
                'phone_mobile',
                'phone_home',
                'city_name',
                'skype',
                'twitter_slug',
                'instagram_slug',
                'facebook_name',
                'vk_slug',
                'music',
                'movies',
                'tv',
                'books',
                'games',
                'avatar',
                'facebook_id',
                'vk_id',
                'sex',
                'relation',
                'life_main',
                'smoking',
                'alcohol',
                'vk_can_post',
                'vk_can_write_private_message',
                'birthday',
                'UploadExternalUrl'
            ],
        ];

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                           => 'ID',
            'role'                         => 'Роль',
            'slug'                         => 'Для url',
            'title'                        => 'Подпись',
            'avatar'                       => 'Аватар',
            'first_name'                   => 'Имя',
            'last_name'                    => 'Фамилия',
            'timezone'                     => Yii::t('common', 'Timezone'),
            'email'                        => 'Email',
            'title_gen'                    => 'Подпись в рп',
            'avatar_original'              => 'Аватара источник',
            'sex'                          => 'Пол',
            'birthday'                     => 'День рождения',
            'phone_mobile'                 => 'Номер мобильного',
            'phone_home'                   => 'Номер домашнего',
            'city_name'                    => 'Город',
            'skype'                        => 'Skype',
            'twitter_slug'                 => 'Аккаунт в Twitter',
            'instagram_slug'               => 'Аккаунт в Instagram',
            'facebook_id'                  => 'Facebook ID',
            'facebook_name'                => 'Подпись в Facebook',
            'vk_id'                        => 'Вконтакте ID',
            'vk_slug'                      => 'Аккаунт во Вконтакте',
            'google_plus_slug'             => 'Аккаунт в Google Plus',
            'vk_can_post'                  => 'Открыта стена во вконтакте?',
            'vk_can_write_private_message' => 'Открыта личка во вконтакте?',
            'relation'                     => 'Семейное положение',
            'life_main'                    => 'Главное в жизни',
            'smoking'                      => 'Отношение к курению',
            'alcohol'                      => 'Отношение к алкоголю',
            'music'                        => 'Любимая музыка',
            'movies'                       => 'Любимые фильмы',
            'tv'                           => 'Любимые телешоу',
            'books'                        => 'Любимые книги',
            'games'                        => 'Любимые игры',
            'about'                        => 'О себе',
            'password_hash'                => 'Хеш пароля',
            'auth_key'                     => 'Ключ авторизации',
            'created'                      => 'Зарегистрирован',
            'updated'                      => 'Обновлён',
            'comment_visible_count'        => 'Комментариев',
            'post_count'                   => 'Материалов',
            'passwordNew'                  => 'Новый пароль',
            'UploadFile'                   => 'локальную',
            'UploadExternalUrl'            => 'из интернета',
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = parent::fields();
        unset(
            $fields['role'],
            $fields['password_hash'],
            $fields['auth_key'],
            $fields['created'],
            $fields['updated']
        );
        return $fields;
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->isAttributeChanged('title')) {
                    if (empty($this->avatar) && ($insert || strpos($this->avatar, Yii::$app->params['directoryAvatars']) === 1)) {
                        $this->avatar = static::getAvatarUrlByString($this->title, empty($this->email) ? $this->vk_id : $this->email);
                    }

                    $this->setSlugAuto();
                }

                if ($this->getScenario() === 'administrator') {
                    $this->setPassword(Yii::$app->getSecurity()->generateRandomString());
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert && $this->getScenario() === 'administrator') {
            static::sendEmailInvite($this->email);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return PostQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['author_id' => 'id']);
    }

    /**
     * @return CommentQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['author_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotos()
    {
        return $this->hasMany(Photo::className(), ['author_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'upload' => [
                'class'     => UploadBehavior::className(),
                'attribute' => 'avatar',
            ],
        ];
    }

    /**
     * @return UserQuery
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @param array|User $user
     * @return array
     */
    public static function getUrlParams($user)
    {
        return [
            'user/view',
            'slug' => $user['slug'],
        ];
    }

    /**
     * @param boolean $scheme
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function getUrl($scheme = false)
    {
        return Url::toRouteFrontend(static::getUrlParams($this), $scheme);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::find()
            ->select([
                'id',
                'role',
                'title',
                'first_name',
                'last_name',
                'email',
                'avatar',

                'password_hash',
                'auth_key',
            ])
            ->where(['id' => $id])
            ->one();
    }

    /**
     * @inheritdoc
     * @throws \yii\base\NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @return string
     */
    public function getPasswordNew()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @param string $password
     * @return boolean
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model.
     *
     * @param string $password
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * @param string $password
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\Exception
     */
    public function setPasswordNew($password)
    {
        if ($password) {
            $this->setPassword($password);
        }
    }

    /**
     * Generates "remember me" key.
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\Exception
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
    }

    /**
     * @return string
     */
    public function getNameString()
    {
        if ($this->first_name || $this->last_name) {
            $parts = array_filter([$this->first_name, $this->last_name], function ($item) {
                return !empty($item);
            });

            return implode(' ', $parts);
        } else {
            return $this->title;
        }
    }

    /**
     * @param string $email
     * @return bool
     */
    public static function sendEmailInvite($email)
    {
        $user = User::find()
            ->tokenByEmail($email)
            ->addSelect('title, role')
            ->one();

        return Yii::$app->getMailer()
            ->compose(
                [
                    'html' => 'authInvite-html',
                    'text' => 'authInvite-text',
                ],
                [
                    'username'   => $user->title,
                    'token'      => $user->token,
                    'toFrontend' => !in_array($user->role, static::$rolesBackend, true),
                ]
            )
            ->setTo($email)
            ->setSubject(Yii::t('common', 'Invitation'))
            ->send();
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public static function dictionaryRole()
    {
        return Yii::$app->getDb()->createCommand('SELECT unnest(enum_range(NULL::user_roles))')->queryColumn();
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public static function dictionaryRoleLabels()
    {
        $roles = static::dictionaryRole();
        return array_combine($roles, $roles);
    }

    /**
     * @return array
     */
    public static function dictionarySexLabels()
    {
        return [
            1 => 'женский',
            2 => 'мужской',
        ];
    }

    /**
     * @return array
     */
    public static function dictionaryRelationLabels()
    {
        return [
            1 => 'не женат/не замужем',
            2 => 'есть друг/есть подруга',
            3 => 'помолвлен/помолвлена',
            4 => 'женат/замужем',
            5 => 'всё сложно',
            6 => 'в активном поиске',
            7 => 'влюблён/влюблена',
        ];
    }

    /**
     * @return array
     */
    public static function dictionaryLifeMainLabels()
    {
        return [
            1 => 'семья и дети',
            2 => 'карьера и деньги',
            3 => 'развлечения и отдых',
            4 => 'наука и исследования',
            5 => 'совершенствование мира',
            6 => 'саморазвитие',
            7 => 'красота и искусстсво',
            8 => 'слава и влияние',
        ];
    }

    /**
     * @return array
     */
    public static function dictionarySmokingLabels()
    {
        return [
            1 => 'резко негативное',
            2 => 'негативное',
            3 => 'нейтральное',
            4 => 'компромиссное',
            5 => 'положительное',
        ];
    }

    /**
     * @return array
     */
    public static function dictionaryAlcoholLabels()
    {
        return [
            1 => 'резко негативное',
            2 => 'негативное',
            3 => 'нейтральное',
            4 => 'компромиссное',
            5 => 'положительное',
        ];
    }

    /**
     * Sets slug to user using Inflector
     */
    public function setSlugAuto()
    {
        if (!empty($this->slug)) {
            return;
        }

        $slug = Inflector::slug($this->title);
        if (empty($slug)) {
            $slug = 'hz';
        } elseif (strlen($slug) > 26) {
            $slug = substr($slug, 0, 26);
        }
        if ($slug === 'hz' || static::find()->where(['slug' => $slug])->exists()) {
            // человек должен быть очень невезучим, чтобы не пройти это
            $slug .= '-' . mt_rand(1111, 9999);
        }

        $this->slug = $slug;
    }

    /**
     * @param array $data
     * @throws \yii\base\InvalidParamException
     */
    public function loadFromVk($data)
    {
        $this->title_gen = ArrayHelper::getValue($data, 'first_name_gen') . ' ' . ArrayHelper::getValue($data, 'last_name_gen');
        $this->setSlugAuto();

        if ($this->getIsNewRecord() || $this->getOldAttribute('avatar_original') === '' || $this->getOldAttribute('avatar') === '') {
            $this->avatar_original = ArrayHelper::getValue($data, 'photo_max');
            if (!empty($this->avatar_original)) {
                $this->UploadExternalUrl = $this->avatar_original;
            }
        }

        if (array_key_exists('bdate', $data)) {
            $value = explode('.', $data['bdate']);
            if (count($value) === 3) {
                if (strlen($value[1]) === 1) {
                    $value[1] = '0' . $value[1];
                }
                if (strlen($value[0]) === 1) {
                    $value[0] = '0' . $value[0];
                }
                $this->birthday = $value[2] . '-' . $value[1] . '-' . $value[0];
            }
        }

        $this->phone_mobile                 = ArrayHelper::getValue($data, 'mobile_phone');
        $this->phone_home                   = ArrayHelper::getValue($data, 'home_phone');
        $this->city_name                    = ArrayHelper::getValue($data, 'city.title');
        $this->skype                        = ArrayHelper::getValue($data, 'skype');
        $this->twitter_slug                 = ArrayHelper::getValue($data, 'twitter');
        $this->instagram_slug               = ArrayHelper::getValue($data, 'instagram');
        $this->facebook_id                  = ArrayHelper::getValue($data, 'facebook');
        $this->facebook_name                = ArrayHelper::getValue($data, 'facebook_name');
        $this->vk_slug                      = ArrayHelper::getValue($data, 'screen_name');
        $this->vk_can_post                  = ArrayHelper::getValue($data, 'can_post');
        $this->vk_can_write_private_message = ArrayHelper::getValue($data, 'can_write_private_message');
        foreach ([
            'first_name',
            'last_name',
            'sex',

            'relation',
            'life_main',
            'smoking',
            'alcohol',
            'music',
            'movies',
            'tv',
            'books',
            'games'
        ] as $attribute) {
            $this->{$attribute} = ArrayHelper::getValue($data, $attribute);
        }

        if ($this->validate([
            'sex',

            'birthday',

            'phone_mobile',
            'phone_home',
            'city_name',
            'skype',
            'twitter_slug',
            'instagram_slug',
            'facebook_id',
            'facebook_name',

            'relation',
            'life_main',
            'smoking',
            'alcohol',
            'music',
            'movies',
            'tv',
            'books',
            'games'
        ]) === false) {
            foreach ($this->getErrors() as $attribute => $hole) {
                $this->{$attribute} = null;
            }
        }
    }

    /**
     * @param string $title
     * @return string
     */
    public static function getAvatarSymbolByString($title)
    {
        preg_match('/([a-zа-еж-щэ-я])/iu', $title, $matches);
        $char = empty($matches[1]) ? 'Х' : mb_strtoupper($matches[1], Yii::$app->charset);
        return $char;
    }

    /**
     * @param string $title
     * @return string
     */
    public static function getAvatarColorKeyByString($title)
    {
        $hash = md5($title);
        return $hash[0];
    }

    /**
     * @param string $title
     * @param string $titleForColor
     * @return string
     */
    public static function getAvatarUrlByString($title, $titleForColor = null)
    {
        if (empty($titleForColor)) {
            $titleForColor = $title;
        }
        return '/' . Yii::$app->params['directoryAvatars'] . '/' . static::getAvatarColorKeyByString($titleForColor) . bin2hex(static::getAvatarSymbolByString($title)) . '.png';
    }
}
