<?php

namespace common\models;

use \yii\db\ActiveQuery;

/**
 * PostFolderQuery is ActiveQuery with PostFolder scopes.
 *
 * @method PostFolder|array|null one($db = null)
 * @method PostFolder[]|array all($db = null)
 */
class PostFolderQuery extends ActiveQuery
{
    /**
     * @return PostFolderQuery
     */
    public function innerJoinPostPublished()
    {
        $this->innerJoinWith([
            'post' => function ($query) {
                /** @var $query PostQuery */
                $query
                    ->select([
                        'id',
                        'published' => $query::getAsTimestamp('published'),
                    ])
                    ->isPublished();
            },
        ]);

        return $this;
    }

    /**
     * @return PostFolderQuery
     */
    public function forSitemapTimeline()
    {
        $this
            ->innerJoinPostPublished()
            ->innerJoinWith([
                'folder' => function ($query) {
                    /** @var $query FolderQuery */
                    $query->select([
                        'id',
                        'path',
                    ]);
                },
            ])
            ->orderBy([
                'post_id'   => SORT_ASC,
                'folder_id' => SORT_ASC,
            ]);

        return $this;
    }
}
