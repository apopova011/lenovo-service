<?php

namespace common\models;

use Yii;
use \yii\db\ActiveRecord;
use \frontend\models\MainPage;

/**
 * This is the model class for table "feed_navigation".
 *
 * @property integer      $id
 * @property string       $page_resource_type
 * @property integer|null $page_resource_id
 * @property string       $link_resource_type
 * @property integer|null $link_resource_id
 * @property string       $url
 * @property string       $name
 * @property integer      $order
 *
 * @property Folder|Tag           $page
 * @property Folder|Tag|Post|Link $link
 */
class FeedNavigation extends ActiveRecord
{
    /**
     * User friendly russian label.
     */
    const LABEL_SINGULAR_ru_RU = 'Меню';

    /**
     * User friendly russian label.
     */
    const LABEL_PLURAL_ru_RU = 'Меню';

    /**
     * @var string
     */
    public $link_resource_name;

    /**
     * @return array
     */
    public static function pageTypes()
    {
        return [
            'folder'    => 'Директория',
            'tag'       => 'Тэг',
            'main_page' => 'Главная',
        ];
    }

    /**
     * @return array
     */
    public static function linkTypes()
    {
        return [
            'folder'        => 'Директория',
            'link'          => 'Ссылка',
            'post-for-feed' => 'Материал',
            'post-section'  => 'Раздел',
            'tag'           => 'Тэг',
            'main_page'     => 'Главная',
        ];
    }

    /**
     * @return array
     */
    public static function modelClasses()
    {
        return [
            'post-for-feed' => PostForFeed::className(),
            'post-section'  => PostSection::className(),
            'folder'        => Folder::className(),
            'tag'           => Tag::className(),
            'link'          => Link::className(),
        ];
    }

    /**
     * @return array
     */
    public static function pageModels()
    {
        return [
            'main_page'  => new MainPage(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feed_navigation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url', 'page_resource_type', 'link_resource_type'], 'filter', 'filter' => 'trim'],
            [['name', 'url', 'page_resource_type', 'link_resource_type'], 'required'],
            [['name', 'page_resource_type', 'link_resource_type'], 'string', 'max' =>  95],
            [['url'],                                              'string', 'max' => 255],
            [['order'], 'integer'],
            [['page_resource_id', 'link_resource_id'], 'default', 'value' => null],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $auto = parent::scenarios();
        return [
            'administrator' => $auto[static::SCENARIO_DEFAULT],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => 'ID',
            'page_resource_type' => 'Тип страницы',
            'page_resource_id'   => 'Страница',
            'link_resource_type' => 'Тип ссылки',
            'link_resource_id'   => 'Ссылка на',
            'name'               => 'Подпись',
            'url'                => 'Url',
            'order'              => 'Порядок',
        ];
    }

    /**
     * @return PostQuery|FolderQuery|TagQuery|\yii\db\ActiveQuery|MainPage|bool
     */
    public function getPage()
    {
        $pageResourceType = $this->page_resource_type;

        $pageModels = static::pageModels();
        if (array_key_exists($pageResourceType, $pageModels)) {
            return $pageModels[$pageResourceType];
        }

        if (!$this->page_resource_id) {
            return false;
        }

        $modelClasses = static::modelClasses();
        if (array_key_exists($pageResourceType, $modelClasses)) {
            /** @var Post|Folder|Tag|Link $modelClass */
            $modelClass = $modelClasses[$pageResourceType];

            return $this->hasOne($modelClass::className(), ['id' => 'page_resource_id']);
        }

        return false;
    }

    /**
     * @return PostQuery|FolderQuery|TagQuery|\yii\db\ActiveQuery|MainPage|bool
     */
    public function getLink()
    {
        $linkResourceType = $this->link_resource_type;

        $pageModels = static::pageModels();
        if (array_key_exists($linkResourceType, $pageModels)) {
            return $pageModels[$linkResourceType];
        }

        if (!$this->link_resource_id) {
            return false;
        }

        $modelClasses = static::modelClasses();
        if (array_key_exists($linkResourceType, $modelClasses)) {
            /** @var Post|Folder|Tag|Link $modelClass */
            $modelClass = $modelClasses[$linkResourceType];

            return $this->hasOne($modelClass::className(), ['id' => 'link_resource_id']);
        }

        return false;
    }

    /**
     * @inheritdoc
     * @return FeedNavigationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FeedNavigationQuery(get_called_class());
    }

    /**
     * @param array $navigation
     * @return array
     */
    public static function activeByUrl($navigation)
    {
        $currentUrl = Yii::$app->getRequest()->url;

        foreach ($navigation as &$item) {
            if ($currentUrl === $item['url']) {
                $item['active'] = true;
                return $navigation;
            }
        }

        $navigation[0]['active'] = true;

        return $navigation;
    }

    /**
     * @param array $navigation
     * @return array
     */
    public static function forNavigationWidget($navigation)
    {
        foreach ($navigation as &$item) {
            $item['template'] = '<a href="{url}"><h2>{label}</h2></a>';
        }

        return $navigation;
    }

    /**
     * @param string $type
     * @param integer $id
     * @return string
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     */
    public static function getResourceUrl($type, $id)
    {
        $pageModels = static::pageModels();
        if (array_key_exists($type, $pageModels)) {
            $model = $pageModels[$type];
            $url = $model->getUrl(true);
        } else {
            $url = [
                $type . '/update',
                'id' => $id,
            ];
        }

        return $url;
    }
}
