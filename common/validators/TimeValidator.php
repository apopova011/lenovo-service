<?php

namespace common\validators;

use \yii\validators\RegularExpressionValidator;

/**
 * TimeValidator validates that the attribute value is a valid time.
 *
 * @author Kostya M <primat@list.ru>
 */
class TimeValidator extends RegularExpressionValidator
{
    /**
     * @inheritdoc
     */
    public $pattern = '/^((0?[0-9]|1[0-9]|2[0-3])\:)?[0-5][0-9]\:[0-5][0-9]$/';

    /**
     * @inheritdoc
     */
    public $message = '{attribute} должен содержать время.';
}
