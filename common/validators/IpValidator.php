<?php

namespace common\validators;

use \yii\validators\RegularExpressionValidator;

/**
 * IpValidator validates that the attribute value is a valid IP.
 *
 * @author Kostya M <primat@list.ru>
 */
class IpValidator extends RegularExpressionValidator
{
    /**
     * @inheritdoc
     */
    public $pattern = '/^[A-Fa-f0-9\:\.]+$/';

    /**
     * @inheritdoc
     */
    public $message = '{attribute} должен содержать корректный IP.';
}
