<?php

namespace common\validators;

use \yii\validators\RegularExpressionValidator;

/**
 * SlugGooglePlusValidator validates that the attribute value is a valid google plus slug.
 *
 * @author Kostya M <primat@list.ru>
 */
class SlugGooglePlusValidator extends RegularExpressionValidator
{
    /**
     * @inheritdoc
     */
    public $pattern = '/^([0-9]+|\+[A-Za-z0-9A-ЯЁа-яё]+)$/u';

    /**
     * @inheritdoc
     */
    public $message = '{attribute} должен содержать корректную часть url.';
}
