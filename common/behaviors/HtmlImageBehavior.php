<?php

namespace common\behaviors;

use Yii;
use \yii\base\Behavior;
use \yii\base\Exception;
use \yii\db\ActiveRecord;
use \yii\helpers\ArrayHelper;
use \yii\httpclient\Client;
use \common\helpers\File;
use \common\helpers\Url;
use \common\models\Photo;

/**
 * HtmlImageBehavior is behavior for images
 *
 * @property ActiveRecord|\common\models\Post $owner
 */
class HtmlImageBehavior extends Behavior
{
    /**
     * @var string
     */
    public $attribute = 'content';

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    /**
     * @param \yii\base\ModelEvent $event
     * @throws \yii\base\InvalidParamException
     * @throws \lsolesen\pel\PelInvalidDataException
     * @throws \lsolesen\pel\PelJpegInvalidMarkerException
     */
    public function beforeSave($event)
    {
        $attributeChanged = $this->owner->isAttributeChanged($this->attribute);
        $pathMedia = Yii::getAlias('@media');

        if ($attributeChanged) {
            $imageDomain = Url::imageDomain();
            $pattern = '/<img[^>]+src="((?!((http\:)?\/\/' . str_replace('.', '\.', Yii::$app->params['frontendDomain']) . ')?\/)[^"]+)/iu';
            preg_match_all($pattern, $this->owner->{$this->attribute}, $matches, PREG_SET_ORDER);
            foreach ($matches as $attributes) {
                $src = $attributes[1];

                $client = new Client();
                $response = $client
                    ->createRequest()
                    ->setMethod('GET')
                    ->setUrl(html_entity_decode($src))
                    ->addOptions([
                        CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0',
                        CURLOPT_SSL_VERIFYPEER => false,
                        CURLOPT_SSL_VERIFYPEER => false,
                    ])
                    ->send();
                if ($response->getIsOk()) {
                    $subUrl = $this->owner->slug;
                    $fullPath = preg_match('/([^\/]+)(?:$|\/$)/', $src, $url) ? $url[1] : $src;
                    $fullPath = mb_strrpos($fullPath, '.');
                    $subUrl .= ($fullPath === false ? '.jpg' : mb_substr($src, $fullPath));
                    $questionMarkPos = mb_strpos($subUrl, '?');
                    if ($questionMarkPos !== false) {
                        $subUrl = mb_substr($subUrl, 0, $questionMarkPos);
                    }

                    list($fullPath, $subUrl) = UploadBehavior::getFullPath(date('m'), $subUrl);
                    if (file_put_contents($fullPath, $response->getContent())) {
                        if (function_exists('chgrp') && function_exists('chown')) {
                            @chgrp($fullPath, Yii::$app->params['ownerGroup']);
                            @chown($fullPath, Yii::$app->params['ownerUser']);
                        }

                        $this->owner->{$this->attribute} = str_replace($src, $imageDomain . Url::thumbnail($subUrl), $this->owner->{$this->attribute});
                    }
                }
            }

            if (preg_match_all('/(<a[^>]+>)?(<img[^>]+>)(<\/a>)?/iu', $this->owner->{$this->attribute}, $images, PREG_SET_ORDER)) {
                foreach ($images as $imageTags) {
                    if (!preg_match_all('/(src|width|height)="([^"]+)"/iu', $imageTags[2], $imageTagAttributes, PREG_SET_ORDER)) {
                        continue;
                    }
                    $imageTagAttributes = ArrayHelper::map($imageTagAttributes, 1, 2);

                    $src = Url::getSubUrlFromFull($imageTagAttributes['src']);
                    if ($src === false) {
                        continue;
                    }

                    if (preg_match('/\-(\d+)x(\d+)\.(jpe?g|png|gif)$/i', $imageTagAttributes['src'], $sizePreset)) {
                        $src = str_replace($sizePreset[0], '.' . $sizePreset[3], $src);
                    }

                    $fullPath = $pathMedia . $src;
                    if (!file_exists($fullPath)) {
                        continue;
                    }

                    $sizeImageWidth = getimagesize($fullPath);
                    if ($sizeImageWidth === false) {
                        continue;
                    }

                    list($sizeImageWidth, $sizeImageHeight) = $sizeImageWidth;

                    if ($sizeImageWidth > 768 && $sizeImageHeight > 432) {
                        // не обернуто ссылкой на большое изображение
                        if ($imageTags[1] === '' && empty($imageTags[3])) {
                            if ($sizeImageWidth > 1920 || $sizeImageHeight > 1920) {
                                if ($sizeImageWidth > $sizeImageHeight) {
                                    $fullPath = Url::thumbnail($src, 1920, round($sizeImageHeight * 1920 / $sizeImageWidth));
                                } else {
                                    $fullPath = Url::thumbnail($src, round($sizeImageWidth * 1920 / $sizeImageHeight), 1920);
                                }
                            } else {
                                $fullPath = Url::thumbnail($src);
                            }

                            $this->owner->{$this->attribute} = str_replace(
                                $imageTags[2],
                                '<a class="image-big" href="' . $imageDomain . $fullPath . '" target="_blank">' . $imageTags[2] . '</a>',
                                $this->owner->{$this->attribute}
                            );
                        }
                    } else {
                        // обернуто ссылкой на маленькое изображение
                        if (!($imageTags[1] === '' || empty($imageTags[3]))) {
                            $this->owner->{$this->attribute} = str_replace(
                                $imageTags[0],
                                $imageTags[2],
                                $this->owner->{$this->attribute}
                            );
                        }
                    }

                    // размер исходного изображения совпадает с указанными размерами
                    if ($sizeImageWidth === $imageTagAttributes['width'] && $sizeImageHeight === $imageTagAttributes['height']) {
                        // если при этом была пресетная дописка - её надо очистить
                        if ($sizePreset) {
                            $this->owner->{$this->attribute} = str_replace(
                                $imageTags[2],
                                str_replace($sizePreset[0], '.' . $sizePreset[3], $imageTags[2]),
                                $this->owner->{$this->attribute}
                            );
                        }

                        continue;
                    }

                    // текущий пресет совпадает с указанными размерами
                    if ($sizePreset && $sizePreset[1] === $imageTagAttributes['width'] && $sizePreset[2] === $imageTagAttributes['height']) {
                        continue;
                    }
                    $this->owner->{$this->attribute} = str_replace(
                        $imageTags[2],
                        str_replace($imageTagAttributes['src'], $imageDomain . Url::thumbnail($src, $imageTagAttributes['width'], $imageTagAttributes['height']), $imageTags[2]),
                        $this->owner->{$this->attribute}
                    );
                }
            }
        }

        if ($attributeChanged || $this->owner->isAttributeChanged('published') || $this->owner->isAttributeChanged('author_id', false)) {
            preg_match_all('/<img[^>]+?src="([^"]+)"[^>]+?data-authors="true"/', $this->owner->{$this->attribute}, $authorsMatches, PREG_SET_ORDER);
            $pathMediaLength = mb_strlen($pathMedia);
            $deletedPhotoIds = Photo::find()->select(['id'])->andWhere(['post_id' => $this->owner->id])->column();

            foreach ($authorsMatches as $authorsImage) {
                $src = $authorsImage[1];
                $subPath = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, Url::getSubUrlFromFull($src));
                $fullPath = $pathMedia . $subPath;
                $originalFile = preg_replace('/-\d+x\d+\./', '.', $fullPath);

                if (file_exists($originalFile)) {
                    $file = $originalFile;
                } else if (file_exists($fullPath)) {
                    $file = $fullPath;
                } else {
                    Yii::error('Image ' . $fullPath . ' from post ' . $this->owner->id . 'doesn\'t exists', 'photo-add-metadata');
                    continue;
                }

                $imageType = exif_imagetype($file);

                if (!empty($imageType)) {
                    $imageType = image_type_to_mime_type($imageType);
                }

                $fileSubPath = mb_substr($file, $pathMediaLength);
                $photo = Photo::find()->andWhere(['file' => $fileSubPath])->andWhere(['post_id' => $this->owner->id])->one();

                if (empty($photo)) {
                    $photo = new Photo();
                    $photo->file = $fileSubPath;
                    $photo->content_type = $imageType;
                    $photo->post_id = $this->owner->id;
                }

                $photo->post_published = $this->owner->published;

                if ($photo->author_id !== $this->owner->author_id) {
                    $photo->author_id    = $this->owner->author_id;
                    $photo->author_title = $this->owner->author->title;
                    $photo->author_email = $this->owner->author->email;
                }

                if (preg_match('/alt="([^"]+?)"/', $authorsImage[0], $alt)) {
                    $photo->post_content_img_alt = $alt[1];
                }

                $fileBackup = $file . '_backup';

                try {
                    if ($imageType === 'image/jpeg' && (empty($photo->exif_updated) || !empty($photo->getDirtyAttributes()))) {
                        copy($file, $fileBackup);

                        if (!File::addJpegMetadata($file, $photo)) {
                            throw new Exception('Can\'t add metadata to ' . $file);
                        }

                        $photo->exif_updated = date('Y-m-d H:i:s');
                    }
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), 'photo-add-metadata');
                } finally {
                    $photo->setScenario('administrator');
                    $photo->save();

                    $id = array_search($photo->id, $deletedPhotoIds, true);
                    if ($id !== false) {
                        unset($deletedPhotoIds[$id]);
                    }

                    if (file_exists($fileBackup)) {
                        if (filesize($file) < 0.8 * filesize($fileBackup)) {
                            unlink($file);
                            rename($fileBackup, $file);
                        } else {
                            unlink($fileBackup);
                        }
                    }
                }
            }

            if (!empty($deletedPhotoIds)) {
                Photo::deleteAll(['id' => $deletedPhotoIds]);
            }
        }
    }
}
