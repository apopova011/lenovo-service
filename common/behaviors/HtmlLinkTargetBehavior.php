<?php

namespace common\behaviors;

use \yii\base\Behavior;
use \yii\db\ActiveRecord;
use \common\helpers\StringHelper;

/**
 * HtmlLinkTargetBehavior is behavior for adding target="_blank" to links
 *
 * @property ActiveRecord $owner
 */
class HtmlLinkTargetBehavior extends Behavior
{
    /**
     * @var string
     */
    public $attribute = 'content';

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    /**
     * @param \yii\base\ModelEvent $event
     */
    public function beforeSave($event)
    {
        if (!$this->owner->isAttributeChanged($this->attribute)) {
            return;
        }

        preg_match_all('/(<a[^>]+href="[^"]+")(?![^>]+target="_blank")/ui', $this->owner->{$this->attribute}, $matches, PREG_SET_ORDER);
        if (!empty($matches)) {
            $search = $replace = [];
            foreach ($matches as $match) {
                $search[] = $match[1];
                $replace[] = $match[1] . ' target="_blank"';
            }

            $this->owner->{$this->attribute} = StringHelper::mb_str_replace($search, $replace, $this->owner->{$this->attribute});
        }
    }
}
