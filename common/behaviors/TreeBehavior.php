<?php

namespace common\behaviors;

use \yii\base\Behavior;
use \yii\db\ActiveRecord;

/**
 * TreeBehavior is behavior for materialized path tree.
 *
 * @author Kostya M <primat@list.ru>
 *
 * @property ActiveRecord $owner
 */
class TreeBehavior extends Behavior
{
    /**
     * @var string Attribute name for materialized path storage.
     */
    public $attributeTree = 'path';

    /**
     * @var string Attribute name of which are path.
     */
    public $attributeTreeElement = 'id';

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
        ];
    }

    /**
     * @param \yii\base\ModelEvent $event
     */
    public function beforeValidate($event)
    {
        $value = $this->owner->getDirtyAttributes([$this->attributeTreeElement]);
        if (empty($value)) {
            return;
        }

        // если изменился, то меняем path
        $value = static::convertUrlToLtree($value[$this->attributeTreeElement]);
        $parent = $this->getPathParent();
        $this->owner->{$this->attributeTree} = ($parent === '') ? $value : ($parent . '.' . $value);
    }

    /**
     * @return boolean
     */
    public function getIsChild()
    {
        return strpos($this->owner->{$this->attributeTree}, '.') !== false;
    }

    /**
     * Getter for virtual attribute.
     * @return string|false
     */
    public function getParentPath()
    {
        $value = $this->owner->{$this->attributeTree};

        $pos = strrpos($value, '.');
        if ($pos === false) {
            $value = false;
        } else {
            $value = substr($value, 0, $pos);
        }

        return $value;
    }

    /**
     * Return parent key.
     * @return null|string
     */
    public function getParentKey()
    {
        $path = $this->getParentPath();
        if ($path === false) {
            return null;
        }

        $lastPos = strrpos($path, '.');
        if ($lastPos === false) {
            return $path;
        }
        return substr($path, $lastPos);
    }

    /**
     * Getter for virtual attribute.
     * @return string
     */
    public function getPathParent()
    {
        $path = $this->getParentPath();
        return $path === false ? '' : $path;
    }

    /**
     * Setter for virtual attribute.
     * @param string $value
     */
    public function setPathParent($value)
    {
        if ($value !== '') {
            $value .= '.';
        }
        $this->owner->{$this->attributeTree} = $value . static::convertUrlToLtree($this->owner->{$this->attributeTreeElement});
    }

    /**
     * @return \yii\db\ActiveRecord[]
     */
    public function getParentPathModels()
    {
        $path = $this->getParentPath();
        if ($path === false) {
            return [];
        }

        $model = $this->owner;
        return $model::find()->andWhere([$this->attributeTreeElement => explode('.', $path)])->all();
    }

    /**
     * @return \yii\db\ActiveRecord[]
     */
    public function getPathModels()
    {
        $pathArray = $this->getParentPathModels();
        $pathArray[] = $this->owner;
        return $pathArray;
    }

    /**
     * @param string $value
     * @return string
     */
    public static function convertLtreeToUrl($value)
    {
        return str_replace(
            ['.', '_'],
            ['/', '-'],
            $value
        );
    }

    /**
     * @param string $value
     * @return string
     */
    public static function convertUrlToLtree($value)
    {
        return str_replace(
            ['/', '-'],
            ['.', '_'],
            $value
        );
    }

    /**
     * @return string
     */
    public function getPathForUrl()
    {
        return static::convertLtreeToUrl($this->owner->{$this->attributeTree});
    }

    /**
     * @param string $value
     */
    public function setPathForUrl($value)
    {
        $this->owner->{$this->attributeTree} = static::convertUrlToLtree($value);
    }
}
