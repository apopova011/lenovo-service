<?php

namespace common\behaviors;

use Yii;
use \yii\base\Behavior;
use \yii\db\ActiveRecord;
use \common\helpers\StringHelper;

/**
 * HtmlLinkExternalBehavior is behavior for nofollow and delete.
 *
 * @property ActiveRecord $owner
 */
class HtmlLinkExternalBehavior extends Behavior
{
    /**
     * @var string
     */
    public $attributeHtml = 'content';

    /**
     * @var string
     */
    public $attributePublished = 'published';

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    /**
     * @param \yii\base\ModelEvent $event
     * @throws \yii\base\InvalidParamException
     */
    public function beforeSave($event)
    {
        if (empty($this->owner->{$this->attributeHtml}) || empty($this->owner->{$this->attributePublished})) {
            return;
        }

        $content = $this->owner->{$this->attributeHtml};
        if (preg_match_all(
            '/(<a[^>]+href="(?!(?:mailto\:|javascript\:|\/|(?:https?\:)?\/\/' . str_replace('.', '\.', Yii::$app->params['frontendDomain']) . '))(?:https?\:)?\/\/([^"\/]++)[^>]++)>([^<]++)<\/a>/ui',
            $content,
            $matches,
            PREG_SET_ORDER
        )) {
            $domainWhiteList = require(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'external_domains_whitelist.php');
            $additionalDomainWhiteList = Yii::getAlias('@common') . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . 'external_domains_top.php';
            if (file_exists($additionalDomainWhiteList)) {
                $additionalDomainWhiteList = require $additionalDomainWhiteList;
                $domainWhiteList = array_merge($domainWhiteList, $additionalDomainWhiteList);
                $domainWhiteList = array_unique($domainWhiteList);
            }

            $isOld = strtotime($this->owner->{$this->attributePublished}) < strtotime('-6 month');

            $contentNew = $content;
            foreach ($matches as $match) {
                $inWhiteList = in_array(mb_strtolower($match[2]), $domainWhiteList, true);
                $containNofollow = mb_strpos($match[1], ' rel="nofollow"') > 0;

                if (
                    ($inWhiteList && !$containNofollow) ||
                    (!$inWhiteList && $containNofollow && !$isOld)
                ) {
                    // должен содержать и содержит и не старый ИЛИ не должен содержать и не содержит
                    continue;
                }

                if ($inWhiteList) {
                    $search = $match[1];
                    $replace = StringHelper::mb_str_replace(' rel="nofollow"', '', $match[1]);
                } else {
                    if ($isOld) {
                        $search  = $match[0];
                        $replace = $match[3];
                    } else {
                        $search  = $match[1];
                        $replace = $match[1] . ' rel="nofollow"';
                    }
                }

                $contentNew = StringHelper::mb_str_replace(
                    $search,
                    $replace,
                    $contentNew
                );
            }
            if ($contentNew !== $content) {
                $this->owner->{$this->attributeHtml} = $contentNew;
            }
        }
    }
}
