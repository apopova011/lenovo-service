<?php

namespace common\components;

use Exception;
use RuntimeException;
use Yii;

class Dadata
{
    /**
     * @var string clean api base url
     */
    public static $cleanApiUrl = 'https://dadata.ru/api/v2/clean/';

    /**
     * @var string suggestions api base url
     */
    public static $suggestionsApiUrl = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/';

    /**
     * @param string $method
     * @param string $string
     * @return array|bool
     */
    public static function clean($method, $string)
    {
        $request = [$string];

        return static::getResponse(static::$cleanApiUrl . $method, $request);
    }

    /**
     * @param string $method
     * @param string $string
     * @return array|bool
     */
    public static function suggestions($method, $string)
    {
        $request = ['query' => $string];

        return static::getResponse(static::$suggestionsApiUrl . $method, $request);
    }

    /**
     * @param string $method
     * @param string $value
     * @return array
     */
    public static function getSuggestionInfo($method, $value): array
    {
        $result = [];
        $suggestions = static::suggestions($method, $value);

        if ($suggestions && array_key_exists('suggestions', $suggestions)) {
            $result = $suggestions['suggestions'][0];
        }

        return $result;
    }

    /**
     * @param string $url
     * @param array $post
     * @return array|bool
     */
    protected static function getResponse($url, array $post = [])
    {
        $result = false;

        try {
            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Token ' . static::getApiKey(),
                'X-Secret: ' . static::getSecret(),
            ]);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));

            $result = curl_exec($ch);
            $result = json_decode($result, true);

            curl_close($ch);
        } catch (Exception $e) {
            Yii::error($e->getMessage());
        }

        return $result;
    }

    /**
     * @return array
     * @throws Exception
     */
    protected static function getConfig(): array
    {
        $settings = Yii::$app->params['dadataToken'];
        if (!$settings) {
            throw new RuntimeException('Необходимо добавить dadataToken в конфиг');
        }

        return $settings;
    }

    /**
     * @return string
     * @throws Exception
     */
    protected static function getApiKey(): string
    {
        $config = static::getConfig();

        return $config['key'];
    }

    /**
     * @return string
     * @throws Exception
     */
    protected static function getSecret(): string
    {
        $config = static::getConfig();

        return $config['secret'];
    }
}
