<?php

use \yii\db\Migration;

class m151223_081230_user_created_and_updated_trigger extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'user';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        echo '    > create function ', $this->itemName, '_set_created_and_updated() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->itemName}_set_created_and_updated()
RETURNS TRIGGER AS
$$
BEGIN
  IF (TG_OP = 'INSERT') THEN
    NEW.created := NOW();
  ELSE
    IF (
      NEW.role = OLD.role AND
      NEW.slug = OLD.slug AND
      NEW.title = OLD.title AND
      NEW.avatar = OLD.avatar AND
      NEW.timezone = OLD.timezone AND
      COALESCE(NEW.first_name, '') = COALESCE(OLD.first_name, '') AND
      COALESCE(NEW.last_name, '') = COALESCE(OLD.last_name, '') AND
      COALESCE(NEW.email, '') = COALESCE(OLD.email, '') AND
      COALESCE(NEW.title_gen, '') = COALESCE(OLD.title_gen, '') AND
      COALESCE(NEW.avatar_original, '') = COALESCE(OLD.avatar_original, '') AND
      COALESCE(NEW.sex, 0) = COALESCE(OLD.sex, 0) AND
      COALESCE(NEW.birthday, '1970-01-01') = COALESCE(OLD.birthday, '1970-01-01') AND
      COALESCE(NEW.phone_mobile, '') = COALESCE(OLD.phone_mobile, '') AND
      COALESCE(NEW.phone_home, '') = COALESCE(OLD.phone_home, '') AND
      COALESCE(NEW.city_name, '') = COALESCE(OLD.city_name, '') AND
      COALESCE(NEW.skype, '') = COALESCE(OLD.skype, '') AND
      COALESCE(NEW.twitter_slug, '') = COALESCE(OLD.twitter_slug, '') AND
      COALESCE(NEW.instagram_slug, '') = COALESCE(OLD.instagram_slug, '') AND
      COALESCE(NEW.facebook_id, 0) = COALESCE(OLD.facebook_id, 0) AND
      COALESCE(NEW.facebook_name, '') = COALESCE(OLD.facebook_name, '') AND
      COALESCE(NEW.vk_id, 0) = COALESCE(OLD.vk_id, 0) AND
      COALESCE(NEW.vk_slug, '') = COALESCE(OLD.vk_slug, '') AND
      COALESCE(NEW.google_plus_slug, '') = COALESCE(OLD.google_plus_slug, '') AND
      NEW.vk_can_post = OLD.vk_can_post AND
      NEW.vk_can_write_private_message = OLD.vk_can_write_private_message AND
      COALESCE(NEW.relation, 0) = COALESCE(OLD.relation, 0) AND
      COALESCE(NEW.life_main, 0) = COALESCE(OLD.life_main, 0) AND
      COALESCE(NEW.smoking, 0) = COALESCE(OLD.smoking, 0) AND
      COALESCE(NEW.alcohol, 0) = COALESCE(OLD.alcohol, 0) AND
      COALESCE(NEW.music, '') = COALESCE(OLD.music, '') AND
      COALESCE(NEW.movies, '') = COALESCE(OLD.movies, '') AND
      COALESCE(NEW.tv, '') = COALESCE(OLD.tv, '') AND
      COALESCE(NEW.books, '') = COALESCE(OLD.books, '') AND
      COALESCE(NEW.games, '') = COALESCE(OLD.games, '') AND
      COALESCE(NEW.about, '') = COALESCE(OLD.about, '') AND
      COALESCE(NEW.about, '') = COALESCE(OLD.about, '') AND
      COALESCE(NEW.password_hash, '') = COALESCE(OLD.password_hash, '') AND
      COALESCE(NEW.auth_key, '') = COALESCE(OLD.auth_key, '')
    ) THEN
      RETURN NEW;
    END IF;
  END IF;

  NEW.updated := NOW();
  RETURN NEW;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');
        echo '    > create trigger ', $this->itemName, '_before_insert_or_update_set_created_and_updated ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TRIGGER {$this->itemName}_before_insert_or_update_set_created_and_updated
BEFORE INSERT OR UPDATE ON {$tableName}
FOR EACH ROW
EXECUTE PROCEDURE {$this->itemName}_set_created_and_updated();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');
        echo '    > drop trigger ', $this->itemName, '_before_insert_or_update_set_created_and_updated ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP TRIGGER {$this->itemName}_before_insert_or_update_set_created_and_updated ON {$tableName};
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop function ', $this->itemName, '_set_created_and_updated() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP FUNCTION {$this->itemName}_set_created_and_updated();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
