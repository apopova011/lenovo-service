<?php

use \yii\db\Migration;
use \common\models\Folder;

class m160614_130438_post_fk_folder extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'post';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->addForeignKey($this->itemName . '_fk_' . Folder::tableName(), $tableName, 'folder_id_main', Folder::tableName(), 'id', 'RESTRICT', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->dropForeignKey($this->itemName . '_fk_' . Folder::tableName(), $tableName);
    }
}
