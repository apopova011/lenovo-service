<?php

use \yii\db\Migration;
use \console\controllers\DbController;

class m160610_122139_create_ltree extends Migration
{
    /**
     * @inheritdoc
     * @throws \yii\console\Exception
     */
    public function up()
    {
        DbController::executeSqlConsole('CREATE EXTENSION ltree;');
    }

    /**
     * @inheritdoc
     * @throws \yii\console\Exception
     */
    public function down()
    {
        DbController::executeSqlConsole('DROP EXTENSION ltree;');
    }
}
