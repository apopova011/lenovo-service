<?php

use \yii\db\Migration;
use \common\models\Post;

class m160609_115809_post_image_count_function extends Migration
{
    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function up()
    {
        $postTableName = Post::tableName();

        echo '    > create function post_image_count(integer) ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION get_icon_part(ic varchar)
	RETURNS SETOF varchar AS
$$
BEGIN 
    IF ic IS NOT NULL THEN
      RETURN QUERY SELECT '`~!@#$%^&*()+|';
    ELSE 
      RETURN QUERY SELECT (regexp_matches(icon, '([^\/]+?)(\.[^\.]+)?$'))[1];
    END IF;
END
$$
LANGUAGE plpgsql
SQL
        )->execute();

       $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION get_src_part(cont varchar)
	RETURNS SETOF varchar AS
$$
BEGIN 
    IF cont IS NOT NULL THEN
      RETURN QUERY SELECT NULL;
    ELSEIF cont ~ '<img[^>]+src="[^"]' THEN 
      RETURN QUERY SELECT (regexp_matches(content, '<img[^>]+src="[^"]+?([^"\/]+?)(\-\d+x\d+)?(\.[^"\.]+)?', 'g'))[1];
    ELSE 
      RETURN QUERY SELECT NULL;
    END IF;
END
$$
LANGUAGE plpgsql
SQL
        )->execute();

        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION post_image_count(integer)
	RETURNS bigint AS
$$
    SELECT
        MIN(icon_exist) + SUM((src_part IS NOT NULL AND src_part != icon_part)::INT)
    FROM (
        SELECT
            id,
            (icon IS NOT NULL)::INT AS icon_exist,
            get_icon_part(icon) AS icon_part,
            get_src_part(content) AS src_part
        FROM {$postTableName}
        WHERE id = $1
    ) images
    GROUP BY id;
$$
LANGUAGE sql IMMUTABLE
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function down()
    {
        echo '    > drop function post_image_count(integer) ...';
        $time = microtime(true);
        $this->db->createCommand('DROP FUNCTION IF EXISTS post_image_count(integer)')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
