<?php

use \yii\db\Migration;
use \common\models\Post;

class m160927_132821_post_alter_column extends Migration
{
    /**
     * @var string
     */
    public $columnName = 'folder_id_main';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        $postTable = Post::tableName();
        $this->getDb()->createCommand(<<<SQL
ALTER TABLE {$postTable}
   ALTER COLUMN {$this->columnName} DROP NOT NULL;
SQL
)->execute();
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        $postTable = Post::tableName();
        $this->getDb()->createCommand(<<<SQL
ALTER TABLE {$postTable}
   ALTER COLUMN {$this->columnName} SET NOT NULL;
SQL
        )->execute();
    }
}
