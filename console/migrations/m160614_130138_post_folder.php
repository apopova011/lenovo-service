<?php

use \yii\db\Migration;
use \common\models\Folder;
use \common\models\Post;

class m160614_130138_post_folder extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'post_folder';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createTable($tableName, [
            'post_id' => $this->integer()->notNull(),
            'folder_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey($this->itemName . '_pk', $tableName, ['post_id', 'folder_id']);

        $this->addForeignKey($this->itemName . '_fk_' .   Post::tableName(), $tableName, 'post_id',     Post::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey($this->itemName . '_fk_' . Folder::tableName(), $tableName, 'folder_id', Folder::tableName(), 'id', 'CASCADE', 'CASCADE');

        $this->createIndex(
            $this->itemName . '_idx_folder',
            $tableName,
            'folder_id',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_post',
            $tableName,
            'post_id',
            'hash'
        );
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');
    }
}
