<?php

use \yii\db\Migration;

class m160609_115800_post_updated_trigger extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'post';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        echo '    > create function ', $this->itemName, '_set_updated() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->itemName}_set_updated()
RETURNS TRIGGER AS
$$
BEGIN
  IF (TG_OP = 'UPDATE') THEN
    IF (
      NEW.slug = OLD.slug AND
      NEW.name = OLD.name AND
      COALESCE(NEW.title_seo, '') = COALESCE(OLD.title_seo, '') AND
      NEW.description = OLD.description AND
      COALESCE(NEW.content, '') = COALESCE(OLD.content, '') AND
      COALESCE(NEW.icon, '') = COALESCE(OLD.icon, '') AND
      NEW.author_id = OLD.author_id AND
      NEW.folder_id_main  = OLD.folder_id_main AND
      COALESCE(NEW.tags_name, '') = COALESCE(OLD.tags_name, '') AND
      COALESCE(NEW.published, '1970-01-01') = COALESCE(OLD.published, '1970-01-01') AND
      NEW.is_sent_to_index = OLD.is_sent_to_index AND
      NEW.is_in_common_feed = OLD.is_in_common_feed AND
      NEW.is_allow_footer = OLD.is_allow_footer AND
      NEW.is_allow_comments = OLD.is_allow_comments AND
      NEW.is_contain_video  = OLD.is_contain_video AND
      COALESCE(NEW.video_duration, '00:00') = COALESCE(OLD.video_duration, '00:00')
    ) THEN
      RETURN NEW;
    END IF;
  END IF;

  NEW.updated := NOW();
  RETURN NEW;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');
        echo '    > create trigger ', $this->itemName, '_before_insert_or_update_set_updated ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TRIGGER {$this->itemName}_before_insert_or_update_set_updated
BEFORE INSERT OR UPDATE ON {$tableName}
FOR EACH ROW
EXECUTE PROCEDURE {$this->itemName}_set_updated();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');
        echo '    > drop trigger ', $this->itemName, '_before_insert_or_update_set_updated ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP TRIGGER {$this->itemName}_before_insert_or_update_set_updated ON {$tableName};
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop function ', $this->itemName, '_set_updated() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP FUNCTION {$this->itemName}_set_updated();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
