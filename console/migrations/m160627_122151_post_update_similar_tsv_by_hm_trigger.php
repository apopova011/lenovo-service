<?php

use \yii\db\Migration;
use \common\models\Folder;
use \common\models\PostFolder;
use \common\models\PostTag;
use \common\models\Tag;

class m160627_122151_post_update_similar_tsv_by_hm_trigger extends Migration
{
    /**
     * @var string
     */
    protected $itemUpdatedName = 'post';

    /**
     * @var array
     */
    protected $itemTriggeredNames = ['post_tag', 'post_folder'];

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        $folderTable     =     Folder::tableName();
        $tagTable        =        Tag::tableName();
        $postFolderTable = PostFolder::tableName();
        $postTagTable    =    PostTag::tableName();

        echo '    > create function ', $this->itemUpdatedName, '_update_similar_tsv_by_hm() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->itemUpdatedName}_update_similar_tsv_by_hm()
RETURNS TRIGGER AS
$$
DECLARE
  post_id_for_update integer;
BEGIN
    IF (TG_OP = 'DELETE') THEN
        post_id_for_update = OLD.post_id;
    ELSE
        post_id_for_update = NEW.post_id;
    END IF;

    UPDATE {$this->itemUpdatedName} SET similar_tsv =
        setweight(to_tsvector('text_search_config', COALESCE(title_seo, name)), 'A') ||
        setweight(to_tsvector('text_search_config', COALESCE((SELECT string_agg(f.title,                   ' ') FROM {$folderTable} f WHERE f.id IN (SELECT folder_id FROM {$postFolderTable} pf WHERE post_id = post_id_for_update)), '')), 'B') ||
        setweight(to_tsvector('text_search_config', COALESCE((SELECT string_agg(COALESCE(t.title, t.name), ' ') FROM {$tagTable}    t WHERE t.id IN (SELECT tag_id    FROM {$postTagTable}    pt WHERE post_id = post_id_for_update)), '')), 'C') ||
        setweight(to_tsvector('text_search_config', description), 'D')
        WHERE id = post_id_for_update;

    RETURN NULL;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        foreach ($this->itemTriggeredNames as $itemTriggeredName) {
            echo '    > create trigger ', $itemTriggeredName, '_after_insert_or_update_or_delete_update_', $this->itemUpdatedName, '_similar_tsv ...';
            $time = microtime(true);
            $this->db->createCommand(<<<SQL
CREATE TRIGGER {$itemTriggeredName}_after_insert_or_update_or_delete_update_{$this->itemUpdatedName}_similar_tsv
AFTER INSERT OR UPDATE OR DELETE ON {$itemTriggeredName}
FOR EACH ROW
EXECUTE PROCEDURE {$this->itemUpdatedName}_update_similar_tsv_by_hm();
SQL
            )->execute();
            echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
        }
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        foreach ($this->itemTriggeredNames as $itemTriggeredName) {
            echo '    > drop trigger ', $itemTriggeredName, '_after_insert_or_update_or_delete_update_', $this->itemUpdatedName, '_similar_tsv ...';
            $time = microtime(true);
            $this->db->createCommand(<<<SQL
DROP TRIGGER {$itemTriggeredName}_after_insert_or_update_or_delete_update_{$this->itemUpdatedName}_similar_tsv ON {$itemTriggeredName};
SQL
            )->execute();
            echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
        }

        echo '    > drop function ', $this->itemUpdatedName, '_update_similar_tsv_by_hm() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP FUNCTION {$this->itemUpdatedName}_update_similar_tsv_by_hm();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
