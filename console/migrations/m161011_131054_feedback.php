<?php

use \yii\db\Migration;

class m161011_131054_feedback extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'feedback';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createTable($tableName, [
            'id'         => $this->primaryKey(),
            'user_title' => $this->string(95)->notNull(),
            'user_email' => $this->string(255)->notNull(),
            'user_id'    => $this->integer(),
            'text'       => $this->text()->notNull(),
            'created'    => 'timestamp with time zone NOT NULL',
            'sent'       => 'timestamp with time zone',
        ]);

        $this->addForeignKey(
            $this->itemName . '_fk_user',
            $tableName,
            'user_id',
            '{{%user}}',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->createIndex(
            $this->itemName . '_idx_sent',
            $tableName,
            'sent',
            'btree'
        );

        $this->createIndex(
            $this->itemName . '_idx_user',
            $tableName,
            'user_id',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_email',
            $tableName,
            'user_email',
            'hash'
        );
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');
    }
}
