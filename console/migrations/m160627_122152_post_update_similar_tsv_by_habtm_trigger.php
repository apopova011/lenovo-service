<?php

use \yii\db\Migration;
use \common\models\Folder;
use \common\models\PostFolder;
use \common\models\PostTag;
use \common\models\Tag;

class m160627_122152_post_update_similar_tsv_by_habtm_trigger extends Migration
{
    /**
     * @var string
     */
    protected $itemUpdatedName = 'post';
    /**
     * @var array
     */
    protected $itemTriggeredNames = ['tag', 'folder'];

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        $folderTable     =     Folder::tableName();
        $tagTable        =        Tag::tableName();
        $postFolderTable = PostFolder::tableName();
        $postTagTable    =    PostTag::tableName();

        echo '    > create function ', $this->itemUpdatedName, '_update_similar_tsv_by_habtm() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->itemUpdatedName}_update_similar_tsv_by_habtm()
RETURNS TRIGGER AS
$$
DECLARE
    updated_item_id integer;
BEGIN
    IF (TG_OP = 'UPDATE') THEN
        IF (COALESCE(NEW.title, '') = COALESCE(OLD.title, '') AND NEW.name = OLD.name) THEN
            RETURN NULL;
        END IF;
    END IF;

    IF (TG_OP = 'DELETE') THEN
        updated_item_id = OLD.id;
    ELSE
        updated_item_id = NEW.id;
    END IF;

    EXECUTE 'UPDATE {$this->itemUpdatedName} SET similar_tsv = '
        || 'setweight(to_tsvector(''text_search_config'', COALESCE(title_seo, name)), ''A'') || '
        || 'setweight(to_tsvector(''text_search_config'', COALESCE((SELECT string_agg(f.title,                   '' '') FROM {$folderTable} f WHERE f.id IN (SELECT folder_id FROM {$postFolderTable} pf WHERE post_id = {$this->itemUpdatedName}.id)), '''')), ''B'') || '
        || 'setweight(to_tsvector(''text_search_config'', COALESCE((SELECT string_agg(COALESCE(t.title, t.name), '' '') FROM {$tagTable} t    WHERE t.id IN (SELECT tag_id    FROM {$postTagTable}    pt WHERE post_id = {$this->itemUpdatedName}.id)), '''')), ''C'') || '
        || 'setweight(to_tsvector(''text_search_config'', description), ''D'') '
        || 'WHERE id IN (SELECT post_id FROM post_' || TG_TABLE_NAME || ' WHERE ' || TG_TABLE_NAME || '_id = ' || updated_item_id || ')';

    RETURN NULL;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        foreach ($this->itemTriggeredNames as $itemTriggeredName) {
            echo '    > create trigger ', $itemTriggeredName, '_after_update_or_delete_update_', $this->itemUpdatedName, '_similar_tsv ...';
            $time = microtime(true);
            $this->db->createCommand(<<<SQL
CREATE TRIGGER {$itemTriggeredName}_after_update_or_delete_update_{$this->itemUpdatedName}_similar_tsv
AFTER UPDATE OR DELETE ON {$itemTriggeredName}
FOR EACH ROW
EXECUTE PROCEDURE {$this->itemUpdatedName}_update_similar_tsv_by_habtm();
SQL
            )->execute();
            echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
        }
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        foreach ($this->itemTriggeredNames as $itemTriggeredName) {
            echo '    > drop trigger ', $itemTriggeredName, '_after_update_or_delete_update_', $this->itemUpdatedName, '_similar_tsv ...';
            $time = microtime(true);
            $this->db->createCommand(<<<SQL
DROP TRIGGER {$itemTriggeredName}_after_update_or_delete_update_{$this->itemUpdatedName}_similar_tsv ON {$itemTriggeredName};
SQL
            )->execute();
            echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
        }

        echo '    > drop function ', $this->itemUpdatedName, '_update_similar_tsv_by_habtm() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP FUNCTION {$this->itemUpdatedName}_update_similar_tsv_by_habtm();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
