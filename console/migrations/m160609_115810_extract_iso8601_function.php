<?php

use \yii\db\Migration;

class m160609_115810_extract_iso8601_function extends Migration
{
    public function up()
    {
        echo '    > create function extract_iso8601(interval) ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION extract_iso8601(interval)
    RETURNS VARCHAR(63) AS
    $$
    DECLARE result VARCHAR(63);
    BEGIN
        SET intervalstyle = iso_8601;
        SELECT $1::VARCHAR(63) INTO result;
        SET intervalstyle = DEFAULT;
        RETURN result;
    END;
    $$
    LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    public function down()
    {
        echo '    > drop function extract_iso8601(interval) ...';
        $time = microtime(true);
        $this->db->createCommand('DROP FUNCTION IF EXISTS extract_iso8601(interval)')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
