<?php

use \yii\db\Migration;

class m160622_085906_feed_navigation extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'feed_navigation';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'page_resource_type' => $this->string(95)->notNull(),
            'page_resource_id' => $this->integer(),
            'link_resource_type' => $this->string(95)->notNull(),
            'link_resource_id' => $this->integer(),
            'url' => $this->string(255)->notNull()->comment('на момент создания'),
            'name' => $this->string(95)->notNull()->comment('на момент создания'),
            'order' => $this->smallInteger(),
        ]);

        $this->createIndex(
            $this->itemName . '_idx_order',
            $tableName,
            'order',
            'btree'
        );

        $this->createIndex(
            $this->itemName . '_idx_resource',
            $tableName,
            ['page_resource_type', 'page_resource_id'],
            'btree'
        );
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');
    }
}
