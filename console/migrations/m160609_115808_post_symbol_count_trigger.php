<?php

use \yii\db\Migration;

class m160609_115808_post_symbol_count_trigger extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'post';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        echo '    > create function ', $this->itemName, '_set_symbol_count() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->itemName}_set_symbol_count()
RETURNS TRIGGER AS
$$
BEGIN
  IF (TG_OP = 'UPDATE') THEN
    IF (
        NEW.name        = OLD.name AND
        NEW.description = OLD.description AND
        COALESCE(NEW.title_seo, '') = COALESCE(OLD.title_seo, '') AND
        COALESCE(NEW.content, '')   = COALESCE(OLD.content, '')
    ) THEN
      RETURN NULL;
    END IF;
  END IF;
  
  UPDATE {$this->itemName} SET symbol_count = post_symbol_count(NEW.id) WHERE id = NEW.id;

  RETURN NULL;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');
        echo '    > create trigger ', $this->itemName, '_after_insert_or_update_set_symbol_count ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TRIGGER {$this->itemName}_after_insert_or_update_set_symbol_count
AFTER INSERT OR UPDATE ON {$tableName}
FOR EACH ROW
EXECUTE PROCEDURE {$this->itemName}_set_symbol_count();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');
        echo '    > drop trigger ', $this->itemName, '_after_insert_or_update_set_symbol_count ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP TRIGGER {$this->itemName}_after_insert_or_update_set_symbol_count ON {$tableName};
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop function ', $this->itemName, '_set_symbol_count() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP FUNCTION {$this->itemName}_set_symbol_count();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
