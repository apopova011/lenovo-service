<?php

use \yii\db\Migration;

class m160610_155408_comment_votes_value_trigger extends Migration
{
    /**
     * @var string
     */
    protected $itemTriggeredName = 'comment_vote';

    /**
     * @var string
     */
    protected $itemUpdatedName = 'comment';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        echo '    > create function ', $this->itemUpdatedName, '_update_votes_value() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->itemUpdatedName}_update_votes_value()
  RETURNS trigger AS
  $$
DECLARE
  comment_id_for_update integer;
BEGIN
  IF (TG_OP = 'DELETE') THEN
   comment_id_for_update = OLD.comment_id;
  ELSE
   comment_id_for_update = NEW.comment_id;
  END IF;

  UPDATE {$this->itemUpdatedName}
    SET votes_value = (
      SELECT SUM(value)
      FROM {$this->itemTriggeredName}
      WHERE comment_id = {$this->itemUpdatedName}.id
    )
  WHERE {$this->itemUpdatedName}.id = comment_id_for_update;

  RETURN NULL;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create trigger ', $this->itemTriggeredName, '_after_insert_or_update_or_delete ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TRIGGER {$this->itemTriggeredName}_after_insert_or_update_or_delete
AFTER INSERT OR UPDATE OF value OR DELETE
ON {$this->itemTriggeredName}
FOR EACH ROW
EXECUTE PROCEDURE {$this->itemUpdatedName}_update_votes_value();
SQL
        )->execute();

        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        echo '    > drop trigger ', $this->itemTriggeredName, '_after_insert_or_update_or_delete ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP TRIGGER {$this->itemTriggeredName}_after_insert_or_update_or_delete ON {$this->itemTriggeredName};
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop function ', $this->itemUpdatedName, '_update_votes_value() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP FUNCTION {$this->itemUpdatedName}_update_votes_value();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
