<?php

use \yii\db\Migration;
use \common\models\Folder;
use \common\models\PostFolder;
use \common\models\PostTag;
use \common\models\Tag;

class m160627_122150_post_update_similar_tsv_by_post_trigger extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'post';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        $folderTable     =     Folder::tableName();
        $tagTable        =        Tag::tableName();
        $postFolderTable = PostFolder::tableName();
        $postTagTable    =    PostTag::tableName();

        echo '    > create function ', $this->itemName, '_update_similar_tsv_by_', $this->itemName, '() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->itemName}_update_similar_tsv_by_{$this->itemName}()
RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP = 'UPDATE' AND COALESCE(NEW.title_seo, '') = COALESCE(OLD.title_seo, '') AND NEW.name = OLD.name AND NEW.description = OLD.description) THEN
        RETURN NEW;
    END IF;

    NEW.similar_tsv :=
        setweight(to_tsvector('text_search_config', COALESCE(NEW.title_seo, NEW.name)), 'A') ||
        setweight(to_tsvector('text_search_config', COALESCE((SELECT string_agg(f.title, ' ')                   FROM {$folderTable} f WHERE f.id IN (SELECT folder_id FROM {$postFolderTable} pf WHERE post_id = NEW.id)), '')), 'B') ||
        setweight(to_tsvector('text_search_config', COALESCE((SELECT string_agg(COALESCE(t.title, t.name), ' ') FROM {$tagTable}    t WHERE t.id IN (SELECT tag_id    FROM {$postTagTable}    pt WHERE post_id = NEW.id)), '')), 'C') ||
        setweight(to_tsvector('text_search_config', NEW.description), 'D');

    RETURN NEW;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create trigger ', $this->itemName, '_before_insert_or_update_update_', $this->itemName, '_similar_tsv ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TRIGGER {$this->itemName}_before_insert_or_update_update_{$this->itemName}_similar_tsv
BEFORE INSERT OR UPDATE ON {$this->itemName}
FOR EACH ROW
EXECUTE PROCEDURE {$this->itemName}_update_similar_tsv_by_{$this->itemName}();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        echo '    > drop trigger ', $this->itemName, '_before_insert_or_update_update_', $this->itemName, '_similar_tsv ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP TRIGGER {$this->itemName}_before_insert_or_update_update_{$this->itemName}_similar_tsv ON {$this->itemName};
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop function ', $this->itemName, '_update_similar_tsv_by_', $this->itemName, '() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP FUNCTION {$this->itemName}_update_similar_tsv_by_{$this->itemName}();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
