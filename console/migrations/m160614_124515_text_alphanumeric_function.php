<?php

use \yii\db\Migration;

class m160614_124515_text_alphanumeric_function extends Migration
{
    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        echo '    > create function text_alphanumeric(text) ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION text_alphanumeric(text)
    RETURNS TEXT AS
    $$ SELECT regexp_replace(replace(lower($1), 'Ё', 'Е'), E'[^a-zа-я0-9,]', '', 'gi') $$
    LANGUAGE SQL
    IMMUTABLE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function down()
    {
        echo '    > drop function text_alphanumeric(text) ...';
        $time = microtime(true);
        $this->db->createCommand('DROP FUNCTION text_alphanumeric(text);')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
