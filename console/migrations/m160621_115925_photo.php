<?php

use \yii\db\Migration;
use \common\models\Post;
use \common\models\User;

class m160621_115925_photo extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'photo';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'file' => $this->string(255)->notNull(),
            'content_type' => $this->string(15),
            'exif_updated' => 'timestamp without time zone',
            'post_id' => $this->integer()->notNull(),
            'post_published' => 'timestamp without time zone',
            'author_id' => $this->integer()->notNull(),
            'author_title' => $this->string(95)->notNull(),
            'author_email' => $this->string(255)->notNull(),
            'post_content_img_alt' => $this->string(255),
        ]);

        $this->addForeignKey($this->itemName . '_fk_' . User::tableName(), $tableName, 'author_id', User::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey($this->itemName . '_fk_' . Post::tableName(), $tableName, 'post_id',   Post::tableName(), 'id', 'CASCADE', 'CASCADE');

        $this->createIndex(
            $this->itemName . '_idx_author',
            $tableName,
            'author_id',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_post',
            $tableName,
            'post_id',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_file',
            $tableName,
            'file',
            'hash'
        );

        echo '    > create index ', $this->itemName, '_idx_content_type ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE INDEX {$this->itemName}_idx_content_type
    ON {$this->itemName}
    USING hash
    (content_type)
    WHERE content_type IS NOT NULL;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create index ', $this->itemName, '_idx_exif_updated ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE INDEX {$this->itemName}_idx_exif_updated
    ON {$this->itemName}
    USING btree
    (exif_updated)
    WHERE exif_updated IS NOT NULL;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');
    }
}
