<?php

use \yii\db\Migration;

class m160610_123303_post_comment_visible_count_trigger extends Migration
{
    /**
     * @var string
     */
    protected $itemTriggeredName = 'comment';

    /**
     * @var string
     */
    protected $itemUpdatedName = 'post';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        echo '    > create function ', $this->itemUpdatedName, '_update_comment_visible_count_after_', $this->itemTriggeredName, '_insert_or_delete() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->itemUpdatedName}_update_comment_visible_count_after_{$this->itemTriggeredName}_insert_or_delete()
RETURNS TRIGGER AS
$$
DECLARE
    record RECORD;
    delta integer;
BEGIN
    IF (TG_OP = 'DELETE') THEN
        record = OLD;
        delta = -1;
    ELSE
        record = NEW;
        delta = 1;
    END IF;

    IF (record.status = 'banned_by_vote' OR record.status = 'banned_by_moderator') THEN
        RETURN NULL;
    END IF;

    UPDATE {$this->itemUpdatedName}
    SET comment_visible_count = comment_visible_count + delta
    WHERE id = record.post_id;

    RETURN NULL;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create trigger ', $this->itemTriggeredName, '_after_insert_or_delete_update_', $this->itemUpdatedName, '_comment_visible_count ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TRIGGER {$this->itemTriggeredName}_after_insert_or_delete_update_{$this->itemUpdatedName}_comment_visible_count
AFTER INSERT OR DELETE ON {$this->itemTriggeredName}
FOR EACH ROW
EXECUTE PROCEDURE {$this->itemUpdatedName}_update_comment_visible_count_after_comment_insert_or_delete();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;


        echo '    > create function ', $this->itemUpdatedName, '_update_comment_visible_count_after_', $this->itemTriggeredName, '_update() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->itemUpdatedName}_update_comment_visible_count_after_{$this->itemTriggeredName}_update()
RETURNS TRIGGER AS
$$
BEGIN
    IF (NEW.status = OLD.status AND NEW.post_id = OLD.post_id) THEN
        RETURN NULL;
    END IF;

    UPDATE {$this->itemUpdatedName}
        SET comment_visible_count = COALESCE((
            SELECT COUNT(id)
            FROM {$this->itemTriggeredName}
            WHERE post_id = {$this->itemUpdatedName}.id
            AND status NOT IN ('banned_by_vote', 'banned_by_moderator')
            GROUP BY post_id
        ), 0)
        WHERE id IN (NEW.post_id, OLD.post_id);

    RETURN NULL;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create trigger ', $this->itemTriggeredName, '_after_update_update_', $this->itemUpdatedName, '_comment_visible_count ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TRIGGER {$this->itemTriggeredName}_after_update_update_{$this->itemUpdatedName}_comment_visible_count
AFTER UPDATE ON {$this->itemTriggeredName}
FOR EACH ROW
EXECUTE PROCEDURE {$this->itemUpdatedName}_update_comment_visible_count_after_{$this->itemTriggeredName}_update();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        echo '    > drop trigger ', $this->itemTriggeredName, '_after_insert_or_delete_update_', $this->itemUpdatedName, '_comment_visible_count ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP TRIGGER {$this->itemTriggeredName}_after_insert_or_delete_update_{$this->itemUpdatedName}_comment_visible_count ON {$this->itemTriggeredName};
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop function ', $this->itemUpdatedName, '_update_comment_visible_count_after_', $this->itemTriggeredName, '_insert_or_delete() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP FUNCTION {$this->itemUpdatedName}_update_comment_visible_count_after_{$this->itemTriggeredName}_insert_or_delete();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop trigger ', $this->itemTriggeredName, '_after_update_update_', $this->itemUpdatedName, '_comment_visible_count ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP TRIGGER {$this->itemTriggeredName}_after_update_update_{$this->itemUpdatedName}_comment_visible_count ON {$this->itemTriggeredName};
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop function ', $this->itemUpdatedName, '_update_comment_visible_count_after_', $this->itemTriggeredName, '_update() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP FUNCTION {$this->itemUpdatedName}_update_comment_visible_count_after_{$this->itemTriggeredName}_update();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
