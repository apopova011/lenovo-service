<?php

use yii\db\Migration;

/**
 * Class m200421_153824_partners
 */
class m200421_153824_partners extends Migration
{
    /**
     * @var string
     */
    public $itemName = 'partners';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'country' => $this->string(255)->notNull(),
            'region' => $this->string(255)->notNull(),
            'city' => $this->string(255)->notNull(),
            'address' => $this->text()->notNull(),
            'coords' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),
            'phones' => $this->text(),
            'site' => $this->string(255),
            'description' => $this->text(),
            'act' => $this->boolean()->notNull()->defaultValue(false),
        ]);

        $this->createIndex(
            $this->itemName . '_idx_country',
            $tableName,
            'country',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_region',
            $tableName,
            'region',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_city',
            $tableName,
            'city',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_act',
            $tableName,
            'act',
            'hash'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');
    }
}
