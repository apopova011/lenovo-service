<?php

use \yii\db\Migration;

class m160614_125643_folder_updated_trigger extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'folder';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        echo '    > create function ', $this->itemName, '_set_updated() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->itemName}_set_updated()
RETURNS TRIGGER AS
$$
BEGIN
  IF (TG_OP = 'UPDATE') THEN
    IF (
      NEW.slug = OLD.slug AND
      NEW.name = OLD.name AND
      COALESCE(NEW.title, '') = COALESCE(OLD.title, '') AND
      COALESCE(NEW.title_seo, '') = COALESCE(OLD.title_seo, '') AND
      NEW.description = OLD.description AND
      COALESCE(NEW.icon, '') = COALESCE(OLD.icon, '') AND
      NEW.path = OLD.path AND
      NEW.group_id = OLD.group_id
    ) THEN
      RETURN NEW;
    END IF;
  END IF;

  NEW.updated := NOW();
  RETURN NEW;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');
        echo '    > create trigger ', $this->itemName, '_before_insert_or_update_set_updated ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TRIGGER {$this->itemName}_before_insert_or_update_set_updated
BEFORE INSERT OR UPDATE ON {$tableName}
FOR EACH ROW
EXECUTE PROCEDURE {$this->itemName}_set_updated();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');
        echo '    > drop trigger ', $this->itemName, '_before_insert_or_update_set_updated ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP TRIGGER {$this->itemName}_before_insert_or_update_set_updated ON {$tableName};
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop function ', $this->itemName, '_set_updated() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP FUNCTION {$this->itemName}_set_updated();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
