<?php

use \yii\db\Migration;
use \common\models\Post;

class m160926_125917_post_types extends Migration
{
    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        echo '    > create type post_types ...';
        $time = microtime(true);
        $this->db->createCommand('CREATE TYPE post_types AS ENUM (\'section\', \'for_feed\');')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        $this->addColumn(Post::tableName(), 'type', 'post_types NOT NULL DEFAULT \'for_feed\'');
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        $this->dropColumn(Post::tableName(), 'type');

        echo '    > drop type post_types ...';
        $time = microtime(true);
        $this->db->createCommand('DROP TYPE IF EXISTS post_types;')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
