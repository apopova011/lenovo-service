<?php

use \yii\db\Migration;
use \common\models\Post;

class m160609_115807_post_symbol_count_function extends Migration
{
    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function up()
    {
        $postTableName = Post::tableName();
        echo '    > create function post_symbol_count(integer) ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION post_symbol_count(integer)
RETURNS integer AS
$$ SELECT text_count_symbol(name) + text_count_symbol(coalesce(NULLIF(title_seo, name), '')) + text_count_symbol(description) + html_count_symbol(content) FROM {$postTableName} WHERE id = $1 $$
LANGUAGE SQL
IMMUTABLE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function down()
    {
        echo '    > drop function post_symbol_count(integer) ...';
        $time = microtime(true);
        $this->db->createCommand('DROP FUNCTION IF EXISTS post_symbol_count(integer)')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
