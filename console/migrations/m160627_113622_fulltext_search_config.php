<?php

use \yii\db\Migration;

class m160627_113622_fulltext_search_config extends Migration
{
    public function safeUp()
    {
        echo '    > create text search dictionary stemmer_ru ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TEXT SEARCH DICTIONARY stemmer_ru (
    TEMPLATE = snowball,
    language = 'russian',
    stopwords = 'russian'
);
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create text search dictionary hunspell_ru ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TEXT SEARCH DICTIONARY hunspell_ru (
    TEMPLATE  = ispell,
    dictfile  = 'hunspell_ru_ru_utf',
    afffile   = 'hunspell_ru_ru_utf',
    stopwords = 'russian'
);
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create text search dictionary stemmer_en ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TEXT SEARCH DICTIONARY stemmer_en (
    TEMPLATE = snowball,
    language = 'english',
    stopwords = 'english'
);
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create text search dictionary hunspell_en ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TEXT SEARCH DICTIONARY hunspell_en (
    TEMPLATE  = ispell,
    dictfile  = 'hunspell_en_us_utf',
    afffile   = 'hunspell_en_us_utf',
    stopwords = 'english'
);
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create text search dictionary simple_en ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TEXT SEARCH DICTIONARY simple_en (
  TEMPLATE = pg_catalog.simple
);
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create text search configuration text_search_config ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TEXT SEARCH CONFIGURATION text_search_config (
    COPY = pg_catalog.russian
);
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > alter mapping for word, hword, hword_part ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
ALTER TEXT SEARCH CONFIGURATION text_search_config
ALTER MAPPING FOR word, hword, hword_part
WITH hunspell_ru, stemmer_ru;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > alter mapping for asciiword, asciihword, hword_asciipart ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
ALTER TEXT SEARCH CONFIGURATION text_search_config
ALTER MAPPING FOR asciiword, asciihword, hword_asciipart
WITH hunspell_en, stemmer_en;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > alter mapping for float, int, uint ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
ALTER TEXT SEARCH CONFIGURATION text_search_config
ALTER MAPPING FOR float, int, uint
WITH simple_en;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop mapping for email, file, host, sfloat, url, url_path, version, numword, hword_numpart, numhword ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
ALTER TEXT SEARCH CONFIGURATION text_search_config
DROP MAPPING FOR email, file, host, sfloat, url, url_path, version, numword, hword_numpart, numhword;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > set default text search config ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
SET default_text_search_config = 'text_search_config';
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    public function safeDown()
    {
        echo '    > drop text search configuration ...';
        $time = microtime(true);
        $this->db->createCommand('DROP TEXT SEARCH CONFIGURATION text_search_config;')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop text search dictionary stemmer_ru ...';
        $time = microtime(true);
        $this->db->createCommand('DROP TEXT SEARCH DICTIONARY stemmer_ru;')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop text search dictionary hunspell_ru ...';
        $time = microtime(true);
        $this->db->createCommand('DROP TEXT SEARCH DICTIONARY hunspell_ru;')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop text search dictionary stemmer_en ...';
        $time = microtime(true);
        $this->db->createCommand('DROP TEXT SEARCH DICTIONARY stemmer_en;')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop text search dictionary hunspell_en ...';
        $time = microtime(true);
        $this->db->createCommand('DROP TEXT SEARCH DICTIONARY hunspell_en;')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop text search dictionary simple_en ...';
        $time = microtime(true);
        $this->db->createCommand('DROP TEXT SEARCH DICTIONARY simple_en;')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
