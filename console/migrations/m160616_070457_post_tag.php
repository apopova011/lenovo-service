<?php

use \yii\db\Migration;
use \common\models\Post;
use \common\models\Tag;

class m160616_070457_post_tag extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'post_tag';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createTable($tableName, [
            'post_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey($this->itemName . '_pk', $tableName, ['post_id', 'tag_id']);

        $this->addForeignKey($this->itemName . '_fk_' . Post::tableName(), $tableName, 'post_id', Post::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey($this->itemName . '_fk_' .  Tag::tableName(), $tableName, 'tag_id',   Tag::tableName(), 'id', 'CASCADE', 'CASCADE');

        $this->createIndex(
            $this->itemName . '_idx_tag',
            $tableName,
            'tag_id',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_post',
            $tableName,
            'post_id',
            'hash'
        );
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');
    }
}
