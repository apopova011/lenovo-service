<?php

use \yii\db\Migration;

class m160623_090451_post_show_count_trigger extends Migration
{
    /**
     * @var string
     */
    protected $itemTriggeredName = 'statistic_post';

    /**
     * @var string
     */
    protected $itemUpdatedName = 'post';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        echo '    > create function ', $this->itemUpdatedName, '_update_show_count() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->itemUpdatedName}_update_show_count()
RETURNS trigger AS
$$
    DECLARE
      post_id_for_update integer;
    BEGIN
        IF (TG_OP = 'DELETE') THEN
          post_id_for_update = OLD.post_id;
        ELSE
          post_id_for_update = NEW.post_id;
        END IF;

        UPDATE {$this->itemUpdatedName}
        SET show_count = COALESCE((
            SELECT SUM(show_count)
            FROM {$this->itemTriggeredName}
            WHERE
                post_id = {$this->itemUpdatedName}.id
                AND date <= {$this->itemUpdatedName}.published + interval '1 week'
        ), 0)
        WHERE id = post_id_for_update;

        RETURN NULL;
    END
$$
LANGUAGE plpgsql VOLATILE
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create trigger ', $this->itemTriggeredName, '_after_insert_or_update_or_delete ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TRIGGER {$this->itemTriggeredName}_after_insert_or_update_or_delete
AFTER INSERT OR UPDATE OR DELETE
ON {$this->itemTriggeredName}
FOR EACH ROW
EXECUTE PROCEDURE {$this->itemUpdatedName}_update_show_count();
SQL
        )->execute();

        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        echo '    > drop trigger ', $this->itemTriggeredName, '_after_insert_or_update_or_delete ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP TRIGGER {$this->itemTriggeredName}_after_insert_or_update_or_delete ON {$this->itemTriggeredName};
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop function ', $this->itemUpdatedName, '_update_show_count() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP FUNCTION {$this->itemUpdatedName}_update_show_count();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
