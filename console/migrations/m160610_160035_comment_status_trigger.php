<?php

use \yii\db\Migration;

class m160610_160035_comment_status_trigger extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'comment';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        echo '    > create function ', $this->itemName, '_update_status() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->itemName}_update_status()
  RETURNS trigger AS
  $$
BEGIN
  IF (NEW.votes_value <= -5 AND NEW.status = 'new') THEN
    NEW.status = 'banned_by_vote';
  ELSIF (NEW.votes_value >= 5 AND (NEW.status = 'new' OR NEW.status = 'approved')) THEN
    NEW.status = 'excellent_by_vote';
  ELSIF (ABS(NEW.votes_value) < 5 AND (NEW.status = 'excellent_by_vote' OR NEW.status = 'banned_by_vote')) THEN
    NEW.status = 'new';
  END IF;

  RETURN NEW;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create trigger ', $this->itemName, '_before_update_votes_value ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TRIGGER {$this->itemName}_before_update_votes_value
BEFORE UPDATE OF votes_value
ON {$this->itemName}
FOR EACH ROW
EXECUTE PROCEDURE {$this->itemName}_update_status();
SQL
        )->execute();

        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        echo '    > drop trigger ', $this->itemName, '_before_update_votes_value ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP TRIGGER {$this->itemName}_before_update_votes_value ON {$this->itemName};
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop function ', $this->itemName, '_update_status() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP FUNCTION {$this->itemName}_update_status();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
