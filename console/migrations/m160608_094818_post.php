<?php

use \yii\db\Migration;
use \common\models\User;

class m160608_094818_post extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'post';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'slug' => $this->string(31)->notNull(),
            'name' => $this->string(95)->notNull(),
            'title_seo' => $this->string(255),
            'description' => $this->string(1023)->notNull(),
            'icon' => $this->string(255),
            'content' => $this->text(),
            'author_id' => $this->integer()->notNull(),
            'folder_id_main' => $this->integer()->notNull(),
            'tags_name' => $this->string(255)->comment('кэшированные значения'),
            'similar_tsv' => 'tsvector',
            'published' => 'timestamp without time zone',
            'updated' => 'timestamp without time zone NOT NULL',
            'is_sent_to_index' => $this->boolean()->notNull()->defaultValue(false),
            'is_in_common_feed' => $this->boolean()->notNull()->defaultValue(true),
            'is_allow_footer' => $this->boolean()->notNull()->defaultValue(true),
            'is_allow_comments' => $this->boolean()->notNull()->defaultValue(true),
            'is_contain_video' => $this->boolean()->notNull()->defaultValue(false),
            'video_duration' => 'interval',
            'show_count' => $this->integer()->defaultValue(0)->comment('кэшированное значение'),
            'symbol_count' => $this->integer()->defaultValue(0)->comment('кэшированное значение'),
            'comment_visible_count' => $this->integer()->defaultValue(0)->comment('кэшированное значение'),
        ]);

        $this->addForeignKey($this->itemName . '_fk_' . User::tableName(), $tableName, 'author_id', User::tableName(), 'id', 'RESTRICT', 'RESTRICT');

        echo '    > create not null btree index ', $this->itemName, '_idx_published ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE INDEX {$this->itemName}_idx_published
  ON {$tableName}
  USING btree
  (published)
  WHERE published IS NOT NULL;
SQL
        )->execute();

        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        $this->createIndex(
            $this->itemName . '_idx_author',
            $tableName,
            'author_id',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_is_in_common_feed',
            $tableName,
            'is_in_common_feed',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_is_sent_to_index',
            $tableName,
            'is_sent_to_index',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_similar_tsv',
            $tableName,
            'similar_tsv',
            'gin'
        );

        echo '    > alter sequence ', $this->itemName, '_id_seq ...';
        $time = microtime(true);
        $this->db->createCommand('ALTER SEQUENCE ' . $this->itemName . '_id_seq RESTART WITH 1000')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');
    }
}
