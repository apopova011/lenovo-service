<?php

use \yii\db\Migration;

class m160609_115806_html_count_symbol_function extends Migration
{
    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function up()
    {
        echo '    > create function html_count_symbol(text) ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION html_count_symbol(text)
RETURNS integer AS
$$ SELECT text_count_symbol((SELECT string_agg(alt, '') FROM (SELECT (regexp_matches($1, 'alt="([^"]+)', 'g'))[1] alt) alt_list)) + text_count_symbol(html_to_text(html_decode_entity($1))) $$
LANGUAGE SQL
IMMUTABLE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function down()
    {
        echo '    > drop function html_count_symbol(text) ...';
        $time = microtime(true);
        $this->db->createCommand('DROP FUNCTION IF EXISTS html_count_symbol(text)')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
