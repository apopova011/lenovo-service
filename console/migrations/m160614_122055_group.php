<?php

use \yii\db\Migration;

class m160614_122055_group extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'group';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'slug' => $this->string(31)->notNull(),
            'name' => $this->string(63)->notNull(),
        ]);

        echo '    > create unique index ', $this->itemName, '_idx_name ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE UNIQUE INDEX {$this->itemName}_idx_name
    ON "{$this->itemName}"
    USING btree
    (name);
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create unique index ', $this->itemName, '_idx_slug ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE UNIQUE INDEX {$this->itemName}_idx_slug
    ON "{$this->itemName}"
    USING btree
    (slug);
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');
    }
}
