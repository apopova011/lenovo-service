<?php

use \yii\db\Migration;

class m151223_071538_user extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'user';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeUp()
    {
        echo '    > create function blowfish(text) ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION sha256(text) RETURNS text AS 
$$
SELECT encode(digest($1, 'sha256'), 'hex');
$$
LANGUAGE SQL IMMUTABLE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create type user_roles ...';
        $time = microtime(true);
        $this->db->createCommand('CREATE TYPE user_roles AS ENUM (\'guest\', \'registered\', \'verified\', \'moderator\', \'writer\', \'contributor\', \'editor\', \'administrator\');')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'role' => 'user_roles NOT NULL DEFAULT \'guest\'',

            'slug'       => $this->string(31)->notNull(),
            'title'      => $this->string(95)->notNull()->comment('first_name + last_name OR указанный'),
            'avatar'     => $this->string(255),
            'first_name' => $this->string(63),
            'last_name'  => $this->string(63),

            'timezone' => $this->string(31),

            'email' => $this->string(255),

            'title_gen' => $this->string(95)->comment('родительный'),
            'avatar_original' => $this->string(255)->comment('внешний ресурс, откуда производилось скачивание'),
            'sex' => $this->smallInteger(),
            'birthday' => $this->date(),
            'phone_mobile' => $this->string(31),
            'phone_home' => $this->string(31),
            'city_name' => $this->string(63),
            'skype' => $this->string(255),
            'twitter_slug' => $this->string(255),
            'instagram_slug' => $this->string(255),
            'facebook_id' => $this->bigInteger(),
            'facebook_name' => $this->string(255),
            'vk_id' => $this->bigInteger(),
            'vk_slug' => $this->string(255),
            'google_plus_slug' => $this->string(255),

            // data from vk
            'vk_can_post' => $this->boolean(),
            'vk_can_write_private_message' => $this->boolean(),
            'relation' => $this->smallInteger(),
            'life_main' => $this->smallInteger(),
            'smoking' => $this->smallInteger(),
            'alcohol' => $this->smallInteger(),
            'music' => $this->string(1023),
            'movies' => $this->string(1023),
            'tv' => $this->string(1023),
            'books' => $this->string(1023),
            'games' => $this->string(1023),
            'about' => $this->string(1023),

            'password_hash' => $this->string(255),
            'auth_key'      => $this->string(32),

            'created timestamp with time zone NOT NULL',
            'updated timestamp with time zone NOT NULL',

            'comment_visible_count' => $this->integer()->defaultValue(0)->comment('кэшированное значение'),
            'post_count' => $this->integer()->defaultValue(0)->comment('кэшированное значение'),
        ]);

        $this->addCommentOnColumn($tableName, 'updated', 'если менялись какие-либо данные или оставлялись комменты');

        $this->createIndex(
            $this->itemName . '_idx_role',
            $tableName,
            'role',
            'hash'
        );

        echo '    > create not null index ', $this->itemName, '_idx_email ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE INDEX {$this->itemName}_idx_email
  ON {$tableName}
  USING btree
  (email)
  WHERE email IS NOT NULL;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        $this->createIndex(
            $this->itemName . '_idx_slug',
            $tableName,
            'slug',
            'btree'
        );

        $this->createIndex(
            $this->itemName . '_idx_title',
            $tableName,
            'title',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_vk',
            $tableName,
            'vk_id',
            'btree'
        );
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');

        echo '    > drop type user_roles ...';
        $time = microtime(true);
        $this->db->createCommand('DROP TYPE IF EXISTS user_roles;')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop function blowfish(text) ...';
        $time = microtime(true);
        $this->db->createCommand('DROP FUNCTION IF EXISTS blowfish(text);')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
