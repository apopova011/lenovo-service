<?php

use \yii\db\Migration;

class m160609_115804_html_decode_entity_function extends Migration
{
    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function up()
    {
        echo '    > create function html_decode_entity(text) ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION html_decode_entity(text)
RETURNS text AS
$$ SELECT regexp_replace($1, '&[a-z]+;', '*', 'g') $$
LANGUAGE SQL
IMMUTABLE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function down()
    {
        echo '    > drop function html_decode_entity(text) ...';
        $time = microtime(true);
        $this->db->createCommand('DROP FUNCTION IF EXISTS html_decode_entity(text)')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
