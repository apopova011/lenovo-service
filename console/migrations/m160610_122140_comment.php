<?php

use \yii\db\Migration;
use \common\models\Post;
use \common\models\User;

class m160610_122140_comment extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'comment';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeUp()
    {
        echo '    > create type comment_statuses ...';
        $time = microtime(true);
        $this->db->createCommand('CREATE TYPE comment_statuses AS ENUM (\'new\', \'approved\', \'approved_and_not_excellent\', \'banned_by_vote\', \'banned_by_moderator\', \'excellent_by_vote\', \'excellent_by_moderator\');')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'content' => $this->text(),
            'status' => 'comment_statuses',
            'created' => 'timestamp without time zone NOT NULL',
            'user_title' => $this->string(255)->notNull()->comment('подпись на момент оставления коммента'),
            'user_avatar' => $this->string(255)->notNull()->comment('ава на момент оставления коммента'),
            'user_ip' => 'inet',
            'user_agent' => $this->string(255),
            'user_session' => $this->string(32),
            'path' => 'ltree NOT NULL',
            'post_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'votes_value' => $this->integer()->comment('кэшированное значение'),
        ]);

        $this->addForeignKey($this->itemName . '_fk_' . Post::tableName(), $tableName, 'post_id',   Post::tableName(), 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey($this->itemName . '_fk_' . User::tableName(), $tableName, 'author_id', User::tableName(), 'id', 'RESTRICT', 'RESTRICT');

        $this->createIndex(
            $this->itemName . '_idx_post',
            $tableName,
            'post_id',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_author',
            $tableName,
            'author_id',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_created',
            $tableName,
            'created',
            'btree'
        );

        $this->createIndex(
            $this->itemName . '_idx_status',
            $tableName,
            'status',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_content',
            $tableName,
            'content',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_path',
            $tableName,
            'path',
            'gist'
        );

        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');

        echo '    > drop type comment_statuses ...';
        $time = microtime(true);
        $this->db->createCommand('DROP TYPE IF EXISTS comment_statuses;')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
