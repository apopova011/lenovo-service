<?php

use \yii\db\Migration;

class m160616_070450_array_index_function extends Migration
{
    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function up()
    {
        echo '    > create function idx(anyarray, anyelement) ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION idx(ANYARRAY, ANYELEMENT)
    RETURNS INT AS
    $$
    SELECT i FROM (
        SELECT generate_series(array_lower($1,1),array_upper($1,1))
    ) g(i)
    WHERE $1[i] = $2
    LIMIT 1;
$$ LANGUAGE SQL IMMUTABLE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function down()
    {
        echo '    > drop function idx(anyarray, anyelement) ...';
        $time = microtime(true);
        $this->db->createCommand('DROP FUNCTION IF EXISTS idx(anyarray, anyelement)')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
