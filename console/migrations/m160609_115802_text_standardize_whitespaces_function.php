<?php

use \yii\db\Migration;

class m160609_115802_text_standardize_whitespaces_function extends Migration
{
    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function up()
    {
        echo '    > create function text_standardize_whitespaces(text) ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION text_standardize_whitespaces(text)
RETURNS text AS
$$ SELECT regexp_replace(regexp_replace(regexp_replace(regexp_replace(trim(replace(replace($1, E'\r', ''), E'\t', ' '), E'  \n'), E'\n[  ]+', E'\n', 'g'), E'[  ]+\n', E'\n', 'g'), '[  ][  ]+', ' ', 'g'), E'\n\n+', E'\n', 'g') $$
LANGUAGE SQL
IMMUTABLE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function down()
    {
        echo '    > drop function text_standardize_whitespaces(text) ...';
        $time = microtime(true);
        $this->db->createCommand('DROP FUNCTION IF EXISTS text_standardize_whitespaces(text)')->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
