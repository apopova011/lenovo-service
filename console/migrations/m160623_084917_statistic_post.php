<?php

use \yii\db\Migration;
use \common\models\Post;

class m160623_084917_statistic_post extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'statistic_post';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'date' => 'timestamp without time zone NOT NULL',
            'post_id' => $this->integer()->notNull(),
            'show_count' => $this->integer()->notNull()->defaultValue(0),
            'show_count_by_url' => 'json',
        ]);

        $this->addForeignKey($this->itemName . '_fk_' . Post::tableName(), $tableName, 'post_id', Post::tableName(), 'id', 'CASCADE', 'CASCADE');

        $this->createIndex(
            $this->itemName . '_idx_post',
            $tableName,
            'post_id',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_date',
            $tableName,
            'date',
            'btree'
        );
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');
    }
}
