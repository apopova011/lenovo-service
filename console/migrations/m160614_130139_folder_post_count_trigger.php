<?php

use \yii\db\Migration;

class m160614_130139_folder_post_count_trigger extends Migration
{
    /**
     * @var string
     */
    protected $itemTriggeredName = 'post_folder';

    /**
     * @var string
     */
    protected $itemUpdatedName = 'folder';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        echo '    > create function ', $this->itemUpdatedName, '_update_post_count() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->itemUpdatedName}_update_post_count()
RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE {$this->itemUpdatedName}
            SET post_count = post_count - 1
            WHERE id = OLD.folder_id;
    ELSE
        UPDATE {$this->itemUpdatedName}
            SET post_count = post_count + 1
            WHERE id = NEW.folder_id;
    END IF;
    RETURN NULL;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create trigger ', $this->itemTriggeredName, '_after_insert_or_delete_update_', $this->itemUpdatedName, '_post_count ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TRIGGER {$this->itemTriggeredName}_after_insert_or_delete_update_{$this->itemUpdatedName}_post_count
AFTER INSERT OR DELETE ON {$this->itemTriggeredName}
FOR EACH ROW
EXECUTE PROCEDURE {$this->itemUpdatedName}_update_post_count();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        echo '    > drop trigger ', $this->itemTriggeredName, '_after_insert_or_delete_update_', $this->itemUpdatedName, '_post_count ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP TRIGGER {$this->itemTriggeredName}_after_insert_or_delete_update_{$this->itemUpdatedName}_post_count ON {$this->itemTriggeredName};
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop function ', $this->itemUpdatedName, '_update_post_count() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP FUNCTION {$this->itemUpdatedName}_update_post_count();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
