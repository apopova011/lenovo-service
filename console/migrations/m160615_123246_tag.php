<?php

use \yii\db\Migration;
use \common\models\Group;

class m160615_123246_tag extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'tag';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'slug' => $this->string(31)->notNull(),
            'name' => $this->string(63)->notNull(),
            'title' => $this->string(95),
            'title_seo' => $this->string(255),
            'description' => $this->string(1023),
            'group_id' => $this->integer(),
            'updated' => 'timestamp without time zone NOT NULL',
            'post_count' => $this->integer()->defaultValue(0)->comment('кэшированное значение'),
        ]);

        $this->addForeignKey($this->itemName . '_fk_' . Group::tableName(), $tableName, 'group_id', Group::tableName(), 'id', 'SET NULL', 'CASCADE');

        $this->addCommentOnColumn($tableName, 'updated', 'если менялись какие-либо данные или публиковались материалы');

        $this->createIndex(
            $this->itemName . '_idx_group',
            $tableName,
            'group_id',
            'hash'
        );

        echo '    > create unique index ', $this->itemName, '_idx_name ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE UNIQUE INDEX {$this->itemName}_idx_name
    ON {$this->itemName}
    USING btree
    (text_alphanumeric(name::text));
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create unique index ', $this->itemName, '_idx_slug ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE UNIQUE INDEX {$this->itemName}_idx_slug
    ON {$this->itemName}
    USING btree
    (slug);
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');
    }
}
