<?php

use \yii\db\Migration;

class m160622_081809_link extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'link';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string(31)->notNull(),
            'url' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');
    }
}
