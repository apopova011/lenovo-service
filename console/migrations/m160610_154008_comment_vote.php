<?php

use \yii\db\Migration;
use \common\models\Comment;
use \common\models\User;

class m160610_154008_comment_vote extends Migration
{
    /**
     * @var string
     */
    protected $itemName = 'comment_vote';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeUp()
    {
        $tableName = $this->db->quoteTableName('{{%' . $this->itemName . '}}');

        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'value' => $this->smallInteger()->notNull(),
            'comment_id' => $this->integer()->notNull(),
            'user_session' => $this->string(32),
            'user_id' => $this->integer(),
            'user_ip' => 'inet',
            'user_agent' => $this->string(255),
        ]);

        $this->addForeignKey($this->itemName . '_fk_' . Comment::tableName(), $tableName, 'comment_id', Comment::tableName(), 'id', 'CASCADE',  'CASCADE');
        $this->addForeignKey($this->itemName . '_fk_' .    User::tableName(), $tableName, 'user_id',       User::tableName(), 'id', 'SET NULL', 'CASCADE');

        $this->createIndex(
            $this->itemName . '_idx_comment',
            $tableName,
            'comment_id',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_user',
            $tableName,
            'user_id',
            'hash'
        );

        $this->createIndex(
            $this->itemName . '_idx_user_session',
            $tableName,
            'user_session',
            'hash'
        );

        echo '    > create unique index ', $this->itemName, '_idx_comment_id_and_user_session ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE UNIQUE INDEX {$this->itemName}_idx_comment_id_and_user_session
  ON {$this->itemName}
  USING btree
  (comment_id, user_session);
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\console\Exception
     */
    public function safeDown()
    {
        $this->dropTable('{{%' . $this->itemName . '}}');
    }
}
