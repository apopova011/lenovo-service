<?php

use \yii\db\Migration;

class m160609_115801_user_post_count_trigger extends Migration
{
    /**
     * @var string
     */
    protected $itemTriggeredName = 'post';

    /**
     * @var string
     */
    protected $itemUpdatedName = 'user';

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        $tableUpdatedName = $this->db->quoteTableName('{{%' . $this->itemUpdatedName . '}}');

        echo '    > create function ', $this->itemUpdatedName, '_update_post_count() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE OR REPLACE FUNCTION {$this->itemUpdatedName}_update_post_count()
RETURNS TRIGGER AS
$$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE {$tableUpdatedName}
            SET post_count = post_count - 1
            WHERE id = OLD.author_id;
    ELSIF (TG_OP = 'INSERT') THEN
        UPDATE {$tableUpdatedName}
            SET post_count = post_count + 1
            WHERE id = NEW.author_id;
    ELSE
        IF (NEW.author_id = OLD.author_id) THEN
            RETURN NULL;
        END IF;

        UPDATE {$tableUpdatedName}
            SET post_count = COALESCE((
                SELECT COUNT(id)
                FROM {$this->itemTriggeredName}
                WHERE author_id = {$tableUpdatedName}.id
                GROUP BY author_id
            ), 0)
            WHERE id IN (NEW.author_id, OLD.author_id);
    END IF;

    RETURN NULL;
END
$$
LANGUAGE plpgsql VOLATILE;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > create trigger ', $this->itemTriggeredName, '_after_insert_or_update_or_delete_update_', $this->itemUpdatedName, '_post_count ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
CREATE TRIGGER {$this->itemTriggeredName}_after_insert_or_update_or_delete_update_{$this->itemUpdatedName}_post_count
AFTER INSERT OR UPDATE OR DELETE ON {$this->itemTriggeredName}
FOR EACH ROW
EXECUTE PROCEDURE {$this->itemUpdatedName}_update_post_count();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        echo '    > drop trigger ', $this->itemTriggeredName, '_after_insert_or_update_or_delete_update_', $this->itemUpdatedName, '_post_count ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP TRIGGER {$this->itemTriggeredName}_after_insert_or_update_or_delete_update_{$this->itemUpdatedName}_post_count ON post;
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;

        echo '    > drop function ', $this->itemUpdatedName, '_update_post_count() ...';
        $time = microtime(true);
        $this->db->createCommand(<<<SQL
DROP FUNCTION {$this->itemUpdatedName}_update_post_count();
SQL
        )->execute();
        echo ' done (time: ', sprintf('%.3f', microtime(true) - $time), 's)', PHP_EOL;
    }
}
