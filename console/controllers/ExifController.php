<?php

namespace console\controllers;

use Yii;
use \yii\console\Controller;
use \yii\console\Exception;
use \yii\helpers\Console;
use \common\helpers\File;
use \common\models\Photo;

class ExifController extends Controller
{
    /**
     * @var string|null
     */
    public $date;

    /**
     * @inheritdoc
     */
    public $defaultAction = 'fill';

    /**
     * @inheritdoc
     */
    public function options($actionID)
    {
        $options = parent::options($actionID);
        if ($actionID === 'fill') {
            $options[] = 'date';
        }

        return $options;
    }

    /**
     * Resaves authors images. Optional --date=.
     * @return boolean
     * @throws Exception
     * @throws \Exception
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidParamException
     * @throws \lsolesen\pel\PelInvalidDataException
     * @throws \lsolesen\pel\PelJpegInvalidMarkerException
     */
    public function actionFill()
    {
        if (!empty($this->date)) {
            $date = date('Y-m-d H:i:s', strtotime($this->date));
        }

        $pathMedia  = Yii::getAlias('@media');
        $thumbsPath = Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'media';

        $text = 'Resaving images';

        if (!empty($date)) {
            $text .= ' from ' . $date;
            $condition = ['OR', ['exif_updated' => null], ['>=', 'exif_updated', $date]];
        } else {
            $condition = ['exif_updated' => null];
        }

        $text .=  '...';

        if (!Photo::find()->andWhere($condition)->andWhere(['content_type' => 'image/jpeg'])->exists()) {
            echo $text . ' nothing to resave. ', PHP_EOL;

            return $this::EXIT_CODE_NORMAL;
        }

        $photosTotal = Photo::find()->andWhere($condition)->andWhere(['content_type' => 'image/jpeg'])->count();
        $photosCount = $savedCount = $failedCount = 0;

        Console::startProgress(0, $photosTotal, $text . ' ', false);

        foreach (Photo::find()->andWhere($condition)->andWhere(['content_type' => 'image/jpeg'])->each(50) as $image) {
            $transaction = Yii::$app->getDb()->beginTransaction();

            try {
                $files = [];
                $fullPath = $pathMedia . $image->file;

                if (!file_exists($fullPath)) {
                    $failedCount++;

                    Yii::error('Image ' . $fullPath . ' from post ' . $image->post_id . ' doesn\'t exists', 'exif/fill');
                    continue;
                }

                $files[] = $fullPath;

                preg_match('/(.+)\..+?$/', $image->file, $matches);
                $path = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $thumbsPath . $matches[1]);

                foreach (glob($path . '*') as $thumb) {
                    if (preg_match('/' . preg_quote($path, '/') . '-\d+x\d+\.' . '/', $thumb)) {
                        $files[] = $thumb;
                    }
                }

                $failed = false;

                foreach ($files as $filename) {
                    if (File::addJpegMetadata($filename, $image)) {
                        $savedCount++;
                    } else {
                        $failedCount++;
                        $failed = true;

                        Yii::error('Can\'t add exif to ' . $filename, 'exif/fill');
                    }
                }

                $image->exif_updated = $failed ? null : date('Y-m-d H:i:s');
                $image->setScenario('administrator');
                $image->save();

                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollBack();

                throw $e;
            }

            Console::updateProgress(++$photosCount, $photosTotal);
        }

       $text = 'Done. Data changed in ' . $savedCount . ' images and thumbs';

        if ($failedCount) {
            $text .= ', failed ' . $failedCount;
        }

        Console::updateProgress($photosCount, $photosTotal, $text . ' ');
        Console::endProgress();

        return $this::EXIT_CODE_NORMAL;
    }
}
