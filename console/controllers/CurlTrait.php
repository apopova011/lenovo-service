<?php

namespace console\controllers;

/**
 * CurlTrait is trait with curl functions.
 */
trait CurlTrait
{
    /**
     * @param string $url
     * @param null|string $postFields
     * @param bool $allowRedirect
     * @return bool|string
     */
    protected function getByUrl($url, $postFields = null, $allowRedirect = false)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($c, CURLOPT_TIMEOUT, 10);
        if (!empty($postFields)) {
            curl_setopt($c, CURLOPT_POST, 1);
            curl_setopt($c, CURLOPT_POSTFIELDS, $postFields);
        }
        if ($allowRedirect) {
            curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);
        }

        $data = curl_exec($c);
        curl_close($c);

        return $data;
    }
}
