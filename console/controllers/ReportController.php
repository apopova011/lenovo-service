<?php

namespace console\controllers;

use Yii;
use \yii\console\Controller;
use \common\models\Post;
use \common\models\User;

class ReportController extends Controller
{
    /**
     * @var string|null
     */
    public $ids;

    /**
     * @inheritdoc
     */
    public function options($actionID)
    {
        $options = parent::options($actionID);
        if ($actionID === 'authors') {
            $options[] = 'ids';
        }

        return $options;
    }

    /**
     * Shows authors statistics. Optional --ids=.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionAuthors()
    {
        $authorIds = is_string($this->ids) ? 'AND author_id IN (' . $this->ids . ')' : '';
        $fourMonthsAgo = date('Y-m-d', mktime(0, 0, 0, date('m') - 4, date('d'), date('Y')));
        $today = date('Y-m-d');
        $db = Yii::$app->getDb();
        $postTable = $db->quoteTableName('{{%' . Post::tableName() . '}}');
        $userTable = $db->quoteTableName('{{%' . User::tableName() . '}}');

        $sql = <<<SQL
SELECT
    EXTRACT (EPOCH FROM date_trunc('month', published)) as date,
    author_id,
    u.slug as slug,
    COUNT(p.id) AS post_count,
    SUM(symbol_count) AS symbol_count,
    SUM(post_image_count(p.id))  AS image_count,
    SUM(show_count)              AS show_count
FROM {$postTable} p
INNER JOIN {$userTable} u ON (p.author_id = u.id)
WHERE
        date_trunc('month', published) >= '{$fourMonthsAgo}'
    AND date_trunc('month', published) <= '{$today}'
    {$authorIds}
GROUP BY date_trunc('month', published), author_id, u.slug
ORDER BY date_trunc('month', published), author_id, u.slug
SQL;
        $result = $db->createCommand($sql)->queryAll();

        echo "\n" . str_pad('date', 10)    . ' | ' . str_pad(   'author id',  9)                    . ' | ' . str_pad(   'slug',  20, ' ', STR_PAD_LEFT)                    . ' | ' . str_pad(   'post count' , 10, ' ', STR_PAD_LEFT) . ' | ' . str_pad(   'symbol count' , 12, ' ', STR_PAD_LEFT) . ' | ' . str_pad(   'image count' , 11)                    . ' | ' . str_pad(   'show count' , 10, ' ', STR_PAD_LEFT) . "\n";
        foreach ($result as $r) {
            echo date('Y-m-d', $r['date']) . ' | ' . str_pad($r['author_id'], 9, ' ', STR_PAD_LEFT) . ' | ' . str_pad($r['slug'], 20, ' ', STR_PAD_LEFT) . ' | ' . str_pad($r['post_count'], 10, ' ', STR_PAD_LEFT) . ' | ' . str_pad($r['symbol_count'], 12, ' ', STR_PAD_LEFT) . ' | ' . str_pad($r['image_count'], 11, ' ', STR_PAD_LEFT) . ' | ' . str_pad($r['show_count'], 10, ' ', STR_PAD_LEFT) . "\n";
        }

        return $this::EXIT_CODE_NORMAL;
    }
}
