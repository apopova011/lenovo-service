<?php

namespace console\controllers;

use Yii;
use \yii\console\Controller;
use \common\rbac\AuthorRule;
use \common\rbac\UserRoleRule;

class RbacController extends Controller
{
    /**
     * @inheritdoc
     */
    public $defaultAction = 'generate';

    /**
     * Generates rbac rules
     * @throws \Exception
     */
    public function actionGenerate()
    {
        echo 'Generating rules and permissions...';

        $authManager = Yii::$app->getAuthManager();

        $authManager->removeAll();

        // create roles

        $rule = new UserRoleRule();
        $authManager->add($rule);

        foreach ([
            'guest',
            'registered',
            'verified',
            'moderator',
            'writer',
            'contributor',
            'editor',
            'administrator',
        ] as $name) {
            $$name = $authManager->createRole($name);
            $$name->ruleName = $rule->name;
            $authManager->add($$name);
        }
        /** @var \yii\rbac\Role $guest */
        /** @var \yii\rbac\Role $registered */
        /** @var \yii\rbac\Role $verified */
        /** @var \yii\rbac\Role $moderator */
        /** @var \yii\rbac\Role $writer */
        /** @var \yii\rbac\Role $contributor */
        /** @var \yii\rbac\Role $editor */
        /** @var \yii\rbac\Role $administrator */

        // create permissions by author

        $rule = new AuthorRule;
        $authManager->add($rule);

        foreach ([
            'postUpdateAndDeleteOwn' => 'Update and delete own post',
            'commentUpdateAndDeleteOwn' => 'Update and delete own comment',
        ] as $name => $description) {
            $$name = $authManager->createPermission($name);
            $$name->description = $description;
            $$name->ruleName = $rule->name;
            $authManager->add($$name);
        }
        /** @var \yii\rbac\Permission $postUpdateAndDeleteOwn */
        /** @var \yii\rbac\Permission $commentUpdateAndDeleteOwn */

        // create permissions by section

        foreach ([
            'dashboard' => 'Access to dashboard',
            'count' => 'View count of symbols, posts, comments, users and tags',
            'postHitList' => 'View list of posts ordered by hits',
            'postHitNumber' => 'View number of hits post',
            'postIndexAndCreate' => 'Index and create post',
            'postIndexNotOwn' => 'Index not own post',
            'postUpdateAndDelete' => 'Update and delete post',
            'postAuthor' => 'Update post author',
            'postSpecialFeed' => 'Update post is_in_common_feed',
            'postIsSentToIndex' => 'Update post is_sent_to_index',
            'photoIndex' => 'Index and create photo',
            'commentIndexAndCreate' => 'Index and create comment',
            'commentIndexNotOwn' => 'Index not own comments',
            'commentUpdateAndDelete' => 'Update and delete comment',
            'commentMaxVoteValue' => 'Vote value is maximum',
            'userIndexAndCreate' => 'Index and create user',
            'userUpdateAndDelete' => 'Update and delete user',
            'userRole' => 'Update user role',
            'folderIndexAndCreate' => 'Index and create folder',
            'folderUpdateAndDelete' => 'Update and delete folder',
            'tagIndexAndCreate' => 'Index and create tag',
            'tagUpdateAndDelete' => 'Update and delete tag',
            'groupIndexAndCreate' => 'Index and create group',
            'groupUpdateAndDelete' => 'Update and delete group',
            'linkIndexAndCreate' => 'Index and create link',
            'linkUpdateAndDelete' => 'Update and delete link',
            'feedNavigationIndexAndCreate' => 'Index and create feed navigation',
            'feedNavigationUpdateAndDelete' => 'Update and delete feed navigation',
            'partnersIndexAndCreate' => 'Index and create partners',
            'partnersUpdateAndDelete' => 'Update and delete partners',
        ] as $name => $description) {
            $$name = $authManager->createPermission($name);
            $$name->description = $description;
            $authManager->add($$name);
        }
        /** @var \yii\rbac\Permission $dashboard */
        /** @var \yii\rbac\Permission $count */
        /** @var \yii\rbac\Permission $postHitList */
        /** @var \yii\rbac\Permission $postHitNumber */
        /** @var \yii\rbac\Permission $postIndexAndCreate */
        /** @var \yii\rbac\Permission $postIndexNotOwn */
        /** @var \yii\rbac\Permission $postUpdateAndDelete */
        /** @var \yii\rbac\Permission $postAuthor */
        /** @var \yii\rbac\Permission $postSpecialFeed */
        /** @var \yii\rbac\Permission $postIsSentToIndex */
        /** @var \yii\rbac\Permission $photoIndex */
        /** @var \yii\rbac\Permission $commentIndexAndCreate */
        /** @var \yii\rbac\Permission $commentIndexNotOwn */
        /** @var \yii\rbac\Permission $commentUpdateAndDelete */
        /** @var \yii\rbac\Permission $commentMaxVoteValue */
        /** @var \yii\rbac\Permission $userIndexAndCreate */
        /** @var \yii\rbac\Permission $userUpdateAndDelete */
        /** @var \yii\rbac\Permission $userRole */
        /** @var \yii\rbac\Permission $folderIndexAndCreate */
        /** @var \yii\rbac\Permission $folderUpdateAndDelete */
        /** @var \yii\rbac\Permission $tagIndexAndCreate */
        /** @var \yii\rbac\Permission $tagUpdateAndDelete */
        /** @var \yii\rbac\Permission $groupIndexAndCreate */
        /** @var \yii\rbac\Permission $groupUpdateAndDelete */
        /** @var \yii\rbac\Permission $linkIndexAndCreate */
        /** @var \yii\rbac\Permission $linkUpdateAndDelete */
        /** @var \yii\rbac\Permission $feedNavigationIndexAndCreate */
        /** @var \yii\rbac\Permission $feedNavigationUpdateAndDelete */
        /** @var \yii\rbac\Permission $partnersIndexAndCreate */
        /** @var \yii\rbac\Permission $partnersUpdateAndDelete */

        // assign dependency permissions

        $authManager->addChild($commentUpdateAndDeleteOwn, $commentUpdateAndDelete);
        $authManager->addChild($postUpdateAndDeleteOwn, $postUpdateAndDelete);

        // assign roles to permissions

        $authManager->addChild($moderator, $commentIndexAndCreate);
        $authManager->addChild($moderator, $commentIndexNotOwn);
        $authManager->addChild($moderator, $commentUpdateAndDelete);
        $authManager->addChild($moderator, $partnersIndexAndCreate);
        $authManager->addChild($moderator, $partnersUpdateAndDelete);

        $authManager->addChild($writer, $dashboard);
        $authManager->addChild($writer, $postIndexAndCreate);
        $authManager->addChild($writer, $postUpdateAndDeleteOwn);
        $authManager->addChild($writer, $commentIndexAndCreate);
        $authManager->addChild($writer, $commentUpdateAndDeleteOwn);
        $authManager->addChild($writer, $commentMaxVoteValue);
        $authManager->addChild($writer, $partnersIndexAndCreate);
        $authManager->addChild($writer, $partnersUpdateAndDelete);

        $authManager->addChild($contributor, $count);
        $authManager->addChild($contributor, $postHitList);
        $authManager->addChild($contributor, $postIndexNotOwn);
        $authManager->addChild($contributor, $postSpecialFeed);
        $authManager->addChild($contributor, $photoIndex);
        $authManager->addChild($contributor, $commentIndexNotOwn);
        $authManager->addChild($contributor, $commentUpdateAndDelete);
        $authManager->addChild($contributor, $partnersIndexAndCreate);
        $authManager->addChild($contributor, $partnersUpdateAndDelete);

        $authManager->addChild($editor, $postUpdateAndDelete);
        $authManager->addChild($editor, $postAuthor);
        $authManager->addChild($editor, $userIndexAndCreate);
        $authManager->addChild($editor, $userUpdateAndDelete);
        $authManager->addChild($editor, $tagIndexAndCreate);
        $authManager->addChild($editor, $tagUpdateAndDelete);
        $authManager->addChild($editor, $groupIndexAndCreate);
        $authManager->addChild($editor, $groupUpdateAndDelete);
        $authManager->addChild($editor, $partnersIndexAndCreate);
        $authManager->addChild($editor, $partnersUpdateAndDelete);

        $authManager->addChild($administrator, $postHitNumber);
        $authManager->addChild($administrator, $postIsSentToIndex);
        $authManager->addChild($administrator, $userRole);
        $authManager->addChild($administrator, $folderIndexAndCreate);
        $authManager->addChild($administrator, $folderUpdateAndDelete);
        $authManager->addChild($administrator, $linkIndexAndCreate);
        $authManager->addChild($administrator, $linkUpdateAndDelete);
        $authManager->addChild($administrator, $feedNavigationIndexAndCreate);
        $authManager->addChild($administrator, $feedNavigationUpdateAndDelete);
        $authManager->addChild($administrator, $partnersIndexAndCreate);
        $authManager->addChild($administrator, $partnersUpdateAndDelete);

        echo ' done.', PHP_EOL;

        return $this::EXIT_CODE_NORMAL;
    }
}
