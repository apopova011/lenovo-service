<?php

namespace console\controllers;

use Yii;
use \yii\base\Exception;
use \yii\console\Controller;
use \yii\helpers\Console;
use \yii\helpers\Json;
use \yii\web\Request;
use \common\models\Post;
use \common\models\StatisticPost;

/**
 * Fills statistics.
 *
 * @method bool|string getByUrl($url, $postFields = null, $allowRedirect = false)
 */
class StatsFillController extends Controller
{
    use CurlTrait;

    /**
     * Fills post statistics from Yandex metrika.
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidParamException
     */
    public function actionPost()
    {
        $lastDate = StatisticPost::find()->max('date');
        $now = time();
        $today = date('Y-m-d', $now);
        $sixMonthsAgo = date('Y-m-d', mktime(0, 0, 0, date('m') - 6, date('d'), date('Y')));

        if (!$lastDate) {
            $lastDate = $sixMonthsAgo;
        } else {
            $lastDate = date('Y-m-d', strtotime($lastDate));
        }

        $postCount = 0;
        $updatedPostCount = 0;
        $daysToFill = (strtotime($today) - strtotime($lastDate)) / 86400;  // 60 * 60 * 24
        $daysFilled = 0;

        Console::startProgress(0, $daysToFill, 'Filling ' . $lastDate . '... ', false);

        while ($lastDate <= $today) {
            $transaction = Yii::$app->getDb()->beginTransaction();

            try {
                $statistics = $this->getStatisticsFromMetrika(str_replace('-', '', $lastDate));
                if ($statistics === false || !array_key_exists('data', $statistics)) {
                    Yii::error($statistics, 'stats-fill/post');
                    throw new Exception('No data returned from metrika', 34);
                }

                Yii::$app->cacheCommon->set('lastSuccessGetStatisticsFromMetrika', $now);

                foreach ($statistics['data'] as $statistic) {
                    $showCount = $statistic['metrics'][0];
                    $url = $statistic['dimensions'][0]['name'];

                    if ($showCount < Yii::$app->params['postMinShowCount']) {
                        break;
                    }

                    $url = parse_url($url)['path'];
                    $showCountByUrl = Json::encode([$url => $showCount]);

                    $request = new Request();
                    $request->setUrl($url);
                    $request->setBaseUrl('');
                    $path = Yii::$app->frontendUrlManager->parseRequest($request);

                    if (!is_array($path)) {
                        continue;
                    }

                    /** @var array $path */
                    if ($path[0] !== 'post/view') {
                        continue;
                    }

                    $postId = $path[1]['post_id'];
                    $postDate = Post::find()->select(['published'])->andWhere(['id' => $postId])->scalar();
                    if ($postDate === false) {
                        continue;
                    }

                    $oldShowCount = StatisticPost::find()
                        ->select(['show_count_by_url'])
                        ->andWhere(['date' => $lastDate])
                        ->andWhere(['post_id' => $postId])
                        ->scalar();
                    if ($oldShowCount !== false) {
                        $insertedUrls = Json::decode($oldShowCount, true);
                        $insertedUrls[$url] = $showCount;
                        $showCount = array_sum($insertedUrls);
                        $showCountByUrl = Json::encode($insertedUrls);
                        StatisticPost::updateAll(
                            [
                                'show_count' => $showCount,
                                'show_count_by_url' => $showCountByUrl
                            ],
                            [
                                'AND',
                                ['date' => $lastDate],
                                ['post_id' => $postId]
                            ]
                        );

                        $updatedPostCount++;
                    } else {
                        $statisticPost = new StatisticPost();
                        $statisticPost->date = $lastDate;
                        $statisticPost->post_id = $postId;
                        $statisticPost->show_count = $showCount;
                        $statisticPost->show_count_by_url = $showCountByUrl;
                        $statisticPost->save();

                        $postCount++;
                    }
                }

                $transaction->commit();
            } catch (Exception $exception) {
                $transaction->rollBack();

                $exceptionMessage = $exception->getMessage();
                $message = 'Filling stopped on ' . $lastDate . ' with message: ' . $exceptionMessage;

                if ($exception->getCode() === 34) {
                    Yii::error($exceptionMessage, 'getStatisticsFromMetrika');
                } else {
                    Yii::error($exceptionMessage);
                }

                echo $message, PHP_EOL;

                return $this::EXIT_CODE_ERROR;
            }

            $lastDateToTime = strtotime($lastDate);
            $lastDate = date('Y-m-d', mktime(0, 0, 0, date('m', $lastDateToTime), date('d', $lastDateToTime) + 1, date('Y', $lastDateToTime)));

            Console::updateProgress(++$daysFilled, $daysToFill,  'Filling ' . $lastDate . '... ');
        }

        Console::updateProgress($daysToFill, $daysToFill, 'Done. Added: ' . $postCount . ', updated: ' . $updatedPostCount . ' ');
        Console::endProgress();

        return $this::EXIT_CODE_NORMAL;
    }

    /**
     * @param string $date
     * @return mixed
     * @throws Exception
     * @throws \yii\base\InvalidParamException
     */
    protected function getStatisticsFromMetrika($date)
    {
        $params = Yii::$app->params;

        $data = $this->getByUrl('https://beta.api-metrika.yandex.ru/stat/v1/data/?oauth_token=' . $params['metrikaToken'] . '&id=' . $params['metrikaId'] . '&date1=' . $date . '&date2=' . $date . '&pretty=1&limit=500&dimensions=ym:pv:URL&metrics=ym:pv:pageviews');
        if ($data === false) {
            throw new Exception('Failed to get statistics from metrika', 34);
        }

        return Json::decode($data, true);
    }
}
