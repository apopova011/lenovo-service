<?php

namespace console\controllers;

use Yii;
use \yii\console\Controller;
use \yii\helpers\Console;

class AvatarController extends Controller
{
    /**
     * @inheritdoc
     */
    public $defaultAction = 'generate';

    /**
     * Creates default avatars
     * @param integer $width
     * @param integer $height
     * @throws \yii\base\InvalidParamException
     * @return bool
     */
    public function actionGenerate($width, $height)
    {
        $chars = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','А','Б','В','Г','Д','Е','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Э','Ю','Я'];
        $keys = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'];

        $imgDir = Yii::getAlias('@frontend') . '/web/media/' . Yii::$app->params['directoryAvatars'];
        if (!is_dir($imgDir)) {
            mkdir($imgDir, 0775);
        }
        $fontDir = Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'fonts';

        $totalAvatars = count($keys) * count($chars);
        $createdAvatars = 0;

        Console::startProgress(0, $totalAvatars, 'Generating avatars... ', false);

        foreach ($chars as $i => $char) {
            if (($char === 'Q') || ($char === 'Ц') || ($char === 'Щ') || ($char === 'Д')) {
                $top = -2;
            } elseif ($char === 'Й') {
                $top = 2;
            } else {
                $top = 0;
            }

            foreach ($keys as $key) {
                $name = $key . bin2hex($char);

                $image = new \Imagick();
                $draw = new \ImagickDraw();
                $pixel = new \ImagickPixel('#' . Yii::$app->params['backgrounds'][$key]);

                $image->newImage($width, $height, $pixel, 'png');

                $draw->setFillColor(new \ImagickPixel('#FFFFFF'));
                $draw->setFont("$fontDir/segoeuib.ttf");
                $draw->setFontSize(30);
                $draw->setGravity(\Imagick::GRAVITY_CENTER);

                $image->annotateImage($draw, 0, $top, 0, $char);
                $image->setImageFormat('png');
                $image->writeImage("$imgDir/$name.png");

                Console::updateProgress(++$createdAvatars, $totalAvatars);
            }
        }

        Console::updateProgress($createdAvatars, $totalAvatars, 'Avatars generated. ');
        Console::endProgress();

        return $this::EXIT_CODE_NORMAL;
    }
}
