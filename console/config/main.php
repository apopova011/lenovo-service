<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'lenovo-service-console',
    'name' => 'console app of ' . $params['brandName'],
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
          ],
    ],
    'components' => [
        'db' => [
            'schemaCache' => 'cache',
        ],
        'log' => [
            'flushInterval' => 1,
            'targets' => [
                'errorCommon' => [
                    'exportInterval' => 1,
                ],
                'validation' => [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/validation.log',
                    'levels' => [],
                    'categories' => ['validation'],
                    'except' => ['application'],
                    'logVars' => [],
                    'exportInterval' => 1,
                ],
                'sql' => [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/sql.log',
                    'rotateByCopy' => (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'),
                    'levels' => ['info'],
                    'categories' => ['yii\db\Command*'],
                    'logVars' => [],
                    'exportInterval' => 1,
                    'maxFileSize' => 65536,
                ],
                'getStatisticsFromMetrika' => [
                    'class' => 'common\log\EmailTarget',
                    'schemaCache' => 'cache',
                    'levels' => ['error'],
                    'logVars' => [],
                    'categories' => ['getStatisticsFromMetrika'],
                    'message' => [
                        'to' => [$params['emailBugs']],
                        'subject' => 'Metrika stats error',
                    ],
                ],
                'emailError' => [
                    'class' => 'common\log\EmailTarget',
                    'schemaCache' => 'cache',
                    'levels' => ['error'],
                    'logVars' => [],
                    'except' => ['getStatisticsFromMetrika'],
                    'message' => [
                        'to' => [$params['emailBugs']],
                        'subject' => 'Console error',
                    ],
                ],
                'emailError' => [
                    'class' => 'common\log\EmailTarget',
                    'schemaCache' => 'cache',
                    'levels' => ['error'],
                    'logVars' => [],
                    'message' => [
                        'to' => [$params['emailBugs']],
                        'subject' => 'Console error',
                    ],
                ],
            ],
        ],
        'mailer' => [
            'messageConfig' => [
                'from' => [$params['emailInfo'] => $params['brandName']],
            ],
        ],
        'frontendUrlManager' => [
            'class' => 'frontend\components\FrontendUrlManager',
        ],
    ],
    'params' => $params,
];
