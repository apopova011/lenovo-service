<?php

namespace backend\models;

use Yii;
use \yii\data\ActiveDataProvider;
use \common\models\AbstractSearch;
use \common\models\Post;
use \common\models\User;
use \common\validators\DatePgValidator;

/**
 * CommentSearch represents the model behind the search form about Comment.
 */
class CommentSearch extends AbstractSearch
{
    /**
     * @var string|null
     */
    public $content;

    /**
     * @var string|null
     */
    public $status;

    /**
     * @var string|null
     */
    public $created_min;

    /**
     * @var string|null
     */
    public $created_max;

    /**
     * @var string|null
     */
    public $post_id;

    /**
     * @var string|null
     */
    public $author_id;

    /**
     * @var string|null
     */
    public $parent;

    /**
     * @var string|null
     */
    public $any_field;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent', 'content', 'post_id', 'author_id', 'any_field'], 'filter', 'filter' => 'trim'],
            [['content', 'status', 'post_id', 'author_id', 'any_field'], 'string'],
            [['parent'], 'integer'],
            [['created_min', 'created_max'], DatePgValidator::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getBaseModelClass()
    {
        return 'common\\models\\' . parent::getBaseModelClass();
    }

    /**
     * @return array
     */
    public function searchLabels()
    {
        $filters = [
            'excellent' => 'Отличные',
            'banned'    => 'Забаненные',
        ];

        if (Yii::$app->getUser()->can('commentIndexNotOwn')) {
            $filters['usefulForMe'] = 'Моё';
        }

        return $filters;
    }

    /**
     * @inheritdoc
     * @throws \yii\base\UnknownPropertyException
     * @throws \yii\base\InvalidParamException
     */
    public function search($params)
    {
        /** @var \common\models\Comment $baseModelClass */
        $baseModelClass = $this->getBaseModelClass();

        $query = $baseModelClass::find()->with(['author', 'post.folderMain', 'parent']);
        if (!empty($params['filter'])) {
            $query->{$params['filter']}();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ]
        ]);

        $sort = $dataProvider->getSort();
        $sort->attributes['created']['default'] = SORT_DESC;

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if (!empty($this->parent)) {
            $query->andWhere('path ~ \'*.' . $this->parent . '.*{1,}\'');
        }

        if (!empty($this->post_id)) {
            $postIds = Post::find()
                ->select(['id'])
                ->andWhere('UPPER(name) LIKE :name', [
                    ':name' => '%' . mb_strtoupper($this->post_id, Yii::$app->charset) . '%'
                ]);
            $query->andWhere(['post_id' => $postIds]);
        }

        if (!empty($this->author_id)) {
            $authorIds = User::find()
                ->select(['id'])
                ->andWhere('UPPER(title) LIKE :title', [
                    ':title' => '%' . mb_strtoupper($this->author_id, Yii::$app->charset) . '%'
                ]);
            $query->andWhere(['author_id' => $authorIds]);
        }

        $this->addConditionLike($query, 'content');
        $this->addConditionEqual($query, 'status');
        $this->addConditionLess($query, 'created', 'created_max');
        $this->addConditionMore($query, 'created', 'created_min');
        $this->addConditionLikeMultiple($query, [
            'content',
            'user_title',
            'user_ip',
            'user_agent',
        ], 'any_field');

        return $dataProvider;
    }
}
