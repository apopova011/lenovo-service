<?php

namespace backend\models;

use Yii;
use \yii\data\ActiveDataProvider;
use \yii\db\Expression;
use \common\models\AbstractSearch;
use \common\models\Folder;
use \common\models\Post;
use \common\models\PostFolder;
use \common\validators\DatePgValidator;

/**
 * PostSearch represents the model behind the search form about Post.
 */
class PostSearch extends AbstractSearch
{
    /**
     * @var string|null
     */
    public $name;

    /**
     * @var string|null
     */
    public $any_field;

    /**
     * @var string|null
     */
    public $published_min;

    /**
     * @var string|null
     */
    public $published_max;

    /**
     * @var string|null
     */
    public $published_filter;

    /**
     * @var boolean|null
     */
    public $is_in_common_feed;

    /**
     * @var integer|null
     */
    public $author_id;

    /**
     * @var string|null
     */
    public $folders_and_tags;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'published_filter', 'folders_and_tags', 'any_field'], 'filter', 'filter' => 'trim'],
            [['name', 'published_filter', 'folders_and_tags', 'any_field'], 'string'],
            [['published_min', 'published_max'], DatePgValidator::className()],
            [['is_in_common_feed'], 'boolean'],
            [['author_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getBaseModelClass()
    {
        return static::getPostModelClass();
    }

    /**
     * @return string model class
     */
    public static function getPostModelClass()
    {
        $controller = Yii::$app->controller;
        $postModelClass = $controller::className();

        $pos = strrpos($postModelClass, '\\');
        if ($pos !== false) {
            $postModelClass = substr($postModelClass, $pos + 1);
        }

        if (substr($postModelClass, -10) === 'Controller') {
            $postModelClass = substr($postModelClass, 0, -10);
        }

        return 'common\\models\\' . $postModelClass;
    }

    /**
     * @return array
     */
    public function searchLabels()
    {
        return [];
    }

    /**
     * @inheritdoc
     * @throws \yii\base\UnknownPropertyException
     * @throws \yii\base\InvalidParamException
     */
    public function search($params)
    {
        /** @var \common\models\Post $postModelClass */
        $postModelClass = static::getPostModelClass();

        /** @var \common\models\PostQuery $query */
        $query = $postModelClass::find()->select([
            '*',
            'popularity' => 'CASE
                WHEN published IS NULL THEN 0
                WHEN published > now() THEN 0
                WHEN published + \'1 week\' < NOW() THEN show_count
                ELSE ROUND(604800 / extract(epoch from (now() - published)) * show_count)
            END'
        ]);

        $with = ['folderMain', 'author'];

        if (method_exists($postModelClass, 'getFolders')) {
            $with[] = 'folders';
        }
        if (method_exists($postModelClass, 'getTags')) {
            $with[] = 'tags';
        }

        $query->with($with);

        if (!empty($params['filter'])) {
            $query->{$params['filter']}();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ]
        ]);

        $dataProvider->getSort()->attributes['popularity'] = [
            'asc'     => ['popularity' => SORT_ASC],
            'desc'    => ['popularity' => SORT_DESC],
            'default' => SORT_DESC,
        ];

        $sort = $dataProvider->getSort();
        $sort->attributes['published']['default'] = SORT_DESC;
        $sort->attributes['published']['asc'] = [new Expression('published NULLS FIRST')];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $this->addConditionLike($query, 'name');
        $this->addConditionMore($query, 'published', 'published_min');
        $this->addConditionLessNotEqual($query, 'published', 'published_max');
        foreach (['is_in_common_feed', 'author_id'] as $attribute) {
            $this->addConditionEqual($query, $attribute);
        }
        if (!empty($this->folders_and_tags)) {
            foreach (explode(',', $this->folders_and_tags) as $i => $value) {
                $value = mb_strtoupper(trim($value), Yii::$app->charset);

                $folderIds =   Folder::find()->select('id'     )->andWhere('UPPER(name) LIKE \'%' . $value . '%\'');
                $postIds = PostFolder::find()->select('post_id')->andWhere(['IN', 'folder_id', $folderIds]);

                $query->andWhere([
                    'OR',
                    'UPPER(tags_name) LIKE \'%' . $value . '%\'',
                    ['IN', 'id', $postIds],
                ]);
            }
        }
        $this->addConditionLikeMultiple($query, ['slug', 'name', 'title_seo', 'description', 'content'], 'any_field');

        return $dataProvider;
    }

    /**
     * @param int|string $year
     * @return array
     */
    protected static function presetForYear($year)
    {
        return [
            'title' => substr($year, 2),
            'min'   => $year . '-01-01',
            'max'   => date('Y', strtotime('+1 year', strtotime($year . '-01-01'))) . '-01-01',
        ];
    }

    /**
     * @param int|string $year
     * @param int|string $month
     * @return array
     */
    protected static function presetForMonth($year, $month)
    {
        $date = $year . '-' . $month;

        return [
            'title' => substr($year, 2) . '.' . $month,
            'min'   => $date . '-01',
            'max'   => date('Y-m', strtotime('+1 month', strtotime($date . '-01'))) . '-01',
        ];
    }

    /**
     * @param int $timestamp
     * @return int
     */
    protected static function plusWeek($timestamp)
    {
        return $timestamp + 604800;
    }

    /**
     * @return array
     */
    public static function presetsForPublished()
    {
        /** @var Post $postModelClass */
        $postModelClass = static::getPostModelClass();
        $publishedWeeks = $postModelClass::find()->forWeeksList()->asArray()->all();

        $presets = [];
        $yearLast = date('Y');
        $monthLast = date('m');
        $mondayThis = strtotime('Monday this week 00:00:00');
        $weekAgoTo = strtotime('today');
        $weekAgoFrom = $weekAgoTo - 604800;
        $dateAsNowYear = -1;
        $dateAsNowFrom = strtotime($dateAsNowYear . ' year', $weekAgoFrom);
        foreach ($publishedWeeks as $week) {
            $timestampCurrent = (int)$week['timestamp'];

            $monthCurrent = date('m', $timestampCurrent);
            if ($monthCurrent !== $monthLast) {
                $presets[] = static::presetForMonth($yearLast, $monthLast);

                $monthLast = $monthCurrent;

                $yearCurrent = date('Y', $timestampCurrent);
                if ($yearCurrent !== $yearLast) {
                    $presets[] = static::presetForYear($yearLast);

                    $yearLast = $yearCurrent;
                }
            }

            if ($dateAsNowFrom > $timestampCurrent) {
                $dateAsNowFrom = date('Y-m-d', $dateAsNowFrom);
                $dateAsNowTo = date('Y-m-d', strtotime($dateAsNowYear . ' year', $weekAgoTo));
                $presets[] = [
                    'title' => $dateAsNowFrom . ' – ' . $dateAsNowTo,
                    'min'   => $dateAsNowFrom,
                    'max'   => $dateAsNowTo,
                    'class' => 'strong',
                ];

                $dateAsNowFrom = strtotime(--$dateAsNowYear . ' year', $weekAgoFrom);
            }

            $period = [
                'min' => date('Y-m-d', $timestampCurrent),
                'max' => date('Y-m-d', static::plusWeek($timestampCurrent)),
            ];
            if ($timestampCurrent === $mondayThis) {
                $period['title'] = 'Текущая неделя';
                $period['class'] = 'strong';
            } else {
                $period['title'] = $week['title'];
            }
            $presets[] = $period;
        }
        $presets[] = static::presetForMonth($yearLast, $monthLast);
        $presets[] = static::presetForYear($yearLast);

        return $presets;
    }
}
