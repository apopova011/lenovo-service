<?php

namespace backend\models;

use Yii;
use \yii\data\ActiveDataProvider;
use \common\models\AbstractSearch;
use \common\models\Post;
use \common\validators\DatePgValidator;

/**
 * PhotoSearch represents the model behind the search form about Photo.
 */
class PhotoSearch extends AbstractSearch
{
    /**
     * @var string|null
     */
    public $file;

    /**
     * @var integer|null
     */
    public $post_id;

    /**
     * @var string|null
     */
    public $post_published_min;

    /**
     * @var string|null
     */
    public $post_published_max;

    /**
     * @var integer|null
     */
    public $author_id;

    /**
     * @var string|null
     */
    public $any_field;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file', 'post_id', 'any_field'], 'filter', 'filter' => 'trim'],
            [['file', 'post_id', 'any_field'], 'string'],
            [['post_published_min', 'post_published_max'], DatePgValidator::className()],
            [['author_id'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function searchLabels()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function getBaseModelClass()
    {
        return 'common\\models\\' . parent::getBaseModelClass();
    }

    /**
     * @inheritdoc
     * @throws \yii\base\UnknownPropertyException
     * @throws \yii\base\InvalidParamException
     */
    public function search($params)
    {
        /** @var \common\models\Photo $baseModelClass */
        $baseModelClass = $this->getBaseModelClass();

        $query = $baseModelClass::find()->with(['post']);
        if (!empty($params['filter'])) {
            $query->{$params['filter']}();
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if (!empty($this->post_id)) {
            $postIds = Post::find()
                ->select(['id'])
                ->andWhere('UPPER(name) LIKE :name', [
                    ':name' => '%' . mb_strtoupper($this->post_id, Yii::$app->charset) . '%'
                ]);
            $query->andWhere(['post_id' => $postIds]);
        }

        $this->addConditionEqual($query, 'author_id');
        $this->addConditionLike($query, 'file');
        $this->addConditionMore($query, 'post_published', 'post_published_min');
        $this->addConditionLessNotEqual($query, 'post_published', 'post_published_max');
        $this->addConditionLikeMultiple($query, [
            'file',
            'content_type',
            'author_title',
            'author_email',
            'post_content_img_alt'
        ], 'any_field');

        return $dataProvider;
    }
}
