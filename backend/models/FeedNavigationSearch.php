<?php

namespace backend\models;

use \yii\data\ActiveDataProvider;
use \common\models\AbstractSearch;

/**
 * FeedNavigationSearch represents the model behind the search form about FeedNavigation.
 */
class FeedNavigationSearch extends AbstractSearch
{
    /**
     * @var string|null
     */
    public $page_resource_type;

    /**
     * @var integer|null
     */
    public $page_resource_id;

    /**
     * @var string|null
     */
    public $link_resource_type;

    /**
     * @var integer|null
     */
    public $link_resource_id;

    /**
     * @var string|null
     */
    public $url;

    /**
     * @var string|null
     */
    public $name;

    /**
     * @var integer|null
     */
    public $order_min;

    /**
     * @var integer|null
     */
    public $order_max;

    /**
     * @var string|null
     */
    public $any_field;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url', 'page_resource_type', 'link_resource_type', 'any_field'], 'filter', 'filter' => 'trim'],
            [['name', 'url', 'page_resource_type', 'link_resource_type', 'any_field'], 'string'],
            [['page_resource_id', 'link_resource_id', 'order_min', 'order_max'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getBaseModelClass()
    {
        return 'common\\models\\' . parent::getBaseModelClass();
    }

    /**
     * @inheritdoc
     * @throws \yii\base\UnknownPropertyException
     * @throws \yii\base\InvalidParamException
     */
    public function search($params)
    {
        /** @var \common\models\FeedNavigation $baseModelClass */
        $baseModelClass = $this->getBaseModelClass();

        $query = $baseModelClass::find();
        if (!empty($params['filter'])) {
            $query->{$params['filter']}();
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        foreach (['page_resource_type', 'link_resource_type', 'name', 'url'] as $attribute) {
            $this->addConditionLike($query, $attribute);
        }
        foreach (['page_resource_id', 'link_resource_id'] as $attribute) {
            $this->addConditionEqual($query, $attribute);
        }
        $this->addConditionLess($query, 'order', 'order_max');
        $this->addConditionMore($query, 'order', 'order_min');
        $this->addConditionLikeMultiple($query, ['url', 'name'], 'any_field');

        return $dataProvider;
    }
}
