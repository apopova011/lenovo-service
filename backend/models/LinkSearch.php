<?php

namespace backend\models;

use \yii\data\ActiveDataProvider;
use \common\models\AbstractSearch;

/**
 * LinkSearch represents the model behind the search form about Link.
 */
class LinkSearch extends AbstractSearch
{
    /**
     * @var string|null
     */
    public $name;

    /**
     * @var string|null
     */
    public $url;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'filter', 'filter' => 'trim'],
            [['name', 'url'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getBaseModelClass()
    {
        return 'common\\models\\' . parent::getBaseModelClass();
    }

    /**
     * @inheritdoc
     * @throws \yii\base\UnknownPropertyException
     * @throws \yii\base\InvalidParamException
     */
    public function search($params)
    {
        /** @var \common\models\Link $baseModelClass */
        $baseModelClass = $this->getBaseModelClass();

        $query = $baseModelClass::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        foreach (['name', 'url'] as $attribute) {
            $this->addConditionLike($query, $attribute);
        }

        return $dataProvider;
    }
}
