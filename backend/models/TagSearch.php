<?php

namespace backend\models;

use \yii\data\ActiveDataProvider;
use \common\models\AbstractSearch;
use \common\validators\DatePgValidator;

/**
 * TagSearch represents the model behind the search form about Tag.
 */
class TagSearch extends AbstractSearch
{
    /**
     * @var string|null
     */
    public $slug;

    /**
     * @var string|null
     */
    public $name;

    /**
     * @var string|null
     */
    public $title;

    /**
     * @var string|null
     */
    public $title_seo;

    /**
     * @var string|null
     */
    public $updated_min;

    /**
     * @var string|null
     */
    public $updated_max;

    /**
     * @var integer|null
     */
    public $group_id;

    /**
     * @var string|null
     */
    public $any_field;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slug', 'name', 'title', 'title_seo', 'any_field'], 'filter', 'filter' => 'trim'],
            [['slug', 'name', 'title', 'title_seo', 'any_field'], 'string'],
            [['group_id'], 'integer'],
            [['updated_min', 'updated_max'], DatePgValidator::className()],
        ];
    }

    /**
     * @return array
     */
    public function searchLabels()
    {
        return [
            'hasSeo' => 'Засеошенные',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getBaseModelClass()
    {
        return 'common\\models\\' . parent::getBaseModelClass();
    }

    /**
     * @inheritdoc
     * @throws \yii\base\UnknownPropertyException
     * @throws \yii\base\InvalidParamException
     */
    public function search($params)
    {
        /** @var \common\models\Tag $baseModelClass */
        $baseModelClass = $this->getBaseModelClass();

        $query = $baseModelClass::find()->with(['group']);
        if (!empty($params['filter'])) {
            $query->{$params['filter']}();
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ]
        ]);

        $dataProvider->getSort()->attributes['updated']['default'] = SORT_DESC;

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $this->addConditionEqual($query, 'group_id');
        foreach (['slug', 'name', 'title', 'title_seo'] as $attribute) {
            $this->addConditionLike($query, $attribute);
        }
        $this->addConditionLess($query, 'updated', 'updated_max');
        $this->addConditionMore($query, 'updated', 'updated_min');
        $this->addConditionLikeMultiple($query, ['slug', 'name', 'title', 'title_seo', 'description'], 'any_field');

        return $dataProvider;
    }
}
