<?php

namespace backend\models;

use \yii\data\ActiveDataProvider;
use \common\models\AbstractSearch;

/**
 * GroupSearch represents the model behind the search form about Group.
 */
class GroupSearch extends AbstractSearch
{
    /**
     * @var string|null
     */
    public $name;

    /**
     * @var string|null
     */
    public $any_field;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'any_field'], 'filter', 'filter' => 'trim'],
            [['name', 'any_field'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getBaseModelClass()
    {
        return 'common\\models\\' . parent::getBaseModelClass();
    }

    /**
     * @return array
     */
    public function searchLabels()
    {
        return [];
    }

    /**
     * @inheritdoc
     * @throws \yii\base\UnknownPropertyException
     * @throws \yii\base\InvalidParamException
     */
    public function search($params)
    {
        /** @var \common\models\Group $baseModelClass */
        $baseModelClass = $this->getBaseModelClass();

        $query = $baseModelClass::find();
        if (!empty($params['filter'])) {
            $query->{$params['filter']}();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $this->addConditionLike($query, 'name');
        $this->addConditionLikeMultiple($query, [
            'name',
            'slug',
        ], 'any_field');

        return $dataProvider;
    }
}
