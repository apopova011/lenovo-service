<?php

namespace backend\models;

use common\models\AbstractSearch;
use common\models\Partners;
use InvalidArgumentException;
use yii\data\ActiveDataProvider;

class PartnersSearch extends AbstractSearch
{
    /**
     * @var string
     */
    public $country;

    /**
     * @var string
     */
    public $region;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $address;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string|null
     */
    public $site;

    /**
     * @var bool|null
     */
    public $act;
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['country', 'region', 'city', 'address', 'name', 'site'], 'filter', 'filter' => 'trim'],
            [['country', 'region', 'city', 'address', 'name', 'site'], 'string'],
            [['act'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getBaseModelClass(): string
    {
        return 'common\\models\\' . parent::getBaseModelClass();
    }

    /**
     * @inheritdoc
     * @throws InvalidArgumentException
     */
    public function search($params): ActiveDataProvider
    {
        /** @var Partners $baseModelClass */
        $baseModelClass = $this->getBaseModelClass();

        $query = $baseModelClass::find();
        if (!empty($params['filter'])) {
            $query->{$params['filter']}();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $dataProvider->getSort()->attributes['id']['default'] = SORT_DESC;

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        foreach (['country', 'region', 'city', 'address', 'name', 'site'] as $attribute) {
            $this->addConditionLike($query, $attribute);
        }

        $this->addConditionEqual($query, 'act');

        return $dataProvider;
    }
}
