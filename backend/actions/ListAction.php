<?php
namespace backend\actions;

use Yii;
use yii\base\Action;
use yii\web\NotFoundHttpException;
use common\helpers\Url;

/**
 * ListAction represents an action for list.
 *
 * @property \common\controllers\AbstractController $controller
 */
class ListAction extends Action
{
    /**
     * @var string view
     */
    public $view = 'list';

    /**
     * @var string|null name of permission
     */
    public $permissionName;

    /**
     * @var string filter class
     */
    public $filterClass;

    /**
     * @var string filter scenario
     */
    public $filterScenario = 'search';

    /**
     * @var string|boolean default filter
     */
    public $defaultFilter = false;

    /**
     * @var array config GridView
     */
    public $configGridView = [];

    /**
     * @var boolean auto refresh page
     */
    public $withAutoUpdate = false;

    /**
     * @return mixed the result of the action
     * @throws NotFoundHttpException if wrong filter
     * @throws \yii\base\UnknownPropertyException
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function run()
    {
        $filterClass = $this->filterClass ? $this->filterClass : 'backend\\models\\' . $this->controller->getBaseModelClass() . 'Search';

        /** @var \common\models\AbstractSearch $filterModel */
        $filterModel = new $filterClass(/*$this->filterScenario*/);

        return $this->controller->render('/actions/' . $this->view, $this->getParams($filterModel));
    }

    /**
     * @param \common\models\AbstractSearch $filterModel
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\base\UnknownPropertyException
     * @throws \yii\web\ForbiddenHttpException
     */
    protected function getParams($filterModel)
    {
        $filters = method_exists($filterModel, 'searchLabels') ? $filterModel->searchLabels() : [];
        if (empty($_GET['filter'])) {
            if (!empty($this->defaultFilter)) {
                $this->controller->redirect(Url::current(['filter' => $this->defaultFilter]));
            }

            $filterActive = false;
        } else {
            $filterActive = $_GET['filter'];

            if (!array_key_exists($filterActive, $filters)) {
                throw new NotFoundHttpException('The requested filter does not exist.');
            }
        }

        $modelClass = $filterModel->getBaseModelClass();
        $user = Yii::$app->getUser();

        if ($this->permissionName !== false) {
            if ($this->permissionName === null) {
                $this->permissionName = $this->controller->getPermissionName($modelClass, 'index');
            }
            if (!$user->can($this->permissionName)) {
                throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
            }
        }

        $configGridView = $this->configGridView;
        $configGridView['filterModel']  = $filterModel;
        $configGridView['dataProvider'] = $filterModel->search(Yii::$app->getRequest()->getQueryParams());

        if ($this->permissionName !== false) {
            $permissionName = $this->controller->getPermissionName($modelClass, 'index', false);
            if (
                Yii::$app->getAuthManager()->getItem($permissionName) !== null &&
                !$user->can($permissionName)
            ) {
                /** @var \yii\db\ActiveQuery $query */
                $query = $configGridView['dataProvider']->query;
                if ($query->hasMethod('usefulForUser')) {
                    $query->usefulForUser($user->id);
                }
            }
        }

        $formName = $filterModel->formName();
        $configGridView['filterUrl'] = Yii::$app->getRequest()->getQueryParams();
        $configGridView['filterUrl'][0] = '/' . Yii::$app->controller->getRoute();
        if (!empty($_GET[$formName]['text'])) {
            $configGridView['filterUrl'][$formName]['text'] = $_GET[$formName]['text'];
        }

        return [
            'modelLabel' => constant($modelClass . '::LABEL_PLURAL_' . str_replace('-', '_', Yii::$app->language)),
            'filterModel' => $filterModel,
            'configGridView' => $configGridView,
            'withCreate' => method_exists($this->controller, 'actionCreate') || array_key_exists('create', $this->controller->actions()),
            'filters' => $filters,
            'filterActive' => $filterActive,
            'withAutoUpdate' => $this->withAutoUpdate,
        ];
    }
}
