<?php

namespace backend\actions;

use Yii;
use \yii\base\Action;
use \yii\helpers\Json;
use \common\helpers\Html;

/**
 * SelectNamesAction provides model's data for selectize widget.
 * @property \common\controllers\AbstractController $controller
 */
class SelectNamesAction extends Action
{
    /**
     * @var string model class
     */
    public $modelClass;

    /**
     * @var string model scenario
     */
    public $modelScenario = 'administrator';

    /**
     * @return mixed the result of the action
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function run()
    {
        $modelClass = $this->modelClass ? $this->modelClass : 'common\\models\\' . $this->controller->getBaseModelClass();
        /** @var \yii\db\ActiveRecord $model */
        $model = new $modelClass;
        $model->setScenario($this->modelScenario);
        $text = mb_strtoupper($_GET['text'], Yii::$app->charset);
        $query = $model::find();
        $items = $query->andWhere(['like', 'UPPER(name)', $text])->orderBy('name')->limit(15)->all();

        $result = Html::prepareDataForSelectize($items);

        return Json::encode($result);
    }
}
