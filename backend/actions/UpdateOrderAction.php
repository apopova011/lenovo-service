<?php

namespace backend\actions;

use \yii\base\Action;

/**
 * UpdateOrderAction provides resaving models order.
 * @property \common\controllers\AbstractController $controller
 */
class UpdateOrderAction extends Action
{
    /**
     * @var string model class
     */
    public $modelClass;

    /**
     * @var string order attribute
     */
    public $attribute = 'order';

    /**
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function run($order)
    {
        /** @var \yii\db\ActiveRecord $modelClass */
        $modelClass = $this->modelClass ? $this->modelClass : 'common\\models\\' . $this->controller->getBaseModelClass();

        foreach (explode(',', $order) as $i => $id) {
            if (!$modelClass::updateAll([$this->attribute => $i], ['id' => $id])) {
                return false;
            }
        }

        return true;
    }
}
