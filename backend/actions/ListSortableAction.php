<?php

namespace backend\actions;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * ListSortableAction represents an action for a sortable list.
 */
class ListSortableAction extends ListAction
{
    /**
     * @var string view
     */
    public $view = 'list_sortable';

    /**
     * @var array config for order change.
     */
    public $changeOrder;

    /**
     * @param \common\models\AbstractSearch $filterModel
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\base\UnknownPropertyException
     * @throws \yii\web\ForbiddenHttpException
     */
    protected function getParams($filterModel)
    {
        $params = parent::getParams($filterModel);
        $configGridView = $params['configGridView'];

        /** @var \yii\db\ActiveRecord $modelClass */
        $modelClass = $filterModel->getBaseModelClass();
        $get = Yii::$app->getRequest()->get($filterModel->formName());
        $allChosen = true;

        if (array_key_exists('filter', $this->changeOrder)) {
            foreach ($this->changeOrder['filter'] as $i => $filter) {
                if (!array_key_exists('values', $filter)) {
                    $query = $modelClass::find()
                        ->andWhere($filter['attribute'] . ' IS NOT NULL')
                        ->orderBy($filter['attribute']);

                    foreach (ArrayHelper::getColumn($this->changeOrder['filter'], 'attribute') as $attribute) {
                        if ($attribute === $filter['attribute']) {
                            break;
                        }
                        $query->andWhere([$attribute => $get[$attribute]]);
                    }

                    $models = $query->distinct()->all();

                    if (array_key_exists('value', $filter)) {
                        $values = [];
                        foreach ($models as $model) {
                            $values[$model[$filter['attribute']]] = call_user_func($filter['value'], $model);
                        }
                    } else {
                        $values = ArrayHelper::getColumn($models, $filter['attribute']);
                        $values = array_combine($values, $values);
                    }

                    // если в параметрах осталось значение от прошлого выбора, которого нет сейчас
                    if (!$values && $get && array_key_exists($filter['attribute'], $get)) {
                        $queryParams = Yii::$app->getRequest()->getQueryParams();
                        unset($queryParams[$filterModel->formName()][$filter['attribute']]);
                        $queryParams[0] = '/' . Yii::$app->controller->getRoute();
                        $this->controller->redirect($queryParams);
                    }

                    $this->changeOrder['filter'][$i]['values'] = $values;
                }

                $allChosen = $allChosen && (empty($this->changeOrder['filter'][$i]['values']) || !empty($get[$filter['attribute']]));
            }
        }

        if ($allChosen) {
            foreach ($configGridView['columns'] as $i => $column) {
                if (is_array($column) && array_key_exists('attribute', $column)) {
                    $configGridView['columns'][$i]['contentOptions']['data-attribute'] = $column['attribute'];
                }
            }

            $configGridView['filterModel'] = null;
            $configGridView['dataProvider']->sort = false;
            $configGridView['dataProvider']->query->orderBy($this->changeOrder['attribute']);
        }

        return array_merge($params, [
            'configGridView' => $configGridView,
            'changeOrder'    => $this->changeOrder,
            'allChosen'      => $allChosen,
        ]);
    }
}
