<?php

namespace backend\actions;

use Yii;
use yii\base\Action;
use yii\web\NotFoundHttpException;

/**
 * ViewAction represents an action for view.
 * @property \common\controllers\AbstractController $controller
 */
class ViewAction extends Action
{
    /**
     * @var string view
     */
    public $view = 'view';

    /**
     * @var string|null name of permission
     */
    public $permissionName;

    /**
     * @var string model class
     */
    public $modelClass;

    /**
     * @param string|int|array $id model id
     * @return mixed the result of the action
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\base\UnknownPropertyException
     */
    public function run($id)
    {
        $modelClass = $this->modelClass ? $this->modelClass : 'common\\models\\' . $this->controller->getBaseModelClass();

        /** @var \yii\db\ActiveRecord|null $model */
        $model = $modelClass::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException('The requested item does not exist.');
        }

        if ($this->permissionName === null) {
            $this->permissionName = $this->controller->getPermissionName($modelClass, 'view');
        }
        if (!Yii::$app->getUser()->can($this->permissionName, $model)) {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        return $this->controller->render('/actions/' . $this->view, [
            'model' => $model,
        ]);
    }
}
