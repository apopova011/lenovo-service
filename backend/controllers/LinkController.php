<?php

namespace backend\controllers;

use \yii\filters\AccessControl;
use \kartik\grid\ActionColumn;
use \backend\actions\CreateAction;
use \backend\actions\DeleteAction;
use \backend\actions\ListAction;
use \backend\actions\SelectNamesAction;
use \backend\actions\UpdateAction;
use \common\controllers\AbstractController;

/**
 * LinkController implements the CRUD actions for Link model.
 */
class LinkController extends AbstractController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'update' =>      UpdateAction::className(),
            'delete' =>      DeleteAction::className(),
            'create' =>      CreateAction::className(),
            'select' => SelectNamesAction::className(),
            'index' => [
                'class' => ListAction::className(),
                'configGridView' => [
                    'columns' => [
                        'name',
                        [
                            'attribute' => 'url',
                            'format' => 'url',
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{update}',
                        ],
                    ],
                ],
            ],
        ];
    }
}
