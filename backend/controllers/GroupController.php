<?php

namespace backend\controllers;

use Yii;
use \yii\filters\AccessControl;
use \kartik\grid\ActionColumn;
use \backend\actions\CreateAction;
use \backend\actions\DeleteAction;
use \backend\actions\ListAction;
use \backend\actions\UpdateAction;
use \common\controllers\AbstractController;
use \common\helpers\Html;

/**
 * GroupController implements the CRUD actions for Group model.
 */
class GroupController extends AbstractController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function actions()
    {
        return [
            'create' => CreateAction::className(),
            'update' => UpdateAction::className(),
            'delete' => DeleteAction::className(),
            'index' => [
                'class' => ListAction::className(),
                'configGridView' => [
                    'columns' => [
                        'name',
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{update}',
                            'buttons' => [
                                'update' => function ($url, $model, $key) {
                                    if (!Yii::$app->getUser()->can('groupUpdateAndDelete', $model)) {
                                        return '';
                                    }

                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                        'title' => Yii::t('yii', 'Update'),
                                        'aria-label' => Yii::t('yii', 'Update'),
                                        'data-pjax' => '0',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
