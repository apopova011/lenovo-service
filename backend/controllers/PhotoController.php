<?php

namespace backend\controllers;

use Yii;
use \yii\filters\AccessControl;
use \yii\helpers\ArrayHelper;
use \kartik\grid\ActionColumn;
use \backend\actions\ListAction;
use \backend\widgets\RangeColumn;
use \common\controllers\AbstractController;
use \common\helpers\Html;
use \common\helpers\Url;
use \common\models\Photo;
use \common\models\User;

/**
 * PhotoController implements the CRUD actions for Photo model.
 */
class PhotoController extends AbstractController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => ListAction::className(),
                'permissionName' => 'photoIndex',
                'configGridView' => [
                    'columns' => [
                        [
                            'attribute' => 'file',
                            'format' => 'raw',
                            'value' => function ($model) {
                                /** @var Photo $model */
                                $absoluteUrl = '//' . Yii::$app->params['frontendDomain'] . Url::thumbnail($model->file);
                                return Html::img($absoluteUrl, [
                                    'width' => 100,
                                    'height' => 100,
                                    'title' => $model->post_content_img_alt,
                                ]);
                            }
                        ],
                        [
                            'attribute' => 'post_id',
                            'format' => 'raw',
                            'value' => function ($model) {
                                /** @var Photo $model */
                                $post = $model->post;
                                $htmlParams = empty($post->title_seo) ? [] : ['title' => $post->title_seo];
                                $name = Html::tag('span', $post->name, $htmlParams);
                                if (!Yii::$app->getUser()->can('postUpdateAndDelete')) {
                                    return $name;
                                }

                                return Html::a($name, [
                                    'post-' . $post->type . '/update',
                                    'id' => $model->post_id
                                ], ['data-pjax' => 0]);
                            }
                        ],
                        [
                            'class' => RangeColumn::className(),
                            'attribute' => 'post_published',
                            'format' => 'datetime',
                            'filterInputType' => 'datetime-local',
                        ],
                        [
                            'attribute' => 'author_id',
                            'filter' => ArrayHelper::map(User::find()->authors()->all(), 'id', 'title'),
                            'format' => 'raw',
                            'value' => function ($model) {
                                /** @var Photo $model */
                                $user = Yii::$app->getUser();
                                $name = str_replace(' ', '<br />', $model->author['title']);
                                if ($model->author['id'] === $user->getIdentity()->id) {
                                    $name = Html::tag('strong', $name);
                                }

                                if (!$user->can('userUpdateAndDelete', $model)) {
                                    return $name;
                                }

                                return Html::a($name, [
                                    'user/update',
                                    'id' => $model->author['id']
                                ], ['data-pjax' => 0]);
                            }
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{in_frontend}{update}',
                            'buttons' => [
                                'in_frontend' => function ($url, $model) {
                                    /** @var Photo $model */
                                    return Html::a(
                                        '<span class="glyphicon glyphicon-eye-open"></span>',
                                        $model->post->getUrl(true),
                                        [
                                            'target' => '_blank',
                                            'data-pjax' => '0',
                                        ]
                                    );
                                },
                                'update' => function ($url, $model, $key) {
                                    /** @var Photo $model */
                                    if (!Yii::$app->getUser()->can('postUpdateAndDelete', $model->post)) {
                                        return '';
                                    }

                                    return Html::a(
                                        '<span class="glyphicon glyphicon-pencil"></span>',
                                        [
                                            'post/update',
                                            'id' => $model->post_id
                                        ],
                                        [
                                            'title' => Yii::t('yii', 'Update'),
                                            'aria-label' => Yii::t('yii', 'Update'),
                                            'data-pjax' => '0',
                                        ]
                                    );
                                },
                            ],
                        ],
                    ]
                ],
            ],
        ];
    }
}
