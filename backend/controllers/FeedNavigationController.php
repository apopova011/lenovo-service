<?php

namespace backend\controllers;

use \yii\filters\AccessControl;
use \kartik\grid\ActionColumn;
use \backend\actions\CreateAction;
use \backend\actions\DeleteAction;
use \backend\actions\ListSortableAction;
use \backend\actions\UpdateAction;
use \backend\actions\UpdateOrderAction;
use \backend\widgets\RangeColumn;
use \common\controllers\AbstractController;
use \common\helpers\Html;
use \common\models\FeedNavigation;

/**
 * FeedNavigationController implements the CRUD actions for FeedNavigation model.
 */
class FeedNavigationController extends AbstractController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     */
    public function actions()
    {
        return [
            'update' => UpdateAction::className(),
            'delete' => DeleteAction::className(),
            'create' => CreateAction::className(),
            'index' => [
                'class' => ListSortableAction::className(),
                'changeOrder' => [
                    'filter' => [
                        [
                            'attribute' => 'page_resource_type',
                            'label' => 'Страница',
                            'values' => FeedNavigation::pageTypes(),
                        ],
                        [
                            'attribute' => 'page_resource_id',
                            'label' => 'ИД страницы',
                            'value' => function ($model) {
                                /** @var FeedNavigation $model */
                                if (!$model->page){
                                    return $model->page_resource_id;
                                }

                                return $model->page->title ? $model->page->title : $model->page->name;
                            },
                        ],
                    ],
                    'attribute' => 'order',
                ],
                'configGridView' => [
                    'columns' => [
                        [
                            'attribute' => 'page_resource_type',
                            'label' => 'Страница',
                            'filter' => FeedNavigation::pageTypes(),
                            'value' => function ($model) {
                                /** @var FeedNavigation $model */
                                return $model::pageTypes()[$model->page_resource_type];
                            },
                        ],
                        [
                            'attribute' => 'page_resource_id',
                            'format' => 'raw',
                            'value' => function ($model) {
                                /** @var FeedNavigation $model */
                                if (!$model->page){
                                    return null;
                                }

                                $name = $model->page->title ? $model->page->title : $model->page->name;
                                return Html::a(
                                    $name,
                                    $model::getResourceUrl($model->page_resource_type, $model->page_resource_id),
                                    ['data-pjax' => 0]
                                );
                            },
                        ],
                        [
                            'attribute' => 'link_resource_type',
                            'label' => 'Ссылка на',
                            'filter' => FeedNavigation::linkTypes(),
                            'value' => function ($model) {
                                /** @var FeedNavigation $model */
                                return $model::linkTypes()[$model->link_resource_type];
                            },
                        ],
                        [
                            'attribute' => 'link_resource_id',
                            'format' => 'raw',
                            'value' => function ($model) {
                                /** @var FeedNavigation $model */
                                $name = $model->link->title ? $model->link->title : $model->link->name;
                                return Html::a(
                                    $name,
                                    $model::getResourceUrl($model->link_resource_type, $model->link_resource_id),
                                    ['data-pjax' => 0]
                                );
                            },
                        ],
                        [
                            'attribute' => 'url',
                            'format' => 'url',
                        ],
                        'name',
                        [
                            'class' => RangeColumn::className(),
                            'attribute' => 'order',
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{update}',
                        ],
                    ],
                ],
            ],
            'update-order' => UpdateOrderAction::className(),
        ];
    }
}
