<?php

namespace backend\controllers;

use backend\actions\CreateAction;
use backend\actions\DeleteAction;
use backend\actions\ListAction;
use backend\actions\UpdateAction;
use common\controllers\AbstractController;
use common\helpers\Html;
use common\models\Partners;
use kartik\grid\ActionColumn;
use kartik\grid\BooleanColumn;
use yii\filters\AccessControl;

/**
 * Service Controller
 */
class PartnersController extends AbstractController
{
    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(): array
    {
        return [
            'update' => UpdateAction::class,
            'delete' => DeleteAction::class,
            'index' => [
                'class' => ListAction::class,
                'configGridView' => [
                    'columns' => [
                        'name',
                        'country',
                        'region',
                        'city',
                        'address',
                        [
                            'attribute' => 'site',
                            'format' => 'raw',
                            'value' => static function ($model) {
                                /** @var Partners $model */
                                return Html::a($model->site, $model->site, [
                                    '_target' => 'blank',
                                    'data-pjax' => 0,
                                ]);
                            },
                        ],
                        [
                            'class' => BooleanColumn::class,
                            'attribute' => 'act',
                            'trueLabel' => 'Да',
                            'falseLabel' => 'Нет',
                            'vAlign' => 'middle',
                            'value' => static function ($model) {
                                /** @var Partners $model */
                                return !$model->act;
                            },
                        ],
                        [
                            'class' => ActionColumn::class,
                            'template' => '{update}',
                        ],
                    ],
                ],
            ],
            'create' => CreateAction::class,
        ];
    }
}
