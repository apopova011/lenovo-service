<?php

namespace backend\controllers;

use Yii;
use \yii\filters\AccessControl;
use \yii\helpers\Json;
use \yii\web\ForbiddenHttpException;
use \yii\web\NotFoundHttpException;
use \kartik\grid\ActionColumn;
use \backend\actions\DeleteAction;
use \backend\actions\ListAction;
use \backend\actions\UpdateAction;
use \backend\widgets\RangeColumn;
use \common\controllers\AbstractController;
use \common\helpers\Html;
use \common\models\Comment;

/**
 * CommentController implements the CRUD actions for Comment model.
 */
class CommentController extends AbstractController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function actions()
    {
        return [
            'update' => UpdateAction::className(),
            'delete' => DeleteAction::className(),
            'index' => [
                'class' => ListAction::className(),
                'configGridView' => [
                    'columns' => [
                        [
                            'attribute' => 'post_id',
                            'format' => 'raw',
                            'value' => function ($model) {
                                /** @var Comment $model */
                                $user = Yii::$app->getUser();
                                $name = $model->post['name'];
                                if ($model->post['author_id'] === $user->getIdentity()->id) {
                                    $name = Html::tag('strong', $name);

                                    if (!$user->can('postUpdateAndDeleteOwn', $model->post)) {
                                        return $name;
                                    }
                                }

                                if (!$user->can('postUpdateAndDelete', $model->post)) {
                                    return $name;
                                }

                                $htmlParams = [
                                    'data-pjax' => 0,
                                ];

                                if (!empty($model->post['title_seo'])) {
                                    $htmlParams['title'] = $model->post['title_seo'];
                                }
                                return Html::a(
                                    $name,
                                    ['post/update', 'id' => $model->post['id']],
                                    $htmlParams
                                );
                            }
                        ],
                        [
                            'attribute' => 'parent',
                            'label' => 'Ответ на',
                            'format' => 'raw',
                            'value' => function ($model) {
                                /** @var Comment $model */
                                if (!$model->parent) {
                                    return '';
                                }

                                $user = Yii::$app->getUser();
                                $name = $model->parent['id'];
                                if ($model->parent['author_id'] === $user->getIdentity()->id) {
                                    $name = Html::tag('strong', $name);

                                    if (!$user->can('commentUpdateAndDeleteOwn', $model)) {
                                        return $name;
                                    }
                                }

                                if (!$user->can('commentUpdateAndDelete', $model)) {
                                    return $name;
                                }

                                return Html::a(
                                    $name,
                                    [
                                        'comment/update',
                                        'id' => $model->parent['id']
                                    ],
                                    [
                                        'title' => $model->parent['content'],
                                        'data-pjax' => 0
                                    ]
                                );
                            }
                        ],
                        'content',
                        [
                            'class' => RangeColumn::className(),
                            'attribute' => 'created',
                            'format' => 'datetime',
                            'filterInputType' => 'datetime-local',
                        ],
                        [
                            'attribute' => 'author_id',
                            'format' => 'raw',
                            'value' => function ($model) {
                                /** @var Comment $model */
                                $user = Yii::$app->getUser();
                                $name = $model->author['title'];
                                if ($model->author['id'] === $user->getIdentity()->id) {
                                    $name = Html::tag('strong', $name);
                                }

                                if (!$user->can('userUpdateAndDelete', $model)) {
                                    return $name;
                                }

                                return Html::a($name,
                                    [
                                        'user/update',
                                        'id' => $model->author['id']
                                    ],
                                    [
                                        'title' => $model->author['slug'],
                                        'data-pjax' => 0,
                                    ]
                                );
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'filter' => Comment::dictionaryStatusLabels(),
                            'format' => 'raw',
                            'value' => function ($model) {
                                /** @var Comment $model */
                                if (!Yii::$app->getUser()->can('commentUpdateAndDelete')) {
                                    return $model->status;
                                }

                                $voteStatuses = Comment::$voteStatuses;
                                $statuses = in_array($model->status, $voteStatuses, true) !== false ? [$model->status => $model->status] : [];

                                foreach (Comment::dictionaryStatusLabels() as $status) {
                                    if (in_array($status, $voteStatuses, true) === false) {
                                        $statuses[$status] = $status;
                                    }
                                }

                                return Html::activeDropDownList($model, 'status', $statuses, [
                                    'class' => 'form-control update-status',
                                    'data-id' => $model->id,
                                ]);
                            }
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{in_frontend}{update}',
                            'buttons' => [
                                'in_frontend' => function ($url, $model) {
                                    /** @var Comment $model */
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                                        $model->post->getUrl(true) . '#comment-' . $model->id, [
                                            'target' => '_blank',
                                            'data-pjax' => '0',
                                        ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    /** @var Comment $model */
                                    if (!Yii::$app->getUser()->can('commentUpdateAndDelete', $model)) {
                                        return '';
                                    }

                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                        $url, [
                                            'title' => Yii::t('yii', 'Update'),
                                            'aria-label' => Yii::t('yii', 'Update'),
                                            'data-pjax' => '0',
                                        ]);
                                },
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param integer $id
     * @param string $status
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionUpdateStatus($id, $status)
    {
        if (!Yii::$app->getUser()->can('commentUpdateAndDelete')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        /** @var Comment $comment */
        $comment = Comment::findOne(['id' => $id]);

        if (!$comment) {
            throw new NotFoundHttpException('The requested item does not exist.');
        }

        if ($comment->status !== $status) {
            $comment->setScenario('administrator');

            $comment->status = $status;
            $comment->save();
        }

        return Json::encode(['status' => $comment->status]);
    }
}
