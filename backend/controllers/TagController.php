<?php

namespace backend\controllers;

use Yii;
use \yii\filters\AccessControl;
use \yii\helpers\ArrayHelper;
use \kartik\grid\ActionColumn;
use \backend\actions\CreateAction;
use \backend\actions\DeleteAction;
use \backend\actions\ListAction;
use \backend\actions\SelectNamesAction;
use \backend\actions\UpdateAction;
use \backend\widgets\RangeColumn;
use \common\controllers\AbstractController;
use \common\helpers\Html;
use \common\models\Group;
use \common\models\Tag;

/**
 * TagController implements the CRUD actions for Tag model.
 */
class TagController extends AbstractController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     */
    public function actions()
    {
        return [
            'update' =>      UpdateAction::className(),
            'delete' =>      DeleteAction::className(),
            'create' =>      CreateAction::className(),
            'select' => SelectNamesAction::className(),
            'index' => [
                'class' => ListAction::className(),
                'configGridView' => [
                    'columns' => [
                        'name',
                        [
                            'attribute' => 'title',
                            'contentOptions' => function ($model) {
                                /** @var Tag $model */
                                $title = ArrayHelper::getValue($model, 'title_seo');
                                return empty($title) ? [] : ['title' => $title];
                            },
                        ],
                        'title_seo',
                        [
                            'attribute' => 'group_id',
                            'format' => 'raw',
                            'filter' => ArrayHelper::map(Group::find()->asArray()->all(), 'id', 'name'),
                            'value' => function ($model) {
                                /** @var Tag $model */
                                if (!$model->group_id) {
                                    return null;
                                }

                                $name = $model->group->name;

                                if (!Yii::$app->getUser()->can('groupUpdateAndDelete', $model)) {
                                    return $name;
                                }

                                return Html::a($name, ['group/update', 'id' => $model->group_id], ['data-pjax' => '0']);
                            }
                        ],
                        [
                            'class' => RangeColumn::className(),
                            'attribute' => 'updated',
                            'format' => 'datetime',
                            'filterInputType' => 'datetime-local',
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{in_frontend}{update}',
                            'buttons' => [
                                'in_frontend' => function ($url, $model) {
                                    /** @var Tag $model */
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $model->getUrl(true), ['target' => '_blank']);
                                },
                                'update' => function ($url, $model, $key) {
                                    if (!Yii::$app->getUser()->can('tagUpdateAndDelete', $model)) {
                                        return '';
                                    }

                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                        'title' => Yii::t('yii', 'Update'),
                                        'aria-label' => Yii::t('yii', 'Update'),
                                        'data-pjax' => '0',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
