<?php

namespace backend\controllers;

use Yii;
use \yii\filters\AccessControl;
use \yii\helpers\ArrayHelper;
use \yii\web\Controller;
use \common\models\Comment;
use \common\models\Photo;
use \common\models\Post;
use \common\models\Tag;
use \common\models\User;

class DashboardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $authorId = Yii::$app->getUser()->id;
        $postTableName = Post::tableName();

        $now = time();
        $sunday = strtotime('Sunday this week 23:59:59');
        $fourWeeksFromSunday = date('Y-m-d H:i:s', mktime(23, 59, 59, date('m', $sunday), date('d', $sunday) - 35, date('Y', $sunday)));
        $now = date('Y-m-d H:i:s', $now);

        $postCounts   = Post::find()->isPublished()->select(['count' => 'count(id)', 'symbol_count' => 'COALESCE(sum(char_length(content)), 0)'])                     ->asArray()->one();
        $myPostCounts = Post::find()->isPublished()->select(['count' => 'count(id)', 'symbol_count' => 'COALESCE(sum(char_length(content)), 0)'])->byAuthor($authorId)->asArray()->one();

        $postCountsByWeek   = ArrayHelper::map(Post::find()->publishedBetweenByWeek($fourWeeksFromSunday, $now)->select(['count' => 'count(id)', 'symbol_count' => 'sum(symbol_count)'])                     ->asArray()->all(), 'count', 'symbol_count');
        $myPostCountsByWeek = ArrayHelper::map(Post::find()->publishedBetweenByWeek($fourWeeksFromSunday, $now)->select(['count' => 'count(id)', 'symbol_count' => 'sum(symbol_count)'])->byAuthor($authorId)->asArray()->all(), 'count', 'symbol_count');

        return $this->render('index', [
            'popularPostsDay'       => Post::find()->popularInLastDay(1, 10)->asArray()->all(),
            'popularPostsThreeDays' => Post::find()->popularInLastDay(3, 10)->asArray()->all(),
            'popularPostsWeek'      => Post::find()->popularInLastDay(7, 10)->asArray()->all(),

            'notInterestingFeed' => Post::find()->notInteresting()->inCommonFeed()->limit(10)->asArray()->all(),
            'notInterestingAll'  => Post::find()->notInteresting()                ->limit(10)->asArray()->all(),

            'symbolCount'    => $postCounts['symbol_count'],
            'mySymbolCount'  => $myPostCounts['symbol_count'],
            'postCount'      => $postCounts['count'],
            'myPostCount'    => $myPostCounts['count'],
            'photoCount'     => Photo::find()                                 ->count('id'),
            'myPhotoCount'   => Photo::find()            ->byAuthor($authorId)->count('id'),
            'commentCount'   => Comment::find()->showed()                     ->count('id'),
            'myCommentCount' => Comment::find()->showed()->byAuthor($authorId)->count('id'),
            'userCount'      => User::find()                                  ->count('id'),
            'tagCount'       => Tag::find()                                   ->count('id'),

            'symbolCounts'    => array_values($postCountsByWeek),
            'mySymbolCounts'  => array_values($myPostCountsByWeek),
            'postCounts'      => array_keys($postCountsByWeek),
            'myPostCounts'    => array_keys($myPostCountsByWeek),
            'photoCounts'     => Photo::find()  ->postPublishedBetweenByWeek($fourWeeksFromSunday, $now)                               ->select('count(id)')->column(),
            'myPhotoCounts'   => Photo::find()  ->postPublishedBetweenByWeek($fourWeeksFromSunday, $now)          ->byAuthor($authorId)->select('count(id)')->column(),
            'commentCounts'   => Comment::find()->createdBetweenByWeek($fourWeeksFromSunday, $now)      ->showed()                     ->select('count(id)')->column(),
            'myCommentCounts' => Comment::find()->createdBetweenByWeek($fourWeeksFromSunday, $now)      ->showed()->byAuthor($authorId)->select('count(id)')->column(),
            'userCounts'      => User::find()   ->createdBetweenByWeek($fourWeeksFromSunday, $now)                                     ->select('count(id)')->column(),

            'notPublishedPosts'   => Post::find()->notPublishedOrInFuture()->select(['id', 'name', 'author_id'])->orderBy([$postTableName . '.id' => SORT_DESC])                     ->asArray()->all(),
            'myNotPublishedPosts' => Post::find()->notPublishedOrInFuture()->select(['id', 'name'             ])->orderBy([$postTableName . '.id' => SORT_DESC])->byAuthor($authorId)->asArray()->all(),
        ]);
    }
}
