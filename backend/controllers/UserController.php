<?php

namespace backend\controllers;

use Yii;
use \yii\filters\AccessControl;
use \kartik\grid\ActionColumn;
use \backend\actions\CreateAction;
use \backend\actions\DeleteAction;
use \backend\actions\ListAction;
use \backend\actions\UpdateAction;
use \backend\widgets\RangeColumn;
use \common\controllers\AbstractController;
use \common\helpers\Html;
use \common\models\User;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends AbstractController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function actions()
    {
        return [
            'update' => UpdateAction::className(),
            'delete' => DeleteAction::className(),
            'index' => [
                'class' => ListAction::className(),
                'configGridView' => [
                    'columns' => [
                        [
                            'attribute' => 'role',
                            'filter' => User::dictionaryRoleLabels(),
                            'value' => function ($model) {
                                /** @var User $model */
                                return User::dictionaryRoleLabels()[$model->role];
                            }
                        ],
                        'email',
                        'vk_id',
                        'title',
                        [
                            'class' => RangeColumn::className(),
                            'attribute' => 'created',
                            'format' => 'datetime',
                            'filterInputType' => 'datetime-local',
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{in_frontend}{update}',
                            'buttons' => [
                                'in_frontend' => function ($url, $model) {
                                    /** @var User $model */
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $model->getUrl(true), [
                                        'target' => '_blank',
                                        'data-pjax' => '0',
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    if (!Yii::$app->getUser()->can('userUpdateAndDelete', $model)) {
                                        return '';
                                    }

                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                        'title' => Yii::t('yii', 'Update'),
                                        'aria-label' => Yii::t('yii', 'Update'),
                                        'data-pjax' => '0',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ],
            ],
            'create' => CreateAction::className(),
        ];
    }
}
