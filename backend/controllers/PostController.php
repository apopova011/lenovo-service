<?php

namespace backend\controllers;

use Yii;
use \yii\filters\AccessControl;
use \yii\helpers\ArrayHelper;
use \yii\web\BadRequestHttpException;
use \yii\web\ForbiddenHttpException;
use \kartik\grid\ActionColumn;
use \backend\actions\DeleteAction;
use \backend\actions\ListAction;
use \backend\actions\SelectNamesAction;
use \backend\actions\UpdateAction;
use \backend\models\PostSearch;
use \backend\widgets\AutocompleteColumn;
use \backend\widgets\RangeColumn;
use \common\controllers\AbstractController;
use \common\helpers\Html;
use \common\models\Folder;
use \common\models\Post;
use \common\models\Tag;
use \common\models\User;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends AbstractController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actions()
    {
        $indexGridViewColumns = [
            [
                'attribute' => 'name',
                'format'    => 'html',
                'value'     => function ($model) {
                    /** @var Post $model */
                    $htmlParams = empty($model->title_seo) ? [] : ['title' => $model->title_seo];
                    return Html::tag('span', $model->name, $htmlParams);
                }
            ],
            [
                'class'           => RangeColumn::className(),
                'attribute'       => 'published',
                'presets'         => PostSearch::presetsForPublished(),
                'filterInputType' => 'datetime',
                'format'          => 'raw',
                'value'           => function ($model) {
                    return str_replace(' ', '<br />', $model->published);
                }
            ],
            [
                'mergeHeader' => true,
                'attribute'   => 'symbol_count',
            ],
            [
                'class'    => ActionColumn::className(),
                'template' => '{in_frontend}{update}',
                'buttons'  => [
                    'in_frontend' => function ($url, $model) {
                        /** @var Post $model */
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $model->getUrl(true), [
                            'target'    => '_blank',
                            'data-pjax' => '0',
                        ]);
                    },
                    'update'      => function ($url, $model, $key) {
                        if (!Yii::$app->getUser()->can('postUpdateAndDelete', $model)) {
                            return '';
                        }

                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax'  => '0',
                        ]);
                    },
                ],
            ],
        ];

        $add = [];
        $user = Yii::$app->getUser();

        if ($user->can('postSpecialFeed')) {
            $add[] = [
                'attribute'       => 'is_in_common_feed',
                'filter'          => [
                    'Нет',
                    'Да',
                ],
                'format'          => 'boolean',
                'label'           => 'Лента',
                'sortLinkOptions' => ['title' => 'Отображать в общих лентах?'],
            ];
        }

        if ($user->can('postIndexNotOwn')) {
            $add[] = [
                'attribute' => 'author_id',
                'filter'    => ArrayHelper::map(User::find()->authors()->all(), 'id', 'title'),
                'format'    => 'html',
                'value'     => function ($model) {
                    /** @var Post $model */
                    $user = Yii::$app->getUser();
                    $name = str_replace(' ', '<br />', $model->author['title']);
                    if ($model->author['id'] === $user->getIdentity()->id) {
                        $name = Html::tag('strong', $name);
                    }

                    if (!$user->can('userUpdateAndDelete', $model)) {
                        return $name;
                    }

                    return Html::a($name, [
                        'user/update',
                        'id' => $model->author['id'],
                    ], ['data-pjax' => 0]);
                }
            ];
        }

        $baseModelClass = 'common\\models\\' . $this->getBaseModelClass();
        if (method_exists($baseModelClass, 'getFolders') || method_exists($baseModelClass, 'getTags')) {
            $add[] = [
                'class'       => AutocompleteColumn::className(),
                'attribute'   => 'folders_and_tags',
                'url'         => '/post/select-folders-and-tags',
                'label'       => 'Разделы',
                'encodeLabel' => false,
                'format'      => 'raw',
                'value'       => function ($model) {
                    /** @var Post $model */
                    $user = Yii::$app->getUser();
                    $elements = [];
                    if ($model->hasProperty('folders')) {
                        foreach ($model->folders as $folder) {
                            $element = $folder['name'];
                            if ($folder->id === $model->folder_id_main) {
                                $element = '<u>' . $element . '</u>';
                            }

                            if ($user->can('folderUpdateAndDelete', $model)) {
                                $elements[] = Html::a(
                                    $element,
                                    ['folder/update', 'id' => $folder['id']],
                                    ['title' => $folder['title'], 'data-pjax' => 0]
                                );
                            } else {
                                $elements[] = $element;
                            }
                        }
                    }

                    if ($model->hasProperty('tags')) {
                        foreach ($model->tags as $tag) {
                            $element = '<em>' . $tag['name'] . '</em>';

                            if ($user->can('tagUpdateAndDelete', $model)) {
                                $elements[] = Html::a(
                                    $element,
                                    ['tag/update', 'id' => $tag['id']],
                                    ['title' => $tag['title'], 'data-pjax' => 0]
                                );
                            } else {
                                $elements[] = $element;
                            }
                        }
                    }

                    return implode(', ', $elements);
                }
            ];
        }

        $indexGridViewColumns = array_merge(
            array_slice($indexGridViewColumns, 0, 2),
            $add,
            array_slice($indexGridViewColumns, 2)
        );

        if ($user->can('postHitNumber')) {
            array_splice(
                $indexGridViewColumns,
                count($indexGridViewColumns) - 2,
                0,
                [[
                    'attribute'       => 'popularity',
                    'mergeHeader'     => true,
                    'label'           => 'ИП',
                    'sortLinkOptions' => ['title' => 'Индекс популярности за неделю'],
                ]]
            );
        }

        return [
            'update' => [
                'class'          => UpdateAction::className(),
                'permissionName' => 'postUpdateAndDelete'
            ],
            'delete' => [
                'class'          => DeleteAction::className(),
                'permissionName' => 'postUpdateAndDelete'
            ],
            'select' => [
                'class' => SelectNamesAction::className(),
            ],
            'index'  => [
                'class'          => ListAction::className(),
                'permissionName' => 'postIndexAndCreate',
                'filterClass'    => PostSearch::className(),
                'configGridView' => [
                    'columns' => $indexGridViewColumns,
                ],
            ],
        ];
    }

    /**
     * @return \yii\web\Response
     * @throws \yii\base\UnknownPropertyException
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (!Yii::$app->getUser()->can('postIndexAndCreate')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        $session = Yii::$app->getSession();

        $baseModelClass = 'common\\models\\' . $this->getBaseModelClass();
        /** @var Post $model */
        $model = new $baseModelClass;
        $model->setScenario('administrator');
        $model->defaultOnCreate();
        if ($model->save()) {
            $session->setFlash('success', 'Материал добавлен');
            $url = ['update', 'id' => $model->id];
        } else {
            $session->setFlash('error', 'Не удалось добавить материал');
            $url = ['index'];
        }

        return $this->redirect($url);
    }

    /**
     * @return mixed
     * @throws BadRequestHttpException
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSelectFoldersAndTags()
    {
        $response = Yii::$app->getResponse();
        $response->format = $response::FORMAT_JSON;

        $text = Yii::$app->getRequest()->getQueryParam('query');

        if (!is_string($text)) {
            throw new BadRequestHttpException('Не указан параметр text.');
        }

        $text = mb_strtoupper(trim($text), Yii::$app->charset);
        $limit = 15;

        /** @var \common\models\FolderQuery $folders */
        $folders = Folder::find();
        if (!empty($text)) {
            $folders = $folders->andWhere('UPPER(name) LIKE \'%' . $text . '%\'');
        }

        $folders = $folders->orderBy('name')->limit($limit)->all();

        /** @var Folder[] $folders */
        $foldersCount = count($folders);
        if ($foldersCount === $limit) {
            return Html::prepareDataForSelectize($folders);
        }

        $tags = Tag::find()
            ->andWhere('UPPER(name) LIKE \'%' . $text . '%\'')
            ->orderBy('name')
            ->limit($limit - $foldersCount)
            ->all();

        return Html::prepareDataForSelectize(array_merge($folders, $tags));
    }
}
