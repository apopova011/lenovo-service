jQuery(document).ready(function () {
    var menuWrapper = $('.top-menu-wrapper'),
        menu = $('.top-menu'),
        more = menu.find('.more'),
        moreList = $('<ul>').appendTo(more);

    $('.dropdown').on('click', function (e) {
        e.stopPropagation();

        var dropdown = $(this).children('ul');
        $('.dropdown > ul').not(dropdown).hide();

        if (dropdown.is(':hidden')) {
            dropdown.show();
        } else {
            dropdown.hide();
        }
    });

    $(document.body).on('click', function (e) {
        if (!$(e.target).is('.dropdown *')) {
            $('.dropdown > ul').hide();
        }
    });

    function hideItems() {
        var foldable = menu.children('.foldable'),
            i = 1;

        while (menu.height() > menuWrapper.height()) {
            if (foldable.length - i < 0) {
                menu.children('.foldable-last').prependTo(moreList);
                more.addClass('right');
                more.before(menu.children('li.right:last-of-type'));
                break;
            }

            var item = $(foldable[foldable.length - i]);
            if (more.is(':hidden')) {
                more.css('display', 'inline-block');
            }

            item.prependTo(moreList);
            i++;

            if (foldable.length - i < 0) {
                $('.dropdown').addClass('show-glyphicon');
            }
        }
    }

    function foldMenu() {
        if (menu.height() > menuWrapper.height()) {
            hideItems();
        } else {
            var itemInserted = false;

            if (more.is(':visible')) {
                more.hide();
            }

            while (menu.height() <= menuWrapper.height()) {
                var folded = moreList.children();

                if (folded.length == 0) {
                    break;
                }

                var foldableLast = folded.filter('.foldable-last');
                if (menu.children('.foldable-last').length == 0 && foldableLast.length > 0) {
                    menu.append(foldableLast);
                    itemInserted = true;

                    more.removeClass('right');
                    more.after(menu.children('li.right:last-of-type'));
                    continue;
                }

                menu.append($(folded[0]));

                if (menu.children('.foldable').length == 1) {
                    $('.dropdown').removeClass('show-glyphicon');
                }

                itemInserted = true;
            }

            if (moreList.children().length > 0) {
                more.css('display', 'inline-block');
            }

            if (itemInserted && menu.height() > menuWrapper.height()) {
                hideItems();
            }
        }

        menuWrapper.css('overflow', 'visible');

        var height = $(window).innerHeight() - 50,
            dropdowns = menu.find('ul');

        $.each(dropdowns, function (i, item) {
            var dropdown = $(item);

            dropdown.css('max-height', '');
            if (dropdown.height() >= height) {
                dropdown.addClass('scroll-y').css('max-height', height + 'px');
            } else {
                dropdown.removeClass('scroll-y')
            }
        });
    }

    function menuReset() {
        if (typeof window.menuReloadTimer != 'undefined') {
            clearTimeout(window.menuReloadTimer);
        }

        menuWrapper.css('overflow', 'hidden');

        window.menuReloadTimer = setTimeout(function () {
            foldMenu();
        }, 200);
    }

    $(window)
        .on('orientationchange', function () {
            menuReset();
        })
        .on('resize', function () {
            menuReset();
        });

    foldMenu();
});