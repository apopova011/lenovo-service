jQuery(document).ready(function () {
    $('body').on('change', '.update-status', function (e) {
        e.preventDefault();
        var select = $(this);
        jQuery.ajax({
            url: '/comment/update-status',
            method: 'GET',
            data: {
                id: select.data('id'),
                status: select.val()
            }
        });
    });
});

