<?php

use \yii\bootstrap\ActiveForm;
use \yii\helpers\ArrayHelper;
use \yii\web\JqueryAsset;
use \common\helpers\Html;
use \common\models\Folder;
use \common\models\Group;

/**
 * @var yii\web\View $this
 * @var common\models\Folder $model
 */

$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);

$isNewRecord = $model->getIsNewRecord();
$paths = Folder::find()->orderTree();
if (!$isNewRecord) {
    $paths = $paths->andWhere('id != :id', [':id' => $model->id]);
}
$folders = Html::treeForDropDownList($paths->all(), 'path');
$groups = ArrayHelper::map(Group::find()->orderBy('name')->asArray()->all(), 'id', 'name');
?>
<div class="row">
    <div class="col-lg-8">
        <?= $form->field($model, 'name' )->textInput(['maxlength' => true])->hint('Максимально краткое название для меню и списков'); ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => true])->hint('Выводится на странице в заголовке'); ?>
        <?= $form->field($model, 'description')->textarea(['maxlength' => true, 'rows' => 3]); ?>
        <?= $form->field($model, 'PathParent')->dropDownList($folders, ['prompt' => 'Нет']); ?>
        <?= $form->field($model, 'group_id'  )->dropDownList($groups,  ['prompt' => 'Без группы']); ?>
        <div class="text-center">
            <?= Html::submitButton($isNewRecord ? 'Добавить' : 'Обновить', [
                'class' => $isNewRecord ? 'btn btn-success' : 'btn btn-warning',
            ]); ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-warning">
            <div class="panel-heading"><?= $model->getAttributeLabel('icon'); ?></div>
            <div class="panel-body">
                <?php
                $url = $model->getUploadUrl();
                if ($url) {
                    echo Html::img($url, ['width' => '100%']);
                }
                ?>
                <fieldset>
                    <legend>Загрузить новую</legend>
                    <?= $form->field($model, 'UploadFile')->fileInput(); ?>
                    <?= $form->field($model, 'UploadExternalUrl')->textInput(['maxlength' => true]); ?>
                </fieldset>
            </div>
        </div>
        <div class="panel panel-success">
            <div class="panel-heading">СЕО</div>
            <div class="panel-body">
                <?= $form->field($model, 'slug')->textInput(['maxlength' => true]); ?>
                <?php
                if ($isNewRecord) {
                    $this->registerJsFile('/js/jquery.slugit.js', ['depends' => JqueryAsset::className()]);
                    $this->registerJs(<<<JS
$('#folder-name').slugIt({
    events: 'focusout keyup',
    output: '#folder-slug'
});
JS
                        , $this::POS_READY, 'slug-auto');
                }
                ?>
                <?= $form->field($model, 'title_seo')->textInput(['maxlength' => true]); ?>
                <?= $form->field($model, 'path', ['template' => '{error}'])->textInput(); ?>
            </div>
        </div>
        <?php if (!$isNewRecord) { ?>
            <div class="panel panel-info">
                <div class="panel-heading">Информация</div>
                <div class="panel-body">
                    <dl class="dl-horizontal">
                        <dt>Посмотреть</dt>
                        <dd><?= Html::a('на сайте', $model->getUrl(true), ['target' => '_blank']); ?></dd>
                        <dt><?= $model->getAttributeLabel('post_count'); ?></dt>
                        <dd><?= $model->post_count; ?></dd>
                        <dt><?= $model->getAttributeLabel('updated'); ?></dt>
                        <dd><?= Yii::$app->getFormatter()->asDatetime($model->updated); ?></dd>
                    </dl>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
