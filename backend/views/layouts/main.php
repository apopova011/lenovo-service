<?php

/**
 * @var $this \yii\web\View
 * @var $content string
 */

use \backend\assets\AppAsset;
use \backend\widgets\Alert;
use \backend\widgets\FoldableMenu;
use \common\helpers\Html;
use \common\helpers\Url;
use \common\models\User;

AppAsset::register($this);

$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language; ?>">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="theme-color" content="#000000"/>
    <link rel="icon" type="image/png" sizes="192x192" href="<?= Url::to('@web/icon-192x192.png', true); ?>"/>
    <link rel="icon" type="image/x-icon" href="<?= Url::to('@web/favicon.ico', true); ?>" />
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta charset="<?= Yii::$app->charset; ?>"/>
    <title><?= Html::encode($this->title); ?></title>
    <?php $this->head(); ?>
    <?= Html::csrfMetaTags(); ?>
</head>
<body>
<?php $this->beginBody(); ?>

<div class="non-footer">
    <?php
    /** @var yii\web\urlManager $frontendUrlManager */
    $frontendUrlManager = Yii::$app->frontendUrlManager;
    $user = Yii::$app->getUser();
    $isGuest = $user->getIsGuest();

    $menuItems = [];
    if ($isGuest) {
        $menuItems[] = ['label' => Yii::t('common', 'Login'), 'url' => $user->loginUrl];
    } else {
        $menuItems[] = [
            'label' => $user->getIdentity()->title,
            'glyphicon' => 'user',
            'items' => [
                ['label' => Yii::t('common', 'Logout'), 'url' => ['auth/logout']],
            ],
            'options' => [
                'class' => 'right',
            ],
            'foldable' => false
        ];
    }

    if (!$isGuest && in_array($user->getIdentity()->role, User::$rolesBackend, true)) {
        $menuItems = array_merge($menuItems, [
            ['label' => 'Сводка',       'url' => ['dashboard/index'],     'visible' => $user->can('dashboard'),             'foldable' => 'last'],
            ['label' => 'Материалы',    'url' => ['post-for-feed/index'], 'visible' => $user->can('postIndexAndCreate'),    'foldable' => 'last'],
            ['label' => 'Пользователи', 'url' => ['user/index'],          'visible' => $user->can('userIndexAndCreate')],
            ['label' => 'Фото',         'url' => ['photo/index'],         'visible' => $user->can('photoIndex')],
            [
                'label' => 'Структура сайта',
                'items' => [
                    ['label' => 'Тексты',     'url' => ['post-section/index'],    'visible' => $user->can('postIndexAndCreate')],
                    ['label' => 'Где купить', 'url' => ['partners/index'], 'visible' => $user->can('partnersIndexAndCreate')],
                ],
                'foldable' => true
            ],
        ]);
    }

    echo FoldableMenu::widget([
        'items' => $menuItems,
        'brandUrl' => $frontendUrlManager->getHostInfo(),
        'brandLabel' => Yii::t('common', 'To site'),
    ]);
    ?>

    <div class="container">
        <?= Alert::widget(); ?>
        <?= $content; ?>
    </div>
</div>

<footer class="navbar navbar-default">
    <div class="container">
        <p class="navbar-text pull-left">© <?= Yii::$app->params['brandName']; ?>, 2014–<?= date('Y'); ?></p>

        <p class="navbar-text pull-right"><?= Html::mailto(Yii::$app->params['emailSupport']); ?></p>
    </div>
</footer>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
