<?php

use \yii\bootstrap\ActiveForm;
use \yii\jui\AutoComplete;
use \yii\web\JsExpression;
use \dosamigos\selectize\SelectizeAsset;
use \common\helpers\Html;
use \common\models\FeedNavigation;
use \common\models\Folder;
use \frontend\models\MainPage;

/**
 * @var yii\web\View $this
 * @var common\models\FeedNavigation $model
 */
$form = ActiveForm::begin();

$isNewRecord = $model->getIsNewRecord();

if ($isNewRecord) {
    $folders = Folder::find()->parents()->select(['id', 'name', 'title'])->asArray()->all();

    $foldersForSelect = [];
    foreach ($folders as $folder) {
        $foldersForSelect[$folder['id']] = !empty($folder['title']) ? $folder['title'] : $folder['name'];
    }
    ?>
    <fieldset>
        <legend>Страница</legend>
        <?= $form->field($model, 'page_resource_type')->dropDownList($model::pageTypes())->label(false); ?>
        <?= $form->field($model, 'page_resource_id')->hiddenInput()->label(false); ?>
    </fieldset>
    <div class="form-group">
        <div id="page-folder">
            <?= Html::dropDownList('folders', null, $foldersForSelect, [
                'id' => 'folders',
                'class' => 'form-control',
            ]); ?>
        </div>
        <div id="page-tag">
            <?= Html::textInput('tags', null, [
                'id'    => 'tags',
                'class' => 'form-control',
            ]); ?>
        </div>
    </div>
    <fieldset>
        <legend>Ссылка на</legend>
        <?= $form->field($model, 'link_resource_type')->dropDownList($model::linkTypes())->label(false); ?>
        <?= $form->field($model, 'link_resource_id')->hiddenInput()->label(false); ?>
        <?php
        SelectizeAsset::register($this);

        $mainPage = new MainPage();
        $mainPageUrl = $mainPage->getUrl();
        $mainPageName = $mainPage->name;
        ?>
        <?php $this->registerJs(<<<JS
var folders = $('#folders'),
    resourceType = $('#feednavigation-page_resource_type');

function resourseTypeFieldsChange(type) {
    var pageTag = $('#page-tag'),
        pageFolder = $('#page-folder');

    if (type == 'folder') {
        pageTag.addClass('hidden');
        pageFolder.removeClass('hidden');
        $('#feednavigation-page_resource_id').val(folders.val());
    } else if (type == 'tag') {
        pageTag.removeClass('hidden');
        pageFolder.addClass('hidden');
    } else if (type == 'main_page') {
        pageTag.addClass('hidden');
        pageFolder.addClass('hidden');
        $('#feednavigation-page_resource_id').val('');
    }
}

resourseTypeFieldsChange(resourceType.val());

resourceType.change(function() {
    resourseTypeFieldsChange($(this).val());
});

folders.change(function() {
    $('#feednavigation-page_resource_id').val($(this).val());
});

window.tagsData = {};

$('#tags').selectize({
    valueField: 'name',
    labelField: 'name',
    searchField: 'name',
    options: [],
    create: false,
    maxItems: 1,
    load: function(query, callback) {
        if (!query.length) {
            return callback();
        }
        $.ajax({
            url: '/tag/select',
            type: 'GET',
            data: {'text': query},
            dataType: 'json',
            error: function() {
                callback();
            },
            success: function(res) {
                callback(res);
            }
        });
    },
    onLoad: function(data) {
        window.tagsData = data;
    },
    onChange: function(value) {
        var id = '';
        $.each(window.tagsData, function(index, val) {
            if (val.name == value) {
                id = val.id;
            }
        });
        $('#feednavigation-page_resource_id').val(id);
    }
});

var linkType = $('#feednavigation-link_resource_type'),
    linkName = $('#feednavigation-link_resource_name');

if (linkType.val() == 'main_page') {
    linkName.addClass('hidden');
} else {
    linkName.removeClass('hidden');
}

linkType.change(function() {
    var type = $(this).val();

    if (type == 'main_page') {
        linkName.addClass('hidden');
        $('#feednavigation-url').val('{$mainPageUrl}');
        $('#feednavigation-name').val('{$mainPageName}');
        $('#feednavigation-link_resource_id').val('');
    } else {
        linkName.removeClass('hidden');
    }
});
JS
        ); ?>
        <?= $form->field($model, 'link_resource_name', [
            'template' => '{input}{hint}{error}',
            'parts'    => [
                '{input}' => AutoComplete::widget([
                    'model'         => $model,
                    'attribute'     => 'link_resource_name',
                    'clientOptions' => [
                        'source' => new JsExpression(<<<JS
function (request, response) {
    $.ajax({
        url: '/' + $('#feednavigation-link_resource_type').val() + '/select',
        type: 'GET',
        data: { 'text': request.term },
        dataType: 'json',
        success: function (res) {
            response($.map(res, function(item) {
                return {
                    label: item.name,
                    value: item.name,
                    id: item.id,
                    url: item.url
                }
            }));
        }
    });
}
JS
                        ),
                        'select' => new JsExpression(<<<JS
function (event, ui) {
    $('#feednavigation-url').val(ui.item.url);
    $('#feednavigation-link_resource_id').val(ui.item.id);
    $('#feednavigation-name').val(ui.item.label);
}
JS
                        ),
                    ],
                ]),
            ],
        ])->textInput(['maxlength' => true]); ?>
    </fieldset>
<?php } else { ?>
    <?php
    $pageResourceType = $model->page_resource_type;
    $linkResourceType = $model->link_resource_type;
    $pageModels = FeedNavigation::pageModels();
    ?>
    <p>
        Страница:
        <?= !array_key_exists($pageResourceType, $pageModels) ? FeedNavigation::pageTypes()[$pageResourceType] : ''; ?>
        <?php if ($model->page) {
            $name = $model->page->title ? $model->page->title : $model->page->name;
            ?>
            <?= Html::a($name, FeedNavigation::getResourceUrl($pageResourceType, $model->page_resource_id)); ?>
        <?php } ?>
    </p>
    <p>
        Ссылка на:
        <?= !array_key_exists($linkResourceType, $pageModels) ? FeedNavigation::linkTypes()[$linkResourceType] : ''; ?>
        <?php if ($model->link) { ?>
            <?php $name = property_exists($model->link, 'title') ? $model->link->title : $model->link->name; ?>
            <?= Html::a($name, FeedNavigation::getResourceUrl($linkResourceType, $model->link_resource_id)); ?>
        <?php } ?>

    </p>
    <p>
        Порядок:
        <?= Html::a($model->order ? $model->order : 'не задан', [
            'feed-navigation/index',
            'FeedNavigationSearch[page_resource_type]' => $model->page_resource_type,
            'FeedNavigationSearch[page_resource_id]'   => $model->page_resource_id,
        ]); ?>
    </p>
<?php } ?>
<fieldset>
    <?php if ($isNewRecord) { ?>
        <legend>Пункт меню</legend>
    <?php } ?>

    <?= $form->field($model, 'url' )->textInput(['maxlength' => true]); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]); ?>
    <?php
    if (!$isNewRecord) {
        $this->registerJs(<<<JS
window.feedNavigationName = $('#feednavigation-name').val();

$('<a href="#" style="margin-left: 5px">взять исходное</a>').click(function(e) {
    e.preventDefault();
    $('#feednavigation-name').val(window.feedNavigationName).trigger('blur');
}).appendTo('label[for="feednavigation-name"]');
JS
            ,
            $this::POS_READY,
            'feednavigation-undo'
        );
    }
    ?>
</fieldset>
<?= $form->errorSummary($model); ?>
<div class="text-center">
    <?= Html::submitButton($isNewRecord ? 'Добавить' : 'Обновить', [
        'class' => $isNewRecord ? 'btn btn-success' : 'btn btn-warning',
    ]); ?>
</div>
<?php ActiveForm::end(); ?>
