<?php

use \yii\bootstrap\ActiveForm;
use \common\helpers\Html;

/**
 * @var yii\web\View $this
 * @var \common\models\Group $model
 */
$form = ActiveForm::begin();

$isNewRecord = $model->getIsNewRecord();
?>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'slug')->textInput(['maxlength' => true]); ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]); ?>
        <div class="text-center">
            <?= Html::submitButton($isNewRecord ? 'Добавить' : 'Обновить', [
                'class' => $isNewRecord ? 'btn btn-success' : 'btn btn-warning',
            ]); ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
