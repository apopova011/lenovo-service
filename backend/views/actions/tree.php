<?php

use \execut\widget\TreeView;
use \common\helpers\Html;
use \common\helpers\Url;

/**
 * @var yii\web\View $this
 * @var string $modelLabel
 * @var boolean $withCreate
 * @var yii\db\ActiveRecord $items
 */

$this->title = Yii::t('common', 'All') . ' \ ' . $modelLabel;

if ($withCreate) { ?>
    <p class="pull-right">
        <?= Html::a(Yii::t('common', 'Create'), ['create'], ['class' => 'btn btn-success']); ?>
    </p>
<?php } ?>

<h1><?= Yii::t('common', 'List of all'); ?></h1>

<?php
$navItems = [];
/** @var \yii\db\ActiveRecord $item */
foreach ($items as $item) {
    $navItem = [
        'text' => $item->name,
        'href' => Url::toRoute(['update', 'id' => $item->id]),
        'state' => [
            'expanded' => true,
        ],
    ];

    $dotPos = strpos($item->path, '.');
    if ($dotPos === false) {
        $navItems['node-' . $item->path] = $navItem;
    } else {
        $pathParts = array_reverse(explode('.', $item->path));
        $nav = ['node-' . $pathParts[0] => $navItem];

        foreach ($pathParts as $i => $part) {
            if ($i === 0) {
                continue;
            }

            $nav = [
                'node-' . $part => [
                    'nodes' => $nav
                ],
            ];
        }

        $navItems = array_merge_recursive($navItems, $nav);
    }
}

echo TreeView::widget([
    'data' => array_values($navItems),
    'template' => '{tree}',
    'options' => [
        'id' => 'tree-view',
    ],
    'clientOptions' => [
        'enableLinks' => true,
        'levels' => 2,
    ],
]);

$this->registerJs(<<<JS
$('#tree-view').on('nodeSelected', function(event, data) {
    window.location.href = data.href;
});
JS
)
?>
