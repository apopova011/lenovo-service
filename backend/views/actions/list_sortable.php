<?php

use \yii\bootstrap\ActiveForm;
use \yii\jui\JuiAsset;
use \yii\widgets\Pjax;
use \kartik\grid\GridView;
use \common\helpers\Html;
use \common\helpers\StringHelper;
use \common\helpers\Url;

/**
 * @var yii\web\View $this
 * @var string $modelLabel
 * @var boolean $withCreate
 * @var array $filters
 * @var string|boolean $filterActive
 * @var common\models\AbstractSearch $filterModel
 * @var array $configGridView
 * @var boolean $withAutoUpdate
 * @var array $changeOrder
 * @var bool $allChosen
 */

?>
<div class="btn-toolbar pull-right">
    <?php
    if ($withCreate) {
        echo Html::a(Yii::t('common', 'Create'), ['create'], ['class' => 'btn btn-success pull-right']);
    }

    if ($filters !== []) {
        ?>
        <div class="btn-group pull-right"><?php
        foreach ($filters as $name => $value) {
            $class = 'btn btn-info';
            if ($name === $filterActive) {
                $class .= ' active';
            }

            echo Html::a($value, Url::current(['filter' => $name]), ['class' => $class]);
        }
        ?></div><?php
    }
    if ($filterModel->isAttributeActive('any_field')) {
        $form = ActiveForm::begin([
            'action'  => ['index'],
            'method'  => 'get',
            'options' => ['class' => 'pull-right form-inline'],
        ]);
        echo $form->field($filterModel, 'any_field', [
            'options'  => ['class' => 'input-group'],
            'template' => '{input}{button}',
            'parts'    => [
                '{button}' => '<span class="input-group-btn">' . Html::submitButton(Yii::t('common', 'Search'), ['class' => 'btn btn-primary']) . '</span>'
            ],
        ]);
        ActiveForm::end();
    }
    ?>
</div>

<h1><?= Yii::t('common', 'List'); ?><?php
    if ($filterActive) {
        /** @var string $filterActive */
        $this->title = $filters[$filterActive];
        echo StringHelper::mb_lcfirst($this->title);
    } else {
        $this->title = Yii::t('common', 'All');
        echo ' ' . Yii::t('common', 'of all');
    }
    $this->title .= ' \ ' . $modelLabel;
?></h1>
<?php
Pjax::begin([
    'options' => [
        'id' => 'pjax-container',
    ],
]);
if (array_key_exists('filter', $changeOrder)) {
    $configGridView['id'] = 'grid-container';
    $form = ActiveForm::begin([
        'action'  => ['index'],
        'method'  => 'get',
        'options' => ['class' => 'pull-left'],
    ]);
    ?>
    <div class="input-group">
        <?php
        /** @var array $changeOrder */
        $chosen = [];
        foreach ($changeOrder['filter'] as $filter) {
            $attribute = $filter['attribute'];
            $chosen[$attribute] = $filterModel->$attribute;
            echo $form
                ->field($filterModel, $attribute, [
                    'options' => [
                        'class' => 'table-filter'
                    ]
                ])
                ->dropDownList($filter['values'], ['prompt' => 'Нет', 'id' => $attribute . '-select'])
                ->label(false);
        } ?>
        <span class="input-group-btn"><?= Html::submitButton(Yii::t('common', 'Search'), [
            'class' => 'btn btn-info'
        ]); ?></span>
    </div>
    <?php
    ActiveForm::end();
}
if ($allChosen) {
    $controllerId = Yii::$app->controller->id;

    echo Html::tag('p', 'Порядок элементов можно менять перетаскивая строки таблицы.', [
        'class' => 'text-muted table-filter-help'
    ]);

    JuiAsset::register($this);
    $this->registerJs(<<<JS
var tableBody = $('.kv-grid-table tbody');

tableBody.sortable({
    update: function(event, ui) {
        var order = [];
        $.each(tableBody.find('tr'), function(i, item) {
            order.push($(item).data('key'))
        });

        $.ajax({
            url: '/{$controllerId}/update-order',
            method: 'GET',
            dataType: 'json',
            data: {
                order: order.join(',')
            },
            timeout: 4000,
            error: function (jqXHR) {
                window.location.reload();
            },
            success: function (jqXHR) {
                $.each(tableBody.find('tr'), function(i, tr) {
                    $.each($(tr).find('td'), function(j, td) {
                        if ($(td).data('attribute') === '{$changeOrder['attribute']}') {
                            $(td).text(i)
                        }
                    });
                });
            }
        });
    }
});
JS
    );
} else {
    echo Html::tag('p', 'Выберите для изменения порядка элементов.', ['class' => 'text-muted table-filter-help']);
}
$this->registerJs(<<<JS
$('.table-filter select').on('change', function() {
    $(this).closest('form').submit();
})
JS
);
?>
<?php
if ($withAutoUpdate) {
    $checkboxHtmlId = 'grid-auto_update';

    echo Html::checkbox($checkboxHtmlId, false, [
        'id'    => $checkboxHtmlId,
        'label' => Yii::t('common', 'Refresh automatically'),
    ]);

    $this->registerJs(<<<JS
$('#{$checkboxHtmlId}').on('change', function(e) {
     window.autoUpdateGrid();
});

$('#pjax-container').on('afterFilter', '#{$configGridView['id']}', function(){
    if ($('#{$checkboxHtmlId}').prop('checked')) {
        window.autoUpdateGridTimeout = setTimeout(window.autoUpdateGrid, 7000);
    }
});

window.autoUpdateGrid = function() {
    if ($('#{$checkboxHtmlId}').prop('checked')) {
        jQuery.fn.yiiGridView.apply(document.getElementById('{$configGridView['id']}'), ['applyFilter']);
    } else if (typeof window.autoUpdateGridTimeout !== 'undefined') {
        clearTimeout(window.autoUpdateGridTimeout);
    }
}
JS
        , $this::POS_READY, $checkboxHtmlId);
}
$configGridView['pjax'] = false;
$configGridView['export'] = false;
$configGridView['responsiveWrap'] = false;

echo GridView::widget($configGridView);
Pjax::end();
