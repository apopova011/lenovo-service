<?php

use \yii\bootstrap\ActiveForm;
use \yii\web\JqueryAsset;
use \kartik\grid\GridView;
use \common\helpers\Html;
use \common\helpers\StringHelper;
use \common\helpers\Url;

/**
 * @var yii\web\View $this
 * @var string $modelLabel
 * @var boolean $withCreate
 * @var array $filters
 * @var string|boolean $filterActive
 * @var common\models\AbstractSearch $filterModel
 * @var array $configGridView
 * @var boolean $withAutoUpdate
 */
?>
<div class="btn-toolbar pull-right">
    <?php
    if ($withCreate) {
        echo Html::a(Yii::t('common', 'Create'), ['create'], ['class' => 'btn btn-success pull-right']);
    }

    if ($filters !== []) { ?>
        <div class="btn-group pull-right">
            <?php foreach ($filters as $name => $value) {
                $class = 'btn btn-info';
                if ($name === $filterActive) {
                    $class .= ' active';
                }

                echo Html::a($value, Url::current(['filter' => $name]), ['class' => $class]);
            } ?>
        </div>
    <?php } ?>
    <?php if ($filterModel->isAttributeActive('any_field')) {
        $form = ActiveForm::begin([
            'action'  => ['index'],
            'method'  => 'get',
            'options' => ['class' => 'pull-right form-inline'],
        ]);
        echo $form->field($filterModel, 'any_field', [
            'options'  => ['class' => 'input-group'],
            'template' => '{input}{button}',
            'parts'    => [
                '{button}' => '<span class="input-group-btn">' . Html::submitButton(Yii::t('common', 'Search'), ['class' => 'btn btn-primary']) . '</span>'
            ],
        ]);
        ActiveForm::end();
    }
    ?>
</div>

<h1><?= Yii::t('common', 'List'); ?><?php
    if ($filterActive) {
        /** @var string $filterActive */
        $this->title = $filters[$filterActive];
        echo StringHelper::mb_lcfirst($this->title);
    } else {
        $this->title = Yii::t('common', 'All');
        echo ' ' . Yii::t('common', 'of all');
    }
    $this->title .= ' \ ' . $modelLabel;
?></h1><?php

if ($withAutoUpdate) {
    $configGridView['id'] = 'grid-container';
    $checkboxHtmlId = 'grid-auto_update';

    echo Html::checkbox($checkboxHtmlId, false, [
        'id'    => $checkboxHtmlId,
        'label' => Yii::t('common', 'Refresh automatically'),
    ]);

    $this->registerJs(<<<JS
$('#{$checkboxHtmlId}').on('change', function(e) {
     window.autoUpdateGrid();
});

$('#pjax-container').on('afterFilter', '#{$configGridView['id']}', function(){
    if ($('#{$checkboxHtmlId}').prop('checked')) {
        window.autoUpdateGridTimeout = setTimeout(window.autoUpdateGrid, 7000);
    }
});

window.autoUpdateGrid = function() {
    if ($('#{$checkboxHtmlId}').prop('checked')) {
        jQuery.fn.yiiGridView.apply(document.getElementById('{$configGridView['id']}'), ['applyFilter']);
    } else if (typeof window.autoUpdateGridTimeout !== 'undefined') {
        clearTimeout(window.autoUpdateGridTimeout);
    }
}
JS
        , $this::POS_READY, $checkboxHtmlId);
}

$configGridView['pjax'] = true;
$configGridView['pjaxSettings'] = [
    'options' => [
        'id' => 'pjax-container',
    ],
];
$configGridView['export'] = false;
$configGridView['responsiveWrap'] = false;

echo GridView::widget($configGridView);

$this->registerJsFile('/js/comment_list.js', ['depends' => JqueryAsset::className()]);
