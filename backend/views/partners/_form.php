<?php

use common\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;

/**
 * @var $this yii\web\View
 * @var $model common\models\Partners
 */
$this->registerJsVar('dadataToken', Yii::$app->params['dadataToken']['key']);
?>
<div class="row">
    <?php $form = ActiveForm::begin(); ?>
    <div class="col-lg-12">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]); ?>
        <?= $form->field($model, 'country')->textInput(['maxlength' => true]); ?>
        <?= $form
            ->field($model, 'address')
            ->hint(false)
            ->error(false)
            ->widget(AutoComplete::class, [
                'options' => [
                    'class' => 'form-control',
                ],
                'clientOptions' => [
                    'source' => new JsExpression(<<<JS
function(request, response) {
    $.ajax({
        type: 'POST',
        url: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address',
        contentType: 'application/json',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Token ' + dadataToken,
        },
        data: JSON.stringify({'query': request.term}),
        success: function(data) {
          if (typeof data !== 'undefined') {
              response(data.suggestions);
          }
        }
    });
}
JS
                    ),
                    'minLength' => 2,
                    'select' => new JsExpression(<<<JS
function(event, ui) {
  $('#service-region').val(ui.item.data.region_with_type);
  $('#service-city').val(ui.item.data.city_with_type);
  $('#service-coords').val(ui.item.data.geo_lat + ', ' + ui.item.data.geo_lon);
}
JS
                    ),
                ],
            ]);
        ?>
        <?= $form->field($model, 'region')->textInput(['maxlength' => true]); ?>
        <?= $form->field($model, 'city')->textInput(['maxlength' => true]); ?>
        <?= $form->field($model, 'coords')->widget(MaskedInput::className(), ['mask' => '([-]9{1,3}.9{1,10}, [-]9{1,3}.9{1,10})']); ?>
        <div class="form-group">
            <?= Html::a('Получить координаты', '#', [
                'class' => 'btn btn-success',
                'id' => 'service-get-coords',
            ]) ?>
            <?php
            $this->registerJs(<<<JS
$('#service-get-coords').on('click', function(e) {
  e.preventDefault();
  
  var addressValue = $('#service-address').val();
    $.ajax({
        type: 'POST',
        url: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address',
        contentType: 'application/json',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Token ' + dadataToken,
        },
        data: JSON.stringify({'query': addressValue}),
        success: function(data) {
          if (typeof data !== 'undefined') {
              var address = data.suggestions[0];

              $('#service-coords').val(address.data.geo_lat + ', ' + address.data.geo_lon);
          }
        }
    });

});
JS
                , $this::POS_READY, 'get-coords'
            );
            ?>
        </div>
        <?= $form->field($model, 'site')->textInput(['maxlength' => true]); ?>
        <?= $form->field($model, 'description')->textarea(['rows' => 6]); ?>
        <?= $form->field($model, 'phones')->textarea(['rows' => 4]); ?>
        <?= $form->field($model, 'act')->checkbox(); ?>
        <?= $form->errorSummary($model); ?>
        <div class="text-center">
            <?= Html::submitButton(
                $model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-warning']
            ); ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
