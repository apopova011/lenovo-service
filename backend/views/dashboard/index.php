<?php

use \yii\bootstrap\Tabs;
use \common\helpers\Html;
use \common\helpers\StringHelper;
use \common\models\Post;

/**
 * @var yii\web\View $this
 * @var array $symbolCounts
 * @var array $mySymbolCounts
 * @var array $postCounts
 * @var array $myPostCounts
 * @var array $photoCounts
 * @var array $myPhotoCounts
 * @var array $commentCounts
 * @var array $myCommentCounts
 * @var array $userCounts
 * @var integer $symbolCount
 * @var integer $mySymbolCount
 * @var integer $postCount
 * @var integer $photoCount
 * @var integer $myPhotoCount
 * @var integer $myPostCount
 * @var integer $commentCount
 * @var integer $myCommentCount
 * @var integer $userCount
 * @var integer $tagCount
 * @var array $notPublishedPosts
 * @var array $myNotPublishedPosts
 */

$this->title = Yii::t('common', 'Dashboard');

$user = Yii::$app->getUser();
$userId = $user->id;

$canCount = $user->can('count');
$frontendUrlManager = Yii::$app->frontendUrlManager;
?>
<h1><?= $this->title; ?></h1>

<?php if ($user->can('dashboard')) { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-info">
                <div class="panel-heading">Информация:</div>
                <div class="panel-body">
                    <dl class="dl-horizontal">
                        <?php if ($canCount) { ?>
                            <dt>Знаков:</dt>
                            <dd>
                                <?= StringHelper::numberShortSize($symbolCount); ?>:
                                <?= Html::listWithDeltas($symbolCounts, 4); ?>
                            </dd>
                        <?php } ?>
                        <dt>Моих знаков:</dt>
                        <dd>
                            <?= StringHelper::numberShortSize($mySymbolCount); ?>:
                            <?= Html::listWithDeltas($mySymbolCounts, 4); ?>
                        </dd>
                        <?php if ($canCount) { ?>
                            <dt>Постов:</dt>
                            <dd>
                                <?= $postCount; ?>:
                                <?= Html::listWithDeltas($postCounts, 4); ?>
                            </dd>
                        <?php } ?>
                        <dt>Моих постов:</dt>
                        <dd>
                            <?= $myPostCount; ?>:
                            <?= Html::listWithDeltas($myPostCounts, 4); ?>
                        </dd>
                        <?php if ($canCount) { ?>
                            <dt>Фото:</dt>
                            <dd>
                                <?= $photoCount; ?>:
                                <?= Html::listWithDeltas($photoCounts, 4); ?>
                            </dd>
                        <?php } ?>
                        <dt>Моих фото:</dt>
                        <dd>
                            <?= $myPhotoCount; ?>:
                            <?= Html::listWithDeltas($myPhotoCounts, 4); ?>
                        </dd>
                        <?php if ($canCount) { ?>
                            <dt>Комментариев:</dt>
                            <dd>
                                <?= $commentCount; ?>:
                                <?= Html::listWithDeltas($commentCounts, 4); ?>
                            </dd>
                        <?php } ?>
                        <dt>Моих комментариев:</dt>
                        <dd>
                            <?= $myCommentCount; ?>:
                            <?= Html::listWithDeltas($myCommentCounts, 4); ?>
                        </dd>
                        <?php if ($canCount) { ?>
                            <dt>Пользователей:</dt>
                            <dd>
                                <?= $userCount; ?>:
                                <?= Html::listWithDeltas($userCounts, 4); ?>
                            </dd>
                            <dt>Тэгов:</dt>
                            <dd><?= $tagCount; ?></dd>
                        <?php } ?>
                        <?php if (stripos(strtoupper(PHP_OS), 'WIN') !== 0) { ?>
                            <dt>LA:</dt>
                            <dd>
                                <?php $load = sys_getloadavg(); ?>
                                <?= $load[0]; ?>,
                                <?= $load[1]; ?>,
                                <?= $load[2]; ?>
                            </dd>
                        <?php } ?>
                    </dl>
                </div>
            </div>
            <?php if (!empty($notPublishedPosts)) { ?>
                <div class="panel panel-warning">
                    <div class="panel-heading">Неопубликованные</div>
                    <div class="panel-body">
                        <?php
                        $items = [];
                        foreach ([
                                'notPublishedPosts'   => 'Все',
                                'myNotPublishedPosts' => 'Мои',
                            ] as $var => $label) {
                            $allowStrong = $var !== 'myNotPublishedPosts';

                            $content = empty($myNotPublishedPosts) ? '' : '<br/>';
                            foreach ($$var as $i => $post) {
                                $name = $post['name'];
                                if ($allowStrong && $userId === $post['author_id']) {
                                    $name = Html::tag('strong', $name);
                                }

                                $content .= Html::tag('p', Html::a($name, [
                                    'post/update',
                                    'id' => $post['id']
                                ]));
                            }

                            $items[] = [
                                'label'   => $label,
                                'content' => $content,
                                'active'  => $var === 'myNotPublishedPosts',
                            ];
                        }

                        echo empty($myNotPublishedPosts) ? $items[0]['content'] : Tabs::widget(['items' => $items]);
                        ?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="col-lg-6">
            <?php if ($user->can('postHitList')) { ?>
                <div class="panel panel-info">
                    <div class="panel-heading">Самое популярное</div>
                    <div class="panel-body">
                        <?php
                        $postHitNumber = $user->can('postHitNumber');
                        $htmlParams = [];
                        $items = [];
                        foreach ([
                                'popularPostsDay'       => 'За сутки',
                                'popularPostsThreeDays' => 'За 3 дня',
                                'popularPostsWeek'      => 'За неделю',
                            ] as $var => $label) {
                            $content = '<br/>';
                            foreach ($$var as $i => $post) {
                                $name = $post['name'];
                                if ($userId === $post['author_id']) {
                                    $name = Html::tag('strong', $name);
                                }
                                if ($postHitNumber) {
                                    $htmlParams = ['title' => $post['show_count']];
                                }
                                $content .= Html::tag('p', Html::a(
                                    $name,
                                    $frontendUrlManager->createAbsoluteUrl(Post::getUrlParams($post)),
                                    $htmlParams
                                ));
                            }

                            $items[] = [
                                'label' => $label,
                                'content' => $content,
                            ];
                        }
                        echo Tabs::widget(['items' => $items]);
                        ?>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">Не стоили внимания</div>
                    <div class="panel-body">
                        <?php
                        $items = [];
                        foreach ([
                                'notInterestingFeed' => 'Из ленты',
                                'notInterestingAll'  => 'Все',
                            ] as $var => $label) {
                            $content = '<br/>';
                            foreach ($$var as $i => $post) {
                                $name = $post['name'];
                                if ($userId === $post['author_id']) {
                                    $name = Html::tag('strong', $name);
                                }
                                $content .= Html::tag('p', Html::a(
                                    $name,
                                    $frontendUrlManager->createAbsoluteUrl(Post::getUrlParams($post)),
                                    ['title' => ($post['show_count'] === 0 ? '<' . Yii::$app->params['postMinShowCount'] : $post['show_count']) . ' за ' . $post['period']]
                                ));
                            }
                            $items[] = [
                                'label' => $label,
                                'content' => $content,
                            ];
                        }
                        echo Tabs::widget(['items' => $items]);
                        ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>
