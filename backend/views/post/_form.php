<?php

use \yii\bootstrap\ActiveForm;
use \yii\helpers\ArrayHelper;
use \yii\web\JsExpression;
use \dosamigos\selectize\SelectizeTextInput;
use \nex\datepicker\DatePicker;
use \common\helpers\Html;
use \common\helpers\Url;
use \common\models\Folder;
use \common\models\Post;
use \common\models\PostTag;
use \common\models\Tag;
use \common\models\User;

/**
 * @var yii\web\View $this
 * @var common\models\Post $model
 */
$form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
    'id' => 'post-form',
]);

$this->registerJs(<<<JS
$('#post-form').on('beforeValidateAttribute', function (event, attribute, messages, deferreds) {
    if (attribute.name === 'content') {
        tinymce.triggerSave();
    }
    return true;
});
JS
    , $this::POS_READY, 'post-form');

$isNewRecord = $model->getIsNewRecord();
$user = Yii::$app->getUser();
?>
<div class="row">
    <div class="col-lg-8">
        <?= $form->field($model, 'name')->textarea([
            'maxlength' => true,
            'rows' => 3,
            'class' => 'form-control post-title tile'
        ]); ?>
        <?= $form->field($model, 'title_seo')->textInput(['maxlength' => true]); ?>
        <div class="row">
            <div class="col-sm-6 zi6">
                <?= $form->field($model, 'slug')->textInput(['maxlength' => true]); ?>
            </div>
            <div class="col-sm-6 zi6">
                <?php
                $publishHtml = '. ' . Html::a('Опубликовать', '#post-published', [
                    'id' => 'publish-post',
                    'title' => 'Внести в поле +3 минуты от текущего времени',
                    'class' => 'dashed'
                ]);
                if (empty($model->published)) {
                    $html = 'Невидимый' . $publishHtml;
                } else {
                    $html = '<a href="' . $model->getUrl(true) . '" target="_blank">';
                    $html = time() < strtotime($model->published) ? 'Отложенная публикация, доступен по ' . $html . 'прямой ссылке</a>' . $publishHtml : $html . 'Видимый</a>';
                }
                if (!empty($model->published)) {
                    $model->published = date('Y-m-d H:i', strtotime($model->published));
                }
                echo $form
                    ->field($model, 'published', [
                        'options' => [
                            'class' => date('Y-m-d', strtotime($model->published)) === date('Y-m-d') ? 'has-success' : '',
                        ]
                    ])
                    ->widget(DatePicker::className(), [
                        'language' => 'ru-RU',
                        'containerOptions' => [
                            'id' => 'post-published-wrapper',
                        ],
                        'clientOptions' => [
                            'sideBySide' => false,
                            'showTodayButton' => false,
                            'showClear' => false,
                            'format' => 'YYYY-MM-DD HH:mm',
                        ],
                        'clientEvents' => [
                            'dp.change' => new JsExpression(<<<JS
function (e) {
    var fieldPostPublished = $('.field-post-published'),
        published = $("#post-published").val();
    if ($.trim(published) == '') {
        fieldPostPublished.removeClass('has-success');
        return;
    }

    if (published.split(' ')[0] == moment().format('YYYY-MM-DD')) {
        fieldPostPublished.addClass('has-success');
    } else {
        fieldPostPublished.removeClass('has-success');
    }
}
JS
                            ),
                        ],
                    ])
                    ->hint($html);
                $this->registerJs(<<<JS
$('#publish-post').on('click', function (e) {
    e.preventDefault();

    $('#post-published-wrapper').data('DateTimePicker').date(moment().add(3, 'minutes'));
});
JS
                    , $this::POS_READY, 'published-datetimepicker');
                ?>
            </div>
        </div>
        <?= $form->field($model, 'description')->textarea([
            'maxlength' => true,
            'rows' => 10,
            'class' => 'form-control post-description tile'
        ]); ?>
        <?php
        $this->registerJs(<<<JS
jQuery.fn.extend({
    insertAtCaret: function (text) {
        return this.each(function (i) {
            if (document.selection) {
                this.focus();
                var sel = document.selection.createRange();
                sel.text = text;
                this.focus();
            }
            else if (this.selectionStart || this.selectionStart == '0') {
                var startPos = this.selectionStart;
                var endPos = this.selectionEnd;
                var scrollTop = this.scrollTop;
                this.value = this.value.substring(0, startPos) + text + this.value.substring(endPos, this.value.length);
                this.focus();
                this.selectionStart = startPos + text.length;
                this.selectionEnd = startPos + text.length;
                this.scrollTop = scrollTop;
            } else {
                this.value += text;
                this.focus();
            }
        })
    }
});
    
updateDescriptionSymbolsCount = function () {
    var count = $('#post-description').val().length;
    var counter = $('#descriptionSymbolsCount');
    if (count < 200) {
        counter.removeClass().addClass('text-danger');
    } else if (count < 250) {
        counter.removeClass().addClass('text-warning');
    } else if (count < 300) {
        counter.removeClass().addClass('text-success');
    } else if (count < 400) {
        counter.removeClass().addClass('text-warning');
    } else {
        counter.removeClass().addClass('text-danger');
    }
    counter.text('(' + count + ')');
};

updateNameSymbolsCount = function () {
    var count = $('#post-name').val().length;
    var counter = $('#nameSymbolsCount');
    if (count < 20) {
        counter.removeClass().addClass('text-danger');
    } else if (count < 50) {
        counter.removeClass().addClass('text-warning');
    } else if (count < 70) {
        counter.removeClass().addClass('text-success');
    } else if (count < 96) {
        counter.removeClass().addClass('text-warning');
    } else {
        counter.removeClass().addClass('text-danger');
    }
    counter.text('(' + count + ')');
};

updateTitleSeoSymbolsCount = function () {
    var count = $('#post-title_seo').val().length;
    var counter = $('#titleSeoSymbolsCount');
    if (count < 20) {
        counter.removeClass().addClass('text-danger');
    } else if (count < 50) {
        counter.removeClass().addClass('text-warning');
    } else if (count < 150) {
        counter.removeClass().addClass('text-success');
    } else if (count < 255) {
        counter.removeClass().addClass('text-warning');
    } else {
        counter.removeClass().addClass('text-danger');
    }
    counter.text('(' + count + ')');
};
    
var symbols = '<span style="margin-left: 5px" class="symbols"><a href="#" title="Евро">€</a><a href="#" title="Неразрывный пробел">⎵</a><a href="#" title="Неразрывный дефис">&#8209;</a><a href="#" title="Длинное тире">—</a><a href="#" title="Левая кавычка">«</a><a href="#" title="Правая кавычка">»</a><a href="#" title="Многоточие">…</a></span>';

$('<span id="descriptionSymbolsCount" title="Количество символов в описании"></span>' ).appendTo('label[for="post-description"]');
$('<span id="nameSymbolsCount"        title="Количество символов в названии"></span>' ).appendTo('label[for="post-name"]');
$('<span id="titleSeoSymbolsCount"    title="Количество символов в заголовке"></span>').appendTo('label[for="post-title_seo"]');

$(symbols).appendTo('label[for="post-name"]').clone(true).appendTo('label[for="post-description"]');

$('.symbols>a').on('click', function (e) {
    e.preventDefault();
    var link = $(this);
    var input = link.closest('label').next();
    if (link.text() === '⎵') {
        input.insertAtCaret(' ');
    } else {
        input.insertAtCaret(link.text());
    }
    input.focus();
});
    
$('#post-name'       ).on('focus', updateNameSymbolsCount       ).on('keyup', updateNameSymbolsCount);
$('#post-description').on('focus', updateDescriptionSymbolsCount).on('keyup', updateDescriptionSymbolsCount);
$('#post-title_seo'  ).on('focus', updateTitleSeoSymbolsCount   ).on('keyup', updateTitleSeoSymbolsCount);

updateNameSymbolsCount();
updateDescriptionSymbolsCount();
updateTitleSeoSymbolsCount();
JS
            ,
            $this::POS_READY,
            'labels'
        );
        ?>
        <?= $form->field($model, 'content', [
            'template' => "{input}\n{hint}\n{error}"
        ])->widget('backend\extensions\tinymce\Tinymce', ['admin' => true]); ?>
        <?= $form->field($model, 'is_contain_video')->checkbox(); ?>
        <?= $form
            ->field($model, 'video_duration')
            ->widget(DatePicker::className(), [
                'language' => 'ru-RU',
                'clientOptions' => [
                    'sideBySide' => false,
                    'showTodayButton' => false,
                    'showClear' => false,
                    'format' => 'HH:mm:ss',
                ],
                'clientEvents' => [
                    'dp.show' => new JsExpression(<<<JS
function (e) {
    var el = $('#post-video_duration');
    if (el.val() == '') {
        el.val('00:05:30').change();
    }
}
JS
                    ),
                ],
            ])
            ->hint('Не заполняйте для статей.'); ?>
        <?php
        $this->registerJs(<<<JS
var isContainVideoCheckbox = $("#post-is_contain_video"),
    videoDuration = $(".field-post-video_duration");

function toggleDuration() {
    if (isContainVideoCheckbox.prop("checked")) {
        videoDuration.show();
    } else {
        videoDuration.hide();
    }
}

toggleDuration();
isContainVideoCheckbox.change(function() {
    toggleDuration();
});
JS
            , $this::POS_READY, 'toggle_duration');
        ?>
        <?php if ($model->hasProperty('tags')) { ?>
            <?php
            $modelClass = $model::className();
            $lastPostIds = $modelClass::find()->select('id')->isPublished()->publishedInLastDay(7);
            $tagsIds = PostTag::find()
                ->select(['tag_id'])
                ->andWhere(['IN', 'post_id', $lastPostIds])
                ->groupBy('tag_id')
                ->orderBy('count(post_id) DESC')
                ->limit(7)
                ->column();
            $tags = !empty($tagsIds) ? Tag::find()
                ->select('name')
                ->andWhere(['IN', 'id', $tagsIds])
                ->orderBy(['idx(array[' . implode(',', $tagsIds) . '], id)' => SORT_ASC])
                ->column() : [];

            foreach ($tags as &$tagName) {
                $tagName = Html::a($tagName, '#post-tags_name', ['class' => 'dashed']);
            }
            ?>
            <?= $form->field(
                $model,
                'tags_name',
                [
                    'template' => "{label}\n{input_spec}\n{hint}\n{error}",
                    'parts' => [
                        '{input_spec}' => SelectizeTextInput::widget([
                            'model' => $model,
                            'attribute' => 'tags_name',
                            'clientOptions' => [
                                'valueField' => 'name',
                                'labelField' => 'name',
                                'searchField' => 'name',
                                'delimiter' => ', ',
                                'plugins' => ['remove_button'],
                                'persist' => false,
                                'hideSelected' => true,
                                'create' => new JsExpression('function(input){return {value: input, name: input};}'),
                                'render' => [
                                    'option_create' => new JsExpression(<<<JS
function (data, escape) {
    return '<div class="create">Добавить <strong>' + escape(data.input) + '</strong>&hellip;</div>';
}
JS
                                    )
                                ],
                                'load' => new JsExpression(<<<JS
function (query, callback) {
    if (!query.length) {
        return callback();
    }
    jQuery.ajax({
        url: '/tag/select',
        type: 'GET',
        data: {'text': query},
        dataType: 'json',
        error: function() {
            callback();
        },
        success: function(res) {
            callback(res);
        }
    });
}
JS
                                )
                            ]
                        ]),
                        '{label}' => Html::activeLabel($model, 'tags_name', ['label' => $model->getAttributeLabel('tags_name')]) . ' <span class="tags-help" id="tags-help">' . implode(', ', $tags). '</span>'
                    ],
                ]
            )->textInput(['maxlength' => 255]); ?>
            <?php
            $this->registerJs(<<<JS
var selectize = $('#post-tags_name').selectize()[0].selectize;

$('#tags-help').on('click', 'a', function(e) {
    e.preventDefault();
    
    var name = $(this).text();
    
    selectize.addOption({value:name, name:name});
    selectize.addItem(name);
});
JS
                ,
                $this::POS_READY,
                'tags-help'
            ); ?>
        <?php } ?>
        <?php
        $authors = User::find();
        if ($user->can('postAuthor')) {
            $authors->authors();
        } else {
            $authors->andWhere([
                'id' => [
                    Yii::$app->params['defaultAuthor']['id'],
                    $user->id
                ]
            ]);
        }
        echo $form->field($model, 'author_id')->dropDownList(ArrayHelper::map(
            $authors->orderBy('title')->asArray()->all(),
            'id',
            'title'
        ));

        echo $form->field($model, 'is_allow_footer'  )->checkbox();
        echo $form->field($model, 'is_allow_comments')->checkbox();

        $postSpecialFeed = $user->can('postSpecialFeed');
        if ($postSpecialFeed) { ?>
            <br/>
        <?php } ?>
        <?php if ($postSpecialFeed) {
            echo $form->field($model, 'is_in_common_feed')->checkbox();
        }

        if ($user->can('postIsSentToIndex')) { ?>
            <br/>
            <?= $form->field($model, 'is_sent_to_index')->checkbox(); ?>
        <?php } ?>
        <?= $form->errorSummary($model); ?>
        <div class="text-center">
            <?= Html::submitButton($isNewRecord ? 'Добавить' : 'Обновить', [
                'class' => $isNewRecord ? 'btn btn-success' : 'btn btn-warning',
            ]); ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-warning">
            <div class="panel-heading"><?= $model->getAttributeLabel('icon'); ?></div>
            <div class="panel-body">
                <?php
                if ($model->icon) { ?>
                    <?= Html::img(Url::thumbnail($model->icon, 240, 135)); ?>
                <?php } ?>
                <fieldset>
                    <legend>Загрузить новую</legend>
                    <?= $form->field($model, 'UploadFile')->fileInput(); ?>
                    <?= $form->field($model, 'UploadExternalUrl')->textInput(['maxlength' => true]); ?>
                </fieldset>
            </div>
        </div>
        <?php if ($model->hasProperty('folders')) { ?>
            <div class="panel panel-danger">
                <div class="panel-heading">Разделы</div>
                <div class="panel-body">
                    <?php
                    $mainFolders = Folder::find()->parents()->orderBy('name')->all();
                    $foldersByGroups = [];
                    $folders = Folder::find()
                        ->select(['id', 'path', 'group_id'])
                        ->orderBy(['group_id' => SORT_ASC, 'path' => SORT_ASC])
                        ->with([
                            'group' => function ($query) {
                                /** @var \yii\db\ActiveQuery $query */
                                $query->select(['id', 'name']);
                            },
                        ])
                        ->asArray()
                        ->all();

                    foreach ($folders as $folder) {
                        $foldersByGroups[empty($folder['group']) ? 'Остальные' : $folder['group']['name']][$folder['id']] = $folder['path'];
                    }
                    ?>
                    <?= $form->field($model, 'folderIds', [
                        'template' => "{input}\n{hint}\n{error}",
                        'parts' => [
                            '{input}' => Html::activeListInput('checkboxListWithHeading', $model, 'folderIds', $foldersByGroups, ['separator' => '<br/>']),
                        ]
                    ]); ?>
                    <?= $form->field($model, 'folder_id_main')->dropDownList(ArrayHelper::map($mainFolders, 'id', 'name'), ['prompt' => 'Нет']); ?>
                    <?php $this->registerJs(<<<JS
$('#post-folder_id_main').change(function() {
    $(':checkbox[value=' + $(this).val() + ']').prop('checked', 'true');
});

$(':checkbox').change(function() {
    var name = $(this);
    if (name.prop('checked') == false) {
        return;
    }
    name = name.parent().text();
    var type = name.substring(name.indexOf('.') + 1, name.length);
    $('label:contains(' + name.substring(0, name.indexOf('.')) + ')').filter(':first').children().prop('checked', 'true');
});
JS
                        , $this::POS_READY, 'folders'); ?>
                </div>
            </div>
        <?php } ?>
        <?php if (!$isNewRecord) { ?>
            <div class="panel panel-info">
                <div class="panel-heading">Информация</div>
                <div class="panel-body">
                    <dl class="dl-horizontal">
                        <dt>Посмотреть</dt>
                        <dd><?= Html::a('на сайте', $model->getUrl(true), ['target' => '_blank']); ?></dd>
                        <?php if ($user->can('userUpdateAndDelete')) { ?>
                            <dt><?= $model->getAttributeLabel('author_id'); ?></dt>
                            <dd><?= Html::a($model->author['title'], ['user/update', 'id' => $model->author->id]); ?></dd>
                        <?php } ?>
                        <dt><?= $model->getAttributeLabel('comment_visible_count'); ?></dt>
                        <dd><?= $model->comment_visible_count; ?></dd>
                        <dt><?= $model->getAttributeLabel('symbol_count'); ?></dt>
                        <dd><?= $model->symbol_count; ?></dd>
                        <dt><?= $model->getAttributeLabel('updated'); ?></dt>
                        <dd><?= Yii::$app->getFormatter()->asDatetime($model->updated); ?></dd>
                    </dl>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
