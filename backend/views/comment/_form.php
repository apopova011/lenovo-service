<?php

use \yii\bootstrap\ActiveForm;
use \common\helpers\Html;
use \common\models\Comment;

/**
 * @var yii\web\View $this
 * @var common\models\Comment $model
 */
$form = ActiveForm::begin();

$isNewRecord = $model->getIsNewRecord();
?>
<div class="row">
    <div class="col-lg-8">
        <?= $form->field($model, 'content', ['template' => "{input}\n{hint}\n{error}"])->textarea(['rows' => 6]); ?>
        <?= $form->field($model, 'status')->dropDownList(Comment::dictionaryStatusLabels()); ?>

        <div class="text-center">
            <?= Html::submitButton($isNewRecord ? 'Добавить' : 'Обновить', [
                'class' => $isNewRecord ? 'btn btn-success' : 'btn btn-warning',
            ]); ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-danger">
            <div class="panel-heading">Местонахождение</div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt><?= $model->getAttributeLabel('post_id'); ?></dt>
                    <dd><?= Html::a($model->post['name'], ['post/update', 'id' => $model->post_id]); ?></dd>
                    <?php if ($model->isChild) { ?>
                        <dt><?= $model->getAttributeLabel('PathParent'); ?></dt>
                        <dd><?= Html::a('комментарий', ['update', 'id' => $model->parentKey]); ?></dd>
                    <?php } ?>
                    <dt><?= $model->getAttributeLabel('created'); ?></dt>
                    <dd><?= Yii::$app->getFormatter()->asDatetime($model->created); ?></dd>
                    <dt><?= $model->getAttributeLabel('votes_value'); ?></dt>
                    <dd><?= $model->votes_value; ?></dd>
                    <dt>Посмотреть</dt>
                    <dd><?= Html::a('на сайте', $model->post->getUrl(true) . '#comment-' . $model->id, ['target' => '_blank']); ?></dd>
                </dl>
            </div>
        </div>
        <div class="panel panel-info">
            <div class="panel-heading"><?= Html::a($model->author['title'], [
                'user/update',
                'id' => $model->author_id,
            ]); ?> на момент коммента</div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt><?= $model->getAttributeLabel('user_title'); ?></dt>
                    <dd><?= $model->user_title; ?></dd>
                    <dt><?= $model->getAttributeLabel('user_avatar'); ?></dt>
                    <dd><?= $model->user_avatar; ?></dd>
                    <dt><?= $model->getAttributeLabel('user_ip'); ?></dt>
                    <dd><?= $model->user_ip; ?></dd>
                    <dt><?= $model->getAttributeLabel('user_agent'); ?></dt>
                    <dd><?= $model->user_agent; ?></dd>
                    <dt><?= $model->getAttributeLabel('user_session'); ?></dt>
                    <dd><?= $model->user_session; ?></dd>
                </dl>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
