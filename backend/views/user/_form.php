<?php

use \yii\bootstrap\ActiveForm;
use \yii\web\JqueryAsset;
use \nex\datepicker\DatePicker;
use \common\helpers\Html;
use \common\models\User;

/**
 * @var yii\web\View $this
 * @var common\models\User $model
 */
$isNewRecord = $model->getIsNewRecord();
?>
<div class="row">
    <?php $form = ActiveForm::begin(['id' => 'user-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="col-lg-8">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]); ?>
        <?php
        if (Yii::$app->getUser()->can('userRole')) {
            echo $form->field($model, 'role')->dropDownList(User::dictionaryRoleLabels());
        }
        ?>
        <?= $form->field($model, 'email'     )->textInput(['maxlength' => true]); ?>
        <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]); ?>
        <?= $form->field($model, 'last_name' )->textInput(['maxlength' => true]); ?>
        <?= $form->field($model, 'timezone'); ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Личное</div>
                    <div class="panel-body">
                        <?= $form->field($model, 'sex')->dropDownList(User::dictionarySexLabels(), ['prompt' => 'Не указан']); ?>
                        <?= $form->field($model, 'birthday')->widget(DatePicker::className(), [
                            'language' => 'ru-RU',
                            'clientOptions' => [
                                'sideBySide' => false,
                                'showTodayButton' => false,
                                'showClear' => false,
                                'format' => 'YYYY-MM-DD',
                            ],
                        ]); ?>
                        <?= $form->field($model, 'relation')->dropDownList(User::dictionaryRelationLabels(), ['prompt' => 'Неизвестно']); ?>
                        <?= $form->field($model, 'about')->textarea(['maxlength' => true, 'rows' => 3]); ?>
                        <fieldset>
                            <legend>Предпочтения</legend>
                            <?= $form->field($model, 'life_main')->dropDownList(User::dictionaryLifeMainLabels(), ['prompt' => 'Неизвестно']); ?>
                            <?= $form->field($model, 'smoking'  )->dropDownList(User::dictionarySmokingLabels(),  ['prompt' => 'Неизвестно']); ?>
                            <?= $form->field($model, 'alcohol'  )->dropDownList(User::dictionaryAlcoholLabels(),  ['prompt' => 'Неизвестно']); ?>
                            <?= $form->field($model, 'music' )->textarea(['maxlength' => true, 'rows' => 3]); ?>
                            <?= $form->field($model, 'movies')->textarea(['maxlength' => true, 'rows' => 3]); ?>
                            <?= $form->field($model, 'tv'    )->textarea(['maxlength' => true, 'rows' => 3]); ?>
                            <?= $form->field($model, 'books' )->textarea(['maxlength' => true, 'rows' => 3]); ?>
                            <?= $form->field($model, 'games' )->textarea(['maxlength' => true, 'rows' => 3]); ?>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Контакты</div>
                    <div class="panel-body">
                        <?= $form->field($model, 'phone_mobile')->textInput(['maxlength' => true]); ?>
                        <?= $form->field($model, 'phone_home'  )->textInput(['maxlength' => true]); ?>
                        <?= $form->field($model, 'city_name'   )->textInput(['maxlength' => true]); ?>
                        <fieldset>
                            <legend>Электронные</legend>
                            <?= $form->field($model, 'skype'           )->textInput(['maxlength' => true]); ?>
                            <?= $form->field($model, 'twitter_slug'    )->textInput(['maxlength' => true]); ?>
                            <?= $form->field($model, 'instagram_slug'  )->textInput(['maxlength' => true]); ?>
                            <?= $form->field($model, 'google_plus_slug')->textInput(['maxlength' => true]); ?>
                            <?= $form->field($model, 'facebook_id'     )->textInput(['maxlength' => true]); ?>
                            <?= $form->field($model, 'facebook_name'   )->textInput(['maxlength' => true]); ?>
                        </fieldset>
                        <fieldset>
                            <legend>Вконтакте</legend>
                            <?= $form->field($model, 'vk_id'  )->textInput(['maxlength' => true]); ?>
                            <?= $form->field($model, 'vk_slug')->textInput(['maxlength' => true]); ?>
                            <?= $form->field($model, 'vk_can_post'                 )->dropDownList([0 => 'Не разрешено', 1 => 'Разрешено'], ['prompt' => 'Неизвестно']); ?>
                            <?= $form->field($model, 'vk_can_write_private_message')->dropDownList([0 => 'Не разрешено', 1 => 'Разрешено'], ['prompt' => 'Неизвестно']); ?>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
        <?= $form->errorSummary($model); ?>
        <div class="text-center">
            <?= Html::submitButton(
                $isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'),
                ['class' => $isNewRecord ? 'btn btn-success' : 'btn btn-warning']
            ); ?>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-warning">
            <div class="panel-heading"><?= $model->getAttributeLabel('avatar'); ?></div>
            <div class="panel-body">
                <?php
                $url = $model->getUploadUrl();
                if ($url) {
                    echo Html::img($url, ['width' => '100%']);
                }
                ?>
                <fieldset>
                    <legend>Загрузить новую</legend>
                    <?= $form->field($model, 'UploadFile')->fileInput(); ?>
                    <?= $form->field($model, 'UploadExternalUrl')->textInput(['maxlength' => true]); ?>
                </fieldset>
            </div>
        </div>
        <div class="panel panel-success">
            <div class="panel-heading">СЕО</div>
            <div class="panel-body">
                <?= $form->field($model, 'slug')->textInput(['maxlength' => true]); ?>
                <?php
                if ($isNewRecord) {
                    $this->registerJsFile('/js/jquery.slugit.js', ['depends' => JqueryAsset::className()]);
                    $this->registerJs(<<<JS
$('#user-title').slugIt({
    events: 'focusout keyup',
    output: '#user-slug'
});
JS
                        , $this::POS_READY, 'slug-auto');
                }
                ?>
                <?= $form->field($model, 'title_gen')->textInput(['maxlength' => true]); ?>
            </div>
        </div>
        <?php
        if (!$isNewRecord) {
            $formatter = Yii::$app->getFormatter();
            ?>
            <div class="panel panel-info">
                <div class="panel-heading"><?= Yii::t('common', 'Information'); ?></div>
                <div class="panel-body">
                    <dl class="dl-horizontal">
                        <dt>Посмотреть</dt>
                        <dd><?= Html::a('на сайте', $model->getUrl(true), ['target' => '_blank']); ?></dd>
                        <dt><?= $model->getAttributeLabel('created'); ?></dt>
                        <dd><?= $formatter->asDatetime($model->created); ?></dd>
                        <dt><?= $model->getAttributeLabel('post_count'); ?></dt>
                        <dd><?= $model->post_count; ?></dd>
                        <dt><?= $model->getAttributeLabel('comment_visible_count'); ?></dt>
                        <dd><?= $model->comment_visible_count; ?></dd>
                        <dt><?= $model->getAttributeLabel('updated'); ?></dt>
                        <dd><?= $formatter->asDatetime($model->updated); ?></dd>
                        <?php if ($model->avatar_original) { ?>
                            <dt>Аватара</dt>
                            <dd><?= Html::a('источник', $model->avatar_original, ['target' => '_blank']); ?></dd>
                        <?php } ?>
                    </dl>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
