<?php

use \yii\bootstrap\ActiveForm;
use \common\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Link $model
 */
$form = ActiveForm::begin();

$isNewRecord = $model->getIsNewRecord();
?>
<?= $form->field($model, 'name')->textInput(['maxlength' => true]); ?>
<?= $form->field($model, 'url' )->textInput(['maxlength' => true]); ?>
<div class="text-center">
    <?= Html::submitButton($isNewRecord ? 'Добавить' : 'Обновить', [
        'class' => $isNewRecord ? 'btn btn-success' : 'btn btn-warning',
    ]); ?>
</div>
<?php ActiveForm::end(); ?>
