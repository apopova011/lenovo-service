<?php

namespace backend\widgets;

use Yii;
use \yii\helpers\ArrayHelper;
use \yii\helpers\Html;
use \yii\web\JqueryAsset;
use \yii\widgets\Menu;

class FoldableMenu extends Menu
{
    /**
     * @inheritdoc
     */
    public $options = [
        'class' => 'top-menu',
    ];

    /**
     * @var array the wrapper options
     */
    public $wrapperOptions = [
        'tag' => 'div',
        'options' => [
            'class' => 'top-menu-wrapper',
        ],
    ];

    /**
     * @var string the brand label
     */
    public $brandLabel = false;

    /**
     * @var string the brand url
     */
    public $brandUrl = false;

    /**
     * @var array the brand options
     */
    public $brandOptions = [];

    /**
     * @var string the folded menu label
     */
    public $foldedLabel = '<div><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></div>';

    /**
     * @var array the folded menu button options
     */
    public $foldedOptions = [];

    /**
     * Renders the widget.
     */
    public function run()
    {
        echo Html::beginTag($this->wrapperOptions['tag'], $this->wrapperOptions['options']);

        $this->items = array_merge([
            [
                'label' => $this->foldedLabel,
                'foldable' => false,
                'encode' => false,
                'options' => array_merge($this->foldedOptions, ['class' => 'dropdown more'])
            ]
        ], $this->items);

        if ($this->brandLabel !== false) {
            $this->items = array_merge([
                [
                    'label' => $this->brandLabel,
                    'url' => $this->brandUrl === false ? Yii::$app->homeUrl : $this->brandUrl,
                    'foldable' => false,
                    'options' => array_merge($this->brandOptions, ['class' => 'brand']),
                ]
            ], $this->items);
        }

        parent::run();

        echo Html::endTag('div');

        $view = $this->getView();
        $view->registerCssFile('/css/foldable_menu.css');
        $view->registerJsFile('/js/foldable_menu.js', ['depends' => JqueryAsset::className()]);
    }

    /**
     * @inheritdoc
     * @param bool $isChild
     * @return string the rendering result
     * @throws \yii\base\InvalidParamException
     */
    protected function renderItems($items, $isChild = false)
    {
        $n = count($items);
        $lines = [];
        foreach ($items as $i => $item) {
            $options = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'options', []));
            $tag = ArrayHelper::remove($options, 'tag', 'li');
            $class = [];
            if (!array_key_exists('foldable', $item)) {
                $item['foldable'] = true;
            }
            if ($item['active']) {
                $class[] = $this->activeCssClass;
            }
            if ($i === 0 && $this->firstItemCssClass !== null) {
                $class[] = $this->firstItemCssClass;
            }
            if ($i === $n - 1 && $this->lastItemCssClass !== null) {
                $class[] = $this->lastItemCssClass;
            }
            if (!empty($item['items'])) {
                $class[] = 'dropdown';
            }
            if (!$isChild && $item['foldable']) {
                $class[] = $item['foldable'] === 'last' ? 'foldable-last' : 'foldable';
            }
            if (!empty($class)) {
                if (empty($options['class'])) {
                    $options['class'] = implode(' ', $class);
                } else {
                    $options['class'] .= ' ' . implode(' ', $class);
                }
            }
            if (!empty($item['glyphicon'])) {
                $item['label'] = '<span class="glyphicon glyphicon-' . $item['glyphicon'] . '"></span><span class="with-glyphicon">' . $item['label'] . '</span>';
                $item['encode'] = false;
            }

            $menu = $this->renderItem($item);
            if (!empty($item['items'])) {
                $submenuTemplate = ArrayHelper::getValue($item, 'submenuTemplate', $this->submenuTemplate);
                $menu .= strtr($submenuTemplate, [
                    '{items}' => $this->renderItems($item['items'], true),
                ]);
            }
            if ($tag === false) {
                $lines[] = $menu;
            } else {
                $lines[] = Html::tag($tag, $menu, $options);
            }
        }

        return implode("\n", $lines);
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    protected function isItemActive($item)
    {
        if (!array_key_exists('url', $item)) {
            return false;
        }

        if (is_array($item['url']) && isset($item['url'][0])) {
            $route = $item['url'][0];

            $pos = strpos($route, '/');
            if ($pos !== false) {
                $route = substr($route, 0, $pos);
            }

            if ($route === Yii::$app->controller->getUniqueId()) {
                return true;
            }
        } else {
            $pos = strpos($item['url'], '/');
            if ($pos !== false) {
                $item['url'] = substr($item['url'], 0, $pos);
            }
            if ($item['url'] === Yii::$app->getRequest()->getPathInfo()) {
                return true;
            }
        }

        return false;
    }
}
