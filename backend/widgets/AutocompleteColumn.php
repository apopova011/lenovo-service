<?php

namespace backend\widgets;

use \yii\base\InvalidConfigException;
use \yii\base\Model;
use \yii\grid\DataColumn;
use \yii\web\JsExpression;
use \dosamigos\selectize\SelectizeTextInput;

/**
 * Shows SelectizeTextInput in column header
 */
class AutocompleteColumn extends DataColumn
{
    /**
     * @var array of SelectizeTextInput clientOptions
     */
    public $clientOptions = [];

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $type = 'GET';

    /**
     * @var string
     */
    public $searchParameter = 'query';

    /**
     * @return string
     * @throws InvalidConfigException
     * @throws \Exception
     */
    protected function renderFilterCellContent()
    {
        if (!($this->grid->filterModel instanceof Model)) {
            throw new InvalidConfigException('The "filterModel" property must be Model.');
        }
        if ($this->attribute === null) {
            throw new InvalidConfigException('The "attribute" property must be set.');
        }
        if ($this->url === null) {
            throw new InvalidConfigException('The "url" property must be set.');
        }

        $out = SelectizeTextInput::widget([
            'model' => $this->grid->filterModel,
            'attribute' => $this->attribute,
            'clientOptions' => array_merge($this->getClientOptions(), $this->clientOptions),
        ]);

        return $out;
    }

    /**
     * @return array
     */
    protected function getClientOptions()
    {
        return [
            'valueField' => 'name',
            'labelField' => 'name',
            'searchField' => 'name',
            'delimiter' => ', ',
            'plugins' => ['remove_button'],
            'persist' => false,
            'hideSelected' => true,
            'create' => true,
            'render' => [
                'option_create' => new JsExpression(<<<JS
function (data, escape) {
    return '<div class="create">' + escape(data.input) + '</div>';
}
JS
                )
            ],
            'load' => new JsExpression(<<<JS
function (query, callback) {
    if (!query.length) {
        return callback();
    }
    jQuery.ajax({
        url: '{$this->url}',
        type: '{$this->type}',
        data: {'{$this->searchParameter}': query},
        dataType: 'json',
        error: function() {
            callback();
        },
        success: function(res) {
            callback(res);
        }
    });
}
JS
            )
        ];
    }
}
